package co.quicko.whatfix.overlay;

import java.util.Stack;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.KeyCodes;

import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.CrossMessager.CrossSourceListener;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;

public class Picker<T extends JavaScriptObject> extends Engager {
    private static final String TOUCH_EVENT_SOURCE_NOT_FOUND = "touch_event_source_not_found";
    private static final String CONFIRM_TOUCH_EVENT_SOURCE = "confirm_touch_event_source";
    private static final String PICKER_PICK_EVENT = "picker_pick";
    private static final String PICKER_META_EVENT = "picker_meta";
    protected PickerListener<T> listener;
    private CrossListener crosser;
    private boolean informMeta = false;
    private CrossListener metaCrosser;
    protected ShadowDOM shadow;
    
    private Stack<Element> selectionStack = new Stack<Element>();

    public Picker() {
        initializeShadow();
    }

    public void setListener(PickerListener<T> listener) {
        this.listener = listener;
    }

    @Override
    public void show() {
        if (listener != null) {
            super.show();

            crosser = new CrossListener() {
                @Override
                public void onMessage(String type, String content) {
                    T picked = DataUtil.create(content);
                    PckImpl<T> impl = pckImpl();
                    listener.onFrame(picked, ((EngImpl) impl).frame);
                    impl.pick(listener, picked);
                }
            };
            CrossMessager.addListener(crosser, PICKER_PICK_EVENT);

            metaCrosser = new CrossListener() {
                @Override
                public void onMessage(String type, String content) {
                    pckImpl().informMeta(listener, content);
                }
            };
            CrossMessager.addListener(metaCrosser, PICKER_META_EVENT);
        }
    }

    @Override
    public void hide() {
        super.hide();
        frozenElement = null;
        selectionStack.clear();
        CrossMessager.removeListener(crosser, PICKER_PICK_EVENT);
        CrossMessager.removeListener(metaCrosser, PICKER_META_EVENT);
    }
    
    public void initializeShadow() {
    	shadow = new ShadowDOM(this);
    }

    public void setInformMeta(boolean informMeta) {
        this.informMeta = informMeta;
    }

    @Override
    protected EngImpl createTop() {
        return new TopPckImpl<T>();
    }

    @Override
    protected EngImpl createFrame() {
        return new FramePckImpl<T>();
    }

    @SuppressWarnings("unchecked")
    protected PckImpl<T> pckImpl() {
        return (PckImpl<T>) impl;
    }

    public static interface PickerListener<T extends JavaScriptObject> {
        public T onSource(Element picked);

        public void onFrame(T data, Element frame);

        public void onTop(T picked);

        public void onMeta(String meta);
    }

    @Override
    protected void onPreviewNativeEvent(Element source,
            PreviewEngageEvent event) {
        String type = event.getType();
        if (null != frozenElement) {
            // Mouse selection is frozen. Now listen for broad selection
            if (type.equals("keyup")) {
                event.cancel();
                int keyCode = event.getKeyCode();
                switch (keyCode) {
                    case KeyCodes.KEY_UP:
                        if (event.getCtrlKey()) {
                            Element parent = frozenElement.getParentElement();
                            if(DOMUtil.isShadowRoot(parent)) {
                                parent = DOMUtil.getPsuedoParent(parent);
                            }
                            if (null != parent) {
                                selectionStack.push(frozenElement);
                                frozenElement = parent;
                                engage(frozenElement);
                            }
                        }
                        break;

                    case KeyCodes.KEY_DOWN:
                        if (event.getCtrlKey()
                                && selectionStack.size() > 0) {
                            Element popped = selectionStack.pop();
                            frozenElement = popped;
                            engage(frozenElement);
                        }
                        break;

                    case KeyCodes.KEY_ENTER:
                        pickElement(frozenElement);
                        break;
                }
            } else if (type.equals("keydown") || type.equals("keypress")) {
                // block propagation
                event.cancel();
            }
        } else if (((EngImpl) pckImpl()).isAnyElementFrozen) {
            event.cancel();
        } else if (type.equals("click") || (Enterpriser.hasFeature(AdditionalFeatures.glue_pointer_events) && type.equals("pointerdown"))) {
            if (!DOMUtil.isShadowHost(source) || event.isActualTarget(source)) {
                event.cancel();
                if (event.getCtrlKey()) {
                    frozenElement = source;
                    frozenElement.focus();
                    CrossMessager.sendMessageToTop(EngImpl.PICKER_FROZEN_EVENT, "");
                } else if (Enterpriser.hasFeature(
                        AdditionalFeatures.editor_ignore_auto_click_event)) {
                       if(isTrusted(event.event)) {
                          pickElement(source); 
                       }
                } else {
                    pickElement(source);
                }
            }
        } else if (event.getAltKey() && type.equals("touchstart") && Enterpriser
                .hasFeature(AdditionalFeatures.editor_touchevent)) {
            handleTouchEvent(source, event);
        } else if (type.equals("mousedown") || type.equals("mouseup")) {
            event.cancel();
            if (Enterpriser
                    .hasFeature(AdditionalFeatures.editor_rightclick_select)
                    && event.getButton() == 2) {
                pickElement(source);
            }
        } else if (informMeta && type.equals("mouseover")) {
            pckImpl().informMeta(listener, getMeta(source));
            super.onPreviewNativeEvent(source, event);
        } else {
            shadow.handleMouseEvent(event.getNativeEvent());
			super.onPreviewNativeEvent(source, event);
        }
    }
    
    private void handleTouchEvent(Element source, PreviewEngageEvent event) {
        if (!DOMUtil.isShadowHost(source) || event.isActualTarget(source)) {
            frozenElement = source;
            if (event.getAltKey()) {
                // Send cross message to determine which frame consist of the
                // <source> element.
                // Find the frame and call onPreviewNativeEvent for that.
                CrossMessager.sendMessageToTop(
                        EngImpl.PICKER_CONTAINS_TOUCHEVENT_SOURCE, "");
                CrossMessager.addListener(new CrossSourceListener() {
                    
                    @Override
                    public void onMessage(String type, String content,
                            JavaScriptObject sourceWindow) {
                        pickElement(frozenElement);
                    }
                }, CONFIRM_TOUCH_EVENT_SOURCE);
                CrossMessager.addListener(new CrossSourceListener() {
                    @Override
                    public void onMessage(String type, String content,
                            JavaScriptObject sourceWindow) {
                        pickElement(frozenElement);
                    }
                }, TOUCH_EVENT_SOURCE_NOT_FOUND);
            }
        }
    }

    protected void pickElement(Element source) {
        T picked = listener.onSource(source);
        if (picked != null) {
            pckImpl().pick(listener, picked);
        }
        CrossMessager.sendMessageToTop(EngImpl.PICKER_UN_FROZEN_EVENT, "");
    }

    private String getMeta(Element source) {
        String id = source.getId();
        return id == null ? "" : id;
    }

    protected static interface PckImpl<T extends JavaScriptObject> {
        public void pick(PickerListener<T> listener, T picked);

        public void informMeta(PickerListener<T> listener, String meta);
    }

    private static class TopPckImpl<T extends JavaScriptObject>
            extends TopEngImpl implements PckImpl<T> {
        @Override
        public void pick(PickerListener<T> listener, T picked) {
            listener.onTop(picked);
        }

        @Override
        public void informMeta(PickerListener<T> listener, String meta) {
            listener.onMeta(meta);
        }
    }

    private static class FramePckImpl<T extends JavaScriptObject>
            extends FrameEngImpl implements PckImpl<T> {
        @Override
        public void pick(Picker.PickerListener<T> listener, T picked) {
            CrossMessager.sendMessageToParent(PICKER_PICK_EVENT,
                    StringUtils.stringifyObject(picked));
        }

        @Override
        public void informMeta(PickerListener<T> listener, String meta) {
            CrossMessager.sendMessageToParent(PICKER_META_EVENT, meta);
        }
    }
    
    protected native boolean isTrusted(NativeEvent event) /*-{
        return event.isTrusted === undefined ? true : event.isTrusted;      
    }-*/;
}
