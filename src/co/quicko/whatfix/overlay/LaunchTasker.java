package co.quicko.whatfix.overlay;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.EmbedData.TaskerNudgeCountersData;
import co.quicko.whatfix.data.EmbedData.TaskerSettings;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.TaskListNudgeCondition;
import co.quicko.whatfix.data.TaskerInfo;
import co.quicko.whatfix.data.TaskerInfo.TaskerData;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.player.MobileUtil;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.security.UserInfo;
import co.quicko.whatfix.service.ContentManager;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;

public abstract class LaunchTasker extends Launcher {
    public static final String WIDGET_ID = "_tasker_wfx_";
    public static final String LAUNCHER_ID = "_tasker_launcher_wfx_";
    public static final int WIDTH = 90;
    public static final int HEIGHT = 90;
    protected InlineLabel notificationCircle;
    private static boolean callbackCompleted = false;
    protected TaskerInfo taskerInfo;
    protected Tracker tracker;
    protected ContentManager manager;
    protected boolean active = false;
    protected FlowPanel taskerLauncherContainer;
    protected FlowPanel taskerAnimationElement;
    protected PopupPanel taskerAnimationContainer;
    protected boolean isTaskerOpen = false;
    protected boolean animationSupported ;
    protected AsyncCallback<Void> callback;
    private Widget taskerProgressor;
    protected Anchor taskerCircle;
    protected boolean shouldFocusOnLauncher = false;
    private boolean shouldShowTaskerArrow = true;
    private FocusPanel arrowContainer;
    protected boolean shouldShowTaskerTooltip = false;
    protected TaskListNudgeCondition tasklistNudgeCondition;
    /**
     * Duration in ms, taken to transition tasklist from left to right position
     * or vice versa
     */
    private static final int TASKER_POSITION_CHANGE_DURATION = 600;
    protected static final String TASKER_POSITION_BOTTOM_LEFT = "bl";
    protected static final String TASKER_POSITION_BOTTOM_RIGHT = "br";
    private static final String TASKER_POSITION_LEFT_EVENT = "bottomLeft";
    private static final String TASKER_POSITION_RIGHT_EVENT = "bottomRight";
    private static final String TASKER_ARROW_RIGHT_ARIA_LABEL = "Move tasklist to right";
    private static final String TASKER_ARROW_LEFT_ARIA_LABEL = "Move tasklist to left";
    
    public static final String SHOULD_INCREMENT_NUDGE_COUNT = "should_increment_nudge_count";
    public static final String SHOULD_INCREMENT_TASK_COUNT = "should_increment_task_complete_count";
    public static final String SHOULD_RESET_COUNTERS = "should_reset_counters";
    
    public static final String NUDGE_EVAL_TIMESTAMP = "nudge_eval_timestamp";
    public static final String LAST_RESET_TIMESTAMP = "last_reset_timestamp";
    public static final String TASK_COMPLETE_COUNT = "task_complete_count";
    public static final String NUDGE_COUNT = "nudge_count";
    public static final int TOOLTIP_DELAY = 200;
    private static boolean isTaskerVisible = true;

    protected Map<String, String> taskerNudgeCounterData = new HashMap<>();

    public LaunchTasker(Settings settings, String prefix) {
        super(settings, prefix);
        taskerInfo = new TaskerInfo();
        addStyleName(Overlay.CSS.taskerPositionTransition());
        tracker = GWT.create(WfTracker.class);
        manager = GWT.create(ContentManager.class);
        manager.initialize(settings.ent_id());
        Common.handleZoom(this);
    }
    public LaunchTasker(Settings settings) {
        super(settings);
    }

    public void initialize(AsyncCallback<Void> cb) {
        active = true;
        tracker.initialize(settings, manager, this);
        initializeListeners();
        fetchFlows(cb);
    }

    @Override
    protected Frame getFrame() {
        return Framers.tasker().buildFrame();
    }

    private void showTasker() {
        if ("open".equals(settings.state())) {
            settings.state(null);
            onClick(null);
        } else {
            show();
        }
    }

    @Override
    public void onClose(CloseEvent<PopupPanel> event) {
        taskerProgressor.removeStyleName(Overlay.CSS.progressorDisplay());
        
        widgetPopup.setVisible(false);
        widgetPopup.hide();
        
        taskerAnimationContainer.removeStyleName(Overlay.CSS.taskerShadow());

        show();

        closeTasklistAnimation();
        isTaskerOpen = false;
        fallback();
    }

    @Override
    public void onClick(ClickEvent event) {
        if(null != event) {
            event.preventDefault();
        }
        openTasker();
    }
    
    protected void openTasker() {
        setPosition(taskerAnimationContainer, getWidgetWidth(), getWidgetHeight(),
                false);
        taskerLauncherContainer.addStyleName(Overlay.CSS.hideElement());

        taskerAnimationContainer.getElement().getStyle()
                .setHeight(getWidgetHeight(), Unit.PX);
        taskerAnimationContainer.getElement().getStyle()
                .setWidth(getWidgetWidth(), Unit.PX);

        openTasklistAnimation();
        isTaskerOpen = true;
        fallback();
    }
    
    private void closeTasklistAnimation() {
        taskerAnimationElement.removeStyleName(getExpandedStyle());
    }

    private void openTasklistAnimation() {
        taskerAnimationElement.addStyleName(getExpandedStyle());
    }

    private String getExpandedStyle() {
        if (settings.target() == null) {
            return Overlay.CSS.taskerAnimationCircleExpanded();
        } else {
            return Overlay.CSS.taskerAnimationRectangleExpanded();
        }
    }

    private String getNormalStyle() {
        if (settings.target() == null) {
            return Overlay.CSS.taskerAnimationCircle();
        } else {
            return Overlay.CSS.taskerAnimationRectangle();
        }
    }

    private void setUpAnimation() {
        callback = callback();
        animationSupported = Common.CssAnimation.registerCssCallback(
                taskerAnimationElement.getElement(), "background-color",
                callback);
    }

    private void fallback() {
        if (!animationSupported) {
            callback.onSuccess(null);
        }
    }

    private AsyncCallback<Void> callback() {
        return new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                onSuccess(null);
            }

            @Override
            public void onSuccess(Void result) {
                if (isTaskerOpen) {
                    widgetPopup.show();
                    widgetPopup.setVisible(false);
                    taskerProgressor
                            .addStyleName(Overlay.CSS.progressorDisplay());
                    taskerAnimationContainer
                            .addStyleName(Overlay.CSS.taskerShadow());
                } else {
                    taskerAnimationContainer.getElement().getStyle().setHeight(0,
                            Unit.PX);
                    taskerAnimationContainer.getElement().getStyle().setWidth(0,
                            Unit.PX);
                    taskerLauncherContainer
                            .removeStyleName(Overlay.CSS.hideElement());
                    refreshLauncherPosition();
                }
            }
        };
    }
    
    protected void setToolTip() {
        // Override this method to set tooltip
    }

    protected void onLoaded() {
        if (isTaskerOpen) {
            widgetPopup.setVisible(true);
            hide();
            // Delaying the appearence of tooltip allowing tasklist to
            // completely load after animation before tooltip is placed on it
            Scheduler.get().scheduleFixedDelay(() -> {
                if (shouldShowTaskerTooltip) {
                    setToolTip();
                }
                return false;
            }, TOOLTIP_DELAY);
        }
    }
    
    @Override
    public void show() {
        super.show();
        refreshLauncherPosition();
        if (shouldFocusOnLauncher) {
            taskerCircle.getElement().setTabIndex(0);
            Scheduler.get().scheduleFixedDelay(() -> {

                taskerCircle.getElement().focus();
                return false;
            }, 800);
            shouldFocusOnLauncher = false;
        }
    }

    @Override
    protected void showDeck(String content) {
    }

    @Override
    protected int getLauncherWidth() {
        return WIDTH;
    }

    @Override
    protected int getLauncherHeight() {
        return HEIGHT;
    }

    @Override
    protected int getWidgetWidth() {
        return Framers.MIN_TASKER_WIDTH;
    }

    @Override
    protected int getWidgetHeight() {
        return Framers.MIN_TASKER_HEIGHT;
    }

    @Override
    protected String getLauncherId() {
        return LAUNCHER_ID;
    }

    @Override
    protected String getWidgetId() {
        return WIDGET_ID;
    }
    
    protected void beforeTaskListLoad() {
    }

    protected void showFirstTime() {
        if (canShow()) {
            beforeTaskListLoad();
            showTasker();
            MobileUtil.createTaskList();
        } else {
            MobileUtil.noTaskList();
            destroy();
        }
    }

    protected static boolean showProgress() {
        return "show".equalsIgnoreCase(Themer.value(Themer.TASK_LIST.PROGESS));
    }

    protected boolean canShow() {
        return !showProgress()
                || !(((TaskerSettings) settings).hide_on_complete()
                        && 0 == getRemaining());
    }

    protected void updateRemaining() {
        int remaining = getRemaining();
        notificationCircle.setText(Integer.toString(remaining));
        Customizer.remainingTasksCount(remaining);
        
        if (remaining == 0 && !callbackCompleted) {
            tracker.onTasksCompleted();
            callbackCompleted = true;
        }
        updateAccessibilityLabel();
        
    }

    private void updateAccessibilityLabel() {
        int remaining = getRemaining();
        String altTxt = new StringBuilder(settings.label()).append(",")
                .append(String.valueOf(remaining > 0 ? remaining : 0))
                .append(" ").append(suffix(remaining)).toString();
        taskerCircle.getElement().setAttribute(AriaProperty.LABEL, altTxt);
        taskerCircle.getElement().setAttribute(AriaProperty.EXPANDED,
                Boolean.FALSE.toString());
    }

    private String suffix(int remaining) {
        if (remaining == 1) {
            return Overlay.CONSTANTS.taskListSinglePending();
        }
        return Overlay.CONSTANTS.taskListMultiplePending();
    }
    public int getRemaining() {
        int remaining = taskerInfo.total() - taskerInfo.completed();
        if (remaining <= 0) {
            return 0;
        } else {
            return remaining;
        }
    }

    protected void initializeListeners() {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                if (frame == null) {
                    return;
                }
                CrossMessager.sendMessageToFrame(frame.getElement(), "tasks",
                        StringUtils.stringifyObject(taskerInfo.data()));
            }
        }, "send_tasks");
        
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                onLoaded();                
            }
        }, "tasker_frame_loaded");
    }

    protected void fetchFlows(final AsyncCallback<Void> cb) {
        tracker.fetchFlows(invalidatableCb(new AsyncCallback<TaskerInfo>() {
            @Override
            public void onSuccess(TaskerInfo result) {
                taskerInfo = result;
                updateRemaining();
                showFirstTime();
                cb.onSuccess(null);
            }

            @Override
            public void onFailure(Throwable caught) {
                destroy();
            }
        }));
    }

    protected void update(String flow_id) {
        tracker.update(taskerInfo, flow_id,
                invalidatableCb(new AsyncCallback<TaskerInfo>() {
                    @Override
                    public void onSuccess(TaskerInfo result) {
                        updateRemaining();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                    }
                }));
    }

    protected AsyncCallback<TaskerInfo> invalidatableCb(
            final AsyncCallback<TaskerInfo> cb) {
        AsyncCallback<TaskerInfo> newCb = new AsyncCallback<TaskerInfo>() {
            @Override
            public void onFailure(Throwable caught) {
                if (isActive()) {
                    cb.onFailure(caught);
                }
            }

            @Override
            public void onSuccess(TaskerInfo result) {
                if (isActive()) {
                    cb.onSuccess(result);
                }

            }
        };
        return newCb;
    }

    @Override
    public void destroy() {
        super.destroy();
        removeListeners();
        active = false;
    }

    protected void removeListeners() {
        CrossMessager.removeListener(this, "send_tasks");
        CrossMessager.removeListener(this, "tasker_frame_loaded");
    }

    public boolean isActive() {
        return active;
    }

    public void trackCompleted(String userId) {
    }

    @Override
    protected Widget launcher(Settings settings) {
        FlowPanel outerContainer = new FlowPanel();

        taskerLauncherContainer = getTaskerLauncherContainer();
        outerContainer.add(taskerLauncherContainer);

        taskerAnimationContainer = getTaskerAnimationContainer();
        outerContainer.add(taskerAnimationContainer);

        setUpAnimation();

        return outerContainer;
    }

    protected FlowPanel getTaskerLauncherContainer() {
        FlowPanel taskerLauncherContainer = new FlowPanel();
        boolean isWcagEnabled = Enterpriser.hasFeature(AdditionalFeatures.wcag_enable);

        HorizontalPanel panel = new HorizontalPanel();
        panel.setStyleName(Overlay.CSS.taskerTable());
        Widget img = Common.html(SVGElements.TASKER_ICON,
                Overlay.CSS.taskerImg());
        img.getElement().setAttribute(AriaProperty.HIDDEN,
                String.valueOf(true));
        Image taskerImage = Image.wrap(img.getElement());
        taskerCircle = Common.imagePredAnchor(taskerImage);
        taskerCircle.addClickHandler(this);
        taskerCircle.setStyleName(Overlay.CSS.taskerLauncher());
        Themer.applyTheme(taskerCircle, Themer.STYLE.FONT, Themer.FONT);

        notificationCircle = new InlineLabel("");
        notificationCircle.addClickHandler(this);
        notificationCircle.setStyleName(Overlay.CSS.taskerNotification());

        String color = settings.color();
        String borderColor = ((TaskerSettings) settings).border_color();

        if (color != null) {
            OverlayUtil.setStyle(notificationCircle, OverlayUtil.BORDER_COLOR,
                    color);

            if (borderColor == null) {
                OverlayUtil.setStyle(taskerCircle, OverlayUtil.BACKGROUND,
                        color);
            } else {
                String[] properties = new String[] { OverlayUtil.BACKGROUND,
                        OverlayUtil.BORDER_COLOR };
                String[] colors = new String[] { color, borderColor };
                OverlayUtil.setStyle(taskerCircle, properties, colors);
            }
        } else {
            Themer.applyTheme(notificationCircle, Themer.STYLE.BORDER_COLOR,
                    Themer.TASK_LIST.LAUNCHER_COLOR, taskerCircle,
                    Themer.STYLE.BACKGROUND, Themer.TASK_LIST.LAUNCHER_COLOR);
        }

        panel.add(taskerCircle);
        if (showProgress()) {
            panel.add(notificationCircle);
        }

        // Allow dynamic positioning of tasklist only in desktop view and when
        // it is not show for preview
        if (!DOMUtil.isMobile() && !isPreview()) {
            setTaskerArrowPosition(panel);
        }
        taskerLauncherContainer.add(panel);

        taskerCircle.addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                taskerCircle.getElement().focus();
                if (!DOMUtil.isMobile() && !isPreview()
                        && null != arrowContainer && shouldShowTaskerArrow
                        && isWcagEnabled) {
                    togglePanel(arrowContainer, false);
                }
            }
        });

        // Removing arrow onBlur of tasker circle
        taskerCircle.addDomHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (!DOMUtil.isMobile() && !isPreview()
                        && null != arrowContainer && isWcagEnabled) {
                    togglePanel(arrowContainer, true);
                }
            }
        }, BlurEvent.getType());

        if (settings.target() != null) {
            taskerLauncherContainer.addStyleName(Overlay.CSS.widgetHidden());
        }
        return taskerLauncherContainer;
    }

    /**
     * Sets position of tasklist arrow based on current tasklist position. This
     * arrow is used to reposition tasklist dynamically at enduser side
     */
    private void setTaskerArrowPosition(HorizontalPanel panel) {
        Widget rightArrow = Common.html(SVGElements.TASKER_RIGHT_ARROW);
        Widget leftArrow = Common.html(SVGElements.TASKER_LEFT_ARROW);
        initArrowContainer(rightArrow, leftArrow, panel);

        // Show arrow on mouse over event
        panel.addDomHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                // Wrapping in shouldShowTaskerArrow boolean to make sure that
                // arrow is not shown during transition from one position to
                // other
                if (shouldShowTaskerArrow) {
                    togglePanel(arrowContainer, false);
                }
            }
        }, MouseOverEvent.getType());

        // Remove arrow on mouse out event
        panel.addDomHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                togglePanel(arrowContainer, true);
            }
        }, MouseOutEvent.getType());
        
        if (Enterpriser.hasFeature(AdditionalFeatures.wcag_enable)) {
            // Keeps focus on arrow when moved to arrow container
            arrowContainer.addFocusHandler(new FocusHandler() {
                @Override
                public void onFocus(FocusEvent event) {
                    if (shouldShowTaskerArrow) {
                        togglePanel(arrowContainer, false);
                    }

                }
            });

            // Remove arrow on blur event
            arrowContainer.addDomHandler(new BlurHandler() {

                @Override
                public void onBlur(BlurEvent event) {
                    togglePanel(arrowContainer, true);
                }

            }, BlurEvent.getType());
        }
    }

    /**
     * Inserts left/right arrow in arrowContainer based on position of tasklist
     */
    private void insertArrowInContainer(HorizontalPanel panel,
            Widget rightArrow, Widget leftArrow) {
        if (TASKER_POSITION_BOTTOM_LEFT.equalsIgnoreCase(getPosition())) {
            arrowContainer.addStyleName(Overlay.CSS.taskerArrowContainerLeft());
            arrowContainer.add(rightArrow);
            arrowContainer.getElement().setAttribute(AriaProperty.LABEL,
                    TASKER_ARROW_RIGHT_ARIA_LABEL);
            panel.add(arrowContainer);
        } else {
            arrowContainer
                    .addStyleName(Overlay.CSS.taskerArrowContainerRight());
            arrowContainer.add(leftArrow);
            arrowContainer.getElement().setAttribute(AriaProperty.LABEL,
                    TASKER_ARROW_LEFT_ARIA_LABEL);
            panel.insert(arrowContainer, 0);
        }
    }

    /**
     * This method is used to toggle visibility condition
     */
    private void togglePanel(FocusPanel panel, boolean shouldHide) {
        String remove = Overlay.CSS.taskerArrowContainerShow();
        String add = Overlay.CSS.taskerArrowContainerHide();
        if (!shouldHide) {
            remove = Overlay.CSS.taskerArrowContainerHide();
            add = Overlay.CSS.taskerArrowContainerShow();
        }
        panel.removeStyleName(remove);
        panel.addStyleName(add);
    }
    
    /**
     * Resets the arrow container, arrows, attributes and styles on position
     * change
     */
    private void resetArrowContainer(FocusPanel arrowContainer,
            Widget leftArrow, Widget rightArrow) {
        togglePanel(arrowContainer, true);
        arrowContainer.getElement()
                .removeAttribute(TASKER_ARROW_RIGHT_ARIA_LABEL);
        arrowContainer.getElement()
                .removeAttribute(TASKER_ARROW_LEFT_ARIA_LABEL);
        arrowContainer.removeFromParent();
        arrowContainer.removeStyleName(Overlay.CSS.taskerArrowContainerLeft());
        arrowContainer.removeStyleName(Overlay.CSS.taskerArrowContainerRight());
        rightArrow.removeFromParent();
        leftArrow.removeFromParent();
    }

    /**
     * Returns the arrow container which is shown for tasklist
     */
    private void initArrowContainer(Widget rightArrow, Widget leftArrow,
            HorizontalPanel panel) {
        arrowContainer = new FocusPanel();
        arrowContainer.setStyleName(Overlay.CSS.taskerArrowContainer());
        arrowContainer.addStyleName(Overlay.CSS.noOutline());
        arrowContainer.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onTaskerArrowClick(rightArrow, leftArrow, panel);
            }
        });

        if (Enterpriser.hasFeature(AdditionalFeatures.wcag_enable)) {
            // Remove no outline for WCAG
            arrowContainer.removeStyleName(Overlay.CSS.noOutline());

            // Handling Enter key for WCAG
            arrowContainer.addKeyDownHandler(new KeyDownHandler() {

                @Override
                public void onKeyDown(KeyDownEvent event) {
                    if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                        onTaskerArrowClick(rightArrow, leftArrow, panel);
                    }
                }
            });
        }
        togglePanel(arrowContainer, true);
        // Insert left/right arrow based on position
        insertArrowInContainer(panel, rightArrow, leftArrow);
    }

    /**
     * Update launcher position when changed from left/right
     */
    private void updateLauncherPosition(String updatedPosition) {
        Style taskerAnimationElementStyle = taskerAnimationElement.getElement()
                .getStyle();
        taskerAnimationElementStyle.clearLeft();
        taskerAnimationElementStyle.clearRight();
        setTaskerAnimationPosition(updatedPosition,
                taskerAnimationElementStyle);
        refreshLauncherPosition();
    }

    protected PopupPanel getTaskerAnimationContainer() {
        PopupPanel taskerAnimationContainer = new PopupPanel();        
        taskerAnimationContainer.setStyleName(Overlay.CSS.taskerAnimationContainer());
        
        FlowPanel innerContainer = new FlowPanel();
        
        taskerAnimationElement = new FlowPanel();

        taskerAnimationElement.setStyleName(getNormalStyle());

        taskerAnimationElement
                .addStyleName(Overlay.CSS.taskerLauncherAnimation());

        String position = getPosition();

        Style taskerAnimationElementStyle = taskerAnimationElement.getElement()
                .getStyle();
        
        String color = settings.color();

        if (color == null) {
            color = Themer.value(Themer.TASK_LIST.LAUNCHER_COLOR);
        }

        taskerAnimationElementStyle.setBackgroundColor(color);
        
        setTaskerAnimationPosition(position, taskerAnimationElementStyle);

        innerContainer.add(taskerAnimationElement);
        
        taskerProgressor = Common.getSVGProgressor(Overlay.CSS.taskerSpinner(),
                Overlay.CSS.taskerProgressor());
        innerContainer.add(taskerProgressor);
        
        taskerAnimationContainer.add(innerContainer);
        
        return taskerAnimationContainer;
    }
    
    private void setTaskerAnimationPosition(String position,
            Style taskerAnimationElementStyle) {
        if (position.equals(TASKER_POSITION_BOTTOM_LEFT)) {
            setLeft(taskerAnimationElementStyle, 0);
        } else {
            if (showProgress() && settings.target() == null) {
                setRight(taskerAnimationElementStyle, 15);
            } else {
                setRight(taskerAnimationElementStyle, 0);
            }
        }
    }

    @Override
    protected void setRelPosition(PopupPanel popup, int width, int height) {
        if (clientWidth() < 640) {
            // For Mobile device, to show TL as a Popup
            // setting Rel Position is not required
            // Refer to TaskerLauncher::MobileTaskerLauncher for how it is being
            // positioned in mobile.
            return;
        }

        Element target = null;
        try {
            target = relativeElement(Document.get(), settings.target());
        } catch (Exception e) {
        }
        Console.debugOn("TaskList Target");
        if (target == null) {
            Console.debug("Target Element not found.");
            Console.debugOff();
            return;
        }
        Console.debug(target.getId());
        Console.debugOff();

        Style popStyle = popup.getElement().getStyle();
        OverlayUtil.clearPosition(popStyle);
        String position = getPosition();
        if ("bl".equals(position)) {
            setLeft(popStyle, target.getAbsoluteRight());
        } else {
            setRight(popStyle, target.getAbsoluteLeft());
        }
        setBottom(popStyle, clientHeight() - target.getAbsoluteTop());

    }

    @Override
    protected void setFixedPosition(PopupPanel popup, int width, int height,
            boolean isLauncher) {
        int totalHeight = clientHeight();

        Element elem = popup.getElement();
        Style style = elem.getStyle();

        // If panel is a launcher here, we dont clear lef and right attribute as
        // it effects transition. We are setting tasklist position only from
        // left now and same is adjusted accordingly
        if (!isLauncher) {
            OverlayUtil.clearPosition(style);
        } else {
            style.clearTop();
            style.clearBottom();
        }
        String position = getPosition();
        if ("bl".equals(position)) {
            setLeft(style, 15);
        } else if ("br".equals(position)) {
            if (isLauncher) {
                int elementWidth = elem.getClientWidth();
                if (elementWidth != 0) {
                    int right = !showProgress() ? 15 : 0;
                    setLeft(style,
                            Window.getClientWidth() - right - elementWidth);
                }
            } else {
                setRight(style, 15);
            }
        }

        if (isLauncher) {
            setTop(style, (totalHeight - (height + 15)));
        } else {
            setBottom(style, 15);
        }
    }

    protected String getPosition() {
        // If user has changed the tasklist position, fetch that preference
        String userPosition = UserPreferences.getTasklistUserPosition();
        if (StringUtils.isNotBlank(userPosition)
                && (TASKER_POSITION_BOTTOM_LEFT.equalsIgnoreCase(userPosition)
                        || TASKER_POSITION_BOTTOM_RIGHT
                                .equalsIgnoreCase(userPosition))) {
            return userPosition;
        }

        String position = settings.position();
        if (position == null || ("rtm").equalsIgnoreCase(position)) {
            position = Themer.value(Themer.TASK_LIST.POSITION);
        }
        if (position == null
                || !(position.equals("bl") || position.equals("br"))) {
            position = "bl";
        }
        return position;
    }

    private void onTaskerArrowClick(Widget rightArrow, Widget leftArrow,
            HorizontalPanel panel) {
        shouldShowTaskerArrow = false;
        resetArrowContainer(arrowContainer, leftArrow, rightArrow);
        String updatedPosition = TASKER_POSITION_BOTTOM_LEFT
                .equalsIgnoreCase(getPosition()) ? TASKER_POSITION_BOTTOM_RIGHT
                        : TASKER_POSITION_BOTTOM_LEFT;
        UserPreferences.saveTasklistUserPosition(updatedPosition);
        insertArrowInContainer(panel, rightArrow, leftArrow);
        updateLauncherPosition(updatedPosition);
        sendPositionChangeGaEvent(updatedPosition);

        // Hides arrows when position is changing
        new Timer() {
            @Override
            public void run() {
                shouldShowTaskerArrow = true;
            }
        }.schedule(TASKER_POSITION_CHANGE_DURATION);
    }
    
    private void sendPositionChangeGaEvent(String updatedPosition) {
        Common.tracker().onEvent(EmbedEvent.TASK_LIST_POSITION_CHANGE.builder()
                .segment(settings.segment_id(), settings.segment_name())
                .srcId(TrackerEventOrigin.TASK_LIST.getSrcName())
                .replaceInPage("taskerPosition",
                        TASKER_POSITION_BOTTOM_LEFT.equalsIgnoreCase(
                                updatedPosition) ? TASKER_POSITION_LEFT_EVENT
                                        : TASKER_POSITION_RIGHT_EVENT)
                .widget(TrackerEventOrigin.TASK_LIST).build());
    }

    protected static interface Tracker {

        public void initialize(Settings settings, ContentManager manager,
                LaunchTasker launcher);

        public void fetchFlows(final AsyncCallback<TaskerInfo> callback);

        public void refresh(TaskerInfo taskerInfo,
                final AsyncCallback<TaskerInfo> callback);

        public void update(TaskerInfo taskerInfo, String flow_id,
                AsyncCallback<TaskerInfo> callback);

        public void onTasksCompleted();

        public void getNudgeCounters(
                Map<String, String> taskerNudgeCounterData, AsyncCallback<String> callback);
        
        public void updateNudgeCounters(Where counters);
    }

    protected static class WfTracker implements Tracker {
        private Settings settings;
        private ContentManager manager;
        private LaunchTasker launcher;

        @Override
        public void initialize(Settings settings, ContentManager manager,
                LaunchTasker launcher) {
            this.settings = settings;
            this.manager = manager;
            this.launcher = launcher;
        }

        @Override
        public void fetchFlows(final AsyncCallback<TaskerInfo> callback) {
            JsArrayString check_complete = getContentIdToCheckComplete(settings.order());
            String[][] mixSettings = settings.mixedTypeValue();
            UserInfo user = Security
                    .unq_id_with_enc_status(AppUser.getUserId());
            manager.tasks(mixSettings[0], mixSettings[1], Security.user_id(),
                    user, settings.order(), check_complete, new AsyncCallback<TaskerData>() {

                        @Override
                        public void onSuccess(TaskerData tasks) {
                            callback.onSuccess(new TaskerInfo(tasks, (TaskerSettings)settings));
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            callback.onFailure(null);
                        }
                    });
        };
        
        private JsArrayString getContentIdToCheckComplete(JsArrayString order) {
            JsArrayString check_complete = null;
            if (settings.isGroupPresent()) {
                JavaScriptObjectExt groups = settings.groups();
                check_complete = DataUtil.createArray().cast();
                if (order != null) {
                    for (int index = 0; index < order.length(); index++) {
                        if (groups.hasKey(order.get(index))) {
                            Group group = (Group) groups
                                    .value(order.get(index));
                            JsArrayString content_ids = group.content_ids();
                            if (content_ids != null) {
                                for (int i = 0; i < content_ids.length(); i++) {
                                    check_complete.push(content_ids.get(i));
                                }
                            }
                        }
                    }
                }
            }
            return check_complete;
        }

        private String unq_id() {
            return Security.unq_id(AppUser.getUser());
        }

        @Override
        public void refresh(TaskerInfo taskerInfo,
                AsyncCallback<TaskerInfo> callback) {
            fetchFlows(callback);
        }

        @Override
        public void update(TaskerInfo taskerInfo, String flow_id,
                AsyncCallback<TaskerInfo> callback) {
            if (taskerInfo.setCompleted(flow_id)) {
                launcher.trackCompleted(unq_id());
            }
            callback.onSuccess(taskerInfo);
        }

        @Override
        public void onTasksCompleted() {
            Customizer.onTasksCompleted();
        }

        @Override
        public void getNudgeCounters(Map<String, String> taskerNudgeCounterData,
                AsyncCallback<String> callback) {
            FlowService.IMPL.getNudgeCounters(settings.segment_id(),
                    new AsyncCallback<TaskerNudgeCountersData>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            // Do nothing
                        }

                        @Override
                        public void onSuccess(TaskerNudgeCountersData result) {
                            taskerNudgeCounterData.put(NUDGE_COUNT,
                                    String.valueOf(result.nudge_count()));
                            taskerNudgeCounterData.put(NUDGE_EVAL_TIMESTAMP,
                                    String.valueOf(
                                            result.nudge_eval_timestamp()));
                            taskerNudgeCounterData.put(TASK_COMPLETE_COUNT,
                                    String.valueOf(
                                            result.task_complete_count()));
                            taskerNudgeCounterData.put(LAST_RESET_TIMESTAMP,
                                    String.valueOf(
                                            result.last_reset_timestamp()));
                            callback.onSuccess(null);
                        }
                    });
        }

        @Override
        public void updateNudgeCounters(Where counters) {
            FlowService.IMPL.updateNudgeCounters(counters,
                    settings.segment_id(), new AsyncCallback<String>() {
                        @Override
                        public void onSuccess(String result) {
                            if(StringUtils.isNotBlank(result)) {
                                Console.debug("Nudge update failed: "+result);                                
                            }
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            Console.debug("Nudge update failed: "
                                    + caught.getMessage());
                        }
                    });
        }
        
    }



    /**
     * This method has to be overriden when launcher is being user for
     * previewing on dashboard
     * 
     * @return
     */
    protected boolean isPreview() {
        return false;
    }
    
	public static boolean isTaskerVisible() {
		return isTaskerVisible;
	}
	
	public static void updateTaskerVisibility(boolean isTaskerVisible) {
		LaunchTasker.isTaskerVisible = isTaskerVisible;
	}
    
}
