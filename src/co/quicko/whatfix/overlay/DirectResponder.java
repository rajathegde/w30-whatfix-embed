package co.quicko.whatfix.overlay;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Browser;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.PossibleElementList;
import co.quicko.whatfix.overlay.Actioner.DirectErrorReller;
import co.quicko.whatfix.overlay.Actioner.DirectObserver;
import co.quicko.whatfix.overlay.Actioner.DirectReller;
import co.quicko.whatfix.overlay.actioner.ActionerPopover;
import co.quicko.whatfix.player.tip.SmartTipPlayer;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.FindListener;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.FinderEight;
import co.quicko.whatfix.workflowengine.alg.FinderUtil;
import co.quicko.whatfix.workflowengine.data.finder.FinderLogger;

public class DirectResponder extends AbstractResponder {

    public DirectResponder(Draft draft, int step,
            FindListener... listeners) {
        super(draft, step, listeners);
        setFocusToElement();
    }

    @Override
    public void startSearcher() {
        if (helper.supportsSpotlight()) {
            Actioner.hideSpotlightTimer.cancel();
            Actioner.hideSpotlightTimer.schedule(Actioner.SPOTLIGHT_HIDE_INTERVAL);
        }
        super.startSearcher();
    }

    @Override
    protected void addSubscriber() {
        mutationBus.addSubscriber(this);
    }

    @Override
    public JsArray<JavaScriptObjectExt> find(AsyncCallback<Boolean> laterCb) {
        long sTime = System.currentTimeMillis();
        PossibleElementList<Element> matches = Finder
                .findBest(Document.get(), draft, action);
        getLogger().log(WfxLogLevel.PERFORMANCE, DirectResponder.class, "find",
                WfxModules.WORKFLOW_ENGINE,
                "Time taken by findBest method of Finder: {0} ms",
                (System.currentTimeMillis() - sTime));
        return initializeStep(laterCb, matches);
    }

    @Override
    public JsArray<JavaScriptObjectExt> initializeStep(
            final AsyncCallback<Boolean> laterCb,
            PossibleElementList<Element> matches) {
        if (FinderUtil.isFound(matches)) {
            if (ContentType.flow.name().equals(action.type())) {
                setStepFindTimeAndCount(matches.size());
            }
            List<Element> matchesElements = matches.getElements();
            wrapElement(matchesElements, new DirectObserver(this));
            Element best = matches.get(0);
            if (findListeners != null) {
                for (FindListener listener : findListeners) {
                    listener.postFind(action, best, getStepFindData());
                }
            }
            if (isForCollection()) {
                SmartTipPlayer.setStepFindData(getStepFindData());
                helper.init(matchesElements);
                watchRel(best, getDirectReller(),
                        Enterpriser.hasFeature(AdditionalFeatures.wcag_enable));
                return null;
            }
            helper.showPop(best);
            watchRel(best, new DirectReller(draft, action, this),
                    Enterpriser.hasFeature(AdditionalFeatures.wcag_enable));
            scroll(best, action.placement(), helper.getPopper());
            return null;
        } else {
            return FinderLogger.getFinderFailureComponent(action.step(),
                    action.type());
        }
    }
    
    private void scroll(final Element element, final String placement,
            final ActionerPopover popover) {
        final String pmt = ActionerPopover.placement(placement);
        Actioner.impl.scheduleFixedDelay(() -> {
            scrollIfNeeded(element, placement, pmt, popover);
            return false;
        }, getGap());
    }

    /*
     * Scroll gap is very specifically set to 245, since it was conflicting with
     * relocator default gap (250). Be very cautious while changing this value.
     */
    private int getGap() {
        return 245;
    }

    @Override
    protected int getParent() {
        return 0;
    }
    
    @Override
    protected boolean canEnableNewSmartTips() {
        return helper.canEnableNewSmartTips();
    }

    private DirectReller getDirectReller() {
        return helper.isValidator()
                ? new DirectErrorReller(draft, action, this)
                : new DirectReller(draft, action, this);
    }

    @Override
    protected Helper getHelperInfoStatic() {
        return new HelperInfoStatic() {
            @Override
            public boolean hideOnScrollEnabled(Element element) {
                if (this.isMutationBased) {
                    return false;
                }
                return !(element != null
                        && OverlayUtil.hasSingleScroll(element));
            }
        };
    }

    @Override
    public void onMiss(JsArray<JavaScriptObjectExt> reason) {
        if (Browser.isChromium()) {
            FinderEight.fillVisualFinderData(action, reason, null);
        }
        super.onMiss(reason);
    }

    /**
     * Two parts: 1. Customizer.customScroll implemented in AC 2. Whatfix code
     * logic <br>
     * If Customizer.customScroll returns true, means codebase logic won't be
     * executed. If false, that means either customizer don't have this callback
     * defined or they do want to use scroll callback (probably doing other
     * stuff here, but they didn't use scrollIntoView) and want Whatfix to
     * perform scroll.
     */
    private void scrollIfNeeded(final Element element, final String placement,
            String pmt, ActionerPopover popover) {
        boolean alignTop = Actioner.alignTop(pmt);
        boolean alignRight = Actioner.alignRight(pmt);
        Boolean hasSingleScroll = null;
        if (Customizer.customScroll(action.flow_id(), action.step(), element)) {
            getLogger().log(WfxLogLevel.DEBUG, DirectResponder.class, "scroll",
                    WfxModules.JIT, "Custom scroll logic is used");
        } else if (needScroll(element, alignTop, alignRight)
                || !OverlayUtil.isOnScreen(element)) {
            getLogger().log(WfxLogLevel.DEBUG, DirectResponder.class, "scroll",
                    WfxModules.JIT,
                    "Scroll is needed, Element is not on screen");
            boolean hasSingleVerticalScroll = OverlayUtil
                    .hasSingleVerticalScroll(element,
                            Customizer.ignoreScrollOnBody());
            boolean hasSingleHorizontalScroll = OverlayUtil
                    .hasSingleHorizontalScroll(element,
                            Customizer.ignoreScrollOnBody());
            hasSingleScroll = hasSingleVerticalScroll;
            if (!hasSingleVerticalScroll || !hasSingleHorizontalScroll) {
                Common.scrollIntoView(element, alignTop);
            } else {
                Actioner.scrollIntoCenterOfViewPort(element, placement,
                        hasSingleHorizontalScroll);
            }
        } else {
            // Adding because of sonar
        }
        Actioner.alignPopover(popover, !alignTop, hasSingleScroll, element);
    }
}
