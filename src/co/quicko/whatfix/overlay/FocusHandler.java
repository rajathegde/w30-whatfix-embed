package co.quicko.whatfix.overlay;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.Event;

public class FocusHandler {
    private static List<FocusListener> listeners;

    private static JavaScriptObject nativeFocusListener;
    private static JavaScriptObject nativeBlurListener;

    public static void addListener(FocusListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<FocusListener>();
            attachNativeListener();
        }
        listeners.add(listener);
    }

    public static void removeListener(FocusListener listener) {
        if (listeners == null) {
            return;
        }
        listeners.remove(listener);

        if (listeners.size() == 0) {
            listeners = null;
            detachNativeListener();
        }
    }

    private static native void attachNativeListener() /*-{
		nativeFocusListener = function(event) {
			@co.quicko.whatfix.overlay.FocusHandler::fireFocus(Lcom/google/gwt/user/client/Event;)(event);
		}

		nativeBlurListener = function(event) {
			@co.quicko.whatfix.overlay.FocusHandler::fireBlur(Lcom/google/gwt/user/client/Event;)(event);
		}

		if ($wnd.addEventListener) {
			$wnd.addEventListener("focus", nativeFocusListener, true);
			$wnd.addEventListener("blur", nativeBlurListener, true);
		} else {
			$wnd.attachEvent("focus", nativeFocusListener, true);
			$wnd.attachEvent("blur", nativeBlurListener, true);
		}
    }-*/;

    private static native void detachNativeListener() /*-{
		if ($wnd.removeEventListener) {
			$wnd.removeEventListener("focus", nativeFocusListener, true);
			$wnd.removeEventListener("blur", nativeBlurListener, true);
		} else {
			$wnd.removeEvent("focus", nativeFocusListener, true);
			$wnd.removeEvent("blur", nativeBlurListener, true);
		}
    }-*/;

    public static interface FocusListener extends EventHandler {
        public void onFocus(Event event);

        public void onBlur(Event event);
    }

    private static void fireFocus(Event event) {
        for (FocusListener listener : listeners) {
            listener.onFocus(event);
        }
    }

    private static void fireBlur(Event event) {
        for (FocusListener listener : listeners) {
            listener.onBlur(event);
        }
    }
}