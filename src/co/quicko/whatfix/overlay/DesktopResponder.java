package co.quicko.whatfix.overlay;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.DesktopNativeUtils;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.DesktopElement;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;
import co.quicko.whatfix.workflowengine.DesktopFinder;
import co.quicko.whatfix.workflowengine.DesktopFinderVersions;
import co.quicko.whatfix.workflowengine.FindListener;
import co.quicko.whatfix.workflowengine.desktop.DesktopObjects.FoundElementDetails;

public class DesktopResponder extends AbstractResponder {

    private DesktopFinder desktopFinder = new DesktopFinder();
    private static final String DESKTOP_FOUND_ELEMENT = "desktop_found_element";

    int step;

    public DesktopResponder(Draft draft, int step, FindListener[] listeners) {
        super(draft, step, listeners);
        this.step = step;
    }

    /**
     * global checker to track the found element and keep it consistent.
     */
    private DesktopElement foundElement;
    
    @Override
    public JsArray<JavaScriptObjectExt> find(AsyncCallback<Boolean> laterCb) {
        // get the finder version old one and then use it to run the algorithm.
        int finderVersion = draft.step_finder_ver(step);
        if (finderVersion == DesktopFinderVersions.TWO.getVersionCode()) {
            finderVersionTwo(laterCb);
        } else {
            oldFinderVersionOne(laterCb);
        }
        // returning something empty here to avoid default searcher from
        // assuming I found the element already .
        return DataUtil.createArray();
    }

    private void finderVersionTwo(final AsyncCallback<Boolean> laterCb) {
        startFinder(laterCb);
    }

    private void startFinder(AsyncCallback<Boolean> laterCb) {
        final long start = System.currentTimeMillis();
        desktopFinder.findBest(draft.step(step),
                new AsyncCallback<DesktopElement>() {
                    @Override
                    public void onSuccess(DesktopElement result) {
                        if (result == null) {
                            // this means no element was found.
                            // this may also happen in cases when the window is
                            // minimized and no properties are returned
                            foundElement = null;
                            helper.hidePopper();
                            return;
                        }
                        laterCb.onSuccess(true);
                        if (foundElement == null
                                || foundElement.top() != result.top()
                                || foundElement.left() != result.left()) {
                            getLogger().log(WfxLogLevel.PERFORMANCE,
                                    DesktopResponder.class, "",
                                    WfxModules.WORKFLOW_ENGINE,
                                    "total time taken from start to finish:"
                                            + (System.currentTimeMillis()
                                                    - start));
                            helper.hidePopper();
                            helper.showPop(getCoordinates(result.top(),
                                    result.left(), result.width(),
                                    result.height()));
                            foundElement = result;
                        }
                        // send element details to native for element level tracking. 
                        FoundElementDetails details = DataUtil.create();
                        details.copyAll(foundElement);
                        details.height(details.rect_bottom() - details.rect_top());
                        details.width(details.rect_right() - details.rect_left());
                        details.step(step);
                        details.completion_actions(draft.step(step).completion_actions());
                        DesktopNativeUtils.writeToIPC(MessageUtil.makeMessage(
                                DESKTOP_FOUND_ELEMENT, JsonUtils.stringify(details)));
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        // never will be called, success handles both failure
                        // and success.
                    }
                });
    }

    private void oldFinderVersionOne(AsyncCallback<Boolean> laterCb) {
        Step stepToBeFound = DataUtil.create();
        stepToBeFound.marks(draft.step_marks(step));
        stepToBeFound.element_xpath(draft.step_element_xpath(step));
        stepToBeFound.completion_actions(draft.step_completion_actions(step));
        CrossMessager.sendMessage("start_finder",
                StringUtils.stringifyObject(stepToBeFound));
    }

    private Coordniates getCoordinates(int top, int left, int width,
            int height) {
        Coordniates coords = DataUtil.create();
        coords.top(top);
        coords.left(left);
        coords.bottom(top + height);
        coords.right(left + width);
        coords.offsetWidth(width);
        coords.offsetHeight(height);
        return coords;
    }

    @Override
    protected int getParent() {
        return 0;
    }

    @Override
    protected void addSubscriber() {
        // no impl
    }
}
