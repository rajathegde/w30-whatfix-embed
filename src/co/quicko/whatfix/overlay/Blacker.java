package co.quicko.whatfix.overlay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.CSS;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.overlay.Picker.PickerListener;

public class Blacker extends Picker<Element> implements PickerListener<Element> {
    private List<OverBlack> overs = new ArrayList<OverBlack>();
    private String blurValue;

    public Blacker() {
        setListener(this);
    }

    public void stop() {
        super.hide();
        for (OverBlack over : overs) {
            over.hide();
        }
        overs.clear();
        clearIgnores();
    }

    public void pause() {
        super.hide();
    }
    
    public void setBlurValue(String blurValue) {
        this.blurValue  = blurValue;
    }
    
    public String getBlurValue() {
        return blurValue;
    }

    @Override
    public Element onSource(Element element) {
        hideEngagers();
        OverBlack black = new OverBlack(this, element);
        addIgnores(black.getElement());
        overs.add(black);
        return null;
    }

    @Override
    public void onFrame(Element picked, Element frame) {
    }

    @Override
    public void onTop(Element picked) {
    }

    @Override
    public void onMeta(String meta) {
    }

    private void remove(OverBlack over) {
        overs.remove(over);
        removeIgnores(over.getElement());
        over.hide();
    }

    @Override
    protected void onPreviewUnknownEvent(Element source,
            PreviewEngageEvent event) {
        event.cancel();
    }

    private static class OverBlack extends PopupPanel implements
            MouseOverHandler, MouseOutHandler, ClickHandler {
        private Blacker blacker;

        OverBlack(Blacker blacker, Element element) {
            this.blacker = blacker;

            Label w = new Label();
            w.addClickHandler(this);
            w.addMouseOutHandler(this);
            w.addMouseOverHandler(this);
            setWidget(w);
            setPixelSize(element.getOffsetWidth(), element.getOffsetHeight());
            setPopupPosition(element.getAbsoluteLeft(),
                    element.getAbsoluteTop());
            setStyleName(Overlay.CSS.blackOverPop());

            if(CSS.isBlurSupported()) { 
                if (this.blacker.getBlurValue() != null) {
                    Map<String, String> blurStyle = new HashMap<String, String>();
                    blurStyle.put("backdrop-filter",
                            "blur(" + this.blacker.getBlurValue() + "px);");
                    Common.appendToStyle(this.getElement(), blurStyle);
                }else {
                    addStyleName(Overlay.CSS.blackOverBlur());
                }
            } else {
                w.setStyleName(Overlay.CSS.blackOver());
            }
            
            show();
        }
        
        @Override
        public void onMouseOver(MouseOverEvent event) {
            getWidget().addStyleName(Overlay.CSS.blackOverSelect());
        }

        @Override
        public void onMouseOut(MouseOutEvent event) {
            getWidget().removeStyleName(Overlay.CSS.blackOverSelect());
        }

        @Override
        public void onClick(ClickEvent event) {
            blacker.remove(this);
        }
    }
}
