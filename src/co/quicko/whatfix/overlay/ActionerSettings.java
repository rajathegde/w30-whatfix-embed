package co.quicko.whatfix.overlay;

import com.google.gwt.core.client.JavaScriptObject;

import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.overlay.Actioner.Settings;

public class ActionerSettings extends JavaScriptObject implements Settings {
    protected ActionerSettings() {
    }

    public static ActionerSettings copy(Settings settings) {
        ActionerSettings actionerSettings = DataUtil.create();
        actionerSettings.staticShowTime(settings.staticShowTime());
        actionerSettings.staticCloseTime(settings.staticCloseTime());
        actionerSettings.staticClose(settings.staticClose());
        return actionerSettings;
    }

    @Override
    public final native int staticShowTime() /*-{
        return this.static_show_time ? this.static_show_time : 500;
    }-*/;

    public final native void staticShowTime(int staticShowTime) /*-{
        this.static_show_time = staticShowTime;
    }-*/;

    @Override
    public final native int staticCloseTime() /*-{
        return this.static_close_time ? this.static_close_time : 1000;
    }-*/;

    public final native void staticCloseTime(int staticCloseTime) /*-{
        this.static_close_time = staticCloseTime;
    }-*/;

    @Override
    public final native boolean staticClose() /*-{
        return this.static_close ? true : false;
    }-*/;

    public final native void staticClose(boolean staticClose) /*-{
        this.static_close = staticClose;
    }-*/;
}
