package co.quicko.whatfix.overlay;

import com.google.gwt.event.dom.client.KeyCodes;

import co.quicko.whatfix.common.editor.StepCompletionActions;

public enum KeyboardEvents {
    KEY_TAB(
            StepCompletionActions.ON_KEYBOARD_TAB.getValue(),
            KeyCodes.KEY_TAB),
    KEY_ENTER(
            StepCompletionActions.ON_KEYBOARD_ENTER.getValue(),
            KeyCodes.KEY_ENTER),
    KEY_ESC(
            StepCompletionActions.ON_KEYBOARD_ESC.getValue(),
            KeyCodes.KEY_ESCAPE);
    
    private int keyCode;
    private int keyNativeCode;

    private KeyboardEvents(int keyCode, int keyNativeCode) {
        this.keyCode = keyCode;
        this.keyNativeCode = keyNativeCode;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public int getKeyNativeCode() {
        return keyNativeCode;
    }

    public static Integer getKeyCode(int keyNativeCode) {
        for (KeyboardEvents event : values()) {
            if (event.getKeyNativeCode() == keyNativeCode) {
                return event.getKeyCode();
            }
        }
        return null;
    }

}
