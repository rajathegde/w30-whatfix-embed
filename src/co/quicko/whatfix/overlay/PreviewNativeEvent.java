package co.quicko.whatfix.overlay;

import com.google.gwt.user.client.Event.NativePreviewEvent;

public class PreviewNativeEvent extends PreviewEngageEvent {
    protected NativePreviewEvent nativePreviewEvent;

    public PreviewNativeEvent(NativePreviewEvent nativePreviewEvent) {
        super(nativePreviewEvent.getNativeEvent());
        this.nativePreviewEvent = nativePreviewEvent;
    }
    
    @Override
    public void cancel() {
        nativePreviewEvent.cancel();
    }
}
