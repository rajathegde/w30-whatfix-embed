package co.quicko.whatfix.overlay;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.shared.EventHandler;

public interface EngageEventHandler extends EventHandler {
    void onEngage(NativeEvent event);
}
