package co.quicko.whatfix.overlay;

import java.util.List;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.MouseOutEvent;

import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;

public interface ResponderHelper {
    public boolean supportsSpotlight();

    public void showPop(Element element);

    public void showPop(Coordniates coords);
    
    public void placePop(Element element);

    public void placePop(Coordniates coords);

    public void movePop(Element element);

    public void movePop(Coordniates coords);

    public void showEngagers();

    public void hideEngagers();

    public void hidePopper();

    public void hideOnOverlap(Element element);

    public void hideOnScroll();

    public void dispose();

    public void onMouseOut(MouseOutEvent event);

    public void handleClose();

    public void handleNext();

    public void handleBack();

    public void init(List<Element> matches);

    public void init(Coordniates coords);

    public void setActionElement(Element actionElement);

    public void validate(Element element);

    public boolean isValid();

    public void onValidationError();
    
    public void onStepCompletion(String action);

    public void showErrorPopup(Element element);

    public void showErrorPopup(Coordniates coords);

    public void hideErrorPopup();

    public void showErrorEngagers(Element element);

    public void showErrorEngagers(Coordniates coords);

    public void hideErrorEngagers();

    public void disposeErrorPopper(boolean isFromDestroy);

    public void setIFramesCointainerZIndex(int z_index);

    public int getIFramesCointainerZIndex();

    public boolean hideOnScrollEnabled(Element element);
    
    public void onStabilized(Element element);

    public void setWcagListener(Element element);

    public void placePopAndSetWcag(Element element);

    public void handleKeyDownListener();
    
    public boolean canEnableNewSmartTips();

}
