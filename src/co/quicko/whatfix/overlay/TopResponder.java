package co.quicko.whatfix.overlay;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.ZMaxer;
import co.quicko.whatfix.data.ActionerInitData;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.overlay.Actioner.TopReller;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;
import co.quicko.whatfix.player.tip.SmartTipPlayer;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.FindListener;

public class TopResponder extends MiddleResponder {
    private CrossListener engShow;
    private CrossListener engHide;
    private CrossListener next;
    private CrossListener optionalNext;
    private CrossListener miss;
    private CrossListener over;
    private CrossListener hideOnMove;
    private CrossListener staticErrorHide;
    private CrossListener staticErrorEngagerHide;
    private CrossListener staticErrorDispose;
    private CrossListener staticValidationError;
    private CrossListener keyDownHandler;
    private CrossListener hideWcagPopper;
    private CrossListener stepCompletion;
    private boolean isMissEvent = false;
    private int delay;

    public TopResponder(Draft draft, int step, int parent,
            FindListener... listeners) {
        super(draft, step, parent, listeners);

        engShow = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                helper.showEngagers();
            }
        }, "actioner_show_eng");

        engHide = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                helper.hideEngagers();
            }
        }, "actioner_hide_eng");

        next = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                if (StringUtils.isNotBlank(message)) {
                    JavaScriptObjectExt evaluationData = DataUtil
                            .create(message);
                    action.branch_element_evaluation(evaluationData);
                }
                onNext();
            }
        }, "actioner_next");

        optionalNext = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                onOptionalNext();
            }
        }, "actioner_optional_next");

        miss = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                JsArray<JavaScriptObjectExt> reason = DataUtil.createArray();
                if (StringUtils.isNotBlank(message)) {
                    reason = DataUtil
                            .<JsArray<JavaScriptObjectExt>> create(message);
                }
                onMiss(reason);
            }
        }, "actioner_miss");

        over = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                helper.hideOnOverlap(null);
            }
        }, "actioner_over");

        hideOnMove = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                helper.hideOnScroll();
            }
        }, "actioner_hide_on_move");

        staticErrorHide = CrossMessager
                .addListener(new ResponderListener() {
                    @Override
                    public void processMessage(String coordindates) {
                        onStaticHideError();
                    }
                }, "actioner_static_hide_error");

        staticErrorEngagerHide = CrossMessager
                .addListener(new ResponderListener() {
                    @Override
                    public void processMessage(String coordindates) {
                        onStaticHideErrorEngager();
                    }
                }, "actioner_static_hide_error_engager");

        staticErrorDispose = CrossMessager
                .addListener(new ResponderListener() {
                    @Override
                    public void processMessage(String coordindates) {
                        helper.disposeErrorPopper(false);
                    }
                }, "actioner_static_validation_end");

        staticValidationError = CrossMessager
                .addListener(new ResponderListener() {
                    @Override
                    protected void processMessage(String message) {
                        helper.onValidationError();
                    }
                }, "actioner_static_validation_error");

        keyDownHandler = CrossMessager.addListener(new ResponderListener() {
            @Override
            protected void processMessage(String message) {
                helper.handleKeyDownListener();
            }
        }, "send_focus_on_tip");

        hideWcagPopper = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                helper.hidePopper();
            }
        }, "popper_hide");

        stepCompletion = CrossMessager.addListener(new ResponderListener() {
            @Override
            protected void processMessage(String message) {
                helper.onStepCompletion(message);
            }
        }, "actioner_step_completion");
    }

    @Override
    public void startSearcher() {
        if (helper.supportsSpotlight()) {
            Actioner.hideSpotlightTimer.cancel();
            Actioner.hideSpotlightTimer.schedule(Actioner.SPOTLIGHT_HIDE_INTERVAL);
        }
        super.startSearcher();
    }

    @Override
    protected void onInit(Coordniates coords, String evaluationData,
            ActionerInitData data) {
        if (ContentType.flow.name().equals(action.type())) {
            setStepFindTimeAndCount(data.getStepCount());
        } else {
            setStepFindData(data.getStepFindData());
        }
        if (evaluationData != null) {
            action.branch_element_evaluation(
                    DataUtil.create(evaluationData));
        }
        if (isForCollection()) {
            SmartTipPlayer.setStepFindData(data.getStepFindData());
            // get the maximum z-index from the iFrame to the document top.
            int z_index = ZMaxer.getZIndex(match);
            helper.setIFramesCointainerZIndex(z_index);
            helper.init(coords);
            watchRel(match, new TopReller(parent, this), false);
            CrossMessager.sendMessageToFrames(INFO_ICON_INIT,
                    getState());
            return;
        }
        helper.showPop(coords);
        delay = (Customizer.getRelocatorDelayTime() > 0)
                ? Customizer.getRelocatorDelayTime()
                : (Enterpriser.isRelocatorDelayEnabled() ? 100 : 0);
        
        // Delaying initializing of Relocator, if element is not properly
        // placed with showPop. Only to be used on flickering of popups.
        if (delay > 0) {
            Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {

                @Override
                public boolean execute() {
                    watchRel(match,
                            new TopReller(parent, TopResponder.this), false);
                    return false;
                }
            }, delay);
        } else {
            watchRel(match, new TopReller(parent, this), false);
        }

        if(coords.needScroll()){
            scroll();
        }
        // send step/live event from top in case of iframe
        if (findListeners != null) {
            Element matchedFrame = match;
            for (FindListener listener : findListeners) {
                listener.postFind(action, matchedFrame, getStepFindData());
            }
        }
    }

    @Override
    protected void onMove(Coordniates coords) {
        helper.movePop(coords);
    }

    @Override
    protected void onStaticShow(Coordniates coords) {
        helper.placePop(coords);
    }

    @Override
    protected void onStaticShowError(Coordniates coords) {
        helper.showErrorPopup(coords);
    }

    @Override
    protected void onStaticShowErrorEnagager(Coordniates coords) {
        helper.showErrorEngagers(coords);
    }

    private void onStaticHideError() {
        helper.hideErrorPopup();
    }

    private void onStaticHideErrorEngager() {
        helper.hideErrorEngagers();
    }

    @Override
    public void destroy() {
        super.destroy();

        CrossMessager.removeListener(engShow, "actioner_show_eng");
        CrossMessager.removeListener(engHide, "actioner_hide_eng");
        CrossMessager.removeListener(next, "actioner_next");
        CrossMessager.removeListener(optionalNext,
                "actioner_optional_next");
        CrossMessager.removeListener(miss, "actioner_miss");
        CrossMessager.removeListener(over, "actioner_over");
        CrossMessager.removeListener(hideOnMove, "actioner_hide_on_move");
        CrossMessager.removeListener(staticErrorHide,
                "actioner_static_hide_error");
        CrossMessager.removeListener(staticErrorEngagerHide,
                "actioner_static_hide_error_engager");
        CrossMessager.removeListener(staticErrorDispose,
                "actioner_static_validation_end");
        CrossMessager.removeListener(staticValidationError,
                "actioner_static_validation_error");
        CrossMessager.removeListener(keyDownHandler, "send_focus_on_tip");
        CrossMessager.removeListener(hideWcagPopper, "popper_hide");
        CrossMessager.removeListener(stepCompletion,
                "actioner_step_completion");
    }

    @Override
    public void onOptionalNext() {
        helper.handleOptionalNext();
    }

    @Override
    public void onMiss(JsArray<JavaScriptObjectExt> reason) {
        if (isMissEvent) {
            return;
        }
        isMissEvent = true;
        helper.handleMiss(reason);
    }

    @Override
    protected void watchRelocator() {
        watchRel(match, new TopReller(parent, this), false);
    }
}