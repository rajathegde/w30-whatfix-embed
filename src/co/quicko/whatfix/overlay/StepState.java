package co.quicko.whatfix.overlay;

import co.quicko.whatfix.data.Draft;

import com.google.gwt.core.client.JavaScriptObject;

public class StepState extends JavaScriptObject {
    protected StepState() {
    }

    public final native Draft draft() /*-{
		return this.draft;
    }-*/;

    public final native void draft(Draft draft) /*-{
		this.draft = draft;
    }-*/;

    public final native int step() /*-{
		return this.step;
    }-*/;

    public final native void step(int step) /*-{
		this.step = step;
    }-*/;

    public final native void mode_live_edit(boolean mode_live_edit) /*-{
		this.mode_live_edit = mode_live_edit;
    }-*/;

    public final native boolean mode_live_edit() /*-{
		return this.mode_live_edit ? this.mode_live_edit : false;
    }-*/;
    
    /*
     * used during test case live edit
     */
    public final native void testcase(boolean testcase) /*-{
        this.testcase = testcase;
    }-*/;

    public final native boolean testcase() /*-{
        return this.testcase ? this.testcase : false;
    }-*/;
}
