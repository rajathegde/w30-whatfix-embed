package co.quicko.whatfix.overlay.elementwrap;

import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.overlay.Responder;

public interface WrapObserver {

    public void onInit(Element source);

    public void onAction(Element source);

    public void onNearOver();

    public void onNearOut();

    public void onActivate(Element source);

    public void onDeactivate(Element source);

    public void onEmphasize(Element source);

    public void onFocusOut(Element source);

    public void onDeEmphasize();

    public void onKeyDown(Element source);

    public void onStepCompletion(String action);

    // TODO: Need to re-think of better place to put this method.
    public void moveState();

    public Responder getResponder();

}