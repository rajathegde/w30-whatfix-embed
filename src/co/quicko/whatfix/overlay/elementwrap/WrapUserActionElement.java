package co.quicko.whatfix.overlay.elementwrap;

import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.editor.UserActionCompletionType;
import co.quicko.whatfix.overlay.elementwrap.WrapperFactory.WrapperInfo;

public class WrapUserActionElement extends WrapFlowElement {

    public WrapUserActionElement(List<Integer> type, List<Element> elements,
            WrapObserver listener, WrapScheduler scheduler, WrapperInfo info) {
        super(type, elements, listener, scheduler, info);
        if (type.contains(UserActionCompletionType.ON_PAGE_VIEW.getValue())) {
            Scheduler.get().scheduleDeferred(new ExistsChecker());
        }
    }

    private class ExistsChecker implements ScheduledCommand {

        @Override
        public void execute() {
            listener.onAction(null);
        }
    }
}