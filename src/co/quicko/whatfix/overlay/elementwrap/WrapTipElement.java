package co.quicko.whatfix.overlay.elementwrap;

import java.util.List;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window.Navigator;

import co.quicko.whatfix.common.Browser;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.editor.TipType;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.FocusHandler;
import co.quicko.whatfix.overlay.FocusHandler.FocusListener;
import co.quicko.whatfix.overlay.PreviewEngageEvent;
import co.quicko.whatfix.overlay.elementwrap.WrapperFactory.WrapperInfo;
import co.quicko.whatfix.overlay.elementwrap.mouseoverhandler.OverHandler;
import co.quicko.whatfix.overlay.elementwrap.mouseoverhandler.OverStaticIconWcagHandler;

public class WrapTipElement extends ElementWrap {

    public WrapTipElement(List<Integer> type, List<Element> elements,
            WrapObserver listener, WrapScheduler scheduler, boolean validate,
            WrapperInfo info) {
        super(listener, info.hasShadowPath());
        Element primary = elements.get(0);
        if (type.contains(TipType.STATIC_ACTION.getValue())) {
            overHandler = new OverStaticHandler(this, primary, validate);
        } else if (validate) {
            overHandler = new ValidationHandler(this, primary);
        }

        if (info.isWagEnabled()) {
            if (type.contains(TipType.STATIC_ACTION.getValue())) {
                overHandler = new OverStaticWcagHandler(this, primary,
                        validate);
            } else if (type.contains(TipType.STATIC_INFO.getValue())) {
                overHandler = new OverStaticIconWcagHandler(this, primary);
            }
        }

        handleOverRegistration(elements, info.hasShadowPath());
    }

    private static class ValidationHandler extends OverHandler
            implements FocusListener {
        private Timer hover;
        private Timer unHover;

        private boolean isFocused = false;

        public ValidationHandler(WrapTipElement wrap, Element element) {
            super(wrap, element);
            FocusHandler.addListener(this);
            onInit();
        }

        protected void onInit() {
            if (wrap.listener != null) {
                wrap.listener.onInit(parent);
            }
        }

        @Override
        protected void onMouseMove(PreviewEngageEvent event) {
        }

        protected boolean isNotTarget(Element source) {
            return !isOrHasChild(parent, source);
        }
        
        /**
         * Since Salesforce Lightning has overridden native code for contains
         * function. Hence introduced a custom node equality check added
         * followed by contains function.
         *
         * @param parent
         * @param child
         */
        private static boolean isOrHasChild(Element parent, Element child) {
            return (parent == child) || parent.isOrHasChild(child);
        }

        @Override
        protected void onNearOver() {
            if (unHover != null) {
                unHover.cancel();
                unHover = null;
            }

            if (hover != null) {
                hover.cancel();
            }

            hover = new Timer() {
                @Override
                public void run() {
                    onHover();
                }
            };
            hover.schedule(Actioner.settings().staticShowTime());
        }

        @Override
        protected void onNearOut() {
            if (hover == null || isFocused) {
                return;
            }

            hover.cancel();
            hover = null;

            if (Actioner.settings().staticClose()) {
                hideWidgetsWithoutClose();
                if (!Browser.isMobile()) {
                    return;
                }
            }

            if (unHover != null) {
                unHover.cancel();
            }

            unHover = new Timer() {
                @Override
                public void run() {
                    onUnHover();
                }
            };
            unHover.schedule(Actioner.settings().staticCloseTime());
        }

        protected void onHover() {
            if (wrap.listener == null) {
                return;
            }
            executeOnHover();
        }

        private void onUnHover() {
            if (wrap.listener == null) {
                return;
            }
            executeUnHover();
        }

        @Override
        public void onEngage(NativeEvent nativeEvent) {
            super.onEngage(nativeEvent);
            Event event = nativeEvent.cast();
            if ("focusin".equals(event.getType())) {
                onFocus(event);
            } else if ("focusout".equals(event.getType())) {
                onBlur(event);
            }
        }

        @Override
        public void onFocus(Event event) {
            if (eventOnElement(event, false)) {
                isFocused = true;
                onNearOver();
            }
        }

        @Override
        public void onBlur(Event event) {
            if (eventOnElement(event, false)) {
                isFocused = false;
                onNearOut();
            }
            if (eventOnElement(event, true)) {
                executeOnFocusOut();
            }
        }

        protected void executeOnHover() {
            wrap.listener.onEmphasize(parent);
        }

        protected void executeUnHover() {
            wrap.listener.onDeEmphasize();
        }

        protected void executeOnFocusOut() {
            wrap.listener.onFocusOut(parent);
        }

        private boolean eventOnElement(Event event,
                boolean readEventFromChild) {
            EventTarget target = event.getEventTarget();
            if (!Element.is(target)) {
                return false;
            }

            Element source = Element.as(target);
            return readEventFromChild ? eventFromChild(event, source)
                    : parent == source;
        }

        /**
         * triggers the event if one of the child elements looses focus and
         * focus goes out of children group
         * 
         * @param event
         * @param source
         * @return
         */
        private boolean eventFromChild(Event event, Element source) {
            return isOrHasChild(parent, source) && !isChildFocused(event);
        }

        /**
         * Get the currently focused element 1. If the related target is
         * available for blur event, use it else get the current active element
         * (Done for cross browser compatibility) 2. Check if currently focused
         * element is child of parent
         * 
         * @param blurEvent
         * @return
         */
        private boolean isChildFocused(Event blurEvent) {
            EventTarget target = null;
            try {
                target = blurEvent.getRelatedEventTarget();
            } catch (Exception e) {
                /**
                 * event.getRelatedEventTarget() tends to throw an exception,
                 * occurs on Salesforce Lightning's input elements.
                 */
            }
            if (target != null && !Element.is(target)) {
                return false;
            }
            Element focused = target != null ? Element.as(target)
                    : Common.getFocussedElement();
            return focused != null && isOrHasChild(parent, focused);
        }

        @Override
        public void destroy() {
            FocusHandler.removeListener(this);
        }

        protected void hideWidgetsWithoutClose() {
            if (hover != null) {
                hover.cancel();
                hover = null;
            }
            if (unHover != null) {
                unHover.cancel();
            }
            unHover = new Timer() {
                @Override
                public void run() {
                    wrap.listener.onDeEmphasize();
                }
            };
            unHover.schedule(Actioner.settings().staticCloseTime());
        }
    }

    private static class OverStaticWcagHandler extends OverStaticHandler {
        public OverStaticWcagHandler(WrapTipElement wrap, Element element,
                boolean validate) {
            super(wrap, element, validate);
        }

        @Override
        protected void onKeyDown(int keyCode) {
            if (wrap.listener == null) {
                return;
            }
            if (keyCode == KeyCodes.KEY_TAB
                    || (keyCode == KeyCodes.KEY_RIGHT
                            && Navigator.getPlatform().startsWith("Mac"))
                    || (keyCode == KeyCodes.KEY_DOWN
                            && Navigator.getPlatform().startsWith("Win"))) {
                wrap.listener.onKeyDown(parent);
            }
        }
    }

    private static class OverStaticHandler extends ValidationHandler {

        private boolean validate;

        public OverStaticHandler(WrapTipElement wrap, Element element,
                boolean validate) {
            super(wrap, element);
            this.validate = validate;
            onInit();
        }

        @Override
        protected void executeOnHover() {
            wrap.listener.onActivate(parent);
            if (this.validate) {
                super.executeOnHover();
            }
        }

        @Override
        protected void executeUnHover() {
            wrap.listener.onDeactivate(parent);
            if (this.validate) {
                super.executeUnHover();
            }
        }

        @Override
        protected void executeOnFocusOut() {
            if (this.validate) {
                super.executeOnFocusOut();
            }
        }

        @Override
        protected void hideWidgetsWithoutClose() {
            if (this.validate) {
                super.hideWidgetsWithoutClose();
            }
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * co.quicko.whatfix.overlay.actioner.ElementWrap.ValidationHandler#
         * onInit() This method will be invoked two times during instance
         * creation. This is because this.validate will not be set when this is
         * called first time
         */
        @Override
        protected void onInit() {
            if (this.validate) {
                super.onInit();
            }
        }

    }

}