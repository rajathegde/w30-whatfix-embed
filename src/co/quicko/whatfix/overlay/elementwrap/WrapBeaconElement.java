package co.quicko.whatfix.overlay.elementwrap;

import java.util.List;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;

import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.ShadowDOM;
import co.quicko.whatfix.overlay.elementwrap.WrapperFactory.WrapperInfo;
import co.quicko.whatfix.overlay.elementwrap.actionhandler.ActionHandler;
import co.quicko.whatfix.overlay.elementwrap.mouseoverhandler.OverStaticIconWcagHandler;

public class WrapBeaconElement extends ElementWrap {
    
    private ActionHandler handler;
    private HandlerRegistration actionRegn;
    private ShadowDOM actionRegnForShadow;

    public WrapBeaconElement(List<Integer> type, List<Element> elements,
            WrapObserver listener, WrapScheduler scheduler, WrapperInfo info) {
        super(listener, info.hasShadowPath());
        Element primary = elements.get(0);
        if (type.contains(StepCompletionActions.ON_CLICK.getValue())) {
            handler = new ActionHandler(this, Customizer.getClickEvent(),
                    elements, null);
        }

        if (handler != null) {
            if (info.hasShadowPath()) {
                actionRegnForShadow = new ShadowDOM(handler, elements);
            } else {
                actionRegn = Event.addNativePreviewHandler(handler);
            }
        }
        setEngagerHandler(primary);
        if (info.isWagEnabled()) {
            overHandler = new OverStaticIconWcagHandler(this, primary);
        }
        handleOverRegistration(elements, info.hasShadowPath());
    }

    public void dispose() {
        if (actionRegn != null) {
            handler.destroy();
            actionRegn.removeHandler();
            actionRegn = null;
        }

        if (actionRegnForShadow != null) {
            handler.destroy();
            actionRegnForShadow.destroy();
            actionRegnForShadow = null;
        }
        super.dispose();
    }

}