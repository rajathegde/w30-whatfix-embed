package co.quicko.whatfix.overlay.elementwrap;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.Arrays;
import java.util.List;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;

import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.KeyboardEvents;
import co.quicko.whatfix.overlay.ShadowDOM;
import co.quicko.whatfix.overlay.elementwrap.WrapperFactory.WrapperInfo;
import co.quicko.whatfix.overlay.elementwrap.actionhandler.ActionHandler;
import co.quicko.whatfix.overlay.elementwrap.actionhandler.AutoClickHandler;
import co.quicko.whatfix.overlay.elementwrap.actionhandler.ClickHandler;
import co.quicko.whatfix.overlay.elementwrap.actionhandler.RightClickActionHandler;
import co.quicko.whatfix.overlay.elementwrap.actionhandler.SelectionChangeHandler;
import co.quicko.whatfix.overlay.elementwrap.actionhandler.TextHandler;

public class WrapFlowElement extends ElementWrap {

    private HandlerRegistration actionRegn;
    private ShadowDOM actionRegnForShadow;
    private ActionHandler handler;
    
    private static Element element;
    private static List<Integer> actions;

    public WrapFlowElement(List<Integer> type, List<Element> elements,
            WrapObserver listener, WrapScheduler scheduler, WrapperInfo info) {
        super(listener, info.hasShadowPath());
        Element primary = elements.get(0);
        setShownElementData(type, primary);
        getLogger().log(WfxLogLevel.DEBUG, WrapFlowElement.class,
                "WrapFlowElement", WfxModules.JIT,
                "hasShadowPath: {0}, type: {1}, is wcagEnabled: {2}",
                info.hasShadowPath(), type, info.isWagEnabled());
        Integer keyBoardCode = getKeyBoardShortCode(type);
        
        if (type.contains(StepCompletionActions.ON_CLICK.getValue())) {
            handler = initializeClickHandler(elements, info, keyBoardCode);
        } else if (type.contains(StepCompletionActions.ON_HOVER.getValue())) {
            handler = new ActionHandler(this, "mouseover", elements,
                    StepCompletionActions.ON_HOVER.getAction(),
                    keyBoardCode);
        } else if (type
                .contains(StepCompletionActions.ON_TYPING_TEXT.getValue())) {
            handler = new TextHandler(this, primary, scheduler,
                    StepCompletionActions.ON_TYPING_TEXT.getAction(),
                    keyBoardCode, info);
        } else if (type.contains(
                StepCompletionActions.ON_SELECTION_CHANGE.getValue())) {
            handler = new SelectionChangeHandler(this, primary,
                    Customizer.getClickEvent(), scheduler,
                    StepCompletionActions.ON_SELECTION_CHANGE.getAction(),
                    info, keyBoardCode);
        } else if (type.contains(
                StepCompletionActions.ON_MOUSE_RIGHT_CLICK.getValue())) {
            handler = new RightClickActionHandler(this, "contextmenu", elements,
                    StepCompletionActions.ON_MOUSE_RIGHT_CLICK.getAction(),
                    keyBoardCode);
        } else if (keyBoardCode != null) {
            handler = new ActionHandler(this, null, elements,
                    StepCompletionActions
                            .get(KeyboardEvents.getKeyCode(keyBoardCode))
                            .getAction(),
                    keyBoardCode);
        } else {
            // Do nothing..Added for Sonar Issue fix
        }

        if (handler != null) {
            getLogger().log(WfxLogLevel.DEBUG, WrapFlowElement.class,
                    handler.getClass().getSimpleName(), WfxModules.JIT,
                    "EventType: {0}", type);
            if (info.hasShadowPath()) {
                actionRegnForShadow = new ShadowDOM(handler, elements,
                        keyBoardCode);
            } else {
                actionRegn = Event.addNativePreviewHandler(handler);
            }
        }
        if (null != info.getContentType()
                && info.getContentType().equals("flow")) {
            setEngagerHandler(primary);
        }
        handleOverRegistration(elements, info.hasShadowPath());
    }

    private ActionHandler initializeClickHandler(List<Element> elements,
            WrapperInfo info, Integer keyBoardCode) {
        return (info.isAutoExecutableStep() || info.isRecaptureModeEnabled())
                ? new AutoClickHandler(this, Customizer.getActionEvents(),
                        elements, StepCompletionActions.ON_CLICK.getAction(),
                        keyBoardCode)
                : new ClickHandler(this, Customizer.getActionEvents(), elements,
                        StepCompletionActions.ON_CLICK.getAction(),
                        keyBoardCode);
    }

    public void dispose() {
        if (actionRegn != null) {
            handler.destroy();
            actionRegn.removeHandler();
            actionRegn = null;
        }

        if (actionRegnForShadow != null) {
            handler.destroy();
            actionRegnForShadow.destroy();
            actionRegnForShadow = null;
        }
        super.dispose();
        setShownElementData(null,null);
    }

    /*
     * This method will return the native Event Code for the Selected KeyBoard
     * Event Selected
     */

    private Integer getKeyBoardShortCode(List<Integer> type) {
        return Arrays.stream(KeyboardEvents.values())
                .filter(e -> type.contains(e.getKeyCode())).findFirst()
                .map(KeyboardEvents::getKeyNativeCode)
                .orElse(null);

    }
    
    /*
     * This method is to expose the element on which tooltip 
     * is show, for usage during auto recording
     */
    private static void setShownElementData(List<Integer> type, Element currentElement) {
        actions = type;
        element = currentElement;
    }
    
    public static Element getShownElement() {
        return element;
    }
    
    public static List<Integer> getActions() {
        return actions;
    }
}