package co.quicko.whatfix.overlay.elementwrap;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.List;
import java.util.Map;

import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.AutoExecutionConfig;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap.WrapScheduler;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.alg.FinderUtil;

/**
 * All the observations/actions on element is handled by this class.
 * 
 * @author gauravsankhla
 */
public class WrapperFactory {
    
    public static final String MANUAL_EXECUTION_MODE = "manual_execution_mode";
    public static final String RECAPTURE_MODE = "recapture_mode";

    private WrapperFactory() {
    }

    public static ElementWrap getElementWrapper(List<Integer> type,
            List<Element> elements, WrapObserver listener,
            WrapScheduler scheduler, boolean validate, Step step,
            Map<String,Boolean> runModes) {      
        WrapperInfo info = new WrapperInfo(step,
                runModes.get(MANUAL_EXECUTION_MODE),
                runModes.get(RECAPTURE_MODE));
        String contentType = info.getContentType();
        getLogger().log(WfxLogLevel.DEBUG, WrapperFactory.class,
                "getElementWrapper", WfxModules.JIT,
                "ContentType: {0}, Step completion type: {1}, hasShadowPath: {2}, isWcagEnabled: {3}",
                contentType, type, info.hasShadowPath(), info.isWagEnabled());
        if (contentType.equals(ContentType.flow.name())
                || contentType.equals(ContentType.testcase.name())) {
            return new WrapFlowElement(type, elements, listener, scheduler,
                    info);
        } else if (contentType.equals(ContentType.beacon.name())) {
            return new WrapBeaconElement(type, elements, listener, scheduler,
                    info);
        } else if (contentType
                .equals(ContentType.USER_ACTION.toString())) {
            return new WrapUserActionElement(type, elements, listener,
                    scheduler, info);
        } else if (contentType.equals(ContentType.assistant.name())) {
            /*
             * Currently Element wrap is not needed. In future, with requirement
             * create new WrapAssistantElement accordingly.
             */
            return null;
        } else {
            return new WrapTipElement(type, elements, listener, scheduler,
                    validate, info);
        }
    }

    public static class WrapperInfo {
        private Step step;
        private boolean switchedToManual;
        private boolean recaptureMode;

        private WrapperInfo(Step step, boolean switchedToManual, boolean recaptureMode) {
            this.step = step;
            this.switchedToManual = switchedToManual;
            this.recaptureMode = recaptureMode;
        }

        public boolean hasShadowPath() {
            return FinderUtil.hasShadowPath(step.marks());
        }

        public boolean isWagEnabled() {
            return Enterpriser.hasFeature(AdditionalFeatures.wcag_enable);
        }

        public String getContentType() {
            return step.type();
        }

        public boolean isAutoExecutableStep() {
            return AutoExecutionConfig.isAutoExecutableStep(step)
                    && !isSwitchedToManual();
        }

        public String input() {
            return Common.replaceCustomContent(step.input(),
                    Customizer.getCustomData());
        }

        public boolean isSwitchedToManual() {
            return this.switchedToManual;
        }

        public boolean isRecaptureModeEnabled() {
            return this.recaptureMode;
        }
        
    }
}
