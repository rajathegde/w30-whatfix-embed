package co.quicko.whatfix.overlay.elementwrap.mouseoverhandler;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;

import co.quicko.whatfix.overlay.EngageEventHandler;
import co.quicko.whatfix.overlay.PreviewEngageEvent;
import co.quicko.whatfix.overlay.PreviewNativeEvent;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;

public class OverHandler implements NativePreviewHandler, EngageEventHandler {
    private static int SAFE_GAP = 50;

    protected ElementWrap wrap;
    protected Element parent;

    private int startX;
    private int startY;
    private int endX;
    private int endY;

    private boolean over = true;

    public OverHandler(ElementWrap wrap, Element element) {
        this.wrap = wrap;
        this.parent = ElementWrap.REACHER.reachValid(element);

        // hide engagers early on.
        startX = -SAFE_GAP;
        startY = -SAFE_GAP;
        endX = parent.getOffsetWidth() + SAFE_GAP;
        endY = parent.getOffsetHeight() + SAFE_GAP;
    }

    private void onPreviewEvent(PreviewEngageEvent event) {
        Element source = wrap.getTargetElement(event);
        if (source == null) {
            return;
        }

        // Sub menus loose focus when mouse is shifted to engagers. So,
        // removing engagers early on.
        String type = event.getType();
        if ("mousemove".equals(type)) {
            onMouseMove(event);
        }

        if (isNotTarget(source)) {
            return;
        }

        if ("mouseover".equals(type)) {
            onNearOver();
        } else if ("mouseout".equals(type)) {
            onNearOut();
        } else if ("keydown".equals(type)) {
            onKeyDown(event.getKeyCode());
        }
    }

    @Override
    public void onPreviewNativeEvent(NativePreviewEvent event) {
        onPreviewEvent(new PreviewNativeEvent(event));
    }

    protected void onMouseMove(PreviewEngageEvent event) {
        if (nearer(event.getNativeEvent())) {
            onNearOver();
        } else {
            onNearOut();
        }
    }

    protected void onNearOut() {
        if (!over || wrap.listener == null) {
            return;
        }
        wrap.listener.onNearOut();
        over = false;
    }

    protected void onNearOver() {
        if (over || wrap.listener == null) {
            return;
        }
        wrap.listener.onNearOver();
        over = true;
    }

    protected void onKeyDown(int keyCode) {
    }

    private boolean nearer(NativeEvent event) {
        int relX = getRelativeX(parent, event);
        int relY = getRelativeY(parent, event);
        return relX > startX && relX < endX && relY > startY && relY < endY;
    }

    private int getRelativeX(Element target, NativeEvent e) {
        return e.getClientX() - target.getAbsoluteLeft()
                + target.getScrollLeft()
                + target.getOwnerDocument().getScrollLeft();
    }

    private int getRelativeY(Element target, NativeEvent e) {
        return e.getClientY() - target.getAbsoluteTop() + target.getScrollTop()
                + target.getOwnerDocument().getScrollTop();
    }

    public void destroy() {
    }

    protected boolean isNotTarget(Element source) {
        return source != parent;
    }

    @Override
    public void onEngage(NativeEvent event) {
        onPreviewEvent(new PreviewEngageEvent(event));
    }
}