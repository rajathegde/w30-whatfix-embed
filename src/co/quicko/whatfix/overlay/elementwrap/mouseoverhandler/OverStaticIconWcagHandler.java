package co.quicko.whatfix.overlay.elementwrap.mouseoverhandler;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Window.Navigator;

import co.quicko.whatfix.overlay.elementwrap.ElementWrap;

public class OverStaticIconWcagHandler extends OverHandler {

    public OverStaticIconWcagHandler(ElementWrap wrap, Element element) {
        super(wrap, element);
    }

    @Override
    protected void onKeyDown(int keyCode) {
        if (wrap.listener == null) {
            return;
        }
        if (keyCode == KeyCodes.KEY_TAB
                || (keyCode == KeyCodes.KEY_RIGHT
                        && Navigator.getPlatform().startsWith("Mac"))
                || (keyCode == KeyCodes.KEY_DOWN
                        && Navigator.getPlatform().startsWith("Win"))) {
            wrap.listener.onKeyDown(parent);
        }
    }
}