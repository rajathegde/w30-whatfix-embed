package co.quicko.whatfix.overlay.elementwrap.actionhandler;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.ArrayList;
import java.util.List;

import com.gargoylesoftware.htmlunit.javascript.host.event.MouseEvent;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;

import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.overlay.EngageEventHandler;
import co.quicko.whatfix.overlay.KeyboardEvents;
import co.quicko.whatfix.overlay.PreviewEngageEvent;
import co.quicko.whatfix.overlay.PreviewNativeEvent;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;

public class ActionHandler implements NativePreviewHandler, EngageEventHandler {
    protected ElementWrap wrap;
    private String actionName;
    protected String type;
    protected List<Element> parents;
    protected Integer keyBoardNativeCode;

    public ActionHandler(ElementWrap wrap, String type, List<Element> elements,
            String actionName) {
        this(wrap, type, elements, actionName, null);
    }

    public ActionHandler(ElementWrap wrap, String type, List<Element> elements,
            String actionName, Integer keyBoardNativeCode) {
        this.wrap = wrap;
        this.type = type;
        this.actionName = actionName;
        parents = new ArrayList<Element>(elements.size());
        for (int index = 0; index < elements.size(); index++) {
            parents.add(ElementWrap.REACHER.reachValid(elements.get(index)));
        }
        this.keyBoardNativeCode = keyBoardNativeCode;
    }

    public void onPreviewEvent(PreviewEngageEvent event) {
        if ((!event.getType().equals(type)) && keyBoardNativeCode == null) {
            return;
        }
        verifyActionElement(event);
    }

    protected void verifyActionElement(PreviewEngageEvent event) {
        Element source = wrap.getTargetElement(event);
        if (source == null) {
            return;
        }
        if (event.getType().equals(type)) {
            for (Element parent : parents) {
                tryAction(source, parent);
            }
        } else if (null != event.getNativeEvent()) {
            verifyKeyboardActionEvent(event.getNativeEvent().getKeyCode(),
                    source);
        } else {
            // Do nothing..Added for Sonar Issue fix
        }

    }

    protected void verifyKeyboardActionEvent(int keyCode, Element source) {
        if (keyCode == keyBoardNativeCode.intValue()) {
            if (type != null) {
                // Update Action since it is sent in Analytics Event.
                this.actionName = StepCompletionActions
                        .get(KeyboardEvents.getKeyCode(keyBoardNativeCode))
                        .getAction();
            }
            doAction(source);
        }
    }

    @Override
    public void onPreviewNativeEvent(NativePreviewEvent event) {
        onPreviewEvent(new PreviewNativeEvent(event));
    }

    protected void doAction(Element source) {
        // Scheduler on text may still call this when tip moves away to
        // another element. Ensuring that we are taking action only before
        // dispose.
        if (wrap.listener == null) {
            return;
        }
        getLogger().log(WfxLogLevel.DEBUG, ActionHandler.class, "doAction",
                WfxModules.JIT, "Step is completed with Action : {0}",
                actionName);
        if (StringUtils.isNotBlank(actionName)) {
            wrap.listener.onStepCompletion(actionName);
        }
        wrap.listener.onAction(source);
        wrap.dispose();
    }

    protected void tryAction(Element source, Element parent) {
        Element actualSource = source;
        while (source != null) {
            if (source == parent) {
                doAction(actualSource);
                return;
            }
            source = DOMUtil.getPsuedoParent(source);
        }
        addSourceMismatchLog();
    }

    protected void addSourceMismatchLog() {
        if (!type.equals(MouseEvent.TYPE_MOUSE_OVER)) {
            getLogger().log(WfxLogLevel.DEBUG, ActionHandler.class, "tryAction",
                    WfxModules.JIT,
                    "Step is not completed as Source is not same as Defined");
        }
    }

    @Override
    public void onEngage(NativeEvent event) {
        onPreviewEvent(new PreviewEngageEvent(event));
    }

    public void destroy() {
    }
}