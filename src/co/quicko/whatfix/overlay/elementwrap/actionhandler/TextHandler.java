package co.quicko.whatfix.overlay.elementwrap.actionhandler;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.Collections;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.NodeList;


import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap.WrapScheduler;
import co.quicko.whatfix.overlay.elementwrap.WrapperFactory.WrapperInfo;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.alg.FinderUtil;

public class TextHandler extends ActionHandler implements RepeatingCommand {
    private InputElement text;
    private boolean updated = false;
    private int idleTimes = 0;
    private String defaultValue;
    private WrapperInfo info;
    private int sameCheck = 5;

    public TextHandler(ElementWrap wrap, Element element,
            WrapScheduler scheduler, String actionName,
            Integer keyBoardNativeCode, WrapperInfo info) {
        super(wrap, "keydown", Collections.singletonList(element), actionName,
                keyBoardNativeCode);
        this.info = info;
        scheduler.scheduleFixedDelay(this, 500);
        if (Finder.IsText(element)) {
            getLogger().log(WfxLogLevel.DEBUG, TextHandler.class, "TextHandler",
                    WfxModules.JIT,
                    "Element is input/textarea/contenteditable div");
             initializeTextElement(element);
        } else {
            getLogger().log(WfxLogLevel.DEBUG, TextHandler.class, "TextHandler",
                    WfxModules.JIT,
                    "Element is not input/textarea/contenteditable div");
            Element innerText = getInnerInputElement(element);
            /*
             * If innerText is same as element it means we didn’t find any
             * input/textarea/contenteditable div. So we don’t initialize
             * listener
             */
            if (innerText != element) {
                 initializeTextElement(innerText);
            }
        }
    }
    
    private void  initializeTextElement(Element element) {
        text = (InputElement) element;
        if (text != null && text.getValue() != null) {
            defaultValue = text.getValue();
        }
        if (hasValue()) {
            idleTimes = 5;
        }
    }
    
    /*
     * This method returns the inner textable element:[input/textarea/content
     * editable div] inside the outer element if it is present otherwise returns
     * the outer element itself.
     */
    private Element getInnerInputElement(Element element) {
        Element ele = FinderUtil.getInnerInputElement(element)
                .orElse(element);
        if (ele == element) {
            NodeList<Element> divElements = DOMUtil
                    .getElementsByQuerySelector(element, DivElement.TAG);
            for (int i = 0; i < divElements.getLength(); i++) {
                Element divElement = divElements.getItem(i);
                if (OverlayUtil.isVisible(divElement)
                        && Finder.isContentEditable(divElement)) {
                    return divElement;
                }
            }
        }
        return ele;
    }

    @Override
    protected void tryAction(Element source, Element parent) {
        if (source == parent) {
            updated = true;
            idleTimes = 1;
            Scheduler.get().scheduleDeferred(new TextChecker());
        }
    }

    @Override
    public boolean execute() {
        if (!updated && hasValue() && info.isAutoExecutableStep()) {
            boolean valueUpdated = !text.getValue().equals(defaultValue);
            if (!valueUpdated) {
                sameCheck--;
                if (sameCheck <= 0) {
                    updated = true;
                }
            } else {
                updated = true;
            }
        } else if (!updated && hasValue()) {
            updated = !text.getValue().equals(defaultValue);
        } else if (!updated) {
            return true;
        } else if (idleTimes <= 0) {
            doAction(text);
            return false;
        } else {
            idleTimes--;
        }
        return true;
    }

    private boolean hasValue() {
        if (text == null) {
            return false;
        } else if (text.getValue() != null) {
            String value = text.getValue();
            return value != null && value.length() != 0;
        } else {
            String elementValue = text.getInnerText();
            return elementValue != null;
        }
    }

    private class TextChecker implements ScheduledCommand {
        @Override
        public void execute() {
            if (!hasValue()) {
                idleTimes = 5;
            }
        }
    }
}