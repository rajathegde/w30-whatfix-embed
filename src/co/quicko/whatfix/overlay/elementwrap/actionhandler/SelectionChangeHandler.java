package co.quicko.whatfix.overlay.elementwrap.actionhandler;

import java.util.Collections;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.user.client.Event;

import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.overlay.ChangeHandler;
import co.quicko.whatfix.overlay.ChangeHandler.ChangeListener;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap.WrapScheduler;
import co.quicko.whatfix.overlay.elementwrap.WrapperFactory.WrapperInfo;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.data.strategy.ElementSelectorStrategy;

public class SelectionChangeHandler extends ActionHandler
        implements ChangeListener {

    private NodeList<Element> inputElements = null;
    private WrapperInfo stepInfo;
    
    public SelectionChangeHandler(ElementWrap wrap, Element element,
            String type, WrapScheduler scheduler, String actionName,
            WrapperInfo info, Integer keyBoardNativeCode) {
        super(wrap, type, Collections.singletonList(element), actionName,
                keyBoardNativeCode);
        this.stepInfo = info;
        this.inputElements = DOMUtil.getElementsByQuerySelector(element, "input");
        if (isDefaultElement(element)) {
            ChangeHandler.addListener(this);
        } else {
            scheduler.scheduleFixedDelay(new TextChangeHandler(element),
                    500);
        }
    }

    @Override
    public void onChange(Event event) {

        EventTarget target = event.getEventTarget();
        if (!Element.is(target)) {
            return;
        }

        Element asElement = Element.as(target);

        for (Element parent : parents) {
            if (parent.isOrHasChild(asElement)) {
                doAction(asElement);
                ChangeHandler.removeListener(this);
                break;
            }
        }
    }

    private boolean isDefaultElement(Element element) {
        if (isOrHasSelect(element)) {
            return true;
        }
        JsArray<Element> elements = ElementSelectorStrategy
                .findByCssSelector(element, "input[type='radio']");
        if ((null != elements && elements.length() != 0)) {
            return true;
        }
        return false;
    }

    private boolean isOrHasSelect(Element element) {
        JsArray<Element> elements = ElementSelectorStrategy
                .findByCssSelector(element, "select");
        if (elements != null && elements.length() != 0) {
            return true;
        }
        return Finder.isSelect(element);
    }

    @Override
    protected void tryAction(Element source, Element parent) {
    }

    @Override
    public void destroy() {
        ChangeHandler.removeListener(this);
    }

    private class TextChangeHandler implements RepeatingCommand {

        private Element actualElement;
        private String selectedValue;
        private boolean preSelected;

        public TextChangeHandler(Element element) {
            actualElement = element;
            selectedValue = getValue();
            if (StringUtils.isNotBlank(selectedValue)) {
                preSelected = true;
            } else {
                preSelected = false;
            }
        }

        @Override
        public boolean execute() {
            if (isValueChanged()) {
                doAction(actualElement);
                return false;
            }
            return true;
        }

        private boolean isValueChanged() {
            String newValue = getValue();
            if (StringUtils.isNotBlank(newValue)) {
                if (stepInfo.isAutoExecutableStep()) {
                    if (newValue.equals(stepInfo.input())) {
                        return true;
                    }
                } else {
                    if (preSelected && (selectedValue.equals(newValue))) {
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }

        private String getValue() {
            StringBuilder value = new StringBuilder();
            if (null != inputElements && inputElements.getLength() != 0) {
                for (int index = 0; index < inputElements.getLength(); index++) {
                    value.append(getElementValue(inputElements.getItem(index)));
                }
            }
            String elementValue = stepInfo.isAutoExecutableStep()
                    ? AppController.getInnerText(actualElement)
                    : actualElement.getInnerText();
            if (StringUtils.isBlank(elementValue)) {
                elementValue = getElementValue(actualElement);
            }
            if (StringUtils.isNotBlank(elementValue)) {
                value.append(elementValue);
            }
            return value.toString();
        }
    }

    private static native String getElementValue(Element element)/*-{
        return element.value;
    }-*/;
}