package co.quicko.whatfix.overlay.elementwrap.actionhandler;

import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.overlay.PreviewEngageEvent;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;

public class ClickHandler extends ActionHandler {

    private ElementWrap wrap;
    protected Set<String> eventTypes;

    public ClickHandler(ElementWrap wrap,
            JsArrayString types,
            List<Element> elements, String actionName,
            Integer keyBoardNativeCode) {
        super(wrap, actionName, elements, actionName, keyBoardNativeCode);
        this.wrap = wrap;
        this.eventTypes = JsUtils.toSet(types);
    }

    @Override
    protected void tryAction(Element source, Element parent) {
        Element actualSource = source;
        while (source != null) {
            if (source == parent) {
                if (null != wrap.listener) {
                    wrap.listener.moveState();
                }
                doAction(actualSource);
                return;
            }
            source = DOMUtil.getPsuedoParent(source);
        }
        addSourceMismatchLog();
    }

    @Override
    public void onPreviewEvent(PreviewEngageEvent event) {
        if ((!eventTypes.contains(event.getType()))
                && keyBoardNativeCode == null) {
            return;
        }
        verifyActionElement(event);

    }

    @Override
    public void verifyActionElement(PreviewEngageEvent event) {
        Element source = wrap.getTargetElement(event);
        if (source == null) {
            return;
        }
        if (eventTypes.contains(event.getType())) {
            for (Element parent : parents) {
                tryAction(source, parent);
            }
        } else if (null != event.getNativeEvent()) {
            verifyKeyboardActionEvent(event.getNativeEvent().getKeyCode(),
                    source);
        } else {
            // Do nothing..Added for Sonar Issue fix
        }

    }

}
