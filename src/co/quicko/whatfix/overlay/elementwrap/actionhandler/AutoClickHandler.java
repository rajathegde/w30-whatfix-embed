package co.quicko.whatfix.overlay.elementwrap.actionhandler;

import java.util.List;

import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.editor.AutoExecutionActions;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;
import co.quicko.whatfix.workflowengine.AppController;

/**
 * This class is used to listen to step completion actions ONLY for AUTO_CLICK
 * for autoexec flow.
 * 
 * @author malhar
 *
 */
public class AutoClickHandler extends ClickHandler {

    public AutoClickHandler(ElementWrap wrap, JsArrayString types,
            List<Element> elements, String actionName,
            Integer keyBoardNativeCode) {
        super(wrap, types, elements, actionName, keyBoardNativeCode);
        this.eventTypes = AppController
                .getAutoExecEvents(AutoExecutionActions.ON_CLICK, eventTypes);
    }

}
