package co.quicko.whatfix.overlay.elementwrap.actionhandler;

import java.util.List;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;

import co.quicko.whatfix.overlay.PreviewEngageEvent;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;

public class RightClickActionHandler extends ActionHandler {

    public RightClickActionHandler(ElementWrap wrap, String type,
            List<Element> elements, String actionName,
            Integer keyBoardNativeCode) {
        super(wrap, type, elements, actionName, keyBoardNativeCode);
    }

    @Override
    public void onPreviewEvent(PreviewEngageEvent event) {
        if (null == event.getNativeEvent() || event.getNativeEvent()
                .getButton() != NativeEvent.BUTTON_RIGHT) {
            return;
        }
        verifyActionElement(event);
    }

    @Override
    public void verifyActionElement(PreviewEngageEvent event) {
        Element source = wrap.getTargetElement(event);
        if (source == null) {
            return;
        }
        for (Element parent : parents) {
            tryAction(source, parent);
        }

    }

}
