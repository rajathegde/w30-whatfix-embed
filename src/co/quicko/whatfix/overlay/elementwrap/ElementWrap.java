package co.quicko.whatfix.overlay.elementwrap;

import java.util.List;

import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.ButtonElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;

import co.quicko.whatfix.common.Browser;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.PreviewEngageEvent;
import co.quicko.whatfix.overlay.ShadowDOM;
import co.quicko.whatfix.overlay.elementwrap.mouseoverhandler.OverHandler;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;

public abstract class ElementWrap {
    public static ValidElementReacher REACHER = GWT
            .create(ValidElementReacher.class);

    // at the element.
    protected HandlerRegistration overRegn;
    protected OverHandler overHandler;
    protected ShadowDOM overRegnForShadow;

    public WrapObserver listener;

    public ElementWrap(WrapObserver listener,
            boolean hasShadowPath) {
        this.listener = listener;
    }

    protected void setEngagerHandler(Element element) {
        if (Overlay.CSS.mark_strength() != 0) {
            if (!Browser.isMobileIosDevice()) {
                /*
                 * Do not perform engager engage and disengage as it causes
                 * double click issue on IOS mobile devices.
                 */
                overHandler = new OverHandler(this, element);
            }
        }
    }

    protected void handleOverRegistration(List<Element> elements,
            boolean hasShadowPath) {
        if (overHandler != null) {
            if (hasShadowPath) {
                overRegnForShadow = new ShadowDOM(overHandler, elements);
            } else {
                overRegn = Event.addNativePreviewHandler(overHandler);
            }
        }
    }

    public void dispose() {
        if (overRegnForShadow != null) {
            overHandler.destroy();
            overRegnForShadow.destroy();
            overRegnForShadow = null;
        }

        if (overRegn != null) {
            overHandler.destroy();
            overRegn.removeHandler();
            overRegn = null;
        }

        listener = null;
    }

    public Element getTargetElement(PreviewEngageEvent event) {
        Element source = AppController.getEngagerElement(event);
        if (Customizer.engagerElementFromPath()) {
            // Override srcElement with composed path if the flag
            // engager_element_from_path is set to true
            EventTarget targetElement = DOMUtil
                    .getComposedPathTarget(event.getNativeEvent());
            source = targetElement != null ? event.getTarget(targetElement)
                    : null;
        }
        if (source == null) {
            source = event.getTargetElement();
        }
        return source;
    }

    public static interface WrapScheduler {
        public void scheduleFixedDelay(RepeatingCommand cmd, int delayMs);
    }

    public static class ValidElementReacher {
        public Element reachValid(Element element) {
            return element;
        }
    }

    /**
     * Events for Spans under Button are not triggered by firefox. But Chrome
     * does not have this restriction. As per W3C, it is not valid to have
     * interactive components under Button.
     * 
     * http://forum.jquery.com/topic/jquery-
     * event-bubbling-on-button-not-working-as-expected-in-firefox
     */
    public static class W3CValidElementReacher extends ValidElementReacher {
        @Override
        public Element reachValid(Element element) {
            Element parent = element.getParentElement();
            int tries = 5;
            while (parent != null && tries != 0) {
                if (ButtonElement.TAG.equalsIgnoreCase(parent.getTagName())) {
                    return parent;
                }
                parent = parent.getParentElement();
                tries--;
            }
            return element;
        }
    }
}