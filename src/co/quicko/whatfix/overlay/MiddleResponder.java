package co.quicko.whatfix.overlay;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.StringUtils;
import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;
import co.quicko.whatfix.data.ActionerInitData;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.overlay.Actioner.MiddleReller;
import co.quicko.whatfix.overlay.Actioner.PartState;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;
import co.quicko.whatfix.overlay.actioner.ActionerPopover;
import co.quicko.whatfix.workflowengine.FindListener;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.data.finder.FinderLogger;

public class MiddleResponder extends AbstractResponder {
    protected int parent;

    private CrossListener init;
    private CrossListener hide;
    private CrossListener move;
    private CrossListener scroll;
    private CrossListener staticShow;
    private CrossListener staticErrorShow;
    private CrossListener infoIconInit;
    private CrossListener staticErrorEngagerShow;
    private CrossListener found;

    protected Element match;
    private boolean isIframeFound = false;

    private AsyncCallback<Boolean> laterCallback;

    protected static final String INFO_ICON_INIT = "info_icon_init";
    public static final String IFRAME_FOUND = "iframe_found";
    protected static final String ACTIONER_INIT = "actioner_init";
    private static final String ACTIONER_HIDE = "actioner_hide";

    public MiddleResponder(final Draft draft, final int step, int parent,
            FindListener... listeners) {
        super(draft, step, listeners);
        this.parent = parent;

        hide = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                Actioner.hide(draft, step);
            }
        }, ACTIONER_HIDE);

        move = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String coordinates) {
                Coordniates coords = DataUtil.create(coordinates);
                coords.relative(match);
                onMove(coords);
            }
        }, "actioner_move");

        scroll = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String message) {
                scroll();
            }
        }, "actioner_scroll");

        staticShow = CrossMessager.addListener(new ResponderListener() {
            @Override
            public void processMessage(String coordindates) {
                Coordniates coords = DataUtil.create(coordindates);
                coords.relative(match);
                onStaticShow(coords);
            }
        }, "actioner_static_show");

        staticErrorShow = CrossMessager
                .addListener(new ResponderListener() {
                    @Override
                    public void processMessage(String coordindates) {
                        Coordniates coords = DataUtil.create(coordindates);
                        coords.relative(match);
                        onStaticShowError(coords);
                    }
                }, "actioner_static_show_error");

        staticErrorEngagerShow = CrossMessager
                .addListener(new ResponderListener() {
                    @Override
                    public void processMessage(String coordindates) {
                        Coordniates coords = DataUtil.create(coordindates);
                        coords.relative(match);
                        onStaticShowErrorEnagager(coords);
                    }
                }, "actioner_static_show_error_engager");

        infoIconInit = CrossMessager.addListener(new ResponderListener() {
            @Override
            protected void processMessage(String message) {
                onTipInitialized();
            }
        }, INFO_ICON_INIT);

        found = CrossMessager.addListener(new ResponderListener() {
            @Override
            protected void processMessage(String message) {
                isIframeFound = true;
            }
        }, IFRAME_FOUND);
    }

    protected void onTipInitialized() {
        CrossMessager.sendMessageToFrames(INFO_ICON_INIT, getState());
    }

    @Override
    public JsArray<JavaScriptObjectExt> find(final AsyncCallback<Boolean> laterCb) {
        clearInit();
        long sTime = System.currentTimeMillis();
        Element matchElement = Finder
                .findBest(Document.get(), action, parent);
        getLogger().log(WfxLogLevel.PERFORMANCE, this.getClass(), "find",
                WfxModules.WORKFLOW_ENGINE,
                "Time taken by findBest method of Finder: {0} ms",
                (System.currentTimeMillis() - sTime));
        match = matchElement != null ? matchElement : null;
        if (match == null) {
            return FinderLogger.getFinderFailureComponent(action.step(),
                    action.type());
        }

        laterCallback = laterCb;

        init = CrossMessager.addListener(
                (CrossListener) (type, content) -> actionerInit(content),
                ACTIONER_INIT);

        PartState state = DataUtil.create();
        state.draft(draft);
        state.step(action.step());
        state.parent(parent + 1);

        if (Customizer.newResponderLogic()) {
            sendMessageToFrameIfNeeded(state);
            if (isIframeFound) {
                // if iframe has been found attaching relocator to handle
                // internal frame reloads and start the responder again
                watchRelocator();
                return null;
            } else {
                return DataUtil.createArray().cast();
            }
        } else {
            // fallback to old logic. Only temporary and will be removed once
            // the new responder logic is stable
            CrossMessager.sendMessageToFrame(match, "actioner_show",
                    StringUtils.stringifyObject(state));
            return DataUtil.createArray().cast();
        }
    }

    protected void watchRelocator() {
        watchRel(match, new MiddleReller(parent, this), false);
    }

    /**
     * Since searcher doesn't stop in Top/Middle Responder until Source sends a
     * live/miss, maintaining a reference of already found frame and restricting
     * redundant messages to the already found frame if actioner_show has been
     * received by that frame already and stopping the searcher. Sending destroy
     * message only to previously and newly found frame instead of all sibling
     * iframes
     */
    private void sendMessageToFrameIfNeeded(PartState state) {
        setLastTrackedFrameElement(match);
        if (!isIframeFound) {
            CrossMessager.sendMessageToFrame(match, "actioner_show",
                    StringUtils.stringifyObject(state));
        }
    }

    @Override
    protected void addSubscriber() {
        // We don't have mutation bus instances in Middle and Top responder
    }

    private static boolean needScroll(Coordniates coords, String placement) {
        int scrollLeft = Window.getScrollLeft();
        int scrollTop = Window.getScrollTop();
        return OverlayUtil.needScroll(coords.left() + scrollLeft,
                coords.right() + scrollLeft, coords.top() + scrollTop,
                coords.bottom() + scrollTop,
                Actioner.alignTop(ActionerPopover.placement(placement))
                        ? Actioner.OVERLAY_AVG_HEIGHT
                        : 0,
                Actioner.alignRight(ActionerPopover.placement(placement))
                        ? Actioner.OVERLAY_AVG_WIDTH
                        : 0);
    }

    protected void onInit(Coordniates coords,
            String evaluationData, ActionerInitData data) {
        ActionerInitData actionerInitData = DataUtil.create();
        actionerInitData.setFlowId(draft.flow_id());
        actionerInitData.setStep(action.step());
        actionerInitData.setCoords(StringUtils.stringifyObject(coords));
        if (StringUtils.isNotBlank(evaluationData)) {
            actionerInitData.setEvaluationData(evaluationData);
        }
        if (ContentType.flow.name().equals(action.type())) {
            actionerInitData.setStepCount(data.getStepCount());
        } else {
            actionerInitData.setStepFindData(data.getStepFindData());
        }
        CrossMessager.sendMessageToParent(ACTIONER_INIT,
                StringUtils.stringifyObject(actionerInitData));
        watchRel(match, new MiddleReller(parent, this), false);
    }

    protected void onMove(Coordniates coords) {
        CrossMessager.sendMessageToParent("actioner_move",
                getState(coords));
    }

    protected void onStaticShow(Coordniates coords) {
        CrossMessager.sendMessageToParent("actioner_static_show",
                getState(coords));
    }

    @Override
    public void onOptionalNext() {
        CrossMessager.sendMessageToTop("actioner_optional_next",
                getState());
    }

    @Override
    public void onMiss(JsArray<JavaScriptObjectExt> reason) {
        String failureReason = "";
        if (!JsUtils.isEmpty(reason)) {
            failureReason = Actioner.SEPERATOR +  StringUtils.stringifyArray(reason);;
        }
        CrossMessager.sendMessageToTop("actioner_miss",
                getState() + failureReason);
    }

    protected void scroll() {
        CrossMessager.sendMessageToFrame(match, "actioner_scroll",
                getState());
    }

    protected void onStaticShowError(Coordniates coords) {
        CrossMessager.sendMessageToParent("actioner_static_show_error",
                getState(coords));
    }

    protected void onStaticShowErrorEnagager(Coordniates coords) {
        CrossMessager.sendMessageToParent(
                "actioner_static_show_error_engager", getState(coords));
    }

    @Override
    public void destroy() {
        super.destroy();
        CrossMessager.removeListener(hide, ACTIONER_HIDE);
        CrossMessager.removeListener(move, "actioner_move");
        CrossMessager.removeListener(scroll, "actioner_scroll");
        CrossMessager.removeListener(staticShow, "actioner_static_show");
        CrossMessager.removeListener(staticErrorShow,
                "actioner_static_show_error");
        CrossMessager.removeListener(staticErrorEngagerShow,
                "actioner_static_show_error_engager");
        CrossMessager.removeListener(infoIconInit, INFO_ICON_INIT);
        CrossMessager.removeListener(found, IFRAME_FOUND);
        isIframeFound = false;
        clearInit();

        // destroy messages to iframe are now restricted to only the last found
        // iframe if it is valid(if the iframe has not reloaded or got removed)
        // instead of all sibling iframes. This is fallback to old logic and
        // will be removed once new responder logic is stable.
        if (!Customizer.newResponderLogic()
                || null == getLastTrackedFrameElement()) {
            CrossMessager.sendMessageToFrames(ACTIONER_HIDE, getState());
        } else if (isLastTrackedFrameElementValid()) {
            CrossMessager.sendMessageToFrame(getLastTrackedFrameElement(),
                    ACTIONER_HIDE, getState());
        }
    }

    private void clearInit() {
        if (init != null) {
            CrossMessager.removeListener(init, ACTIONER_INIT);
            init = null;
        }
    }

    @Override
    protected int getParent() {
        return parent;
    }

    private void actionerInit(String content) {
        ActionerInitData actionerInitData = DataUtil
                .<ActionerInitData> create(content);
        if (draft.flow_id().equals(actionerInitData.getFlowId())
                && actionerInitData.getStep() == action.step()) {
            Coordniates coords = DataUtil
                    .create(actionerInitData.getCoords());
            String evaluationData = actionerInitData
                    .getEvaluationData();
            coords.relative(match);
            coords.needScroll(coords.needScroll()
                    || needScroll(coords, action.placement()));
            // destroy the relocator used to handle reloading iframe scenario
            if (null != relocator && Customizer.newResponderLogic()) {
                relocator.stop();
            }
            onInit(coords, evaluationData, actionerInitData);
            laterCallback.onSuccess(true);
            clearInit();
        }
    }

    private boolean isLastTrackedFrameElementValid() {
        return IFrameElement.TAG
                .equalsIgnoreCase(getLastTrackedFrameElement().getTagName())
                && null != DOMUtil
                        .getContentWindow(getLastTrackedFrameElement());
    }

    protected abstract class ResponderListener implements CrossListener {
        @Override
        public void onMessage(String type, String content) {
            int seperatorFirst = content.indexOf(Actioner.SEPERATOR);
            String flowId = content.substring(0, seperatorFirst);
            content = content
                    .substring(seperatorFirst + Actioner.SEPERATOR.length());
            int messageSeperator = content.indexOf(Actioner.SEPERATOR);

            int stepInMessage;
            String message;

            if (messageSeperator != -1) {
                stepInMessage = Integer
                        .parseInt(content.substring(0, messageSeperator));
                message = content
                        .substring(messageSeperator + Actioner.SEPERATOR.length());
            } else {
                stepInMessage = Integer.parseInt(content);
                message = null;
            }

            if (flowId == draft.flow_id()
                    && stepInMessage == action.step()) {
                processMessage(message);
            }
        }

        protected abstract void processMessage(String message);
    }
}