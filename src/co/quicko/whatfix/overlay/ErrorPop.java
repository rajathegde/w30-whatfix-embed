package co.quicko.whatfix.overlay;

import co.quicko.whatfix.data.Step;

public class ErrorPop extends StepStaticPop {

    public ErrorPop(Step step, String footnote, String placement) {
        super(step, footnote, placement);
    }

    @Override
    public boolean closeable() {
        return false;
    }

}