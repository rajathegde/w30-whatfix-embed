package co.quicko.whatfix.overlay;

import com.google.gwt.user.client.Timer;

import co.quicko.whatfix.overlay.AbstractResponder.AutoExecutionHelper;
import co.quicko.whatfix.data.AutoExecutionConfig;
import co.quicko.whatfix.data.Step;
/**
 * @author Prashanth
 * 
 *         This is Wrapper for a Timer used only for Auto-Exec Glass .
 *
 */
public class WrapAutoExecTimer {

    private Timer timer;

    public WrapAutoExecTimer(ResponderHelper helper, Step action, int timeout) {
        initialiseTimer(((AutoExecutionHelper) helper), action, timeout);
    }

    private void initialiseTimer(AutoExecutionHelper helper, Step action,
            int timeout) {
        if (helper.supportsSpotlight()
                && AutoExecutionConfig.isAutoExecutableStep(action)) {
            // AutoExecGlass.HIDE_AUTO_GLASS_TIME
            timer = new Timer() {
                @Override
                public void run() {
                    helper.showAlternateTooltip();
                    this.cancel();

                }
            };
            timer.schedule(timeout);
        }
    }

    public void cancelTimer() {
        if (timer != null && timer.isRunning()) {
            timer.cancel();
            timer=null;
        }
    }

}