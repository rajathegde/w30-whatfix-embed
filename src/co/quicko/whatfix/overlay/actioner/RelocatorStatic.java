package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ScrollEvent;

import co.quicko.whatfix.data.AutoExecutionConfig;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.ScrollEventHandler;
import co.quicko.whatfix.overlay.ScrollEventHandler.ScrollListener;

public class RelocatorStatic extends Relocator implements ScrollListener {

    private boolean scrolling;
    private boolean moved;
    protected String type;
    private boolean isScrollEventRegistered = false;
    private boolean overlapCheckNeeded;

    public RelocatorStatic(Element element, Reller impl, int autoExecuteAction,
            boolean isOverlapCheckNeeded) {
        super(element, impl);
        if (impl.scrollOnHideEnabled(element)) {
            ScrollEventHandler.addHandler(this);
            isScrollEventRegistered = true;
        }
        if (AutoExecutionConfig.isAutoExecutableStep(autoExecuteAction)) {
            this.type = AutoExecutionConfig.AUTO_EXEC;
        }
        this.overlapCheckNeeded = isOverlapCheckNeeded;
    }
    
    @Override
    public boolean execute() {
        if (scrolling) {
            return true;
        }

        return super.execute();
    }


    @Override
    public void stop() {
        super.stop();
        if (isScrollEventRegistered) {
            ScrollEventHandler.removeHandler(this);
            isScrollEventRegistered = false;
        }
    }
    
    @Override
    public int getGap() {
        return 1000;
    }

    @Override
    protected int getSameCheckCount() {
        // Performance improvement when there are more than 50 tips
        return StepConfigurations.getTipRecheckCount();
    }
    
    @Override
    protected boolean trackOverlap() {
        return true;
    }

    @Override
    public void onScroll(ScrollEvent event) {
        scrolling = true;
        if (!elementNotMoved()) {
            moved = true;
            impl.scrolled();
        }
    }

    @Override
    public void onScrollStopped() {
        scrolling = false;
        if ((!elementNotMoved() || moved) && !isOverlapped()) {
            impl.moved(element);
            moved = false;
        }        
    }

    protected boolean isOverlapped() {
        return OverlayUtil.isOverlapped(element, false);
    }

    @Override
    protected boolean isStabilized() {
        if (type == null) {
            return false;
        }
        int stabilityFactor = AutoExecutionConfig.getStabilityFactorTips(type);
        return (stabilityFactor < 0 ? false
                : (getSameCheckCount() - sameChecks == stabilityFactor));
    }

    @Override
    protected boolean performAutoExecAction() {
        if (stabilized && (!overlapCheckNeeded || !isOverlapped())) {
            impl.stabilized(element);
            this.stabilized = false;
            // Allow some time for changes on the element after event is
            // fired
            return true;
        }
        return false;
    }
}
