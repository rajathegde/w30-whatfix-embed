package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.data.JavaScriptObjectExt;

public class SearcherDesktop extends Searcher {
    private boolean stop = false;

    public SearcherDesktop(boolean optional, boolean selector) {
        super(optional, selector);
    }

    @Override
    public boolean execute() {
        if (stop) {
            return false;
        }

        JsArray<JavaScriptObjectExt> findError = responder
                .find(new AsyncCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean foundLater) {
                        if (foundLater) {
                        	retry = 0;
                        }
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        // no impl
                    }
                });
        // error null means element is found
		boolean found = (null == findError);
		if (found) {
			responder.setLastMissOptional(false);
			return false;
		}

		retry += 1;
		// retry logic only applies to optional step for desktop
		if (optional) {
			if (retry < optRetries) {
				return true;
			} else {
				responder.setLastMissOptional(true);
				responder.onOptionalNext();
			}
		}

		// keep looping this till we have a relocator.
		return true;
	}

    @Override
    public void stop() {
        stop = true;
    }
}
