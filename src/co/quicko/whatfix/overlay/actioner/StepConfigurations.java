package co.quicko.whatfix.overlay.actioner;

import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.StepRetries;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.security.Enterpriser;

/**
 * Configurations for the no of retries used by the algorithm to find the
 * element.
 * 
 * Each configuration can be done at step level also
 * 
 */
public class StepConfigurations {

    public static final int RETRY_GAP_IN_MS = 500;
    
    public static final int RETRY_GAP_IN_MS_FOR_DESKTOP = 400;

    // 5 seconds
    private static final int OPT_RETRIES = 10;

    // 1 second (optional step following a missed optional step)
    private static final int OPT_NEXT_RETRIES = 2;

    // 1 second
    private static final int OPT_CSS_RETRIES = 2;

    // 0 second (optional step following a missed optional step)
    private static final int OPT_CSS_NEXT_RETRIES = 1;

    
    private static final int TIP_RECHECK_COUNT = 15;

    /**
     * Added for an observation, to be removed post FIX-10965.
     */
    private static final int MISS_EVENT_RETRY_GAP = 200;
    private static final int MISS_EVENT_RETRIES = 10;
    private static final int STEP_RETRIES = 120;
    
    public static int getRetryGaps() {
        int retryGapInMs = getStepConfigurations().retry_gap_in_ms();
        if (retryGapInMs >= 0) {
            // The minimum retry gap is capped at 200ms
            return retryGapInMs > 200 ? retryGapInMs : 200;
        }
        return RETRY_GAP_IN_MS;
    }

    public static int getFlowRetryGaps() {
        int retryGapInMs = getStepConfigurations().retry_gap_in_ms();
        if (retryGapInMs >= 0) {
            return getRetryGaps();
        }
        return Enterpriser.isGracefulFailureEnabled() ? MISS_EVENT_RETRY_GAP
                : RETRY_GAP_IN_MS;
    }

    public static int getOptRetries() {
        int optRetries = getStepConfigurations().opt_step_retries();
        if (optRetries >= 0) {
            return optRetries;
        }

        // For backward compatibility
        int optionalRetries = Customizer.getOptionalRetries();
        optionalRetries = (optionalRetries > 0) ? optionalRetries : OPT_RETRIES;
        return optionalRetries;
    }
    
    public static int getRetries() {
        int stepRetries = getCustomRetries();
        return stepRetries > 0 ? stepRetries : STEP_RETRIES;
    }

    private static int getCustomRetries() {
        return getStepConfigurations().step_retries();
    }

    /**
     * To be removed/replaced post FIX-10965.
     */
    public static int getFlowRetriesCount() {
        int retries = getCustomRetries();
        if (retries > 0) {
            return retries;
        }
        return Enterpriser.isGracefulFailureEnabled() ? MISS_EVENT_RETRIES
                : STEP_RETRIES;
    }

    /*
     * The algorithm searches for the element once by default, hence the number
     * of retries after that can be zero
     */
    public static int getOptNextRetries() {
        int optNextRetries = getStepConfigurations()
                .opt_consecutive_step_retries();
        return (optNextRetries >= 0) ? optNextRetries : OPT_NEXT_RETRIES;
    }

    public static int getOptCssRetries() {
        int optCssRetries = getStepConfigurations().opt_css_step_retries();
        return (optCssRetries >= 0) ? optCssRetries : OPT_CSS_RETRIES;
    }

    public static int getOptCssNextRetries() {
        int optCssNextRetries = getStepConfigurations()
                .opt_consecutive_css_step_retries();
        return (optCssNextRetries >= 0) ? optCssNextRetries
                : OPT_CSS_NEXT_RETRIES;
    }

    public static int getTipRecheckCount() {
        int tipRecheckCount = getStepConfigurations().tip_recheck_count();
        return (tipRecheckCount >= 0) ? tipRecheckCount : TIP_RECHECK_COUNT;
    }

    private static StepRetries getStepConfigurations() {
        StepRetries customStepRetries = Customizer.getStepConfigurations();
        return customStepRetries != null ? customStepRetries
                : (StepRetries) DataUtil.create();
    }
    
    public static int getDesktopRetryGaps() {
        int retryGapInMs = getStepConfigurations().retry_gap_in_ms();
        if (retryGapInMs >= 0) {
            // The minimum retry gap is capped at 200ms
            return retryGapInMs > 200 ? retryGapInMs : 200;
        }
        return RETRY_GAP_IN_MS_FOR_DESKTOP;
    }
}
