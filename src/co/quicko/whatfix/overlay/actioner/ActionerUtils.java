package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.Actioner.PartState;
import co.quicko.whatfix.overlay.ActionerSettings;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.FramesData;
import co.quicko.whatfix.overlay.MiddleResponder;
import co.quicko.whatfix.overlay.Responder;
import co.quicko.whatfix.workflowengine.app.Segmenter;
import co.quicko.whatfix.workflowengine.data.strategy.ExistStrategy;

public final class ActionerUtils {

    private ActionerUtils() {
    }

    public static void actionerShow(String content) {
        PartState state = DataUtil.create(content);
        // send acknowledgement to parent so that searcher can stop
        String message = state.draft().flow_id() + Actioner.SEPERATOR
                + state.step();
        CrossMessager.sendMessageToParent(MiddleResponder.IFRAME_FOUND,
                message);
        Actioner.show(state.draft(), state.step(), state.parent(), true);
    }

    public static void actionerReshow(String content) {
        int seperatorIndex = content.indexOf(Actioner.SEPERATOR);

        String flowId = content.substring(0, seperatorIndex);
        int step = Integer.parseInt(
                content.substring(seperatorIndex + Actioner.SEPERATOR.length(),
                        content.length()));
        Responder responder = Actioner.getResponder(flowId, step);
        if (responder == null) {
            return;
        }
        Actioner.reShow(responder);
    }

    public static void actionerSettings(String content) {
        Actioner.settings = DataUtil.<ActionerSettings> create(content);
    }

    public static void findElement(String content) {
        JavaScriptObjectExt data = DataUtil.create(content);
        String operator = data.valueAsString("operator");
        String selectors = data.valueAsString("selector");
        int sel = selectors.lastIndexOf("<<");
        if (sel == -1) {
            JsArray<Element> elements = ExistStrategy.getElements(operator,
                    selectors.trim());
            String found = "false";
            if (elements != null && elements.length() > 0) {
                found = "true";
            }
            data.put("found", found);
            CrossMessager.sendMessageToTop("element_found",
                    StringUtils.stringifyObject(data));
        } else {
            String selector = selectors.substring(sel + 2);
            JsArray<Element> elements = ExistStrategy.getElements(operator,
                    selector.trim());
            if (elements == null || elements.length() == 0) {
                data.put("found", "false");
                CrossMessager.sendMessageToTop("element_found",
                        StringUtils.stringifyObject(data));
            } else {
                data.put("selector", selectors.substring(0, sel));
                CrossMessager.sendMessageToFrame(elements.get(0),
                        "find_element", StringUtils.stringifyObject(data));
            }
        }
    }

    public static void enterpriseSettings(String content) {
        FramesData framesData = DataUtil.<FramesData> create(content);
        FramesData.setData(framesData);
        Segmenter.startSchedulerToFindTagsIfRequired();
    }

    public static void enterpriseSettingsAllFrames(String type,
            String content) {
        FramesData framesData = DataUtil.<FramesData> create(content);
        FramesData.setData(framesData);
        CrossMessager.sendMessageToFrames(type, content);
        Segmenter.startSchedulerToFindTagsIfRequired();
    }

    public static void enterpriseSendSettings(JavaScriptObject source) {
        CrossMessager.sendMessageToSource(source,
                "enterprise_settings", StringUtils.stringifyObject(
                        FramesData.getData(Actioner.settings)));
    }
}