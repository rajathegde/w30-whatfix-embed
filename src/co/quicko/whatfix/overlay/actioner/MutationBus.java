package co.quicko.whatfix.overlay.actioner;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.user.client.Timer;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.PossibleElementList;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.DirectResponder;
import co.quicko.whatfix.overlay.MutationRecord;
import co.quicko.whatfix.overlay.Responder;
import co.quicko.whatfix.overlay.ShadowDOM;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.workflowengine.MultiStepFinder;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.alg.FinderUtil;

public class MutationBus {

    private static final MutationBus instance = new MutationBus();
    private int throttleTimeMs;
    private int scrollTimeoutMs;
    private static final int DEFAULT_THROTTLE_TIME = 300;
    private static final int DEFAULT_SCROLL_TIMEOUT = 30;
    private long prevTime;
    private Map<String, Responder> responderMap = new HashMap<>();
    private Set<JavaScriptObject> shadowParentsSet = new HashSet<>();
    private List<String> notFoundStepKeys = new ArrayList<>();
    private List<Step> notFoundSteps = new ArrayList<>();
    private Set<Responder> alreadyFoundSteps = new HashSet<>();
    private Set<String> attributeFiltersSet = new HashSet<>();
    
    private Timer timer = new Timer() {
        @Override
        public void run() {
            runSubscribers(false);
        }
    };

    private MutationBus() {
        /*
         * Add global listeners
         */
        addMutationObserver(Document.get().getDocumentElement());
        int customThrottleTime = Customizer.getMutationThrottleTime();
        throttleTimeMs = customThrottleTime > 0 ? customThrottleTime
                : DEFAULT_THROTTLE_TIME;
        int customScrollTimeout = Customizer.getSmartTipScrollTimeout();
        scrollTimeoutMs = customScrollTimeout > 0 ? customScrollTimeout
                : DEFAULT_SCROLL_TIMEOUT;
        JavaScriptObject window = (DOMUtil.nativeCurrentWindow() != null)
                ? DOMUtil.nativeCurrentWindow()
                : DOMUtil.nativeDefaultWindow();
        addScrollListener(window, scrollTimeoutMs);
        JsArrayString attributeFilters = Customizer
                .getMutationExcludeAttributeNames();
        if (JsUtils.isNotEmptyArray(attributeFilters)) {
            attributeFiltersSet = JsUtils.toSet(attributeFilters);
        }
        getLogger().log(WfxLogLevel.DEBUG, MutationBus.class, "MutationBus",
                WfxModules.JIT, "throttleTimeMs: {0}, scrollTimeoutMs: {1}",
                throttleTimeMs, scrollTimeoutMs);
    }

    public static MutationBus getInstance() {
        return instance;
    }

    public void addSubscriber(Responder responder) {
        String key = getResponderState(responder);
        responderMap.put(key, responder);
        timer.cancel();
        timer.schedule(throttleTimeMs);
    }

    public void removeSubscriber(Responder responder) {
        String key = getResponderState(responder);
        responderMap.put(key, null);
    }
    
    public void attachShadowListeners(Responder responder) {
        if (FinderUtil.hasShadowPath(responder.getFullStep().marks())) {
            List<Element> shadowParents = ShadowDOM
                    .getShadowParents(responder.getElement());
            shadowParents.forEach((Element shadowParent) -> {
                if (!shadowParentsSet.contains(shadowParent)) {
                    shadowParentsSet.add(shadowParent);
                    addScrollListener(shadowParent, scrollTimeoutMs);
                    addMutationObserver(shadowParent);
                }
            });
        }
    }

	private static String getResponderState(Responder responder) {
		return responder.getState();
	}

    private void throttledRunSubscribers() {
        timer.cancel();
        timer.schedule(throttleTimeMs);
    }

    private void runSubscribers(boolean isScrollEvent) {
        long sTime = System.currentTimeMillis();
        addNotFoundStepsToList();
        findStepsWhichWereNotFound();
        relocateFoundElements(isScrollEvent);
        alreadyFoundSteps.clear();
        getLogger().log(WfxLogLevel.DEBUG, MutationBus.class, "runSubscribers",
                WfxModules.JIT, "Time taken to execute this method: {0} ms",
                (System.currentTimeMillis() - sTime));
    }

    private void runResponderRelocator(boolean isScrollEvent) {
        for (Responder responder : responderMap.values()) {
            if (responder != null && responder.isElementFound()) {
                responder.relocate(isScrollEvent);
            }
        }
    }

    private void addNotFoundStepsToList() {
        for (Responder responder : responderMap.values()) {
            if (responder != null) {
                if (responder.isElementFound()) {
                    addToAlreadyFoundList(responder);
                } else {
                    addToNotFoundList(getResponderState(responder),
                            responder.getFullStep());
                }
            }
        }
    }

    private void addToAlreadyFoundList(Responder responder) {
            alreadyFoundSteps.add(responder);
    }

    /**
     * This method tries to find steps which were not found. It uses new finder
     * logic of MultiStepFinder.
     * 
     * @param isScrollEvent
     */
    private void findStepsWhichWereNotFound() {
        if (notFoundSteps.isEmpty()) {
            return;
        }
        long sTime = System.currentTimeMillis();
        List<PossibleElementList<Element>> list = MultiStepFinder
                .findBest(Document.get(), notFoundSteps);
        long elapsedTime = System.currentTimeMillis() - sTime;
        JavaScriptObjectExt stepFindData = DataUtil.create();
        stepFindData.put("step_find_time", elapsedTime + "");
        stepFindData.put("step_count", notFoundSteps.size() + "");

        Set<Responder> responders = new HashSet<>();
        for (String notFoundStepKey : notFoundStepKeys) {
            responders.add(responderMap.get(notFoundStepKey));
        }
        for (Responder responder : responders) {
            responder.setStepFindData(stepFindData);
        }
        
        getLogger().log(WfxLogLevel.PERFORMANCE, MutationBus.class,
                "findStepsWhichWereNotFound", WfxModules.JIT,
                "Time taken by findBest method of MutiStepFinder to evaluate {0} Beacon/s and/or Smart Tip/s: {1}",
                notFoundStepKeys.size(), (System.currentTimeMillis() - sTime));
        for (int i = 0; i < notFoundStepKeys.size(); i++) {
            PossibleElementList<Element> matches = list.get(i);
            Responder responder = responderMap.get(notFoundStepKeys.get(i));
            responder.initializeStep(Callbacks.emptyCb(), matches);
        }
        notFoundStepKeys.clear();
        notFoundSteps.clear();
    }

    private void relocateFoundElements(boolean isScrollEvent) {
        for (Responder responder : responderMap.values()) {
            if (responder != null && (alreadyFoundSteps.contains(responder)
                    || (responder.isElementFound()
                            && responder instanceof DirectResponder))) {
                responder.relocate(isScrollEvent);
            }
        }
    }

    private static boolean isWfxNode(Element element) {
        boolean result = false;
        try {
            result = StringUtils
                    .isNotBlank(element.getAttribute(Common.DATA_WFX_ATTR));
        } catch (Exception error) {
        }
        return result;
    }
            
    /*
     * checks if all mutations are from whatfix elements
     * if yes then dont run any recalculations
     */
    private boolean canRunMutationSubscribers(
            JsArray<JavaScriptObject> mutationRecords) {
        for (int mutationIndex = 0; mutationIndex < mutationRecords
                .length(); mutationIndex++) {
            MutationRecord mutationRecord = (MutationRecord) mutationRecords
                    .get(mutationIndex);
            String mutationType = mutationRecord.type();
            NodeList<Element> addedNodes = mutationRecord.addedNodes();
            // Check if this Attribute has to be Ignored based on AC Code
            if (checkIfMutationExcludeAttribute(
                    mutationRecord.getAttributeName())) {
                continue;
            }
            if (!((("childList").equals(mutationType)
                    && allWfxNodes(addedNodes))
                    || (("attributes").equals(mutationType)
                            && isWfxNode(mutationRecord.target())))) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfMutationExcludeAttribute(String attributeType) {
        if (StringUtils.isNotBlank(attributeType)
                && attributeFiltersSet.contains(attributeType)) {
            return true;
        }
        return false;
    }

    private static boolean allWfxNodes(NodeList<Element> addedNodes) {
        int wfxNodes = 0;
        for (int nodeIndex = 0; nodeIndex < addedNodes
                .getLength(); nodeIndex++) {
            Element addedNode = addedNodes.getItem(nodeIndex);
            if (isWfxNode(addedNode)) {
                wfxNodes++;
            }
        }
        return (wfxNodes == addedNodes.getLength() && wfxNodes > 0);
    }

    private void mutationCallback(JsArray<JavaScriptObject> mutationRecords) {
        if (canRunMutationSubscribers(mutationRecords)) {
            throttledRunSubscribers();
        }
    }

    private native JavaScriptObjectExt getMutationCallBackFunction() /*-{
         var thisInstance = this;
         return function(mutations) {
             thisInstance.@co.quicko.whatfix.overlay.actioner.MutationBus::mutationCallback(Lcom/google/gwt/core/client/JsArray;)(mutations);
         };
     }-*/;

    private void addMutationObserver(Element element) {
        JavaScriptObjectExt config = DataUtil.create();
        config.put("childList", true);
        config.put("attributes", true);
        config.put("subtree", true);
        JavaScriptObjectExt callback = getMutationCallBackFunction();
        DOMUtil.addMutationObserver(element, config, callback);
    }

    /* 
     * not used scroll event handler because it cannot be
       configured for a 30ms timeout, default is 500
     */
    private native void addScrollListener(JavaScriptObject element,
            int scrollTimeout) /*-{
        var isScrolling;
        var thisInstance = this;
        var scrollHandler = function(e){
            $wnd.clearTimeout( isScrolling );
            // Set a timeout to run after scrolling ends
            isScrolling = $wnd.setTimeout(function() {
                thisInstance.@co.quicko.whatfix.overlay.actioner.MutationBus::runResponderRelocator(Z)(true);
            }, scrollTimeout); 
        }
        element.addEventListener('scroll',scrollHandler,true);
    }-*/;

    public void addToNotFoundList(String stepKey, Step step) {
        if (!notFoundStepKeys.contains(stepKey)) {
            notFoundStepKeys.add(stepKey);
            notFoundSteps.add(step);
        }
    }
    
}
