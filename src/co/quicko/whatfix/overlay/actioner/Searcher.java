package co.quicko.whatfix.overlay.actioner;

import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.overlay.Responder;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.overlay.DirectResponder;
import co.quicko.whatfix.overlay.TopResponder;

public class Searcher implements RepeatingCommand {
    private int normalRetries;

    protected boolean optional;
    protected Responder responder;

    private boolean stop = false;
    protected int retry = 0;

    protected final int optRetries;
    private final int optNextRetries;

    public Searcher(boolean optional, boolean selector) {
        this.optional = optional;

        normalRetries = getNormalRetries();

        if (selector) {
            optRetries = StepConfigurations.getOptCssRetries();
            optNextRetries = StepConfigurations.getOptCssNextRetries();
        } else {
            optRetries = StepConfigurations.getOptRetries();
            optNextRetries = StepConfigurations.getOptNextRetries();
        }
    }

    public void setResponder(Responder responder) {
        this.responder = responder;
    }

    @Override
    public boolean execute() {
        if (stop) {
            return false;
        }
        if (responder instanceof DirectResponder
                || responder instanceof TopResponder) {
            responder.startStepFindTimer();
        }
        JsArray<JavaScriptObjectExt> findError = responder
                .find(new AsyncCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean foundLater) {
                        if (foundLater) {
                            stop = true;
                        }
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                    }
                });
        // error null means element is found
        boolean found = (null == findError);
        if (found) {
            responder.setLastMissOptional(false);
            return false;
        }

        retry += 1;
        int maxRetries;
        if (optional) {
            if (responder.isLastMissOptional()) {
                maxRetries = optNextRetries;
            } else {
                maxRetries = optRetries;
            }
        } else {
            responder.setLastMissOptional(false);
            maxRetries = normalRetries;
        }
        if (retry < maxRetries) {
            return true;
        } else {
            if (optional) {
                responder.setLastMissOptional(true);
                responder.onOptionalNext();
            } else {
                responder.onMiss(findError);
            }
            return false;
        }
    }

    public void stop() {
        stop = true;
    }

    /**
     * @return stepRetries
     */
    protected int getNormalRetries() {
        return StepConfigurations.getFlowRetriesCount();
    }

}