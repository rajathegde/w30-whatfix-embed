package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.data.AutoExecutionConfig;

/**
 * 
 * @author subhadeep
 *
 */
public class AutoExecutionStabilizingRelocator extends Relocator {

    private String type;

    public AutoExecutionStabilizingRelocator(Element element, Reller impl,
            String type) {
        super(element, impl);
        this.type = type;
    }

    @Override
    protected boolean isStabilized() {
        int stabilityFactor = AutoExecutionConfig.getStabilityFactor(type);
        return stabilityFactor < 0 ? false
                : getSameCheckCount() - sameChecks == stabilityFactor;
    }
}
