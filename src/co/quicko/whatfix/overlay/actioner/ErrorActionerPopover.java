package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;

/**
 * Popover extension to show validation error messages. 
 * 
 * @author sujeesh
 *
 */
public class ErrorActionerPopover extends ExtendedActionerPopover {

    public static final String PLACEMENT = "t";

    public ErrorActionerPopover(PopoverExtender popMaker, PopListener listener,
            Draft draft, Step action, int top, int right, int bottom, int left,
            int width, int height) {
        super(popMaker, listener, draft, action, top, right, bottom, left,
                width, height);
        hide();
    }

    @Override
    protected void setId(String widgetType) {
        Common.setId(popover, "wfx-error-tip");
    }

    @Override
    public void showEngagers() {
        setEngagersShown(true);
        if (engagers == null) {
            return;
        }
        for (PopupPanel e : engagers) {
            e.show();
        }
    }
    
    @Override
    protected void buildEngagers(String engagerColor) {
        engagers = new PopupPanel[4];
        engagers[0] = makeEngager();
        engagers[1] = makeEngager();
        engagers[2] = makeEngager();
        engagers[3] = makeEngager();
    }

    protected void applyEngagerTheme(Label dummy) {
        Themer.applyTheme(dummy, Themer.STYLE.BACKGROUD_COLOR,
                Themer.ERROR_TIP.BG_COLOR);
    }
    
    @Override
    protected void hideNow() {
        hidePopper();
    }
}
