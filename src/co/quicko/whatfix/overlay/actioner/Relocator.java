package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;

import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.ZoomFinder;

public class Relocator implements RepeatingCommand {
    // checking if any other better elements gets loaded in first 25 seconds.
    protected int sameChecks;
    protected boolean stop;
    protected Reller impl;
    protected Element element;

    private int top;
    private int left;
    private int right;
    private int bottom;

    private int scrollLeft;
    private int scrollTop;

    private boolean overlapped;
    private boolean sameCheckNeeded = true;
    protected boolean stabilized;

    protected HandlerRegistration closeRegn;
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    public Relocator(Element element, Reller impl) {
        this.element = element;
        this.impl = impl;

        // succ-3375 fix
        double zoom = ZoomFinder.getCSSZoom(element);
        top = (int) Math.round(Actioner.getAbsoluteTop(element) * zoom);
        left = (int) Math.round(Actioner.getAbsoluteLeft(element) * zoom);
        right = (int) Math.round(Actioner.getAbsoluteRight(element) * zoom);
        bottom = (int) Math.round(Actioner.getAbsoluteBottom(element) * zoom);
        scrollLeft = Window.getScrollLeft();
        scrollTop = Window.getScrollTop();

        sameChecks = getSameCheckCount();

        if (impl.trackUnload()) {
            closeRegn = Window.addCloseHandler(new CloseHandler<Window>() {
                @Override
                public void onClose(CloseEvent<Window> event) {
                    Relocator.this.impl.gone();
                }
            });
        }
    }
    
    @Override
    public boolean execute() {
        if (stop) {
            if (closeRegn != null) {
                closeRegn.removeHandler();
                closeRegn = null;
            }
            LOGGER.log(WfxLogLevel.DEBUG, Relocator.class, "execute",
                    WfxModules.JIT, "Stopping the relocator- stop is true");
            return false;
        }

        if (sameChecks > 0) {
            if (sameCheckNeeded && !impl.same(element)) {
                impl.gone();
                LOGGER.log(WfxLogLevel.DEBUG, Relocator.class, "execute",
                        WfxModules.JIT,
                        "Stopping the relocator- exhausted same check");
                return false;
            }
            sameChecks--;
            if (isStabilized()) {
                this.stabilized = true;
                LOGGER.log(WfxLogLevel.DEBUG, Relocator.class, "execute",
                        WfxModules.JIT, "Element is stabilized");
            }
        }

        if (performAutoExecAction()) {
            LOGGER.log(WfxLogLevel.DEBUG, Relocator.class, "execute",
                    WfxModules.JIT,
                    "Necessary autoexecution action has been performed");
            return true;
        }

        sameCheckNeeded = true;

        if (!OverlayUtil.isVisible(element)) {
            impl.gone();
            LOGGER.log(WfxLogLevel.DEBUG, Relocator.class, "execute",
                    WfxModules.JIT,
                    "Stopping the relocator- Element is not visible");
            return false;
        }

        if (trackOverlap()) {
            if (OverlayUtil.isOverlapped(element, false)) {
                if (!overlapped) {
                    overlapped = true;
                    impl.overLapped(element);
                }
                return true;
            }
            
            if (overlapped) {
                overlapped = false;
                impl.overCleared(element);
                return true;
            }
        }

        if (elementNotMoved()) {
            return true;
        }

        if (!OverlayUtil.isAttached(element, Document.get())) {
            impl.gone();
            LOGGER.log(WfxLogLevel.DEBUG, Relocator.class, "execute",
                    WfxModules.JIT,
                    "Stopping the relocator- Element is not attached to DOM");
            return false;
        }

        impl.moved(element);
        return true;
    }

    protected boolean elementNotMoved() {
        // succ-3375 fix
        double zoom = ZoomFinder.getCSSZoom(element);
        int nowTop = (int) Math.round(Actioner.getAbsoluteTop(element) * zoom);
        int nowLeft = (int) Math
                .round(Actioner.getAbsoluteLeft(element) * zoom);
        int nowRight = (int) Math
                .round(Actioner.getAbsoluteRight(element) * zoom);
        int nowBottom = (int) Math
                .round(Actioner.getAbsoluteBottom(element) * zoom);
        int nowScrollLeft = Window.getScrollLeft();
        int nowScrollTop = Window.getScrollTop();

        if (top == nowTop && left == nowLeft && right == nowRight
                && bottom == nowBottom
                && (!impl.scrollAsMoved() || (scrollLeft == nowScrollLeft
                        && scrollTop == nowScrollTop))) {
            return true;
        }

        top = nowTop;
        left = nowLeft;
        right = nowRight;
        bottom = nowBottom;
        scrollLeft = nowScrollLeft;
        scrollTop = nowScrollTop;
        return false;
    }
    
    public void stop() {
        stop = true;
    }

    protected int getSameCheckCount() {
        return 100;
    }

    public int getGap() {
        return 250;
    }

    protected boolean trackOverlap() {
        return impl.trackOverlap();
    }
    
    /**
     * This method is used when we need to let to actioner know about the
     * stability of the element. Useful for few auto execution scenario. Default
     * relocation login need to have any stability requirement
     * 
     * @return
     */
    protected boolean isStabilized() {
        return false;
    }

    public static interface Reller {
        public void gone();

        public void overLapped(Element element);

        public void overCleared(Element element);

        public void moved(Element element);

        public void scrolled();

        public boolean scrollAsMoved();

        public boolean same(Element element);

        public boolean trackUnload();

        public boolean trackOverlap();
        
        public boolean scrollOnHideEnabled(Element element);
        
        public void stabilized(Element element);
    }

    public void setSameCheckNeeded(boolean needed) {
        sameCheckNeeded = needed;
    }

    // If autoexecution action is performed, then it returns true else false.
    // Method is overridden in RelocatorStatic and RelocatorInfo.
    protected boolean performAutoExecAction() {
        if (stabilized) {
            impl.stabilized(element);
            this.stabilized = false;
            // Allow some time for changes on the element after event is
            // fired
            return true;
        }
        return false;
    }
}