package co.quicko.whatfix.overlay.actioner;

/**
 * @author akash
 */
public class AutoExecutionSearcher extends Searcher {

    public AutoExecutionSearcher(boolean optional, boolean selector) {
        super(optional, selector);
    }

    @Override
    protected int getNormalRetries() {
        return StepConfigurations.getRetries();
    }

}
