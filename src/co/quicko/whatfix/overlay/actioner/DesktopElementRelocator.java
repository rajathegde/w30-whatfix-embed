package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.core.client.Scheduler.RepeatingCommand;

import co.quicko.whatfix.data.DesktopElement;

public class DesktopElementRelocator implements RepeatingCommand {
    private int top;
    private int left;
    private int right;
    private int bottom;
    
    protected boolean stop;

    public DesktopElementRelocator(DesktopElement element) {
        this.top = element.rect_top();
        this.left = element.rect_left();
        this.right = element.rect_right();
        this.bottom = element.rect_bottom();
    }
    
    @Override
    public boolean execute() {
        return false;
    }

    public void stop() {
        stop = true;
    }

    protected int getSameCheckCount() {
        return 100;
    }

    public int getGap() {
        return 250;
    }
}
