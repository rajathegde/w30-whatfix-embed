package co.quicko.whatfix.overlay.actioner;

public class SearcherStatic extends Searcher {

    public SearcherStatic(boolean optional, boolean selector) {
        super(optional, selector);
    }
    
    @Override
    protected int getNormalRetries() {
        return 10;
    }
}
