package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.overlay.OverlayUtil;

public class RelocatorInfo extends RelocatorStatic {

    public RelocatorInfo(Element element, Reller impl, int autoExecuteAction) {
        super(element, impl, autoExecuteAction, false);
    }

    @Override
    protected boolean isOverlapped() {
        return OverlayUtil.isOverlapped(element, true);
    }

    @Override
    protected boolean performAutoExecAction() {
        if (stabilized) {
            impl.stabilized(element);
            this.stabilized = false;
            // Allow some time for changes on the element after event is
            // fired
            return true;
        }
        return false;
    }
}