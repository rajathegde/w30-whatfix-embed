package co.quicko.whatfix.overlay.actioner;

import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.AutoExecGlass;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.security.Enterpriser;

public class AutoActionerPopover extends FullActionerPopover {
    private Step step;
    private Draft draft;
    private int skippedSteps;
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();
    // Below boolean will be true only when Tooltip needs to be displayed
    private boolean showManualtooltip = false;

    public AutoActionerPopover(PopoverExtender popMaker, PopListener listener,
            Draft draft, Step action, int top, int right, int bottom, int left,
            int width, int height, PopupPanel[] spotlights) {
        super(popMaker, listener, draft, action, top, right, bottom, left,
                width, height, spotlights);
        this.step = action;
        this.draft = draft;
        showEngagers();
        if (isAutoExecUIEnabled()) {
            setSkippedSteps(popMaker.skippedSteps());
            showGlass();
        }
    }

    private boolean isAutoExecUIEnabled() {
        return !hideGlass();
    }

    private void setSkippedSteps(int skippedSteps) {
        this.skippedSteps = skippedSteps;
    }

    @Override
    protected boolean hideGlass() {
        return !(Enterpriser.isAutoExecUIEnable()
                && Themer.isAutomationUIRequired());
    }

    @Override
    protected NoContentPopup getPopover() {
        return new NoContentPopup() {
            @Override
            public void show() {
                if (showManualtooltip) {
                    super.show();
                }
            }
        };
    }
    
    /*
     * Hide the AutoExecGlass and display the alternate Tooltip
     */
    public void showAlterateTooltip() {
        AutoExecGlass.hideAutoExecGlass();
        LOGGER.log(WfxLogLevel.DEBUG, AutoActionerPopover.class, "showAlterateTooltip",
                WfxModules.JIT, "Hiding Auto Exec Glass and showing Tooltip as Step is not completed within 5 seconds");
        this.showManualtooltip = true;
        super.show(top, right, bottom, left, width, height, step.placement());      
    }

    protected boolean isPopoverShowing() {
        return true;
    }

    private void showGlass() {
        LOGGER.log(WfxLogLevel.DEBUG, AutoActionerPopover.class, "showGlass",
                WfxModules.JIT, "Showing auto-exec glass");
        /*
         * currentStepNumber and totalSteps counts are corrupted when Flow
         * branches to another step of the same flow without completing the
         * first step. Refer below for further details:
         * https://whatfix.atlassian.net/browse/SUCC-95
         * 
         */
        if (skippedSteps > step.step()) {
            skippedSteps = 0;
        }
        if (AutoExecGlass.getGlass() == null) {
            AutoExecGlass.setGlass(draft, step, skippedSteps);
        } else {
            AutoExecGlass.updateGlass(step.step() - skippedSteps);
        }
    }

}