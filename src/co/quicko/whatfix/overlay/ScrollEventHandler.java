package co.quicko.whatfix.overlay;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.client.Timer;

public class ScrollEventHandler {

    private static final int TIMER_GAP = 500;
    
    private static List<ScrollListener> handlers;
    private static JavaScriptObject nativeScrollListener;
    private static boolean isScrolling;
    private static long lastScrollTimeStamp;
    
    private static Timer timer = new Timer() {
        @Override
        public void run() {
            if ((System.currentTimeMillis() - lastScrollTimeStamp) > TIMER_GAP) {
                isScrolling = false;
                timer.cancel();
                fireScrollStopped();
            }
        }
    };

    public static void addHandler(ScrollListener handler) {
        if (handlers == null) {
            handlers = new ArrayList<ScrollListener>();
            attachNativeListener();
        }
        handlers.add(handler);
    }

    public static void removeHandler(ScrollListener handler) {
        if (handlers == null) {
            return;
        }
        handlers.remove(handler);

        if (handlers.size() == 0) {
            handlers = null;
            detachNativeListener();
        }
    }

    private static native void attachNativeListener() /*-{
		nativeScrollListener = function(event) {
			@co.quicko.whatfix.overlay.ScrollEventHandler::fireScroll(Lcom/google/gwt/event/dom/client/ScrollEvent;)(event);
		}
		if ($wnd.addEventListener) {
			$wnd.addEventListener("scroll", nativeScrollListener, true);
		} else {
			$wnd.attachEvent("scroll", nativeScrollListener, true);
		}
    }-*/;

    private static native void detachNativeListener() /*-{
		if ($wnd.removeEventListener) {
			$wnd.removeEventListener("scroll", nativeScrollListener, true);
		} else {
			$wnd.removeEvent("scroll", nativeScrollListener, true);
		}
    }-*/;
    
    private static void fireScroll(ScrollEvent event) {
        lastScrollTimeStamp = System.currentTimeMillis();
        if (!isScrolling && null != handlers) {
            for (ScrollListener handler : handlers) {
                handler.onScroll(event);
            }
            isScrolling = true;
            timer.scheduleRepeating(TIMER_GAP);
        }
    }

    private static void fireScrollStopped() {
        if (null != handlers) {
            for (ScrollListener handler : handlers) {
                handler.onScrollStopped();
            }
        }
    }
    
    public interface ScrollListener extends ScrollHandler {

        void onScrollStopped();
    }
}
