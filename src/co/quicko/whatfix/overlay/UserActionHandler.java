package co.quicko.whatfix.overlay;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;

import co.quicko.whatfix.common.ShortcutHandler;
import co.quicko.whatfix.common.ShortcutHandler.Shortcut;
import co.quicko.whatfix.common.ShortcutHandler.ShortcutListener;
import co.quicko.whatfix.common.WindowCloseManager;
import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.ChangeHandler.ChangeListener;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;

public class UserActionHandler {
    private HandlerRegistration userActionHandler;
    private UserActionEvent handler;
    private UserActionListener userActionListener;

    public UserActionHandler(UserActionListener listenerImpl) {
        userActionListener = listenerImpl;
    }

    public void startListener(Step step) {
        this.handler = new UserActionEvent(step);
        this.userActionHandler = Event.addNativePreviewHandler(handler);
    }

    public void stopListener() {
        this.userActionHandler.removeHandler();
        this.handler.dispose();
        handler = null;
    }

    public class UserActionEvent
            implements NativePreviewHandler, EngageEventHandler, ChangeListener,
            ShortcutListener, ClosingHandler {
        private Step step;
        String eventType;
        private ShadowDOM shadow;
        Set<Element> shadowRoots;
        private HandlerRegistration windowCloseHandler;
        private boolean isHandlerRemoved;
        private Shortcut shortcutKey;
        public UserActionEvent(Step step) {
            isHandlerRemoved = false;
            shadow = new ShadowDOM(this);
            shadowRoots = new HashSet<>();
            this.step = step;
            eventType = "";
            switch (StepCompletionActions.get(this.step.action())) {
                case ON_CLICK:
                    eventType = Customizer.getClickEvent();
                    break;
                case ON_TYPING_TEXT:
                    eventType = "keydown";
                    break;
                case ON_SELECTION_CHANGE:
                    ChangeHandler.addListener(this);
                    eventType = "change";
                    break;
                case ON_KEYBOARD_ENTER:
                    shortcutKey = new Shortcut(KeyCodes.KEY_ENTER, false, false,
                            false);
                    ShortcutHandler.register(shortcutKey, this);
                    break;
                case ON_KEYBOARD_ESC:
                    shortcutKey = new Shortcut(KeyCodes.KEY_ESCAPE, false,
                            false, false);
                    ShortcutHandler.register(shortcutKey, this);
                    break;
                case ON_KEYBOARD_TAB:
                    shortcutKey = new Shortcut(KeyCodes.KEY_TAB, false, false,
                            false);
                    ShortcutHandler.register(shortcutKey, this);
                    break;
                case ON_PAGE_REFRESH:
                    final String url = Window.Location.getHref();
                    PlayerBase.scheduler
                            .scheduleFixedDelay(new RepeatingCommand() {
                                @Override
                                public boolean execute() {
                                    if (isHandlerRemoved) {
                                        return false;
                                    }
                                    if (!url.equals(
                                            Window.Location.getHref())) {
                                        addAndSendMarks(null);
                                        return false;
                                    }
                                    return true;
                                }
                            }, 1000);
                    windowCloseHandler = WindowCloseManager
                            .addWindowClosingHandler(this);
                    break;
                default:
                    break;
            }
        }

        public void dispose() {
            isHandlerRemoved = true;
            if (this.step.action() == StepCompletionActions.ON_KEYBOARD_ENTER
                    .getValue()) {
                ShortcutHandler.unregister(shortcutKey, this);
            } else if (this.step
                    .action() == StepCompletionActions.ON_KEYBOARD_TAB
                            .getValue()) {
                ShortcutHandler.unregister(shortcutKey, this);
            } else if (this.step
                    .action() == StepCompletionActions.ON_KEYBOARD_ESC
                            .getValue()) {
                ShortcutHandler.unregister(shortcutKey, this);
            }
            if (this.step.action() == StepCompletionActions.ON_PAGE_REFRESH
                    .getValue()) {
                windowCloseHandler.removeHandler();
            }
            if (this.step.action() == StepCompletionActions.ON_SELECTION_CHANGE
                    .getValue()) {
                ChangeHandler.removeListener(this);
            }
            for (Iterator<Element> iterator = shadowRoots.iterator(); iterator
                    .hasNext();) {
                Element element = iterator.next();
                Element shadowRoot = DOMUtil.getShadowRoot(element);
                shadow.removeEngageListener(shadowRoot);
                iterator.remove();
            }
            shadow.destroy();
        }

        @Override
        public void onPreviewNativeEvent(NativePreviewEvent event) {
            onPreviewNativeEvent(
                    new PreviewEngageEvent(event.getNativeEvent()));
        }

        public void onPreviewNativeEvent(
                PreviewEngageEvent previewEngageEvent) {
            Element source = AppController
                    .getEngagerElement(previewEngageEvent);
            boolean appSpecificElement = null != source;
            if (Customizer.engagerElementFromPath()) {
                EventTarget targetElement = DOMUtil.getComposedPathTarget(
                        previewEngageEvent.getNativeEvent());
                source = targetElement != null
                        ? previewEngageEvent.getTarget(targetElement)
                        : null;
            }
            if (source == null) {
                source = previewEngageEvent.getActualTarget();
            }
            if (source == null) {
                return;
            }
            String evtType = previewEngageEvent.getNativeEvent().getType();
            if (evtType.equals(eventType) && tryAction(evtType, source)) {
                shadowRoots.addAll(ShadowDOM.getShadowParents(source));
                addAndSendMarks(source);
            } else if (!appSpecificElement
                    && (evtType.equals("mouseover")
                            || evtType.equals("mouseout"))
                    && shadow.handleMouseEvent(
                            previewEngageEvent.getNativeEvent())) {
                boolean isListenerAdded = evtType.equals("mouseover")
                        ? shadowRoots.add(source)
                        : shadowRoots.remove(source);
            }
        }

        public boolean tryAction(String evtType, Element source) {
            if (evtType.equals(Customizer.getClickEvent())) {
                return true;
            } else if (evtType.equals("keydown")) {
                while (source != null) {
                    if (Finder.IsText(source)) {
                        return true;
                    }
                    source = DOMUtil.getPsuedoParent(source);
                }
            }
            return false;
        }

        @Override
        public void onEngage(NativeEvent event) {
            onPreviewNativeEvent(new PreviewEngageEvent(event));
        }

        @Override
        public void onChange(Event event) {
            EventTarget target = event.getEventTarget();
            if (!Element.is(target)) {
                return;
            }
            Element source = Element.as(target);
            if (source.getTagName().equalsIgnoreCase("select")
                    || ("radio").equals(source.getAttribute("type"))) {
                addAndSendMarks(source);
            }
        }

        private void addAndSendMarks(Element asElement) {
            step.marks(DataUtil.createArray().cast());
            if (asElement != null) {
                step.marks(Finder.getMarks(asElement));
            }
            userActionListener.onUserAction(step);
        }

        @Override
        public void onShortcut(Shortcut shortcut) {
            addAndSendMarks(null);
        }

        @Override
        public void onWindowClosing(ClosingEvent event) {
            addAndSendMarks(null);
        }

    }

    public static interface UserActionListener {
        void onUserAction(Step step);
    }
}
