package co.quicko.whatfix.overlay;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.user.client.Window.Navigator;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.BaseVideoPopup;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.PlayInvoker;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.VideoContentPopup;
import co.quicko.whatfix.common.editor.BeaconType;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Content.Video;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Draft.Condition;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.Themer.BEACON.BeaconAnimationType;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.data.Videos;
import co.quicko.whatfix.overlay.Popover.ClickRegister;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.ContentManager;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.workflowengine.data.strategy.Operators;

public class Beacon extends IconInfo {

    public static final int ICON_HEIGHT = 20;
    public static final int ICON_WIDTH = 20;

    public static final String BEACON_TAG_ID = "_beacon_wfx_";
    public static final String BEACON_SETTINGS = "_wfx_beacon";
    private static final int BEACON_HOVER_TIME = 500;
    private static final String LEFT_POSITION = "l";
    private static final String BEACON_DEFAULT_POSITION = "rt";
    private static final String BEACON_NEW_ARIA_LABEL = "New";
    private static final String BEACON_ARIA_LABEL = "Highlight";

    private Draft draft;
    private Step step;
    private BeaconContent beaconContent;
    private ClickHandler handler;
    private Widget base;
    private SimplePanel beacon;
    private static BeaconAnimationType animationType;

    public Beacon(Draft draft, Element element, ResponderHelper helper,
            Step action, int top, int right, int bottom, int left) {
        super(element, helper, action, top, right, bottom, left);
        this.draft = draft;
        this.step = action;
    }

    @Override
    protected void setWidgeId() {
        Common.setId(panel, "wfx-beacon");
    }

    public static int[] position(int top, int right, int bottom, int left,
            int iconHeight, int iconWidth, String placement) {
        if (placement == null) {
            placement = "tr";
        }
        int iconLeft = 0;
        int iconTop = 0;

        if (placement.contains("t")) {
            iconTop = top - iconHeight / 4;
        } else {
            iconTop = bottom - iconHeight / 4;
        }

        if (placement.contains("l")) {
            iconLeft = left - iconWidth / 4;
        } else {
            iconLeft = right - iconWidth / 4;
        }
        return new int[] { iconLeft, iconTop };
    }

    public static Widget getBeaconBase(Step step,
            BeaconAnimationType animationType) {
        final Widget beacon;
        if (animationType == BeaconAnimationType.NEW) {
            beacon = Common.html(SVGElements.getBeaconNewIcon(
                    Themer.value(Themer.BEACON.BEACON_COLOR)));
            String placement = step.placement() == null
                    ? BEACON_DEFAULT_POSITION
                    : step.placement();
            if (placement.contains(LEFT_POSITION)) {
                beacon.setStyleName(Overlay.CSS.newBeaconLeftAnimation());
            } else {
                beacon.setStyleName(Overlay.CSS.newBeaconRightAnimation());
            }
        } else {
            beacon = Common.anchor("?");
            beacon.setStyleName(Overlay.CSS.base());
            Themer.applyTheme(beacon, Themer.STYLE.BACKGROUD_COLOR,
                    Themer.BEACON.BEACON_COLOR);
        }
        return beacon;
    }
    
    private void setBeaconContent(Step step) {
        beaconContent = (BeaconContent) step.custom_data(Beacon.id(step));
        handler = getHandler();
        if (step.action() == BeaconType.CLICK_ON_ELEMENT.getValue()) {
            return;
        }
        base.addStyleName(Overlay.CSS.beaconBaseCursor());
    }

    private ClickHandler getHandler() {
        Content content = DataUtil.create();
        if (beaconContent == null || beaconContent.contentId() == null) {
            content.flow_id(null);
            content.type(null);
        } else {
            content.flow_id(beaconContent.contentId());
            content.type(beaconContent.contentType());
        }
        return new BeaconHandler(content);
    }

    public void onNext() {
        if (handler != null) {
            Actioner.hide(draft, step.step());
            handler.onClick(null);
            helper.handleClose();
            if (Enterpriser.hasFeature(AdditionalFeatures.wcag_enable)
                    && (beaconContent == null
                            || beaconContent.contentType() == null)) {
                helper.placePopAndSetWcag(null);
            }
        }
    }

    @Override
    protected void refresh(Step step) {
        animationType = getCurrentAnimationType();
        FlowPanel container = new FlowPanel();

        if (Common.CssAnimation
                .whichTransition(container.getElement()) != null) {
            // For IE-9 animation is not supported, so no point adding
            // beaconLauncher for IE-9 which serves only animation.
            // This code block will not execute in IE-9.
            beacon = new SimplePanel();
            setBeaconAnimation(container, beacon, animationType);
        }

        base = getBeaconBase(step, animationType);
        setBeaconAriaLabel(step);
        setBeaconContent(step);
        container.add(base);

        panel.add(container);

        if (step.marks() == null) {
            // For beacon from the integration need to set id
            // if id not set it can impact the existing beacons where some
            // customization is done based on the id.
            Common.setId(panel, Beacon.BEACON_TAG_ID);
        }
        register.register(getBeaconBaseElement(), this);
    }
    
    private void setBeaconAriaLabel(Step step) {
        String ariaLabel;
        if (animationType == BeaconAnimationType.NEW) {
            ariaLabel = Beacon.BEACON_NEW_ARIA_LABEL;
        } else {
            ariaLabel = Beacon.BEACON_ARIA_LABEL;
        }
        if (null != step && StringUtils.isNotBlank(step.note_md())) {
            ariaLabel += ", " + step.note_md();
        }
        base.getElement().setAttribute(AriaProperty.LABEL, ariaLabel);
    }

    public static void setBeaconAnimation(FlowPanel container,
            SimplePanel beacon, BeaconAnimationType animationType) {
        if (animationType == BeaconAnimationType.NEW
                || animationType == BeaconAnimationType.SPECKLE) {
            return;
        }
        container.add(beacon);
        if (animationType == BeaconAnimationType.RIPPLE) {
            beacon.setStyleName(Overlay.CSS.beaconLauncher());
            Themer.applyTheme(beacon, Themer.STYLE.BACKGROUD_COLOR,
                    Themer.BEACON.BEACON_COLOR);
            return;
        }
        SimplePanel radialAnimation = new SimplePanel();
        beacon.setStyleName(Overlay.CSS.beaconRadial());
        radialAnimation.setStyleName(Overlay.CSS.beaconRadialLauncher());
        Themer.applyTheme(beacon, Themer.STYLE.BORDER_COLOR,
                Themer.BEACON.BEACON_COLOR);
        Themer.applyTheme(radialAnimation, Themer.STYLE.BACKGROUD_COLOR,
                Themer.BEACON.BEACON_COLOR);
        container.add(radialAnimation);
    }

    @Override
    protected int[] position(int top, int right, int bottom, int left,
            String placement) {
        return position(top, right, bottom, left, ICON_HEIGHT, ICON_WIDTH,
                placement);
    }

    @Override
    protected String defaultPlacement() {
        return "rt";
    }

    public static String id(Step step) {
        if (BEACON_SETTINGS.equals(step.flow_id())) {
            return step.flow_id();
        }
        return step.flow_id() + ":" + step.step_key();
    }

    public static String analyticsId(Step step) {
        if (BEACON_SETTINGS.equals(step.flow_id())) {
            return "-";
        }
        return step.flow_id() + ":" + step.step_key();
    }
    
    public static BeaconAnimationType getCurrentAnimationType() {
        return BeaconAnimationType
                .getTypeFromName(Themer.value(Themer.BEACON.ANIMATION_TYPE));
    }

    public class BeaconLinkHandler implements HandleContent {

        @Override
        public void handleContent(Content content) {
            String url = content.url();
            if (StringUtils.isNotBlank(url)) {
                PlayInvoker.play(url);
                Common.tracker().onEvent(EmbedEvent.LINK_START.builder()
                                .flow(content.flow_id(), content.title())
                                .segment(Enterpriser.getActiveBeaconSegment().segment_id(),
                                        Enterpriser.getActiveBeaconSegment().name())
                                .srcId(TrackerEventOrigin.BEACON)
                                .build());
            }
        }
    }

    public class BeaconVideoHandler implements HandleContent {

        private Videos getVideoInfo(Video video) {
            Videos videoInfo = DataUtil.create();
            videoInfo.title("");
            videoInfo.list_id("");
            videoInfo.thumbnail();
            videoInfo.video_id(video.url());
            videoInfo.description(video.title());
            return videoInfo;
        }

        @Override
        public void handleContent(Content content) {
            BaseVideoPopup baseVideoPopup = new VideoContentPopup(
                    getVideoInfo((Video) content));
            baseVideoPopup.showPopup();
            Common.tracker().onEvent(EmbedEvent.VIDEO_START.builder()
                    .flow(content.flow_id(), content.title())
                    .segment(Enterpriser.getActiveBeaconSegment().segment_id(),
                            Enterpriser.getActiveBeaconSegment().name())
                    .srcId(TrackerEventOrigin.BEACON).build());
        }
    }

    public class BeaconFlowHandler implements HandleContent {

        @Override
        public void handleContent(Content content) {

            // TODO Do we need to segment information?
            JavaScriptObjectExt message = DataUtil.create();
            message.put("flowId", content.flow_id());
            message.put("srcId", TrackerEventOrigin.BEACON.getSrcName());

            CrossMessager.sendMessageToTop("embed_run_from_id",
                    StringUtils.stringifyObject(message));
        }
    }

    private class BeaconHandler implements ClickHandler {
        private HandleContent handler;
        private Content content;
        private ContentManager manager = GWT.create(ContentManager.class);

        public BeaconHandler(Content content) {
            this.content = content;
            ContentType type = content.getType();
            switch (type) {
                case link:
                    handler = new BeaconLinkHandler();
                    break;
                case video:
                    handler = new BeaconVideoHandler();
                    break;
                default:
                    handler = new BeaconFlowHandler();
                    break;
            }
        }

        @Override
        public void onClick(ClickEvent event) {
            String contentId = content.flow_id();
            if (StringUtils.isBlank(contentId)) {
                return;
            }
            
            if (ContentType.flow.toString().equals(content.type())) {
                // don't pull flow data in case of flow
                // we will directly send a message to top
                // and embed will pull the flow information
                handler.handleContent(content);
            } else {
                manager.content(content, new AsyncCallback<Content>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(Content result) {
                        handler.handleContent(result);
                    }
                });
            }
        }
    }

    private interface HandleContent {

        public void handleContent(Content content);
    }

    public static Draft wrapIntegration(BeaconSetting beaconSettings) {

        JsArray<Condition> conditions = DataUtil.createArray().cast();
        Condition condition = Condition.create("element_selector",
                beaconSettings.target(), Operators.HAS);
        conditions.push(condition);

        Step step = DataUtil.create();
        step.conditions(conditions);
        step.placement(beaconSettings.position());
        step.note_md(beaconSettings.label());
        step.action(BeaconType.CLICK_ON_BEACON.getValue());

        BeaconContent beaconContent = DataUtil.create();
        beaconContent.contentId(beaconSettings.flow_id());
        beaconContent.contentType(ContentType.flow.name());

        Draft draft = DataUtil.create();
        draft.flow_id(Beacon.BEACON_SETTINGS);
        draft.step(1, step);
        draft.custom_data(Beacon.id(draft.step(1)), beaconContent);
        draft.steps(1);
        return draft;
    }

    public static class BeaconContent extends JavaScriptObject {
        protected BeaconContent() {
        }

        public final native void contentId(String contentId) /*-{
			this.contentId = contentId;
        }-*/;

        public final native String contentId() /*-{
			return this.contentId;
        }-*/;

        public final native void contentType(String contentType) /*-{
			this.contentType = contentType;
        }-*/;

        public final native String contentType() /*-{
			return this.contentType;
        }-*/;

        public final native boolean seen() /*-{
			return this.seen;
        }-*/;
    }

    public static class BeaconSetting extends Settings {
        protected BeaconSetting() {
        }

        public final native String flow_id() /*-{
			return this.flow_id;
        }-*/;
    }

    public static class BeaconSeenCount extends JavaScriptObject {

        protected BeaconSeenCount() {
        }

        public final native void beaconSeenCount(String key, double count) /*-{
			this[key] = count;
        }-*/;

        public final native double beaconSeenCount(String key) /*-{
			return this[key] ? this[key] : 0;
        }-*/;
    }
    
    public static class BeaconPopoverExtender extends PopoverExtender {
        @Override
        protected Popover makePopover(Step action, PopListener listener,
                ClickRegister register) {
            return new BeaconPopover(listener);
        }

        @Override
        protected StepPop stepPop(Step step, String footnote,
                String placement, String stepnote) {
            return new BeaconPop(step, footnote, placement);
        }
    }

    @Override
    public void onClick(ClickEvent event) {
        if (step.action() != BeaconType.CLICK_ON_ELEMENT.getValue()) {
            helper.handleNext();
        }
    }

    private Element getBeaconBaseElement() {
        return base.getElement();
    }

    @Override
    protected boolean isCloseable() {
        return false;
    }

    @Override
    protected int getUnhoverTime() {
        return BEACON_HOVER_TIME;
    }

    @Override
    protected int getHoverTime() {
        return BEACON_HOVER_TIME;
    }

    @Override
    protected Element getPopoverElement() {
        if (animationType == BeaconAnimationType.NEW
                || animationType == BeaconAnimationType.SPECKLE) {
            return base.getElement();
        }
        return beacon.getElement();
    }
    
    @Override
    protected boolean isDescriptionPresent() {
        return StringUtils.isNotBlank(step.note_md());
    }

    @Override
    public void setFocusOnI() {
        Scheduler.get().scheduleDeferred(new ScheduledCommand() {

            @Override
            public void execute() {
                getBeaconBaseElement().setTabIndex(0);
                getBeaconBaseElement().focus();
            }
        });

    }

    @Override
    public void onKeyDown(KeyDownEvent event) {
        if (isAccessibilityEvent(event.getNativeKeyCode())) {
            helper.placePopAndSetWcag(null);
        }
    }

    @Override
    public void onKeyUp(KeyUpEvent event) {
        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER
                && step.action() == BeaconType.CLICK_ON_BEACON.getValue()) {
            helper.handleNext();
        } else {
            Element popElement = isDescriptionPresent() ? getPopoverElement()
                    : null;
            if (event.getNativeKeyCode() == KeyCodes.KEY_TAB
                    && event.isShiftKeyDown()) {
                // when tab focus is on beacon and shift+tab
                // is pressed: move to element
                helper.placePopAndSetWcag(null);
            } else {
                if (null != popElement) {
                    // place tooltip if present
                    helper.placePop(popElement);
                }
            }
            getBeaconBaseElement().setTabIndex(-1);
        }
    }
    
    /**
     * To check if the keydown event is for accessibility purpose. Checking for
     * tab navigation and screen reader navigation in chrome and mac
     * 
     * @param keyCode
     */
    private boolean isAccessibilityEvent(int keyCode) {
        return (keyCode == KeyCodes.KEY_TAB
                || (keyCode == KeyCodes.KEY_RIGHT
                        && Navigator.getPlatform().startsWith("Mac"))
                || (keyCode == KeyCodes.KEY_DOWN
                        && Navigator.getPlatform().startsWith("Win"))) ? true
                                : false;
    }
}