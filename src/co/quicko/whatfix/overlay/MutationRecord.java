package co.quicko.whatfix.overlay;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Element;

/*
 * Mutation record Java object 
 * https://developer.mozilla.org/en-US/docs/Web/API/MutationRecord
 */
public class MutationRecord extends JavaScriptObject {
	
	protected MutationRecord() {};
	
	public final native String type() /*-{
		return this.type;
	}-*/;
	
	public final native Element target()/*-{
		return this.target;
	}-*/;
	
	public final native NodeList<Element> addedNodes() /*-{
		return this.addedNodes;
	}-*/;	
	
	public final native String getAttributeName() /*-{
        return this.attributeName;
    }-*/;   

}