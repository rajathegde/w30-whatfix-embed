package co.quicko.whatfix.overlay;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.BodyElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.FrameSetElement;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.client.ui.RootPanel;

import co.quicko.whatfix.workflowengine.alg.DOMUtil;

public class ShadowDOM {

    private List<Element> elements;
    private EngageEventHandler handler;
    private static final String ENGAGE_LISTENER_NAME = "wfx_EngageListener_";
    private String listenerName;
    private Integer keyBoardNativeCode;
    
    public ShadowDOM(EngageEventHandler handler) {
    	this.handler = handler;
        
        // init Listener Name with class name because it will override other
        // listeners if execution happens at same time
        listenerName = ENGAGE_LISTENER_NAME + handler.getClass().getSimpleName()
                + "_" + System.currentTimeMillis();
        initEngageListeners(this, listenerName);
    }
    
    public ShadowDOM(EngageEventHandler handler, List<Element> elements,
            Integer keyBoardCode) {
        this(handler);
        this.elements = elements;
        this.keyBoardNativeCode = keyBoardCode;
        addEngageListeners(true);
    }

    public ShadowDOM(EngageEventHandler handler, List<Element> elements) {
        this(handler, elements, null);
    }

    private void addEngageListeners(boolean addListener) {
        if(elements == null) {
            return;
        }
        for (Element element : elements) {
            String tag = element.getTagName().toLowerCase();
            while (!BodyElement.TAG.equals(tag)
                    && !FrameSetElement.TAG.equals(tag)) {
                Element parent = DOMUtil.getActualParent(element);
                if(parent == null) {
                    break;
                }
                
                if(DOMUtil.isShadowRoot(parent)) {
                    removeEngageListener(parent);
                    if(addListener) {
                        addEngageListener(parent);
                        addKeyboardShortcutListener();
                    }
                    break;
                }
                element = parent;
            }
        }
    }

    public void destroy() {
        addEngageListeners(false);
        removeWindowListenerRegistered(listenerName);
    }

    private void fire(JavaScriptObject event) {
        NativeEvent nativeEvent = ((NativeEvent) event);
        handler.onEngage(nativeEvent);
    }

    public boolean handleMouseEvent(NativeEvent event) {
        EventTarget target = event.getEventTarget();
        if (!Element.is(target)) {
            return false;
        }
        Element source = Element.as(target);
        if (DOMUtil.isShadowHost(source)) {
            Element shadowRoot = DOMUtil.getShadowRoot(source);
            String type = event.getType();
            if ("mouseover".equalsIgnoreCase(type)) {
                removeEngageListener(shadowRoot);
                addEngageListener(shadowRoot);
                return true;
            } else if ("mouseout".equalsIgnoreCase(type)) {
                removeEngageListener(shadowRoot);
                return true;
            }
        }
        return false;
    }
    
    public static List<Element> getShadowParents(Element element) {
        List<Element> shadowParents = new ArrayList<>();
        String tag = element.getTagName().toLowerCase();
        while (!BodyElement.TAG.equals(tag)
                && !FrameSetElement.TAG.equals(tag)) {
            Element parent = DOMUtil.getActualParent(element);
            if (parent == null) {
                break;
            }
            if (DOMUtil.isShadowRoot(parent)) {
                shadowParents.add(parent);
            }
            element = parent;
        }
        return shadowParents;
    }

    private native void initEngageListeners(ShadowDOM shadow, String listenerName) /*-{
    	$wnd[listenerName] = function(event) {
				if (event) {
					shadow.@co.quicko.whatfix.overlay.ShadowDOM::fire(Lcom/google/gwt/core/client/JavaScriptObject;)(event);
				}
			};
    }-*/;

    public void addEngageListener(Element shadowRoot) {
        addEngageListener(shadowRoot, listenerName);
    }

    /*
     * Adds a Listener at Window level for Keyboard Shortcuts
     */
    private void addKeyboardShortcutListener() {
        if (keyBoardNativeCode != null) {
            addKeyboardListener(RootPanel.getBodyElement(), listenerName);
        }
    }

    private static final native void addEngageListener(Element shadowRoot, String listenerName) /*-{
		if (shadowRoot != null) {
			shadowRoot.addEventListener("mouseover", $wnd[listenerName], true);
			shadowRoot.addEventListener("mouseout", $wnd[listenerName], true);
			shadowRoot.addEventListener("click", $wnd[listenerName], true);
            shadowRoot.addEventListener("keyup", $wnd[listenerName], true);
            shadowRoot.addEventListener("keydown", $wnd[listenerName], true);
            shadowRoot.addEventListener("mousedown", $wnd[listenerName], true);
            shadowRoot.addEventListener("focusin", $wnd[listenerName], true);
            shadowRoot.addEventListener("focusout", $wnd[listenerName], true);
		}
    }-*/;
    
    
    private static final native void addKeyboardListener(Element ele, String listenerName)  /*-{
        if(ele!=null) {
            ele.addEventListener("keydown", $wnd[listenerName], true);
        }
        
    }-*/;
    
    public void removeEngageListener(Element shadowRoot) {
        removeEngageListener(shadowRoot, listenerName);
    }

    private static final native void removeEngageListener(
            Element shadowRoot, String listenerName) /*-{
		if (shadowRoot != null && $wnd[listenerName] != null) {
			shadowRoot.removeEventListener("mouseover", $wnd[listenerName],
					true);
			shadowRoot.removeEventListener("mouseout", $wnd[listenerName],
					true);
			shadowRoot.removeEventListener("click", $wnd[listenerName], true);
            shadowRoot.removeEventListener("keyup", $wnd[listenerName], true);
            shadowRoot.removeEventListener("keydown", $wnd[listenerName], true);
            shadowRoot.removeEventListener("mousedown", $wnd[listenerName], true);
            shadowRoot.removeEventListener("focusin", $wnd[listenerName], true);
            shadowRoot.removeEventListener("focusout", $wnd[listenerName], true);
		}
    }-*/;
    
    private native void removeWindowListenerRegistered(String listenerName) /*-{
        delete $wnd[listenerName];
    }-*/;
    
}
