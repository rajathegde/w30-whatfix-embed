package co.quicko.whatfix.overlay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gargoylesoftware.htmlunit.javascript.host.event.MouseEvent;
import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.OptionElement;
import com.google.gwt.dom.client.SelectElement;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Pair;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.editor.AutoExecutionActions;
import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.AutoExecutionConfig;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.DataFeeder;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.FinderUtil;
import co.quicko.whatfix.workflowengine.app.App;
import co.quicko.whatfix.workflowengine.app.AppFactory;
import co.quicko.whatfix.workflowengine.app.AppFiller;

/**
 * Auto executer takes care of fast forward running of flows as well exposes
 * API's required for remote executing the flows.
 * 
 * @author muthu
 *
 */
public class AutoExecuter {

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    private static Map<StepCompletionActions, AutoExecutionActions> actionFallbackMapping = new HashMap<StepCompletionActions, AutoExecutionActions>();
    static {
        actionFallbackMapping.put(StepCompletionActions.ON_NEXT,
                AutoExecutionActions.ON_NEXT);
        actionFallbackMapping.put(StepCompletionActions.ON_CLICK,
                AutoExecutionActions.ON_CLICK);
        actionFallbackMapping.put(StepCompletionActions.ON_HOVER,
                AutoExecutionActions.ON_HOVER);
        actionFallbackMapping.put(StepCompletionActions.ON_TYPING_TEXT,
                AutoExecutionActions.ON_INPUT);
        actionFallbackMapping.put(StepCompletionActions.ON_SELECTION_CHANGE,
                AutoExecutionActions.ON_SELECTION_CHANGE);
    }

    public static void autoExecute(final Draft flow, final Step action,
            final Element element, final AsyncCallback<Boolean> executeCb) {
        AutoExecutionActions stepCompletionAction = getAction(
                action.auto_execute_action(), action.action());
        final String inputValue = Common.replaceCustomContent(action.input(),
                Customizer.getCustomData());
        // Customizer APIs follow zero-based step numbering
        boolean hasCustomExec = Customizer.autoExecute(flow.flow_id(),
                action.step() - 1, element, inputValue,
                stepCompletionAction.name());
        if (hasCustomExec) {
            return;
        }
        switch (stepCompletionAction) {
            case ON_NEXT:
                if (StringUtils.isNotBlank(inputValue)) {
                    setElemValue(action, flow.flow_id(), element, inputValue,
                            new AsyncCallback<Object>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    executeCb.onFailure(caught);
                                }

                                @Override
                                public void onSuccess(Object result) {
                                    executeCb.onSuccess(true);
                                }
                            });
                } else if (AutoExecutionConfig
                        .isAutoTest(flow.auto_execution_mode())
                        || AutoExecutionConfig
                                .isAutoExec(flow.auto_execution_mode())
                        || AutoExecutionConfig.isAutoExecutableStep(action)
                        || flow.recapture_mode()) {
                    executeCb.onSuccess(true);
                }
                break;
            case ON_CLICK:
                // ref:com.google.gwt.user.client.ui.CustomButton
                /*
                 * {@link ElementWrap} listens for a mousedown event if
                 * mousedown_as_click is set to true, so firing a mousedown
                 * event instead of a click event
                 */
                Element elem = Finder.getClickableElement(element);
                CustomEvents.dispatchEvent(elem, MouseEvent.TYPE_MOUSE_DOWN,
                        true, true, true);
                CustomEvents.dispatchEvent(elem, MouseEvent.TYPE_MOUSE_UP, true,
                        true, true);
                CustomEvents.dispatchEvent(elem, MouseEvent.TYPE_CLICK, true,
                        true, true);
                executeCb.onSuccess(false);
                break;
            case ON_HOVER:
                List<String> hoverEvents = AppController.getAutoHoverEvents();
                hoverEvents.forEach(eventType ->
                        CustomEvents.dispatchEvent(
                                element, eventType, true, false, true));
                executeCb.onSuccess(false);
                break;
            case ON_INPUT:
                if (StringUtils.isNotBlank(inputValue)) {
                    setElemValue(action, flow.flow_id(), element, inputValue,
                            new AsyncCallback<Object>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    executeCb.onFailure(caught);
                                }

                                @Override
                                public void onSuccess(Object result) {
                                    executeCb.onSuccess(false);
                                }
                            });
                }
                break;
            case ON_SELECTION_CHANGE:
                if (StringUtils.isNotBlank(inputValue)) {
                    setElementValueForDropDown(element,inputValue, action,
                        new AsyncCallback<Object>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            executeCb.onFailure(caught);
                        }

                        @Override
                        public void onSuccess(Object result) {
                            executeCb.onSuccess(false);
                        }
                    });
               }
                break;
            default:
                break;
        }
    }

    private static void setElemValue(final Step action, String flowId,
            final Element element,
            String value, final AsyncCallback<Object> valueFeedCb) {
        setElementValue(element, value, flowId, action.step(),
                new AsyncCallback<Object>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        valueFeedCb.onFailure(caught);
                    }

                    @Override
                    public void onSuccess(Object result) {
                        valueFeedCb.onSuccess(result);
                    }
                });
    }
    
    /**
     * 
     * Currently handling only input elements, will later work on it for
     * handling custom elements and select boxes also
     * 
     */
    private static void setElementValue(Element element, String value,
            String flowId, int stepNo, final AsyncCallback<Object> cb) {
        Element bestMatch = FinderUtil.getInputElement(element);
        DataFeeder feeder = AppFactory.getFeederForInput(bestMatch);
        if (null != feeder) {
            Map<String, String> data = new HashMap<String, String>();
            data.put("value", value);
            feeder.feedData(bestMatch, data, new AsyncCallback<Object>() {

                @Override
                public void onFailure(Throwable caught) {
                    cb.onFailure(caught);
                }

                @Override
                public void onSuccess(Object result) {
                    cb.onSuccess(result);
                }
            });
        } else if (bestMatch != null) {
            try {
                String lastValue = "";
                switch (bestMatch.getTagName().toLowerCase()) {
                    case "input":
                        lastValue = ((InputElement) bestMatch).getValue();
                        AppController.firePreInputEvents(bestMatch,lastValue);
                        ((InputElement) bestMatch).setValue(value);
                        break;
                    case "textarea":
                        lastValue = ((TextAreaElement) bestMatch).getValue();
                        ((TextAreaElement) bestMatch).setValue(value);
                        break;
                    default:
                        if (!Finder.isContentEditable(bestMatch)) {
                            return;
                        }
                        bestMatch.setInnerText(value);
                        break;
                }
                // Customizer APIs follow zero-based step numbering
                if (!Customizer.onAfterDataEntry(bestMatch, value, flowId,
                        stepNo - 1)) {
                    List<String> events = AppController
                            .getAutoInputEvents(bestMatch);
                    CustomEvents.dispatchSpecialChangeEvent(bestMatch,
                            lastValue, true, false, false, events);
                }
                cb.onSuccess(null);
            } catch (Exception e) {
                LOGGER.log(WfxLogLevel.DEBUG, AutoExecuter.class,
                        "setElementValue", WfxModules.JIT,
                        "Caught Exception {0}", e);
                cb.onFailure(e);
            }
        }
    }
    
    private static void setElementValueForDropDown(final Element element,
            String inputValue, final Step action,
            final AsyncCallback<Object> cb) {
        if (Finder.isSelect(element)) {
            selectOptionFromDropdown(SelectElement.as(element), inputValue, cb);
        } else if (Finder.elementContainsSelect(element)) {
            SelectElement bestMatch = SelectElement.as(
                    element.getElementsByTagName(SelectElement.TAG).getItem(0));
            selectOptionFromDropdown(bestMatch, inputValue, cb);
        } else {
            DataFeeder feeder = AppFactory.getFeederForDropdown();
            if (null != feeder) {
                Map<String, String> data = new HashMap<>();
                data.put("value", inputValue);
                feeder.feedData(element, data, new AsyncCallback<Object>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        cb.onFailure(caught);
                    }

                    @Override
                    public void onSuccess(Object result) {
                        cb.onSuccess(result);
                    }
                });
            }
        }
    }

    protected static void selectOptionFromDropdown(final SelectElement dropdown,
            String inputValue, final AsyncCallback<Object> cb) {
        NodeList<OptionElement> options = dropdown.getOptions();
        for (int index = 0; index < options.getLength(); index++) {
            OptionElement option = options.getItem(index);
            if (option.getText().equals(inputValue)) {
                dropdown.setSelectedIndex(index);
                CustomEvents.dispatchEvent(dropdown, BrowserEvents.CHANGE, true,
                        false, false);
                cb.onSuccess(null);
                break;
            }
        }
    }

    public static AutoExecutionActions getAction(int auto_exec_action,
            int step_completion_action) {
        AutoExecutionActions autoExecutionAction = AutoExecutionActions
                .get(auto_exec_action);
       if (autoExecutionAction == null) {
            AutoExecutionActions autoExecutionFallbackAction = actionFallbackMapping
                    .get(StepCompletionActions.get(step_completion_action));
            return (autoExecutionFallbackAction != null)
                    ? autoExecutionFallbackAction
                    : AutoExecutionActions.DO_NOT_EXECUTE;
        }
        return autoExecutionAction;
    }

    public static String collectData(Element element) {

        if (element instanceof InputElement
                || element instanceof TextAreaElement) {
            if (element instanceof InputElement) {
                return ((InputElement) element).getValue();
            } else {
                return ((TextAreaElement) element).getValue();
            }
        }
        return element.getInnerText(); // For Default Elements
    }
}
