package co.quicko.whatfix.overlay;

import co.quicko.whatfix.common.EventBus.Event;

public class OnPopupShowEvent implements Event {

    private int action;
    private int stepNo;

    public OnPopupShowEvent(int action, int stepNo) {
        this.setAction(action);
        this.setStepNo(stepNo);
    }

    public int getAction() {
        return action;
    }

    public int getStepNo() {
        return stepNo;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public void setStepNo(int stepNo) {
        this.stepNo = stepNo;
    }

}