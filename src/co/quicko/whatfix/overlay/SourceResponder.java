package co.quicko.whatfix.overlay;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.ActionerInitData;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.PossibleElementList;
import co.quicko.whatfix.overlay.Actioner.CrossObserver;
import co.quicko.whatfix.overlay.Actioner.SourceErrorReller;
import co.quicko.whatfix.overlay.Actioner.SourceReller;
import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;
import co.quicko.whatfix.overlay.actioner.ActionerPopover;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.FindListener;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.FinderUtil;
import co.quicko.whatfix.workflowengine.data.finder.FinderLogger;

class SourceResponder extends MiddleResponder {

    private JavaScriptObjectExt branchingEvaluatedData;

    public JavaScriptObjectExt getBranchingEvaluatedData() {
        return branchingEvaluatedData;
    }
    
    public SourceResponder(Draft draft, int step, int parent,
            FindListener... listeners) {
        super(draft, step, parent, listeners);
        branchingEvaluatedData = DataUtil.create();
        setFocusToElement();
    }

    @Override
    public JsArray<JavaScriptObjectExt> find(
            final AsyncCallback<Boolean> laterCb) {
        long sTime = System.currentTimeMillis();
        PossibleElementList<Element> matches = Finder
                .findBest(Document.get(), draft, action);
        getLogger().log(WfxLogLevel.PERFORMANCE, SourceResponder.class, "find",
                WfxModules.WORKFLOW_ENGINE,
                "Time taken by findBest method of Finder: {0} ms",
                (System.currentTimeMillis() - sTime));
        return initializeStep(laterCb, matches);
    }

    @Override
    protected void addSubscriber() {
        mutationBus.addSubscriber(this);
    }

    @Override
    public JsArray<JavaScriptObjectExt> initializeStep(
            final AsyncCallback<Boolean> laterCb,
            PossibleElementList<Element> matches) {
        if (!FinderUtil.isFound(matches)) {
            return FinderLogger.getFinderFailureComponent(action.step(),
                    action.type());
        }
        wrapElement(matches.getElements(), new CrossObserver(this));
        match = matches.get(0);
        Coordniates coords = DataUtil.create();
        coords.init(match, 0);
        coords.needScroll(
                needScroll(match, Actioner.alignTop(action.placement()),
                        Actioner.alignRight(action.placement())));
        ActionerInitData actionerInitData = DataUtil.create();
        actionerInitData.setFlowId(draft.flow_id());
        actionerInitData.setStep(action.step());
        actionerInitData.setCoords(StringUtils.stringifyObject(coords));
        addBranchingData(actionerInitData);
        if (ContentType.flow.name().equals(action.type())) {
            actionerInitData.setStepCount(matches.size());
        } else {
            actionerInitData.setStepFindData(getStepFindData());
        }
        CrossMessager.sendMessageToParent(ACTIONER_INIT,
                StringUtils.stringifyObject(actionerInitData));
        watchRel(match, getSourceReller(),
                Enterpriser.hasFeature(AdditionalFeatures.wcag_enable));
        if (findListeners != null) {
            for (FindListener listener : findListeners) {
                listener.postFind(action, matches.get(0), getStepFindData());
            }
        }
        return null;
    }
    
    private void addBranchingData(ActionerInitData actionerInitData) {
        fillSelectedElementBranchConditions();
        if (!branchingEvaluatedData.isEmpty()) {
            actionerInitData.setEvaluationData(
                    StringUtils.stringifyObject(branchingEvaluatedData));
        }
    }

    @Override
    public void onNext() {
        String message = getState();
        message = addBranchingData(message);
        CrossMessager.sendMessageToTop("actioner_next", message);
    }

    private String addBranchingData(String message) {
        fillSelectedElementBranchConditions();
        if (!branchingEvaluatedData.isEmpty()) {
            message += Actioner.SEPERATOR
                    + StringUtils.stringifyObject(branchingEvaluatedData);
        }
        return message;
    }

    private void fillSelectedElementBranchConditions() {
        Set<String> type = new HashSet<String>();
        type.add("element_selector");
        type.add("element_text");
        Actioner.evaluateAndFillElementConditions(action, branchingEvaluatedData,
                type);
    }

    @Override
    protected void scroll() {
        Common.scrollIntoView(match,
                Actioner.alignTop(ActionerPopover.placement(action.placement())));
    }

    @Override
    protected boolean canEnableNewSmartTips() {
        return helper.canEnableNewSmartTips();
    }

    @Override
    protected Helper getHelperStatic() {
        return new HelperStatic();
    }

    private SourceReller getSourceReller() {
        return helper.isValidator() ? new SourceErrorReller(parent, this)
                : new SourceReller(parent, this);
    }

    @Override
    protected void onTipInitialized() {
        if (this.isMutationBased && relocator != null) {
            relocate(false);
        }
    }
}
