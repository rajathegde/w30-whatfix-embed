package co.quicko.whatfix.overlay;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.PossibleElementList;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.Actioner.PopperListener;
import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;

public interface Responder extends PopperListener {
    public void startSearcher();

    /**
     * 
     * @param laterCb
     * @return Object(reason) in case of failure. If success it will return
     *         null
     */
    public JsArray<JavaScriptObjectExt> find(AsyncCallback<Boolean> laterCb);
    
    public void setLastMissOptional(boolean optional);

    public boolean isLastMissOptional();

    public void destroy();

    public boolean isForCollection();

    public ContentType type();

    public boolean isForState(String flowId, int step);

    public ResponderHelper getHelper();

    public Draft getDraft();

    public Step getFullStep();
    
    public Element getElement();

    public int getStep();

    public String getState();

    public String getState(Coordniates coords);
    
    public String getState(String info);

    void relocate(boolean isScrollEvent);

    default JsArray<JavaScriptObjectExt> initializeStep(
            final AsyncCallback<Boolean> laterCb,
            PossibleElementList<Element> matches) {
        return null;
    }

    boolean isElementFound();

    public void startStepFindTimer();

    public void setStepFindData(JavaScriptObjectExt stepFindData);

    public Element getLastTrackedFrameElement();

    public void setLastTrackedFrameElement(Element element);
}
