package co.quicko.whatfix.overlay;

import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.editor.AutoExecutionActions;
import co.quicko.whatfix.common.editor.TipType;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.AssistantConfig;
import co.quicko.whatfix.data.AutoExecutionConfig;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Draft.Condition;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.Themer.JsTheme;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer.ValidationEventResult;
import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;
import co.quicko.whatfix.overlay.Popover.ClickRegister;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.ScrollEventHandler.ScrollListener;
import co.quicko.whatfix.overlay.actioner.*;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;
import co.quicko.whatfix.overlay.actioner.MutationBus;
import co.quicko.whatfix.overlay.actioner.Relocator;
import co.quicko.whatfix.overlay.actioner.Relocator.Reller;
import co.quicko.whatfix.overlay.actioner.RelocatorInfo;
import co.quicko.whatfix.overlay.actioner.RelocatorStatic;
import co.quicko.whatfix.overlay.actioner.Searcher;
import co.quicko.whatfix.overlay.actioner.SearcherDesktop;
import co.quicko.whatfix.overlay.actioner.SearcherStatic;
import co.quicko.whatfix.overlay.actioner.StepConfigurations;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap;
import co.quicko.whatfix.overlay.elementwrap.WrapObserver;
import co.quicko.whatfix.overlay.elementwrap.WrapperFactory;
import co.quicko.whatfix.player.assistant.AssistantTip;
import co.quicko.whatfix.player.assistant.event.AssistantTipClosedEvent;
import co.quicko.whatfix.player.assistant.event.AssistantTipMissEvent;
import co.quicko.whatfix.player.assistant.event.AssistantTipShowedEvent;
import co.quicko.whatfix.player.beacon.event.BeaconClosedEvent;
import co.quicko.whatfix.player.beacon.event.BeaconShowedEvent;
import co.quicko.whatfix.player.event.StepCompleteActionInState;
import co.quicko.whatfix.player.tip.event.SmartTipAutoCompletedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipClosedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipMissedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipShowedEvent;
import co.quicko.whatfix.player.tip.event.ValidationTipErrorEvent;
import co.quicko.whatfix.player.tip.event.ValidationTipRectifiedEvent;
import co.quicko.whatfix.player.tip.event.ValidationTipValidEvent;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.tracker.useraction.event.UserActionCompleteEvent;
import co.quicko.whatfix.tracker.useraction.event.UserActionMissEvent;
import co.quicko.whatfix.workflowengine.FindListener;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.app.AppFactory;
import co.quicko.whatfix.workflowengine.data.strategy.Strategies;

public abstract class AbstractResponder implements Responder {
    protected Draft draft;
    protected Step action;
    protected Searcher searcher;
    protected ElementWrap wrap;
    protected Relocator relocator;
    protected Helper helper;
    protected ArrayList<FindListener> findListeners;
    private boolean isElementFound;
    protected boolean isMutationBased;
    private Element element;
    protected MutationBus mutationBus;
    private Element lastTrackedFrameElement; 

    private JavaScriptObjectExt stepFindData;
    private long sTime;
    private long searchStartTime;
    private boolean removeTabindexAttribute;

    public AbstractResponder(Draft draft, int step,
            FindListener... listeners) {
        this.draft = draft;
        this.action = draft.step(step);
        this.helper = initHelper();
        this.searcher = initSearcher(step);
        this.findListeners = new ArrayList<>();
        if(listeners != null) {
            this.findListeners.addAll(Arrays.asList(listeners));
        }
        this.isElementFound = false;
        this.isMutationBased = canEnableNewSmartTips();
        if (this.isMutationBased) {
            this.mutationBus = MutationBus.getInstance();
            this.findListeners.add(new ElementFoundListner());
        }
    }
    
    /*
     * Sets isElementFound to true and element 
     * after the element is found,using the postFind hook
     */
    class ElementFoundListner implements FindListener {
        @Override
        public void postFind(Step step, Element best,
                JavaScriptObjectExt data) {
            isElementFound = true;
            element = best;
            stepFindData = data;
        }

        @Override
        public void preFind(Step step) {
        }
    }

    private Helper initHelper() {
        if (AppFactory.isDesktopApp()) {
            return initHelperDesktop();
        } else if (ContentType.tip == type()) {
            return initHelperStatic();
        } else if (ContentType.beacon == type()) {
            return getHelperBeacon();
        } else if (ContentType.USER_ACTION == type()) {
            return getHelperUserAction();
        } else if (ContentType.assistant == type()) {
            return getHelperAssistant();
        } else {
            if (AutoExecutionConfig.isAutoTest(draft.auto_execution_mode())
                    || action.auto_recorded_step()) {
                return new AutoTestingHelper(this);
            } else if (draft.recapture_mode()) {
                return new AutoRecaptureHelper(this);
            } else if (!draft.manual_execution_mode()
                    && AutoExecutionConfig.isAutoExecutableStep(action)) {
                return new AutoExecutionHelper(this);
            }
            return new Helper();
        }
    }

    private Searcher initSearcher(int step) {
        boolean optional = draft.step_optional(step);
        boolean selector = (draft.stepHasSelector(step)
                || ((Customizer.getTrustId()
                        || draft.trust_id_code() == Finder.TRUST)
                        && Finder.idMark(draft.step_marks(step)) != null));
        return helper.initSearcher(optional, selector);
    }

    private Helper initHelperStatic() {
        if (action.action() == TipType.STATIC_INFO.getValue()) {
            return getHelperInfoStatic();
        }
        return getHelperStatic();
    }

    protected Helper getHelperStatic() {
        return new HelperStatic();
    }
    
    protected Helper initHelperDesktop() {
        return new HelperDesktop();
    }

    protected Helper getHelperInfoStatic() {
        return new HelperInfoStatic();
    }

    private Helper getHelperBeacon() {
        return new HelperBeaconInfo();
    }
    
    private Helper getHelperUserAction() {
        return new HelperUserAction();
    }

    private Helper getHelperAssistant() {
        return new HelperAssistant();
    }

    @Override
    public void startSearcher() {
        searcher.setResponder(this);

        // checking if new smart tips is enabled
        if (this.isMutationBased) {
            addSubscriber();
        } else {
            if (this instanceof DirectResponder
                    || this instanceof TopResponder) {
                this.searchStartTime = System.currentTimeMillis();
            }
            if (searcher.execute()) {
                Actioner.impl.scheduleFixedDelay(searcher,
                        helper.getSearcherGap());
            }
        }
    }

    /**
     * Implementation only in Direct and Source Responder
     */
    protected abstract void addSubscriber();

    @Override
    public boolean isLastMissOptional() {
        return helper.isLastMissOptional();
    }

    @Override
    public void setLastMissOptional(boolean optional) {
        helper.setLastMissOptional(optional);
    }

    @Override
    public void destroy() {
        if (searcher != null) {
            searcher.stop();
            searcher = null;
        }

        if (wrap != null) {
            wrap.dispose();
            wrap = null;
        }

        if (relocator != null) {
            Relocator temp = relocator;
            relocator = null;
            temp.stop();
        }

        if (element != null) {
            removeFocusEventListener(this, element);
            element = null;
        }

        if (mutationBus != null) {
            mutationBus.removeSubscriber(this);
        }

        helper.dispose();
    }

    @Override
    public ContentType type() {
        return draft.getType();
    }

    @Override
    public boolean isForCollection() {
        return ContentType.tip == type() || ContentType.beacon == type()
                || ContentType.USER_ACTION == type();
    }

    @Override
    public boolean isForState(String flowId, int step) {
        return (flowId == this.draft.flow_id()
                && step == this.action.step());
    }

    @Override
    public ResponderHelper getHelper() {
        return helper;
    }
    
    public Element getElement() {
        return this.element;
    }
    
    @Override
    public boolean isElementFound() {
        return this.isElementFound;
    }

    @Override
    public void relocate(boolean isScrollEvent) {
        if (this.relocator == null) {
            return;
        }
        if (this.relocator instanceof ScrollListener) {
            ((ScrollListener) this.relocator).onScrollStopped();
        }
        this.relocator.setSameCheckNeeded(!isScrollEvent);
        long sTime = System.currentTimeMillis();
        boolean elementFound = this.relocator.execute();
        getLogger().log(WfxLogLevel.PERFORMANCE, AbstractResponder.class,
                "relocate", WfxModules.JIT,
                "Time taken by Relocator's execute method: {0}",
                (System.currentTimeMillis() - sTime));
        if (elementFound) {
            mutationBus.attachShadowListeners(this);
        }
    }
    
    protected boolean needScroll(Element element, boolean alignTop,
            boolean alignRight) {
        if (OverlayUtil.needScroll(element,
                (alignTop ? Actioner.OVERLAY_AVG_HEIGHT : 0),
                (alignRight ? Actioner.OVERLAY_AVG_WIDTH : 0))) {
            getLogger().log(WfxLogLevel.DEBUG, AbstractResponder.class,
                    "needScroll", WfxModules.JIT,
                    "Need to scroll element in viewport");
            return true;
        }
        boolean isOverlapped = OverlayUtil.isOverlapped(element, false);
        getLogger().log(WfxLogLevel.DEBUG, AbstractResponder.class,
                "needScroll", WfxModules.JIT, "isOverlapped: {0}",
                isOverlapped);
        return isOverlapped;
    }

    private void setupSpotlight() {
        Actioner.hideSpotlightTimer.cancel();
        if (Actioner.mouseOutRegisters != null) {
            for (HandlerRegistration register : Actioner.mouseOutRegisters) {
                if (register != null) {
                    register.removeHandler();
                }
            }
            Actioner.mouseOutRegisters = null;
        }

        JsTheme customTheme = Themer.getJSTheme();
        if (null != customTheme
                && StringUtils.isNotBlank(customTheme.value(Themer.OPACITY))) {
            // let the spotlight opacity setup done by user in AC be there
        } else if (null != draft.step_spotlight_properties(action.step())
                && draft.step_spotlight_properties(action.step())
                        .customOpacityEnabled().equals("true")) {
            String opacityValue = draft.step_spotlight_properties(action.step())
                    .customOpacityValue();
            String currentOpacity = getCurrentSpotlightOpacity();
            if (!opacityValue.isEmpty()) {
                Double opacityValueInPercent = Double.parseDouble(opacityValue);
                if (null != currentOpacity
                        && !currentOpacity.equals(opacityValue)) {
                    Actioner.setupSpotlightWithOpacity(
                            opacityValueInPercent / 100);
                }
            } else {
                // need to reconstruct the spotlight as it might have a
                // different opacity in the previous step
                if (null != currentOpacity && !currentOpacity
                        .equals(Themer.defaultValue("opacity"))) {
                    Actioner.makeSpotlights();
                }
            }
        } else {
            // when opacity is set for a step and this shouldn't be used in
            // following steps with no opacity set
            String currentOpacity = getCurrentSpotlightOpacity();
            if (currentOpacity != null
                    && currentOpacity != Themer.defaultValue("opacity"))
            {
                Actioner.makeSpotlights();
            }
        }

        if (FullActionerPopover.SPOTLIGHT_ON_NONSTICKY
                .equals(Customizer.getSpotlight())
                || (draft.step_spotlight_properties(action.step()) != null
                        && draft.step_spotlight_properties(action.step())
                                .hoverStatus() != null
                        && draft.step_spotlight_properties(action.step())
                                .hoverStatus()
                                .equals(FullActionerPopover.SPOTLIGHT_ON_NONSTICKY))) {
            Actioner.mouseOutRegisters = new HandlerRegistration[Actioner.spotlights.length];
            for (int i = 0; i < Actioner.mouseOutRegisters.length; i++) {
                Actioner.mouseOutRegisters[i] = Actioner.spotlights[i]
                        .addDomHandler(new MouseOutHandler() {
                            @Override
                            public void onMouseOut(MouseOutEvent event) {
                                helper.onMouseOut(event);
                            }
                        }, MouseOutEvent.getType());
            }
        }
    }

    private String getCurrentSpotlightOpacity() {
        if (null != Actioner.spotlights && Actioner.spotlights.length > 0) {
            return Actioner.spotlights[0].getElement().getStyle().getOpacity();
        }
        return null;
    }

    @Override
    public Draft getDraft() {
        return this.draft;
    }

    @Override
    public Step getFullStep() {
        return this.action;
    }

    @Override
    public int getStep() {
        return this.action.step();
    }

    @Override
    public String getState() {
        return draft.flow_id() + Actioner.SEPERATOR + action.step();
    }

    @Override
    public String getState(Coordniates coords) {
        return getState() + Actioner.SEPERATOR + StringUtils.stringifyObject(coords);
    }

    @Override
    public String getState(String info) {
        return getState() + Actioner.SEPERATOR + info;
    }

    protected void wrapElement(List<Element> elements, WrapObserver obsr) {
        Map<String, Boolean>  runModes = new HashMap<>();
        runModes.put(WrapperFactory.MANUAL_EXECUTION_MODE, draft.manual_execution_mode());
        runModes.put(WrapperFactory.RECAPTURE_MODE,draft.recapture_mode());
        wrap = WrapperFactory.getElementWrapper(helper.getStepActions(),
                elements, obsr, Actioner.impl, helper.isValidator(), action,
                runModes);
    }

    protected void watchRel(final Element element, Reller reller,
            boolean focusNeeded) {
        setWcagIfNeeded(element, focusNeeded);
        relocator = helper.getRelocator(element, reller);
        if(!this.isMutationBased) {
        	Actioner.impl.scheduleFixedDelay(relocator, relocator.getGap());
        }
    }
    
    /*
     * Default value is false
     * Override in DirectResponder and SourceResponder
     */
    protected boolean canEnableNewSmartTips() {
        return false;
    }
    
    private void setWcagIfNeeded(final Element element, boolean focusNeeded) {
        if (focusNeeded && draft.type().equals(ContentType.flow.toString())) {
            this.element = element;
            addFocusEventListener(this, element);
            sendMessageToPopover("first_time_focus");
        }
    }

    @Override
    public void onShowed(Element shown) {
        helper.handleShowed(shown);
    }

    @Override
    public void onNext() {
        helper.handleNext();
    }

    @Override
    public void onBack() {
        helper.handleBack();
    }

    @Override
    public void onOptionalNext() {
        helper.handleOptionalNext();
    }

    @Override
    public void onMiss(JsArray<JavaScriptObjectExt> reason) {
        helper.handleMiss(reason);
    }

    @Override
    public void onClose() {
        helper.handleClose();
    }
    
    public void setFocusToElement() {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                if (element != null && "onFocus".equals(content)) {
                    removeTabindexAttribute = !element.hasAttribute("tabindex");
                    element.setTabIndex(element.getTabIndex());
                    element.focus();
                    Scheduler.get().scheduleFixedDelay(() -> {
                        element.focus();
                        return false;
                    }, 100);
                }
            }
        }, "focus_on_element");
    }

    
    //on Shift+Tab key or Up arrow key, focus is sent back to toolTip
    private native void addFocusEventListener(AbstractResponder responder, Element source) /*-{
        if (!$wnd._wfx_elementFocusListener) {
             $wnd._wfx_elementFocusListener = function(e) {
                 if ((e.shiftKey && e.keyCode == "9") || e.keyCode == "38" || e.keyCode == "37") {
                     @co.quicko.whatfix.overlay.AbstractResponder::sendMessageToPopover(Ljava/lang/String;)('true');
                     responder.@co.quicko.whatfix.overlay.AbstractResponder::removeTabindexAttributeIfNeeded()();
                 }
             };
         }
         if (source) {
             source.addEventListener('keydown', $wnd._wfx_elementFocusListener);
         }              
    }-*/;
    
    private native void removeFocusEventListener(AbstractResponder responder, Element source) /*-{
         if ($wnd._wfx_elementFocusListener) {
             source.focus();
             source.removeEventListener('keydown', $wnd._wfx_elementFocusListener);
             $wnd._wfx_elementFocusListener = undefined;
             responder.@co.quicko.whatfix.overlay.AbstractResponder::removeTabindexAttributeIfNeeded()();
         }
    }-*/;


    public static void sendMessageToPopover(String message) {
        CrossMessager.sendMessageToTop("show_focus_on_tooltip", message);
    }      

    public void removeTabindexAttributeIfNeeded() {
        if (removeTabindexAttribute && null != element) {
            element.removeAttribute("tabindex");
        }
    }

    @Override
    public Element getLastTrackedFrameElement() {
        return this.lastTrackedFrameElement;
    }

    @Override
    public void setLastTrackedFrameElement(Element element) {
        this.lastTrackedFrameElement = element;
    }

    protected abstract int getParent();
    
    protected class HelperDesktop extends Helper {
        
        private SearcherDesktop searcher;
        
        @Override
        protected Searcher initSearcher(boolean optional,
                boolean selector) {
            searcher = new SearcherDesktop(optional, selector);
            return searcher;
        }
        
        @Override
        public void showPop(Coordniates coords) {
            setupSpotlight();

            popper = new FullActionerPopover(Actioner.impl,
                    AbstractResponder.this, draft, action, coords.top(),
                    coords.right(), coords.bottom(), coords.left(),
                    coords.offsetWidth(), coords.offsetHeight(),
                    Actioner.spotlights);
            handleShowed(null);
        }

        /**
         * decides the gap in which the searcher should be called again.
         */
        @Override
        public int getSearcherGap() {
            /**
             * if customizer has the value retry_gap_in_ms set, then it will
             * take that value otherwise return the preconfigured value.
             */
            return StepConfigurations.getDesktopRetryGaps();
        }
        
        @Override
        public void dispose() {
            searcher.stop();
            super.dispose();
        }
        
        @Override
        public void handleClose() {
            searcher.stop();
            super.handleClose();
        }
    }
    
    protected class Helper implements ResponderHelper {
        protected ActionerPopover popper;

        @Override
        public boolean supportsSpotlight() {
            return true;
        }

        protected Searcher initSearcher(boolean optional,
                boolean selector) {
            return new Searcher(optional, selector);
        }

        public void showPop(Element element) {
            if (branchBeforeShow()) {
                return;
            }
            setupSpotlight();
            popper = new FullActionerPopover(Actioner.impl, AbstractResponder.this,
                    draft, action, Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element), Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight(), Actioner.spotlights);
            handleShowed(element);
        }

        public void showPop(Coordniates coords) {
            if (branchBeforeShow()) {
                return;
            }
            setupSpotlight();

            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            popper = new FullActionerPopover(Actioner.impl, AbstractResponder.this,
                    draft, action, coords.top() + scrollTop,
                    coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight(),
                    Actioner.spotlights);
            handleShowed(null);
        }
        
        private boolean branchBeforeShow() {
            return Actioner.listener.branchBeforeShow(action, draft);
        }

        public void placePop(Element element) {
            if (popper != null) {
                popper.show(Actioner.getAbsoluteTop(element), Actioner.getAbsoluteRight(element),
                    Actioner.getAbsoluteBottom(element), Actioner.getAbsoluteLeft(element),
                    element.getOffsetWidth(), element.getOffsetHeight(),
                    ActionerPopover.placement(
                            draft.step_placement(action.step())));
            }
        }

        public void placePop(Coordniates coords) {
            if (popper != null) {
                int scrollLeft = Window.getScrollLeft();
                int scrollTop = Window.getScrollTop();
                popper.show(coords.top() + scrollTop,
                        coords.right() + scrollLeft,
                        coords.bottom() + scrollTop, coords.left() + scrollLeft,
                        coords.offsetWidth(), coords.offsetHeight(),
                        ActionerPopover.placement(action.placement()));
            }
        }

        @Override
        public void movePop(Element element) {
            placePop(element);
        }

        @Override
        public void movePop(Coordniates coords) {
            placePop(coords);
        }

        public int getSearcherGap() {
            return StepConfigurations.getFlowRetryGaps();
        }

        public Relocator getRelocator(final Element element,
                Reller reller) {
            return new Relocator(element, reller);
        }

        public void showEngagers() {
            if (popper != null) {
                ((FullActionerPopover) popper).showEngagers();
            }
        }

        public void hideEngagers() {
            if (popper != null) {
                ((FullActionerPopover) popper).hideEngagers();
            }
        }

        public void hideOnOverlap(Element element) {
            if (popper != null) {
                popper.hideOnOverlap();
            }
        }

        public void hidePopper() {
            if (popper != null) {
                popper.hide();
            }
        }

        public void handleShowed(Element shown) {
            Actioner.listener.onShowed(shown);
        }

        public void handleClose() {
            Actioner.listener.onClose();
        }

        @Override
        public void handleNext() {
            Actioner.listener.onNext(action);
        }

        @Override
        public void handleBack() {
            Actioner.listener.onBack(action);
        }

        public void handleOptionalNext() {
            Actioner.listener.onOptionalNext();
        }

        public void handleMiss(JsArray<JavaScriptObjectExt> reason) {
            Actioner.listener.onMiss(reason);
        }

        public void dispose() {
            if (popper != null) {
                popper.dispose();
                popper = null;
            }
        }

        public void onMouseOut(MouseOutEvent event) {
            popper.onMouseOut(event);
        }

        public ActionerPopover getPopper() {
            return popper;
        }

        public int getStepAction() {
            return action.action();
        }

        public List<Integer> getStepActions() {
            return action.getCompletionActions(getStepAction(),
                        action.completion_actions());
        }

        protected boolean isLastMissOptional() {
            return Actioner.missedOptional;
        }

        protected void setLastMissOptional(boolean optional) {
            Actioner.missedOptional = optional;
        }

        @Override
        public void init(List<Element> matches) {
        }

        @Override
        public void init(Coordniates coords) {
        }

        @Override
        public void hideOnScroll() {
        }

        @Override
        public void setActionElement(Element actionElement) {
            action.action_element(actionElement);
        }

        @Override
        public boolean isValid() {
            return true;
        }

        @Override
        public void onValidationError() {
        }
        
        @Override
        public void validate(Element element) {
        }

        @Override
        public void onStepCompletion(String action) {
            // handling analytics for flow step completion.
            EventBus.fire(new StepCompleteActionInState(action));
        }

        @Override
        public void showErrorPopup(Coordniates coords) {
        }

        @Override
        public void showErrorEngagers(Coordniates coords) {
        }

        @Override
        public void hideErrorEngagers() {
        }

        @Override
        public void hideErrorPopup() {
        }

        public boolean isValidator() {
            return false;
        }

        @Override
        public void setIFramesCointainerZIndex(int z_index) {
        }

        @Override
        public int getIFramesCointainerZIndex() {
            return 0;
        }

        @Override
        public void showErrorPopup(Element element) {
        }

        @Override
        public void showErrorEngagers(Element element) {
        }

        @Override
        public void disposeErrorPopper(boolean isFromDestroy) {
        }

        @Override
        public boolean hideOnScrollEnabled(Element element) {
            return false;
        }
        
        @Override
        public void onStabilized(Element element) {
            
        }

        @Override
        public void setWcagListener(Element element) {
        }

        @Override
        public void handleKeyDownListener() {
        }

        @Override
        public void placePopAndSetWcag(Element element) {
        }
        
        /*
         * Default value is false, Override in HelperStatic
         */
        public boolean canEnableNewSmartTips() {
            return false;
        }
    }
    
    protected class AutoExecutionHelper extends Helper {

        protected Responder responder;
        
        private WrapAutoExecTimer scheduleAutoExecTimer;
        
        public AutoExecutionHelper(Responder responder) {
            this.responder = responder;
        }

        @Override
        protected Searcher initSearcher(boolean optional,
                boolean selector) {
            return new AutoExecutionSearcher(optional, selector);
        }

        @Override
        public Relocator getRelocator(Element element, Reller reller) {
            String type = draft.auto_execution_mode();
            if (StringUtils.isBlank(type)) {
                type = AutoExecutionConfig.AUTO_EXEC;
            }
            return new AutoExecutionStabilizingRelocator(element, reller,
                    type);
        }
        
        @Override
        public void onStabilized(Element element) {
            // FEED DATA + Take action
            autoExecuteCallBack(element);
        }

        protected void autoExecuteCallBack(Element element) {
            AutoExecuter.autoExecute(draft, action, element,
                    new AsyncCallback<Boolean>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            JavaScriptObjectExt reason = DataUtil.create();
                            if (null != caught && StringUtils
                                    .isNotBlank(caught.getMessage())) {
                                reason.put("reason", caught.getMessage());
                            }
                            JsArray<JavaScriptObjectExt> failureArray = JavaScriptObject
                                    .createArray().cast();
                            failureArray.push((reason));
                            responder.onMiss(failureArray);
                        }

                        @Override
                        public void onSuccess(Boolean needOnNext) {
                            if (needOnNext) {
                                responder.onNext();
                            }
                        }
                    });
        }
        
        @Override
        public int getStepAction() {
            if(action.auto_execute_action() == 3) {
                return 4;
            }
            return action.auto_execute_action();
        }
        
        @Override
        public List<Integer> getStepActions() {
            return JsUtils
                    .toIntegerList(action.getActionArray(getStepAction()));
        }
        
        @Override
        public void showEngagers() {
        }

        @Override
        public void hideEngagers() {
        }

        @Override
        public void showPop(Element element) {
            if (!isAutoExecGlassEnabled()) {
                setupSpotlight();
            }
            scheduleAutoExecTimer = new WrapAutoExecTimer(helper, action,
                    AutoExecGlass.AUTO_GLASS_HIDE_INTERVAL);
            popper = new AutoActionerPopover(Actioner.impl,
                    AbstractResponder.this, draft, action,
                    Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element),
                    Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight(), Actioner.spotlights);
            handleShowed(element);
        }       
        
        @Override
        public void showPop(Coordniates coords) {
            if (!isAutoExecGlassEnabled()) {
                setupSpotlight();
            }
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            scheduleAutoExecTimer = new WrapAutoExecTimer(helper, action,
                    AutoExecGlass.AUTO_GLASS_HIDE_INTERVAL);
            popper = new AutoActionerPopover(Actioner.impl,
                    AbstractResponder.this, draft, action,
                    coords.top() + scrollTop, coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight(),
                    Actioner.spotlights);
            handleShowed(null);
        }

        private boolean isAutoExecGlassEnabled() {
            return Themer.isAutomationUIRequired();
        }

        @Override
        public int getSearcherGap() {
            return StepConfigurations.getRetryGaps();
        }
        
        /*
         * This method is called if step Completion does not complete even after
         * 5 seconds after Step is found.
         */
        public void showAlternateTooltip() {
            if (popper != null) {
                ((AutoActionerPopover) popper).showAlterateTooltip();
            }
        }

        @Override
        public void onStepCompletion(String action) {           
            super.onStepCompletion(action);
            dispose();
        }
        
        @Override
        public void handleClose() {
           super.dispose();
        }
        
        @Override
        public void dispose() {  
            disposeAutoExecTimer();
            super.dispose();           
        }
              
        private void disposeAutoExecTimer() {
            if (scheduleAutoExecTimer != null) {
                scheduleAutoExecTimer.cancelTimer();
                scheduleAutoExecTimer = null;
            }
        }
     }
    protected class AutoTestingHelper extends AutoExecutionHelper {

        public AutoTestingHelper(Responder responder) {
            super(responder);
        }
        
        @Override
        public int getStepAction() {
            if (action.auto_execute_action() == -1
                    || action.auto_execute_action() == 0) {
                return action.action();
            }
            if (action.auto_execute_action() == 3) {
                return 4;
            }
            return action.auto_execute_action();
        }

        public void showPop(Element element) {
            setupSpotlight();
            popper = new FullActionerPopover(Actioner.impl,
                    AbstractResponder.this, draft, action,
                    Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element),
                    Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight(), Actioner.spotlights);
            handleShowed(element);
        }

        @Override
        public void showPop(Coordniates coords) {
            setupSpotlight();
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            popper = new FullActionerPopover(Actioner.impl,
                    AbstractResponder.this, draft, action,
                    coords.top() + scrollTop, coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight(),
                    Actioner.spotlights);
            handleShowed(null);
        }
    }
    
    private class AutoRecaptureHelper extends AutoTestingHelper {

        public AutoRecaptureHelper(Responder responder) {
            super(responder);
        }

        @Override
        public void onStabilized(Element element) {
            AsyncCallback<Void> callBack = new AsyncCallback<Void>() {
                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(Void result) {
                    autoExecuteCallBack(element);
                }
            };
            Actioner.listener.updateRecaptureStep(action, element, callBack);
        }

        @Override
        public int getStepAction() {
            return action.action();
        }
    }

    protected class HelperStatic extends Helper {
        protected boolean isValid = true;
        protected ErrorActionerPopover errorPopper;
        private Step stepForValidation;
        private CrossListener elementListener;
        protected boolean isMutationBased;

        public HelperStatic() {
            isMutationBased = this.canEnableNewSmartTips();
        }

        @Override
        public boolean canEnableNewSmartTips() {
            return Enterpriser.hasFeature(AdditionalFeatures.smart_tips_new)
                    && DOMUtil.isMutationObserverSupported();
        }

        @Override
        public boolean supportsSpotlight() {
            return false;
        }

        @Override
        protected Searcher initSearcher(boolean optional,
                boolean selector) {
            return new SearcherStatic(optional, selector);
        }

        public void showPop(Element element) {
            popper = new ActionerPopover(Actioner.impl, AbstractResponder.this,
                    draft, action, Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element), Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight());
            handleShowed(element);
        }

        public void showPop(Coordniates coords) {
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            popper = new ActionerPopover(Actioner.impl, AbstractResponder.this,
                    draft, action, coords.top() + scrollTop,
                    coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight());
            handleShowed(null);
        }

        @Override
        public void placePop(Element element) {
            if (popper != null) {
                super.placePop(element);
                handleShowed(element);
            } else {
                showPop(element);
            }
        }

        @Override
        public void placePop(Coordniates coords) {
            if (popper != null) {
                super.placePop(coords);
                handleShowed(null);
            } else {
                showPop(coords);
            }
        }

        @Override
        public void setWcagListener(Element element) {
            if (elementListener != null) {
                removeWcagListener();
            }
            elementListener = CrossMessager
                    .addListener(new CrossListener() {
                        @Override
                        public void onMessage(String type, String content) {
                            if (element != null) {
                                Scheduler.get().scheduleDeferred(
                                        new ScheduledCommand() {

                                            @Override
                                            public void execute() {
                                                element.focus();
                                                removeWcagListener();
                                                hidePopperOnFocusOut();
                                            }
                                        });
                            }
                        }
                    }, "focus_on_smart_tip_element");
        }
        
        protected void hidePopperOnFocusOut() {
        }
        
        @Override
        public List<Integer> getStepActions() {
            return JsUtils
                    .toIntegerList(action.getActionArray(getStepAction()));
        }

        public void removeWcagListener() {
            if (elementListener == null) {
                return;
            }
            CrossMessager.removeListener(elementListener,
                    "focus_on_smart_tip_element");
            elementListener = null;
        }

        @Override
        public void handleKeyDownListener() {
            CrossMessager.sendMessageToTop("show_focus_on_smarttip",
                    "focus");
        }

        @Override
        public void movePop(Element element) {
            if (popper != null && popper.isShowing()) {
                super.placePop(element);
            }
        }

        @Override
        public void movePop(Coordniates coords) {
            if (popper != null && popper.isShowing()) {
                super.placePop(coords);
            }
        };

        @Override
        public int getSearcherGap() {
            int depthOfSourceFrame = draft.step_parents(action.step());
            return (StepConfigurations.getRetryGaps()
                    * (depthOfSourceFrame - getParent() + 1));
        }

        @Override
        public Relocator getRelocator(Element element, Reller reller) {
            return new RelocatorStatic(element, reller,
                    action.auto_execute_action(),
                    isOverlapCheckNeeded(action.auto_execute_action()));
        }

        // For "On-hover" autohover smart tip, we wait until the element is in
        // viewport to perform the action. Reason: SUCC-4410.
        private boolean isOverlapCheckNeeded(int autoExecuteAction) {
            return AutoExecutionConfig.isAutoExecutableStep(autoExecuteAction)
                    && (autoExecuteAction == AutoExecutionActions.ON_HOVER
                            .getValue());
        }

        @Override
        public void showEngagers() {
        }

        @Override
        public void hideEngagers() {
        }

        @Override
        public void hideOnOverlap(Element element) {
            if (popper != null) {
                popper.hideOnOverlap();
            }
        }

        @Override
        public void handleShowed(Element shown) {
            EventBus.fire(new SmartTipShowedEvent(draft, action));
        }

        @Override
        public void handleMiss(JsArray<JavaScriptObjectExt> reason) {
            EventBus.fire(new SmartTipMissedEvent(draft, action, reason));
        }

        @Override
        public void onStepCompletion(String completionAction) {
            // handling analytics for smart tip auto completion.
            EventBus.fire(new SmartTipAutoCompletedEvent(draft, action,
                    completionAction));
        }

        @Override
        public void handleNext() {
        }

        @Override
        public void handleOptionalNext() {
        }

        @Override
        public void handleClose() {
            hidePopper();
            EventBus.fire(new SmartTipClosedEvent(draft, action));
        }

        @Override
        public void onMouseOut(MouseOutEvent event) {
        }

        @Override
        public int getStepAction() {
            return action.action() == TipType.VALIDATION_ONLY.getValue()
                    ? TipType.VALIDATION_ONLY.getValue()
                    : TipType.STATIC_ACTION.getValue();
        }

        @Override
        protected boolean isLastMissOptional() {
            return false;
        }

        @Override
        protected void setLastMissOptional(boolean optional) {
        }

        @Override
        public boolean isValid() {
            return this.isValid;
        }
        
        @Override
        public void onValidationError() {
            EventBus.fire(new ValidationTipErrorEvent(action, draft));
        }

        @Override
        public void validate(Element element) {
            this.isValid = true;
            populateStepForValidation(element);
            ValidationEventResult result = Customizer
                    .onValidate(draft.flow_id(), stepForValidation);
            if (result != null) {
                this.isValid = result.isValid();
                String validationMessage = result.validation_message();
                if (StringUtils.isNotBlank(validationMessage)) {
                    action.validation_errors_md(validationMessage);
                }
                return;
            }
            JsArray<Condition> conditions = action.data_validations();
            if (conditions != null && conditions.length() > 0) {
                if (!Strategies.isFit(stepForValidation, conditions)) {
                    this.isValid = false;
                }
            }
        }

        @Override
        public void showErrorPopup(Coordniates coords) {
            if (errorPopper == null) {
                initErrorActioner(coords);
            }
            if (popper != null) {
                popper.hide();
            }
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            errorPopper.show(coords.top() + scrollTop,
                    coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight(),
                    ErrorActionerPopover.PLACEMENT);
        }

        @Override
        public void hideErrorPopup() {
            if (errorPopper != null) {
                errorPopper.hideAfterMouseleave();
            }
        }

        @Override
        public void showErrorEngagers(Coordniates coords) {
            if (errorPopper == null) {
                initErrorActioner(coords);
            }
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            errorPopper.placeEngagers(coords.top() + scrollTop,
                    coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight());
            errorPopper.showEngagers();
        }

        @Override
        public void hideErrorEngagers() {
            if (errorPopper != null) {
                errorPopper.hideEngagers();
            }
        }

        @Override
        public void showErrorPopup(Element element) {
            if (errorPopper == null) {
                initErrorActioner(element);
            }
            if (popper != null) {
                popper.hide();
            }
            errorPopper.show(Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element), Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight(),
                    ErrorActionerPopover.PLACEMENT);
        }

        @Override
        public void showErrorEngagers(Element element) {
            if (errorPopper == null) {
                initErrorActioner(element);
            }
            errorPopper.placeEngagers(Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element), Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight());
            errorPopper.showEngagers();
        }

        private void initErrorActioner(Element element) {
            Step stepCopy = copyStep(action);
            errorPopper = new ErrorActionerPopover(
                    new ErrorPopoverExtender(), AbstractResponder.this,
                    draft, stepCopy, Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element), Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight());
        }

        private void initErrorActioner(Coordniates coords) {
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            Step stepCopy = copyStep(action);
            errorPopper = new ErrorActionerPopover(
                    new ErrorPopoverExtender(), AbstractResponder.this,
                    draft, stepCopy, coords.top() + scrollTop,
                    coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight());
        }

        // used specifically for error actioner popovers.
        private Step copyStep(Step step) {
            Step stepCopy = StringUtils.copyObject(step);
            stepCopy.description_md(step.validation_errors_md());
            stepCopy.note_md(null);
            return stepCopy;
        }

        /*
         * stepForValidation will be used for validation purposes only,
         * injecting action_element to action and creating copy of it
         * results in infinite loop for some apps, example : google.com
         */
        private void populateStepForValidation(Element element) {
            if(stepForValidation == null) {
                stepForValidation = StringUtils.copyObject(action);
            }
            stepForValidation.action_element(element);
        }

        @Override
        public boolean isValidator() {
            return action.data_validations() != null
                    && action.data_validations().length() > 0;
        }

        private class ErrorPopoverExtender extends PopoverExtender {
            @Override
            protected Popover makePopover(Step action, PopListener listener,
                    ClickRegister register, boolean alignment) {
                return new ErrorPopover(listener, register, alignment);
            }

            @Override
            protected StepPop stepPop(Step step, String footnote,
                    String placement, String stepnote) {
                return new ErrorPop(step, footnote, placement);
            }
        }

        @Override
        public void dispose() {
            disposeErrorPopper(true);
            super.dispose();
        }

        @Override
        public void hideOnScroll() {
            if (errorPopper != null) {
                errorPopper.hide();
            }
        }

        @Override
        public void disposeErrorPopper(boolean isFromDestroy) {
            if (errorPopper != null) {
                errorPopper.dispose();
                errorPopper = null;

                if (!isFromDestroy) {
                    EventBus.fire(new ValidationTipRectifiedEvent(action, draft));
                }
            } else if (!isFromDestroy) {
                EventBus.fire(new ValidationTipValidEvent(action, draft));
            }
        }

        @Override
        public void hidePopper() {
            if (popper != null && !Actioner.settings().staticClose()) {
                popper.hideAfterMouseleave();
            } else {
                super.hidePopper();
            }
            if (popper != null) {
                if (!popper.isShowing()
                        && !Actioner.settings.staticClose()) {
                    removeWcagListener();
                }
            }
        }

        @Override
        public boolean hideOnScrollEnabled(Element element) {
            return isValidator();
        }
        
        @Override
        public void onStabilized(Element element) {
            // FEED DATA + Take action
            AutoExecuter.autoExecute(draft, action, element,
                    new AsyncCallback<Boolean>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            JavaScriptObjectExt reason = DataUtil.create();
                            if (null != caught && StringUtils
                                    .isNotBlank(caught.getMessage())) {
                                reason.put("reason", caught.getMessage());
                            }
                        }
                        @Override
                        public void onSuccess(Boolean needOnNext) {
                            AutoExecutionActions autoExecutionAction = AutoExecuter
                                    .getAction(action.auto_execute_action(),
                                            action.action());
                            if (autoExecutionAction != null) {
                                wrap.listener.onStepCompletion(
                                        autoExecutionAction.getAction());
                            }
                        }
                    });
        }
    }

    protected class HelperInfoStatic extends HelperStatic {
        protected IconInfo info;
        int z_index;

        @Override
        public void init(List<Element> matches) {
            Element best = matches.get(0);
            helper.setIFramesCointainerZIndex(0);
            info = new SmartInfo(best, this, action, Actioner.getAbsoluteTop(best),
                    Actioner.getAbsoluteRight(best), Actioner.getAbsoluteBottom(best),
                    Actioner.getAbsoluteLeft(best));
        }

        @Override
        public void init(Coordniates coords) {
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            info = new SmartInfo(null, this, action,
                    coords.top() + scrollTop, coords.right() + scrollLeft,
                    coords.bottom() + scrollTop,
                    coords.left() + scrollLeft);
        }

        @Override
        public Relocator getRelocator(Element element, Reller reller) {
            return new RelocatorInfo(element, reller,
                    action.auto_execute_action());
        }

        @Override
        public void movePop(Element element) {
            info.show(Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element),
                    Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element),
                    info.placement(action.placement()));
            super.movePop(info.getElement());
        }

        @Override
        public void movePop(Coordniates coords) {
            if (info != null) {
                int scrollLeft = Window.getScrollLeft();
                int scrollTop = Window.getScrollTop();
                info.show(coords.top() + scrollTop,
                        coords.right() + scrollLeft,
                        coords.bottom() + scrollTop,
                        coords.left() + scrollLeft,
                        info.placement(action.placement()));
            }
            super.movePop(info.getElement());
        }

        @Override
        public int getStepAction() {
            return action.action() == TipType.VALIDATION_ONLY.getValue()
                    ? TipType.VALIDATION_ONLY.getValue()
                    : TipType.STATIC_INFO.getValue();
        }

        @Override
        public void dispose() {
            if (info != null) {
                info.dispose();
                info = null;
            }
            super.dispose();
        }

        @Override
        public void hideOnScroll() {
            super.hideOnScroll();
            if (info != null) {
                info.hideOnMove();
            }
        }

        @Override
        public void hideOnOverlap(Element element) {
            if (element != null && Customizer.accelerateTips()) {
                if (OverlayUtil.hasSingleScroll(element)) {
                    return;
                }
            }

            if (info != null) {
                info.hide();
                super.hideOnOverlap(element);
            }
        }

        @Override
        public boolean hideOnScrollEnabled(Element element) {
            return !this.isMutationBased;
        }

        /**
         * set z-index to the helper class. To be used by static tip to set
         * maximum z-index between iFrame and Top to the icon.
         * 
         * z-index will be set to zero if static tip is not inside an
         * iFrame.
         */
        @Override
        public void setIFramesCointainerZIndex(int z_index) {
            this.z_index = z_index;
        }

        /**
         * get z-index from the helper class while setting the z-index of
         * the static tip icon.
         */
        @Override
        public int getIFramesCointainerZIndex() {
            return z_index;
        }

        @Override
        public void placePopAndSetWcag(Element element) {
            if (element != null) {
                super.placePop(element);
                CrossMessager.sendMessageToTop("show_focus_on_smarttip",
                        "focus");
            } else {
                CrossMessager.sendMessage("focus_on_smart_tip_element",
                        "focus");
                CrossMessager.sendMessageToFrames(
                        "focus_on_smart_tip_element", "focus");
            }
        }

        @Override
        public void handleKeyDownListener() {
            info.setFocusOnI();
        }

        @Override
        protected void hidePopperOnFocusOut() {
            if (Actioner.settings().staticClose()) {
                return;
            }
            if (popper != null) {
                hidePopper();
            } else {
                CrossMessager.sendMessageToTop("popper_hide", getState());
            }
        }

    }

    protected class HelperBeaconInfo extends HelperInfoStatic {
        
        @Override
        public void init(List<Element> matches) {
            Element best = matches.get(0);
            helper.setIFramesCointainerZIndex(0);
            info = new Beacon(draft, best, this, action,
                    Actioner.getAbsoluteTop(best), Actioner.getAbsoluteRight(best),
                    Actioner.getAbsoluteBottom(best), Actioner.getAbsoluteLeft(best));
            EventBus.fire(new BeaconShowedEvent(draft, action.step()));
        }

        @Override
        public void init(Coordniates coords) {
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            info = new Beacon(draft, null, this, action,
                    coords.top() + scrollTop, coords.right() + scrollLeft,
                    coords.bottom() + scrollTop,
                    coords.left() + scrollLeft);
            EventBus.fire(new BeaconShowedEvent(draft, action.step()));
        }

        @Override
        public int getStepAction() {
            return action.action();
        }

        @Override
        public void handleNext() {
            info.onNext();
        }

        @Override
        public void handleClose() {
            EventBus.fire(new BeaconClosedEvent(draft, action));
        }

        @Override
        public void handleMiss(JsArray<JavaScriptObjectExt> reason) {
        }
        
        @Override
        public void showPop(Element element) {
            popper = new ActionerPopover(new Beacon.BeaconPopoverExtender(), null,
                    draft, action, Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element), Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight());
        }
        
        @Override
        public void handleShowed(Element shown) {
        }

        @Override
        protected void hidePopperOnFocusOut() {
            if (popper != null) {
                hidePopper();
            } else {
                CrossMessager.sendMessageToTop("popper_hide", getState());
            }
        }
    }


    protected class HelperUserAction extends Helper {
        protected boolean isMutationBased;

        public HelperUserAction() {
            enableMutation();
        }
        
        private void enableMutation(){
            isMutationBased = this.canEnableNewSmartTips();
        }

        // to enable mutation on UserAction
        @Override
        public boolean canEnableNewSmartTips() {
            return Enterpriser.hasFeature(AdditionalFeatures.USER_JOURNEY)
                    && DOMUtil.isMutationObserverSupported();
        }

        @Override
        public void handleNext() {
            Actioner.hide(draft, action.step());
            EventBus.fire(new UserActionCompleteEvent(draft));
        }

        @Override
        public void init(List<Element> matches) {
            //do nothing
        }

        @Override
        public void onStepCompletion(String completionAction) {
            //do nothing
        }

        @Override
        public void handleMiss(JsArray<JavaScriptObjectExt> reason) {
            EventBus.fire(new UserActionMissEvent(action, draft, reason));
        }
    }

    protected class HelperAssistant extends Helper {

        private AssistantConfig config = DataUtil.create();

        public HelperAssistant() {
            config = draft.assistant_config();
            action.action(TipType.ASSISTANT.getValue());
            action.placement(config.placement());
            action.type(ContentType.assistant.name());
            // This is referred in case of re-show after hide.
            draft.step_placement(action.step(), action.placement());
        }

        @Override
        public void showPop(Element element) {
            popper = new ActionerPopover(
                    new AssistantTip(config),
                    AbstractResponder.this, draft, action,
                    Actioner.getAbsoluteTop(element),
                    Actioner.getAbsoluteRight(element),
                    Actioner.getAbsoluteBottom(element),
                    Actioner.getAbsoluteLeft(element), element.getOffsetWidth(),
                    element.getOffsetHeight());
            handleShowed(element);
        }

        @Override
        public void showPop(Coordniates coords) {
            int scrollLeft = Window.getScrollLeft();
            int scrollTop = Window.getScrollTop();
            popper = new ActionerPopover(new AssistantTip(config),
                    AbstractResponder.this, draft, action,
                    coords.top() + scrollTop, coords.right() + scrollLeft,
                    coords.bottom() + scrollTop, coords.left() + scrollLeft,
                    coords.offsetWidth(), coords.offsetHeight());
            handleShowed(element);
        }

        @Override
        public boolean isValidator() {
            return false;
        }

        @Override
        public int getStepAction() {
            return TipType.ASSISTANT.getValue();
        }

        @Override
        public void handleShowed(Element shown) {
            EventBus.fire(new AssistantTipShowedEvent(draft));
        }

        @Override
        public void handleClose() {
            hidePopper();
            EventBus.fire(new AssistantTipClosedEvent(draft));
        }

        @Override
        public void handleMiss(JsArray<JavaScriptObjectExt> reason) {
            EventBus.fire(new AssistantTipMissEvent(draft, reason));
        }

        @Override
        public void showEngagers() {
            // No engager for Assistant tip.
        }

        @Override
        public void hideEngagers() {
            // No engager for Assistant tip.
        }

    }
    
    @Override
    public void startStepFindTimer() {
        this.sTime = System.currentTimeMillis();
    }

    public long getStartTime() {
        return this.sTime;
    }
    
    public void setStepFindTimeAndCount(int stepCount) {
        long elapsedTime = System.currentTimeMillis() - this.sTime;
        // The below value calculates the overall Time taken to find element.
        long elementSearchTime = System.currentTimeMillis()
                - this.searchStartTime;
        this.stepFindData = DataUtil.create();
        this.stepFindData.put("step_find_time", elapsedTime + "");
        this.stepFindData.put("step_count", stepCount + "");
        this.stepFindData.put("step_search_exhaust_time",
                elementSearchTime + "");
    }
    
    @Override
    public void setStepFindData(JavaScriptObjectExt stepFindData) {
        this.stepFindData = stepFindData;
    }

    public JavaScriptObjectExt getStepFindData() {
        return this.stepFindData;
    }
}
