package co.quicko.whatfix.overlay;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.Event;

public class ChangeHandler {
    private static List<ChangeListener> listeners;

    private static JavaScriptObject nativeChangeListener;

    public static void addListener(ChangeListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<ChangeListener>();
            attachNativeListener();
        }
        listeners.add(listener);
    }

    public static void removeListener(ChangeListener listener) {
        if (listeners == null) {
            return;
        }
        listeners.remove(listener);

        if (listeners.size() == 0) {
            listeners = null;
            detachNativeListener();
        }
    }

    private static native void attachNativeListener() /*-{
		nativeChangeListener = function(event) {
			@co.quicko.whatfix.overlay.ChangeHandler::fireChange(Lcom/google/gwt/user/client/Event;)(event);
		}

		if ($wnd.addEventListener) {
			$wnd.addEventListener("change", nativeChangeListener, true);
		} else {
			$wnd.attachEvent("change", nativeChangeListener, true);
		}
    }-*/;

    private static native void detachNativeListener() /*-{
		if ($wnd.removeEventListener) {
			$wnd.removeEventListener("change", nativeChangeListener, true);
		} else {
			$wnd.removeEvent("change", nativeChangeListener, true);
		}
    }-*/;

    public static interface ChangeListener extends EventHandler {

        public void onChange(Event event);
    }

    private static void fireChange(Event event) {
        for (ChangeListener listener : listeners) {
            listener.onChange(event);
        }
    }
}