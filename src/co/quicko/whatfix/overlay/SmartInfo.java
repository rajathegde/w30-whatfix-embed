package co.quicko.whatfix.overlay;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;

import co.quicko.whatfix.common.Browser;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;

public class SmartInfo extends IconInfo {

    private static final int OFFSET = 5;
    private static final int ICON_HEIGHT = 16;
    private static final int ICON_WIDTH = 16;
    private static final String DATA_IMAGE_PNG_BASE64 = "data:image/png;base64,";


    public SmartInfo(Element element, ResponderHelper helper, Step action,
            int top, int right, int bottom, int left) {
        super(element, helper, action, top, right, bottom, left);
    }

    @Override
    protected void setWidgeId() {
        Common.setId(panel, "wfx-smartinfo");
    }

    public static int[] position(int top, int right, int bottom, int left,
            int iconHeight, int iconWidth, int offset, String placement) {
        int elementWidth = right - left;
        int elementHeight = bottom - top;

        int iconTop;
        int iconLeft;
        if ("t".equals(placement)) {
            iconTop = top - iconHeight - offset;
            iconLeft = left + (elementWidth / 2) - (iconWidth / 2);
        } else if ("r".equals(placement)) {
            iconTop = top + (elementHeight / 2) - (iconHeight / 2);
            iconLeft = right + offset;
        } else if ("l".equals(placement)) {
            iconTop = top + (elementHeight / 2) - (iconHeight / 2);
            iconLeft = left - iconWidth - offset;
        } else {
            iconTop = bottom + offset;
            iconLeft = left + (elementWidth / 2) - (iconWidth / 2);
        }

        return new int[] { iconLeft, iconTop };
    }

    @Override
    protected void refresh(Step step) {
        String iconColor = Themer.value(Themer.SMART_TIP.ICON_COLOR);
        HTML anchor;
        if (Themer.SMART_TIP_INFO_ICON
                .equals(Themer.value(Themer.SMART_TIP.ICON_TYPE))) {
            anchor = new HTML(SVGElements.getSmartTipInfoIcon(iconColor));
        } else {
            anchor = new HTML("<img src='" + DATA_IMAGE_PNG_BASE64
                    + Themer.value(Themer.SMART_TIP.ICON_IMAGE) + "'>");
        }
        
        // Empty clickhandler for firing mouse events in mobile ios devices
        if (Browser.isMobileIosDevice()) {
            anchor.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                }
            });
        } else {
            panel.setStyleName(Overlay.CSS.smartInfo());
        }
        
        panel.setWidget(anchor);
        
        register.register(getElement(), this);
    }

    @Override
    protected int[] position(int top, int right, int bottom, int left,
            String placement) {
        return position(top, right, bottom, left, ICON_HEIGHT, ICON_WIDTH,
                OFFSET, placement);
    }

    @Override
    protected String defaultPlacement() {
        return "b";
    }

    @Override
    protected void onNext() {
    }

    @Override
    protected boolean isCloseable() {
        return Actioner.settings().staticClose() && !Browser.isMobile();
    }

    @Override
    protected int getUnhoverTime() {
        return Actioner.settings().staticCloseTime();
    }

    @Override
    protected int getHoverTime() {
        return Actioner.settings().staticShowTime();
    }

    @Override
    protected Element getPopoverElement() {
        return getElement();
    }
}