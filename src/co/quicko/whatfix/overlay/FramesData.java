package co.quicko.whatfix.overlay;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;

import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.data.AppRules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.StepRetries;
import co.quicko.whatfix.data.Enterprise;
import co.quicko.whatfix.data.Tag;
import co.quicko.whatfix.overlay.Actioner.Settings;
import co.quicko.whatfix.security.Enterpriser;

public class FramesData extends JavaScriptObject {
    protected FramesData() {
    }

    public static FramesData getData() {
        return getData(null);
    }

    public static FramesData getData(Settings settings) {
        FramesData framesData = DataUtil.create().cast();
        Enterprise enterpriseForFrames = getEnterpriseForFrames();
        if(enterpriseForFrames != null) {
            framesData.enterprise(enterpriseForFrames);
        }
        Customizer customizerForFrames = getCustomizerForFrames();
        if (customizerForFrames != null) {
            framesData.customizer(customizerForFrames);
        }
        if (settings != null) {
            framesData.settings(ActionerSettings.copy(settings));
        }
        return framesData;
    }

    public static void setData(FramesData framesData) {
        if (framesData == null) {
            return;
        }
        setEnterpriseForFrames(framesData.enterprise());
        setCustomizerForFrames(framesData.customizer());
        setActionerSettings(framesData.settings());
    }

    private static Enterprise getEnterpriseForFrames() {
        return extractEnterpriseForFrames(Enterpriser.enterprise());
    }

    public static void setEnterpriseForFrames(Enterprise ent) {
        if (ent != null) {
            Enterprise.enterprise(ent);
        }
    }

    public static Enterprise extractEnterpriseForFrames(Enterprise enterprise) {
        Enterprise ent = DataUtil.create();
        String[] additionalFeatures = enterprise.additional_features();
        if (additionalFeatures != null && additionalFeatures.length > 0) {
            ent.additional_features(additionalFeatures);
        }
        JsArray<AppRules> appRules = enterprise.app_rules();
        if (JsUtils.isNotEmpty(appRules)) {
            ent.create_page_tags(enterprise.create_page_tags());
            ent.app_rules(appRules);
            JsArray<Tag> tags = enterprise.tags();
            if (JsUtils.isNotEmpty(tags)) {
                ent.tags(tags);
            }
        }
        ent.rule_config_finder(enterprise.rule_config_finder());
        ent.finder_version(enterprise.finder_version());
        return ent.isEmpty() ? null : ent;
    }

    private static Customizer getCustomizerForFrames() {
        Customizer customizer = DataUtil.create().cast();
        StepRetries stepConfigurations = Customizer.getStepConfigurations();
        if (stepConfigurations != null) {
            customizer.step_configurations(stepConfigurations);
        }
        boolean overRidePlatformApp = Customizer.overRidePlatformApp();
        if (overRidePlatformApp) {
            customizer.override_platform_app(overRidePlatformApp);
        }
        boolean ignoreScrollOnBody = Customizer.ignoreScrollOnBody();
        if (ignoreScrollOnBody) {
            customizer.ignore_scroll_on_body(ignoreScrollOnBody);
        }
        JsArrayString actionEvents = Customizer.getActionEvents();
        if (actionEvents != null) {
            customizer.action_events(actionEvents);
        }
        JavaScriptObject customData = Customizer.getCustomData();
        if (customData != null) {
            customizer.custom_data(customData);
        }

        JsArrayString excludeAttributesFilterConfig = Customizer
                .getMutationExcludeAttributeNames();
        if (excludeAttributesFilterConfig != null) {
            customizer.mutation_exclude_attribute_names(
                    excludeAttributesFilterConfig);
        }

        // only enabled for Remedy Force to find iframes with highest z-index
        boolean checkFrameVisibility = Customizer.checkFrameVisibility();
        if (checkFrameVisibility) {
            customizer.check_frame_visibility(checkFrameVisibility);
        }

        // by default enabled
        boolean newResponderLogic = Customizer.newResponderLogic();
        if (newResponderLogic) {
            customizer.new_responder_logic(newResponderLogic);
        }

        LoggerConfig loggerConfig = LoggerConfig.loggerConfig();
        if (loggerConfig != null) {
            customizer.logger_config(loggerConfig);
        }

        int mutationThrottleTime = Customizer.getMutationThrottleTime();
        if (mutationThrottleTime > 0) {
            customizer.mutation_throttle_ms(mutationThrottleTime);
        }

        int smartTipScrollTimeout = Customizer.getSmartTipScrollTimeout();
        if (smartTipScrollTimeout > 0) {
            customizer.smart_tip_scroll_timeout_ms(smartTipScrollTimeout);
        }

        return customizer.isEmpty() ? null : customizer;
    }

    private static void setCustomizerForFrames(Customizer customizer) {
        if (customizer != null) {
            StepRetries step_configurations = customizer.step_configurations();
            if (step_configurations != null) {
                Customizer.setStepConfigurations(step_configurations);
            }
            setOverridePlatformApp(customizer);
            boolean ignore_scroll_on_body = customizer.ignore_scroll_on_body();
            if (ignore_scroll_on_body) {
                Customizer.ignoreScrollOnBody(ignore_scroll_on_body);
            }
            JsArrayString actionEvents = customizer.action_events();
            if (JsUtils.isNotEmptyArray(actionEvents)) {
                Customizer.actionEvents(actionEvents);
            }
            JavaScriptObject customData = customizer.custom_data();
            if (customData != null) {
                Customizer.setCustomData(customData);
            }
            JsArrayString attrFilterConfig = customizer
                    .mutation_exclude_attribute_names();
            if (JsUtils.isNotEmptyArray(attrFilterConfig)) {
                Customizer.mutationExcludeAttributeNames(attrFilterConfig);
            }
            setNewResponderLogic(customizer);
            setLoggerConfig(customizer);
            setMutationThrottleTime(customizer);
            setSmartTipScrollTimeout(customizer);
        }
    }

    private static void setSmartTipScrollTimeout(Customizer customizer) {
        int smartTipScrollTimeout = customizer.smart_tip_scroll_timeout_ms();
        if (smartTipScrollTimeout > 0) {
            Customizer.setSmartTipScrollTimeout(smartTipScrollTimeout);
        }
    }

    private static void setMutationThrottleTime(Customizer customizer) {
        int mutationThrottleTime = customizer.mutation_throttle_ms();
        if (mutationThrottleTime > 0) {
            Customizer.setMutationThrottleTime(mutationThrottleTime);
        }
    }

    protected static void setOverridePlatformApp(Customizer customizer) {
        boolean overridePlatformApp = customizer.override_platform_app();
        if (overridePlatformApp) {
            Customizer.overRidePlatformApp(overridePlatformApp);
        }
    }

    private static void setNewResponderLogic(Customizer customizer) {
        boolean newResponderLogic = customizer.new_responder_logic();
        if (newResponderLogic) {
            Customizer.newResponderLogic(newResponderLogic);
        }
    }

    private static void setLoggerConfig(Customizer customizer) {
        LoggerConfig loggerConfig = (LoggerConfig) customizer.logger_config();
        if (null != loggerConfig) {
            WfxLogFactory.addLoggers(loggerConfig);
        }
    }

    private static void setActionerSettings(ActionerSettings settings) {
        if (settings != null) {
            Actioner.settings(settings);
        }
    }

    private final native Enterprise enterprise() /*-{
        return this.enterprise;
    }-*/;

    private final native void enterprise(Enterprise enterprise) /*-{
        this.enterprise = enterprise;
    }-*/;

    private final native Customizer customizer() /*-{
        return this.customizer;
    }-*/;

    private final native void customizer(Customizer customizer) /*-{
        this.customizer = customizer;
    }-*/;

    private final native ActionerSettings settings() /*-{
        return this.settings;
    }-*/;

    private final native void settings(ActionerSettings settings) /*-{
        return this.settings = settings;
    }-*/;
}
