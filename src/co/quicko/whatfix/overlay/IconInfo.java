package co.quicko.whatfix.overlay;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.OutlineStyle;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodeEvent;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.common.ZMaxer;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.actioner.BodyHacks;

public abstract class IconInfo implements MouseOverHandler, MouseOutHandler,
        ClickHandler, KeyDownHandler, KeyUpHandler {

    protected static final BodyHacks HACK = GWT.create(BodyHacks.class);
    protected static HoverRegister register;
    private Timer hover;
    private Timer unHover;


    static {
        HACK.hack();
        register = new HoverRegister();
    }

    protected ResponderHelper helper;
    protected PopupPanel panel;
    protected Element element;

    private IconInfo(Element element) {
        this.element = element;
        panel = new NoContentPopup();
        panel.setAutoHideEnabled(false);
        panel.setAnimationEnabled(false);
    }

    protected IconInfo(Element element, ResponderHelper helper, Step action,
            int top, int right, int bottom, int left) {
        this(element);
        this.helper = helper;
        setHierarchicalMaxZIndex();
        refresh(action);
        show(top, right, bottom, left, placement(action.placement()));
        setWidgeId();
    }

    protected void setWidgeId() {
    }

    public void show(int top, int right, int bottom, int left,
            String placement) {
        int[] position = position(top, right, bottom, left, placement);
        panel.setPopupPosition(position[0], position[1]);
        panel.show();
        panel.getElement().getStyle().setDisplay(Display.BLOCK);
        panel.getElement().getStyle().setOutlineStyle(OutlineStyle.NONE);
        panel.getElement().setAttribute(Common.DATA_WFX_ATTR, "true");
    }

    public String placement(String placement) {
        return placement == null ? defaultPlacement() : placement;
    }

    public void hideOnMove() {
        if (panel != null) {
            panel.getElement().getStyle().setDisplay(Display.NONE);
        }
    }

    public Element getElement() {
        return panel.getElement();
    }

    public void hide() {
        if (panel != null) {
            panel.hide();
        }
    }

    public void dispose() {
        if (hover != null) {
            hover.cancel();
        }
        if (unHover != null) {
            unHover.cancel();
        }
        register.unRegister(getElement());
        
        if (panel != null) {
            panel.hide();
            panel = null;
        }
    }
    
    /**
     * Finds the maximum z-index in hierarchy from the matched element till the
     * document top and sets it to the icon so that appears on top of all
     * elements.
     */
    private void setHierarchicalMaxZIndex(){
        int zIndex = ZMaxer.getZIndex(element)
                + helper.getIFramesCointainerZIndex() + 1;
        getElement().getStyle().setZIndex(
                zIndex > Integer.MAX_VALUE ? Integer.MAX_VALUE : zIndex);
    }

    protected abstract void onNext();
    
    protected abstract void refresh(Step step);

    protected abstract int[] position(int top, int right, int bottom, int left,
            String placement);

    protected abstract String defaultPlacement();
    
    protected abstract boolean isCloseable();
    
    protected abstract int getUnhoverTime();
    
    protected abstract int getHoverTime();
    
    protected abstract Element getPopoverElement();
    
    @Override
    public void onMouseOut(MouseOutEvent event) {
        if (hover == null) {
            return;
        }

        hover.cancel();
        hover = null;
        if (isCloseable()) {
            return;
        }
        if (unHover != null) {
            unHover.cancel();
        }
        unHover = new Timer() {
            @Override
            public void run() {
                helper.hidePopper();
            }
        };
        unHover.schedule(getUnhoverTime());
    }

    @Override
    public void onMouseOver(MouseOverEvent event) {
        if (unHover != null) {
            unHover.cancel();
            unHover = null;
        }

        if (hover != null) {
            hover.cancel();
        }
        if (isDescriptionPresent()) {
            hover = new Timer() {
                @Override
                public void run() {
                    helper.placePop(getPopoverElement());
                }
            };
            hover.schedule(getHoverTime());
        }
    }

    @Override
    public void onKeyDown(KeyDownEvent event) {
        performOnKeyAction(event);
    }

    @Override
    public void onKeyUp(KeyUpEvent event) {
        performOnKeyAction(event);
    }

    private void performOnKeyAction(KeyCodeEvent event) {
        placePopAndSetWcag(event, getPopoverElement());
        panel.getElement().setTabIndex(-1);
    }

    protected void placePopAndSetWcag(KeyCodeEvent event,
            Element popOverElement) {
        if (event.isShiftKeyDown()) {
            helper.placePopAndSetWcag(null);
        } else {
            helper.placePopAndSetWcag(popOverElement);
        }
    }
    
    @Override
    public void onClick(ClickEvent event) {
    }
    
    protected boolean isDescriptionPresent() {
        return true;
    }

    public void setFocusOnI() {
        Scheduler.get().scheduleDeferred(new ScheduledCommand() {

            @Override
            public void execute() {
                getElement().setAttribute(AriaProperty.LABEL,
                        "Information Tip");
                getElement().setTabIndex(0);
                getElement().focus();
            }
        });
    }
    
    protected static class HoverRegister implements NativePreviewHandler {
        private Map<Element, EventHandler> handlers;
        private HandlerRegistration registration;

        public HoverRegister() {
            handlers = new HashMap<Element, EventHandler>();
        }

        protected void register(Element icon, EventHandler handler) {
            handlers.put(icon, handler);
            if (handlers.size() == 1) {
                registration = Event.addNativePreviewHandler(this);
            }
        }

        protected void unRegister(Element icon) {
            handlers.remove(icon);
            if (handlers.size() == 0) {
                registration.removeHandler();
                registration = null;
            }
        }

        @Override
        public void onPreviewNativeEvent(NativePreviewEvent event) {
            // Do not send events out of icon.
            NativeEvent nativeEvent = event.getNativeEvent();
            Element icon = eventTargetsAnyIcon(nativeEvent);
            if (icon == null) {
                return;
            }
            nativeEvent.stopPropagation();
            String type = nativeEvent.getType();
            if (!("mouseover".equals(type) || "mouseout".equals(type)
                    || "click".equals(type) || "keydown".equals(type)
                    || "keyup".equals(type))) {
                return;
            }

            if ("mouseover".equals(type)) {
                EventHandler handler = handlers.get(icon);
                if (handler != null) {
                    ((MouseOverHandler) (handler)).onMouseOver(null);
                }
            } else if ("mouseout".equals(type)) {
                EventHandler handler = handlers.get(icon);
                if (handler != null) {
                    ((MouseOutHandler) (handler)).onMouseOut(null);
                }
            } else if ("keydown".equals(type)) {
                EventHandler handler = handlers.get(icon);
                if (handler != null) {
                    ((KeyDownHandler) (handler))
                            .onKeyDown(new NativeKeyDownEvent() {
                                @Override
                                public boolean isShiftKeyDown() {
                                    return nativeEvent.getShiftKey();
                                }
                                
                                @Override
                                public int getNativeKeyCode() {
                                    return nativeEvent.getKeyCode();
                                }
                            });
                }
            }
            else if ("keyup".equals(type)) {
                EventHandler handler = handlers.get(icon);
                if (handler != null) {
                    ((KeyUpHandler) (handler)).onKeyUp(new NativeKeyUpEvent() {
                        @Override
                        public boolean isShiftKeyDown() {
                            return nativeEvent.getShiftKey();
                        }

                        @Override
                        public int getNativeKeyCode() {
                            return nativeEvent.getKeyCode();
                        }
                    });
                }
            }
            else {
                EventHandler handler = handlers.get(icon);
                if (handler != null) {
                    ((ClickHandler) (handler)).onClick(null);
                }
            }
        }

        private Element eventTargetsAnyIcon(NativeEvent event) {
            EventTarget target = event.getEventTarget();
            if (Element.is(target)) {
                Element targetElement = Element.as(target);
                for (Element icon : handlers.keySet()) {
                    if (icon.isOrHasChild(targetElement)) {
                        return icon;
                    }
                }
            }
            return null;
        }
    }

    protected static class NativeKeyDownEvent extends KeyDownEvent {
    }

    protected static class NativeKeyUpEvent extends KeyUpEvent {
    }
}
