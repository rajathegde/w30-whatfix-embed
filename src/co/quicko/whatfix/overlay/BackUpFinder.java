package co.quicko.whatfix.overlay;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Draft.Mark;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.UserActionHandler.UserActionListener;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitCloseEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitHideReason;
import co.quicko.whatfix.player.gracefulexit.event.HideGracefulExitEvent;
import co.quicko.whatfix.workflowengine.Finder;

public class BackUpFinder {

    private static BackUpListener listener;
    private static boolean listeningHandlers = false;
    private static boolean isPopUpVisible = false;
    private static int currentStepno;
    private static UserActionHandler userActionHandler;

    protected static void initialize() {
        if (OverlayUtil.isPrimaryWindow()) {
            EventBus.addHandler(OnPopupShowEvent.class,
                    BackUpFinder::onPopupShow);
            EventBus.addHandler(HideGracefulExitEvent.class,
                    BackUpFinder::onHideGracefulExitEvent);
            EventBus.addHandler(GracefulExitCloseEvent.class,
                    BackUpFinder::onGracefulExitCloseEvent);

        } else {
            CrossMessager.addListener(new CrossListener() {
                @Override
                public void onMessage(String type, String content) {
                    Step step = DataUtil.<Step> create(content);
                    userActionHandler.startListener(step);
                    sendMessageToFramesWithMarks(type, step);
                }

            }, "start_user_action_handler");

            CrossMessager.addListener(new CrossListener() {
                @Override
                public void onMessage(String type, String content) {
                    userActionHandler.stopListener();
                    CrossMessager.sendMessageToFrames(type, content);
                }

            }, "stop_user_action_handler");

        }

        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                Step step = DataUtil.<Step> create(content);
                BackUpFinder.onUserAction(step);
            }

        }, "on_user_action");

        userActionHandler = new UserActionHandler(new ListenerImpl());

    }

    public static void hide() {

        stopHandlers();
        if (isPopUpVisible) {
            isPopUpVisible = false;
        EventBus.fire(new HideGracefulExitEvent(currentStepno,
                GracefulExitHideReason.ON_FLOW_CHANGE));
        }
    }

    public static void setBackUpListener(BackUpListener listener) {
        BackUpFinder.listener = listener;
    }

    public static void tryAlternateWayForStep(Draft draft, int stepNo) {
        if (isPopUpVisible) {
            return;
        }
        showDeck(draft, stepNo);
    }

    protected static void showDeck(Draft draft, int step) {
        listener.showPopUp(draft);
    }

    private static void onPopupShow(Event event) {
        isPopUpVisible = true;
        listener.onPopupShow();
        OnPopupShowEvent startListenerEvent = (OnPopupShowEvent) event;
        int action = startListenerEvent.getAction();
        int stepNo = startListenerEvent.getStepNo();
        currentStepno = stepNo;
        if (action != StepCompletionActions.ON_HOVER.getValue()
                && action != StepCompletionActions.ON_NEXT.getValue()) {
            listeningHandlers = true;
            addListenersForUserAction(action, stepNo);
        }
    }

    private static void addListenersForUserAction(int action, int stepNo) {

        Step step = JavaScriptObject.createObject().cast();
        step.action(action);
        step.step(stepNo);
        step.parent_marks(
                JavaScriptObject.createArray().<JsArray<JsArray<Mark>>> cast());
        userActionHandler.startListener(step);
        sendMessageToFramesWithMarks("start_user_action_handler", step);
    }

    public static Step addFrameMarks(Step step, Element frame) {
        Step newStep = JavaScriptObject.createObject().cast();
        newStep.parent_marks(
                JavaScriptObject.createArray().<JsArray<JsArray<Mark>>> cast());
        newStep.action(step.action());
        newStep.step(step.step());
        newStep.parent_marks().push(Finder.getMarks(frame));
        JsUtils.append(newStep.parent_marks(), step.parent_marks());
        return newStep;
    }

    public static void onUserAction(Step step) {
        if (OverlayUtil.isPrimaryWindow()) {
            if (!listener.validateStepMarks(step)) {
                return;
            }
            if (!stopHandlers()) {
                // repeated action
                return;
            }
            listener.onNextAfterMiss(step);
            EventBus.fire(new HideGracefulExitEvent(step.step(),
                    GracefulExitHideReason.ON_STEP_FOUND));
        }
    }

    private static void onGracefulExitCloseEvent(Event event) {
        GracefulExitCloseEvent gracefulExitCloseEvent = (GracefulExitCloseEvent) event;
        if (!gracefulExitCloseEvent.isDestroyModal()) {
            return;
        }
        isPopUpVisible = false;
        stopHandlers();

    }

    private static void onHideGracefulExitEvent(Event event) {
        isPopUpVisible = false;
        stopHandlers();
    }

    public static boolean stopHandlers() {
        if (!listeningHandlers) {
            return false;
        }
        listeningHandlers = false;
        userActionHandler.stopListener();
        CrossMessager.sendMessageToFrames("stop_user_action_handler", "");
        return true;
    }

    private static void sendMessageToFramesWithMarks(String type, Step step) {
        List<Element> frames = CrossMessager.frames();
        for (Element frame : frames) {
            String content = StringUtils
                    .stringifyObject(BackUpFinder.addFrameMarks(step, frame));
            CrossMessager.sendMessageToFrame(frame, type, content);
        }
    }

    public static interface BackUpListener {

        public void showPopUp(Draft draft);

        public void onPopupShow();

        public void onNextAfterMiss(Step step);

        public boolean validateStepMarks(Step capturedStep);
    }

    private static class ListenerImpl implements UserActionListener {

        @Override
        public void onUserAction(Step step) {
            if (OverlayUtil.isTopWindow()) {
                BackUpFinder.onUserAction(step);
            } else {
                CrossMessager.sendMessageToTop("on_user_action",
                        StringUtils.stringifyObject(step));
            }
        }

    }
}