package co.quicko.whatfix.overlay;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.actioner.BodyHacks;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.alg.SVGUtil;

public class Engager implements EngageEventHandler {
    protected EngImpl impl;
    protected Element frozenElement;

    private HandlerRegistration overRegn;
    private List<Element> ignores;

    public Engager() {
        ignores = new ArrayList<Element>();
    }

    public void addIgnores(Element ignore) {
        ignores.add(ignore);
    }

    public void removeIgnores(Element ignore) {
        ignores.remove(ignore);
    }

    public void clearIgnores() {
        ignores.clear();
    }

    public void show() {
        hide();

        impl = OverlayUtil.isTopWindow() ? createTop() : createFrame();
        impl.show();
        overRegn = Event.addNativePreviewHandler(new OverHandler(this));
    }
    
    protected EngImpl createTop() {
        return new TopEngImpl();
    }

    protected EngImpl createFrame() {
        return new FrameEngImpl();
    }

    public void hide() {
        if (impl != null) {
            impl.hide();
            impl = null;
        }
        if (overRegn != null) {
            overRegn.removeHandler();
            overRegn = null;
        }
    }

    public boolean isShowing() {
        return impl != null;
    }

    // TODO
    public void hideEngagers() {
        impl.hideEngagers();
    }

    protected int markStrength(Element element) {
        return Overlay.CSS.mark_strength();
    }

    private void onPreviewNativeEvent(PreviewEngageEvent event) {
        // For lightning shadow-root elements, event firing in capturing mode is
        // not working as they have custom code around it. reading composedPath
        // of event in case of Lightning
        Element source = AppController.getEngagerElement(event);

        if (Customizer.engagerElementFromPath()) {
            // Override srcElement with composed path if the flag
            // engager_element_from_path is set to true
            EventTarget targetElement = DOMUtil
                    .getComposedPathTarget(event.getNativeEvent());
            source = targetElement != null ? event.getTarget(targetElement)
                    : null;
        }
        if (source == null) {
            source = event.getActualTarget();
        }
        if (source == null || ignore(source) || SVGUtil.excludeEvent(source)) {
            return;
        }
        source = SVGUtil.redirectSVG(source);
        onPreviewNativeEvent(source, event);
    }

    protected void onPreviewNativeEvent(Element source,
            PreviewEngageEvent event) {
        String type = event.getType();
        if (type.equals("mouseover") && null == frozenElement) {
            engage(source);
        } else if (type.equals("mouseout") && null == frozenElement) {
            hideEngagers();
        } else {
            onPreviewUnknownEvent(source, event);
        }
    }

    protected void engage(Element source) {
        if (OverlayUtil.isFrame(source)) {
            impl.frame(source);
        } else {
            impl.showEngagers(source, markStrength(source));
        }
    }
    
    public void setShortcutSource(Element shortcutSource) {
        impl.frame(shortcutSource);
    }

    protected void onPreviewUnknownEvent(Element source,
            PreviewEngageEvent event) {
    }

    private boolean ignore(Element source) {
        if (ignores.size() == 0) {
            return false;
        }
        int left = source.getAbsoluteLeft();
        int right = source.getAbsoluteRight();
        int top = source.getAbsoluteTop();
        int bottom = source.getAbsoluteBottom();
        for (Element ignore : ignores) {
            if (left >= ignore.getAbsoluteLeft()
                    && right <= ignore.getAbsoluteRight()
                    && top >= ignore.getAbsoluteTop()
                    && bottom <= ignore.getAbsoluteBottom()) {
                return true;
            }
        }
        return false;
    }

    private static class OverHandler implements NativePreviewHandler {
        private Engager engager;

        private OverHandler(Engager engager) {
            this.engager = engager;
        }

        @Override
        public void onPreviewNativeEvent(NativePreviewEvent event) {
            engager.onPreviewNativeEvent(new PreviewNativeEvent(event));
        }
    }

    protected static abstract class EngImpl implements CrossListener {
        protected static final String PICKER_UN_ENGAGE_EVENT = "picker_un_engage";
        protected static final String PICKER_ENGAGE_EVENT = "picker_engage";
        protected static final String PICKER_FROZEN_EVENT = "picker_frozen";
        protected static final String PICKER_UN_FROZEN_EVENT = "picker_un_frozen";
        protected static final String PICKER_CONTAINS_TOUCHEVENT_SOURCE= "contains_touchevent_source";

        protected boolean isAnyElementFrozen;

        protected Element frame;

        public void frame(Element frame) {
            this.frame = frame;
        }

        public Element frame() {
            return frame;
        }

        public void show() {
            CrossMessager.addListener(this, PICKER_ENGAGE_EVENT,
                    PICKER_UN_ENGAGE_EVENT, PICKER_FROZEN_EVENT,
                    PICKER_UN_FROZEN_EVENT);
        }

        public void hide() {
            CrossMessager.removeListener(this, PICKER_ENGAGE_EVENT,
                    PICKER_UN_ENGAGE_EVENT, PICKER_FROZEN_EVENT,
                    PICKER_UN_FROZEN_EVENT);
            isAnyElementFrozen = false;
        }

        @Override
        public void onMessage(String type, String content) {
            if (PICKER_FROZEN_EVENT.equals(type)) {
                isAnyElementFrozen = true;
                CrossMessager.sendMessageToFrames(PICKER_FROZEN_EVENT, "");
            } else if (PICKER_UN_FROZEN_EVENT.equals(type)) {
                isAnyElementFrozen = false;
                CrossMessager.sendMessageToFrames(PICKER_UN_FROZEN_EVENT, "");
            } else if (PICKER_UN_ENGAGE_EVENT.equals(type)) {
                hideEngagers();
            } else if (PICKER_ENGAGE_EVENT.equals(type)) {
                Coordniates coords = DataUtil.create(content);
                coords.relative(frame);
                showEngagers(coords);
            }
        }

        public abstract void showEngagers(Element element, int strength);

        public abstract void showEngagers(Coordniates coords);

        public abstract void hideEngagers();

        protected static class Coordniates extends JavaScriptObject {

            protected Coordniates() {
            }

            public final void init(Element element, int strength) {
                init(element, strength, false);
            }

            public final void init(Element element, int strength,
                    boolean isGoodElement) {
                double cssZoom = ZoomFinder.getCSSZoom(element);
                int scrollLeft = Window.getScrollLeft();
                int scrollTop = Window.getScrollTop();

                int top = Actioner.getAbsoluteTop(element);
                int right = Actioner.getAbsoluteRight(element);
                int bottom = Actioner.getAbsoluteBottom(element);
                int left = Actioner.getAbsoluteLeft(element);

                top(top - scrollTop);
                right(right - scrollLeft);
                bottom(bottom - scrollTop);
                left(left - scrollLeft);

                iframeZoom(cssZoom);

                offsetWidth(
                        (int) Math.round(element.getOffsetWidth() * cssZoom));
                offsetHeight(
                        (int) Math.round(element.getOffsetHeight() * cssZoom));
                strength(strength);
                good_element(isGoodElement);
            }

            public final void relative(Element element) {
                if (element != null) {
                    double cssZoom = ZoomFinder.getCSSZoom(element);
                    int frameTop = Actioner.getAbsoluteTop(element)
                            - Window.getScrollTop();
                    int frameLeft = Actioner.getAbsoluteLeft(element)
                            - Window.getScrollLeft();
                    int[] frameBorderOffset = BodyHacks.frameBorderOffset(element);
                    int[] framePaddingOffset = BodyHacks.framePaddingOffset(element);
                    /**
                     * The below calculation is made when css zoom is applied to
                     * body element. OR To iframe body or both or nested iframes
                     * The formula behind this is ((element distance) * (zoom of
                     * contained doc))/ zoom of parent document This change is
                     * made for JIRA SUCC-3375
                     * 
                     */
                    int top = (int) Math
                            .round((top() * iframeZoom()) / cssZoom);
                    int right = (int) Math
                            .round((right() * iframeZoom()) / cssZoom);
                    int bottom = (int) Math
                            .round((bottom() * iframeZoom()) / cssZoom);
                    int left = (int) Math
                            .round((left() * iframeZoom()) / cssZoom);

                    top = top + (frameTop + frameBorderOffset[0]
                            + framePaddingOffset[0]);
                    right = right + (frameLeft + frameBorderOffset[1]
                            + framePaddingOffset[1]);
                    bottom = bottom + (frameTop + frameBorderOffset[0]
                            + framePaddingOffset[0]);
                    left = left + (frameLeft + frameBorderOffset[1]
                            + framePaddingOffset[1]);

                    top(top);
                    right(right);
                    bottom(bottom);
                    left(left);
                    iframeZoom(cssZoom);

                }
            }

            public final native void top(int top)/*-{
                this.top = top;
            }-*/;

            public final native void right(int right)/*-{
                this.right = right;
            }-*/;

            public final native void bottom(int bottom)/*-{
                this.bottom = bottom;
            }-*/;

            public final native void left(int left)/*-{
                this.left = left;
            }-*/;

            public final native void offsetWidth(int offsetWidth)/*-{
                this.offsetWidth = offsetWidth;
            }-*/;

            public final native void offsetHeight(int offsetHeight)/*-{
                this.offsetHeight = offsetHeight;
            }-*/;

            public final native void strength(int strength)/*-{
                this.strength = strength;
            }-*/;

            public final native void good_element(boolean good_element)/*-{
                this.good_element = good_element;
            }-*/;

            public final native boolean good_element()/*-{
                return this.good_element ? this.good_element : false;
            }-*/;

            public final native int top()/*-{
                return this.top;
            }-*/;

            public final native int right()/*-{
                return this.right;
            }-*/;

            public final native int bottom()/*-{
                return this.bottom;
            }-*/;

            public final native int left()/*-{
                return this.left;
            }-*/;

            public final native int offsetWidth()/*-{
                return this.offsetWidth;
            }-*/;

            public final native int offsetHeight()/*-{
              return this.offsetHeight;
            }-*/;

            public final native int strength()/*-{
              return this.strength;
            }-*/;
            
            public final native void needScroll(boolean needScroll)/*-{
               this.needScroll = needScroll;
            }-*/;
            
            public final native boolean needScroll()/*-{
                return this.needScroll;
            }-*/;

            public final native void iframeZoom(double zoom)/*-{
                this.iframeZoom = zoom;
            }-*/;

            public final native double iframeZoom()/*-{
               return this.iframeZoom;
            }-*/;
        }
    }

    protected static class TopEngImpl extends EngImpl {
        private PopupPanel[] engagers;

        @Override
        public void show() {
            super.show();
            if (engagers == null) {
                engagers = new PopupPanel[8];
                engagers[0] = makeEngager();
                engagers[1] = makeEngager();
                engagers[2] = makeEngager();
                engagers[3] = makeEngager();

                // used for good element
                engagers[4] = makeEngager();
                engagers[5] = makeEngager();
                engagers[6] = makeEngager();
                engagers[7] = makeEngager();
            }
        }

        private PopupPanel makeEngager() {
            PopupPanel popup = new FixedPopup();
            popup.setStyleName(Overlay.CSS.engaged());
            // for editor the engagers can have max z index
            popup.addStyleName(Overlay.CSS.editorEngaged());
            Label dummy = new Label();
            popup.setWidget(dummy);
            popup.setAutoHideEnabled(false);
            return popup;
        }

        @Override
        public void hide() {
            super.hide();
            if (engagers != null) {
                hideEngagers();
            }
        }

        @Override
        public void hideEngagers() {
            for (PopupPanel e : engagers) {
                e.hide();
            }
        }

        @Override
        public void showEngagers(Element element, int strength) {
            double cssZoom = ZoomFinder.getCSSZoom(element);
            int top = element.getAbsoluteTop();
            int right = element.getAbsoluteRight();
            int bottom = element.getAbsoluteBottom();
            int left = element.getAbsoluteLeft();

            showEngagers(top - Window.getScrollTop(),
                    right - Window.getScrollLeft(),
                    bottom - Window.getScrollTop(),
                    left - Window.getScrollLeft(),
                    (int) Math.round(element.getOffsetWidth() * cssZoom),
                    (int) Math.round(element.getOffsetHeight() * cssZoom), strength,
                    AppController.isGoodElement(element));
        }

        @Override
        public void showEngagers(Coordniates coords) {
            showEngagers(coords.top(), coords.right(), coords.bottom(),
                    coords.left(), coords.offsetWidth(), coords.offsetHeight(),
                    coords.strength(), coords.good_element());
        }

        private void showEngagers(int top, int right, int bottom, int left,
                int offsetWidth, int offsetHeight, int engage,
                boolean isGoodElement) {

            int engagerWidth = offsetWidth + 4 * engage;
            /**
             * The above calculation with offsetwidth is causing overflow when
             * zoom is present, so to rectify it calculating based on right and
             * left positions
             */
            if (Enterpriser.hasFeature(AdditionalFeatures.css_zoom_factor)) {
                engagerWidth = (right + engage) - (left - 2 * engage);
            }

            placeEngager(engagers[0], left - 2 * engage, top - 2 * engage,
                    engage, offsetHeight + 4 * engage);
            placeEngager(engagers[1], right + engage, top - 2 * engage, engage,
                    offsetHeight + 4 * engage);
            placeEngager(engagers[2], left - 2 * engage, top - 2 * engage,
                    engagerWidth, engage);
            placeEngager(engagers[3], left - 2 * engage, bottom + engage,
                    engagerWidth, engage);

            for (int index = 0; index < 4; index++) {
                PopupPanel engagerPop = engagers[index];
                engagerPop.show();
            }
            if (isGoodElement) {
                showEngagersForGoodElement(top, right, bottom, left,
                        offsetWidth, offsetHeight, engage);
            }
        }

        private void showEngagersForGoodElement(int top, int right, int bottom,
                int left, int offsetWidth, int offsetHeight, int engage) {
            int gap = 2 * engage;
            int engagerWidth = offsetWidth + 4 * engage + 2 * gap;
            /**
             * The above calculation with offsetwidth is causing overflow when
             * zoom is present, so to rectify it calculating based on right and
             * left positions
             */
            if (Enterpriser.hasFeature(AdditionalFeatures.css_zoom_factor)) {
                engagerWidth = (right + engage + gap) - (left - 2 * gap);
            }
            placeEngager(engagers[4], left - 2 * gap, top - 2 * gap, engage,
                    offsetHeight + 4 * engage + 2 * gap);
            placeEngager(engagers[5], right + engage + gap, top - 2 * gap,
                    engage, offsetHeight + 4 * engage + 2 * gap);
            placeEngager(engagers[6], left - 2 * gap, top - 2 * gap,
                    engagerWidth, engage);
            placeEngager(engagers[7], left - 2 * gap, bottom + engage + gap,
                    engagerWidth, engage);

            for (int index = 4; index < 8; index++) {
                engagers[index].show();
            }
        }

        private void placeEngager(PopupPanel popup, int left, int top,
                int width, int height) {
            popup.getWidget().setPixelSize(width, height);
            popup.setPopupPosition(left, top);
        }
    }

    protected static class FrameEngImpl extends EngImpl {
        @Override
        public void showEngagers(Element element, int strength) {
            Coordniates coords = JavaScriptObject.createObject().cast();
            coords.init(element, strength,
                    AppController.isGoodElement(element));
            showEngagers(coords);
        }

        @Override
        public void showEngagers(Coordniates coords) {
            CrossMessager.sendMessageToParent(PICKER_ENGAGE_EVENT,
                    StringUtils.stringifyObject(coords));
        }

        @Override
        public void hideEngagers() {
            CrossMessager.sendMessageToParent(PICKER_UN_ENGAGE_EVENT, "");
        }
    }

    @Override
    public void onEngage(NativeEvent event) {
        if (overRegn != null) {
            onPreviewNativeEvent(new PreviewEngageEvent(event));
        }
    }
}
