package co.quicko.whatfix.overlay;

import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;

public class CloseableStepPop extends StepPop {
    public CloseableStepPop(Step step, String footnote, String placement,
            String stepnote) {
        super(step, footnote, placement, stepnote);
    }

    @Override
    public boolean closeable() {
        return Themer.close();
    }
}
