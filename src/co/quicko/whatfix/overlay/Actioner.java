package co.quicko.whatfix.overlay;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.Browser;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.logger.NFRLogger;
import co.quicko.whatfix.common.logger.NFRProperty;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.common.validation.ValidationUtil;
import co.quicko.whatfix.data.CountAndTimestamp;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Draft.Condition;
import co.quicko.whatfix.data.Draft.Mark;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.StaticStepWrapper;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.overlay.BackUpFinder.BackUpListener;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.CrossMessager.CrossSourceListener;
import co.quicko.whatfix.overlay.Engager.EngImpl.Coordniates;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.actioner.ActionerPopover;
import co.quicko.whatfix.overlay.actioner.ActionerUtils;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;
import co.quicko.whatfix.overlay.actioner.Relocator.Reller;
import co.quicko.whatfix.overlay.elementwrap.ElementWrap.WrapScheduler;
import co.quicko.whatfix.overlay.elementwrap.WrapObserver;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.FindListener;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.app.AppFactory;
import co.quicko.whatfix.workflowengine.app.SalesforceApp;
import co.quicko.whatfix.workflowengine.data.strategy.ExistStrategy;
import co.quicko.whatfix.workflowengine.data.strategy.Strategies;

public class Actioner {

    private static final String ELEMENT_FOUND = "element_found";
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();
    // 5s
    static final int SPOTLIGHT_HIDE_INTERVAL = 5000;

    // element finder
    private static List<Responder> responders;

    // marked true if the step is missed & optional. if next step is also
    // optional then it can have lesser retries as page load is already over.
    static boolean missedOptional;

    static PopupPanel[] spotlights;
    static HandlerRegistration[] mouseOutRegisters;
    /**
     * Hides the spotlight in case the next step is not shown within
     * SPOTLIGHT_HIDE_INTERVAL.
     */
    static Timer hideSpotlightTimer = new Timer() {
        @Override
        public void run() {
            Actioner.disposePanels();
        }
    };

    private static StaticQueue queue;

    public static final String SEPERATOR = "!!!";

    public static Settings settings = new CustomizerSettings();
    
    private static Responder desktopRspd;
    
    private Actioner() {
    }

    public static void initialize() {
        // Just to make sure that initialize is happening only once.
        if (responders != null) {
            return;
        }

        responders = new ArrayList<>();

        if (OverlayUtil.isPrimaryWindow()) {
            initializeTopListeners();

        } else {
            initializeFrameListeners();

            /*
             * Case 1: There are cases when the actioner is not initialized on
             * the top and the iframe is requesting for actioner settings. In
             * that case iframe won't receive the settings. We need to send the
             * settings again during playing the smart tip collection
             * 
             * Case 2: The top already has the settings and is listening for
             * actioner_send_settings. Iframe reloads and it requests the top
             * for settings
             */
            // WME-13
            if (!SalesforceApp.isHostExcluded()) {
                CrossMessager
                        .sendMessageToTop("enterprise_send_settings", "");
            }
        }
        // can do custom configurations on actioner initialization for IS apps
        AppController.onActionerInit();
        BackUpFinder.initialize();

        /**
         * desktop cross messages elementFound -> when coordinates have been
         * received from c# and we need to show the popup 
         * hideElement -> weather the element was not there on the screen or 
         * has been hidden due to any reason, optional step to be taken care of 
         * showNext -> on completion of step action
         * 
         * cant use AppFactory as Enterpriser is not initialized till now
         **/
        if (OverlayUtil.isDesktopDevice()) {
            initializeDesktopListeners();
        }

    }

    private static void initializeDesktopListeners() {
        final CrossListener elementFoundListener = (String type,
                String content) -> {
            // hide any existing popover in case of reposition of
            // element
            Step capturedStep = DataUtil.create(content);
            if(capturedStep.marks() != null) {
                desktopRspd.getHelper().hidePopper();
                JsArray<Mark> capturedMarks = capturedStep.marks();
                if (JsUtils.isNotEmpty(capturedMarks)
                        && !isMarksSameAsCurrentStep(capturedMarks)) {
                    return;
                }
                Coordniates coords = DataUtil.create();
                coords.top(capturedStep.top());
                coords.left(capturedStep.left());
                coords.bottom(capturedStep.top() + capturedStep.height());
                coords.right(capturedStep.left() + capturedStep.width());
                coords.offsetWidth(capturedStep.width());
                coords.offsetHeight(capturedStep.height());
                desktopRspd.getHelper().showPop(coords);
            }
        };
        CrossMessager.addListener(elementFoundListener,
                "element_found_desktop");

        final CrossListener hideElementListener = (String type,
                String content) -> {
            if (desktopRspd != null) {
                Step capturedStep = DataUtil.create(content);
                JsArray<Mark> capturedMarks = capturedStep.marks();
                if (JsUtils.isNotEmpty(capturedMarks)
                        && !isMarksSameAsCurrentStep(capturedMarks)) {
                    return;
                }
                desktopRspd.getHelper().hidePopper();
                if (desktopRspd.getDraft()
                        .step_optional(desktopRspd.getStep())) {
                    desktopRspd.getHelper().handleNext();
                }
            }
        };
        CrossMessager.addListener(hideElementListener, "hide_element_desktop");

        final CrossListener showNextListener = (String type,
                String content) -> {
            Step step = DataUtil.create(content);
            if (step.step() == desktopRspd.getStep()) {
                desktopRspd.getHelper().handleNext();
            }
        };
        CrossMessager.addListener(showNextListener, "show_next_desktop");
    }

    private static void initializeFrameListeners() {
        // frames support for showing elements. listener never removed.
        CrossMessager.addListener((CrossListener) (type,
                content) -> ActionerUtils.actionerShow(content),
                "actioner_show");

        CrossMessager
                .addListener(
                        (CrossListener) (type, content) -> ActionerUtils
                                .actionerSettings(content),
                        "actioner_settings");

        CrossMessager
                .addListener(
                        (CrossListener) (type, content) -> ActionerUtils
                                .enterpriseSettings(content),
                        "enterprise_settings");

        CrossMessager.addListener(
                (CrossListener) (type, content) -> ActionerUtils
                        .enterpriseSettingsAllFrames(type, content),
                "enterprise_settings_all_frames");

        CrossMessager.addListener((CrossListener) (type,
                content) -> ActionerUtils.findElement(content), "find_element");
    }

    private static void initializeTopListeners() {
        makeSpotlights();
        queue = new StaticQueue();
        // schedule based on varying time intervals?

        CrossMessager.addListener((CrossListener) (type,
                content) -> ActionerUtils.actionerReshow(content),
                "actioner_reshow");

        // in case a iframe reloads, it rewill request
        // enterprise, cutomizer and actioner settings from top
        CrossMessager.addListener(
                (CrossSourceListener) (type, content, source) -> ActionerUtils
                        .enterpriseSendSettings(source),
                "enterprise_send_settings");
    }

    private static boolean isMarksSameAsCurrentStep(
            JsArray<Mark> capturedMarks) {
        if(null == desktopRspd || null == desktopRspd.getDraft()) {
            return false;
        }
        JsArray<Mark> storedMarks = desktopRspd.getDraft()
                .step_marks(desktopRspd.getStep());
        if (JsUtils.isEmpty(storedMarks)
                || capturedMarks.length() < storedMarks.length()) {
            return false;
        }
        // the order of the stored values would always be the same : name,
        // automation_id, class_name, type
        for (int index = 0; index < storedMarks.length(); index++) {
            if (!storedMarks.get(index).value()
                    .equals(capturedMarks.get(index).value())) {
                return false;
            }
        }
        return true;
    }

    public static Responder getResponder(String flowId, int step) {
        for (int index = 0; index < responders.size(); index++) {
            Responder responder = responders.get(index);
            if (responder.isForState(flowId, step)) {
                return responder;
            }
        }
        return null;
    }

    public static void showBeacons(Draft draft) {
        JavaScriptObjectExt jsObj = draft.custom_data().cast();
        if (0 == jsObj.keys().length) {
            showCollection(draft, ContentType.beacon);
        } else {
            // calling hide to destory beacon responders if any before the
            // beacon is shown 
            hide(ContentType.beacon);
            for (int index = 1; index <= draft.steps(); index++) {
                StaticStep step = new StaticStep(draft, index);
                CountAndTimestamp cs = jsObj.value(
                        draft.flow_id() + ":" + draft.step_key(step.step))
                        .cast();
                if (!cs.seen()) {
                    show(step.draft, step.step, 0, false);
                }
            }
        }
    }

    /**
     * @param draft
     */
    public static void showAssistantTip(Draft draft) {
        show(draft, 1, 0, true);
    }

    public static void showStatic(Draft draft) {
        CrossMessager.sendMessageToFrames("actioner_settings",
                StringUtils.stringifyObject(ActionerSettings.copy(settings)));

        showCollection(draft, ContentType.tip);
    }

	private static void showCollection(Draft draft, ContentType contentType) {
		hide(contentType);

		for (int index = 1; index <= draft.steps(); index++) {
			StaticStep step = new StaticStep(draft, index);
			if (step.draft.step_parents(step.step) > 0) {
				queue.addToQueue(step);
			} else {
				show(step.draft, step.step, 0, false);
			}
		}
	}

    private static class StaticQueue implements RepeatingCommand {
        
        private static int STEP_SHOW_QUEUE_GAP = 10;
        private static int STEP_SHOW_QUEUE_LONG_GAP = 300;

        private List<StaticStep> queue;

        public StaticQueue() {
            queue = new ArrayList<StaticStep>();
        }

        public void addToQueue(StaticStep step) {
            if (!step.canBeShown()) {
                return;
            }
            if (queue.isEmpty()) {
                if(step.draft.step_parents(step.step)>0) {
                    impl.scheduleFixedDelay(this, STEP_SHOW_QUEUE_LONG_GAP);
                } else {
                    impl.scheduleFixedDelay(this, STEP_SHOW_QUEUE_GAP);
                }
                
            }
            queue.add(step);
        }

        @Override
        public boolean execute() {
            if (queue.isEmpty()) {
                return false;
            }
            StaticStep step = queue.remove(0);
            show(step.draft, step.step, 0, false);
            return true;
        }
    }

    private static class StaticStep {
        private Draft draft;
        private int step;

        public StaticStep(Draft draft, int step) {
            this.draft = draft;
            this.step = step;
        }

        private boolean canBeShown() {
            StaticStepWrapper fullStep = (StaticStepWrapper) draft.step(step);
            return !fullStep.seen();
        }
    }

    public static void reShow(Responder responder) {
        Draft draft = responder.getDraft();
        int step = responder.getStep();
        if (responder.isForCollection()) {
            hide(draft, step);
            queue.addToQueue(new StaticStep(draft, step));
            return;
        } else {
            show(draft, step);
        }
    }

    public static void show(Draft draft, int step) {
        show(draft, step, null);
    }

    public static void show(Draft draft, int step,
            FindListener[] findListeners) {
        show(draft, step, 0, true, findListeners);
    }

    public static void show(Draft draft, int step, int parent,
            boolean doHide) {
        show(draft, step, parent, doHide, null);
    }

    private static void show(Draft draft, int step, int parent, boolean doHide,
            FindListener[] findListeners) {
        if (doHide) {
            hide(draft, step);
        }
        Responder rspd;
        if (AppFactory.isDesktopApp() || draft.is_desktop_mode()) {
            rspd = new DesktopResponder(draft, step, findListeners);
            // using a static object for handling cross messaging between
            // nwjs and whatfix embed
            if(desktopRspd != null) {
                desktopRspd.destroy();
            }
            desktopRspd = rspd;
        } else if(AppFactory.isMobileApp()) {
            rspd = new MobileResponder(draft, step, findListeners);
        } else {
            int parents = draft.step_parents(step);
            if (parents == 0) {
                // no frames confusion, direct usage.
                rspd = new DirectResponder(draft, step, findListeners);

            } else if (parents == parent) {
                // the frame that contains the element to be shown.
                rspd = new SourceResponder(draft, step, parent, findListeners);

            } else if (parent == 0) {
                // top window where element is inside frame
                rspd = new TopResponder(draft, step, parent, findListeners);

            } else {
                // frames in the path to element
                rspd = new MiddleResponder(draft, step, parent, findListeners);
            }
        }
        LOGGER.log(WfxLogLevel.DEBUG, Actioner.class, "show", WfxModules.JIT, "Initialized Responder: {0}",
                rspd.getClass().getSimpleName());
        if(null == responders) {
        	responders = new ArrayList<>();
        }
        responders.add(rspd);
        rspd.startSearcher();
    }

    public static void hide(Draft draft, int step) {
        if(responders != null && !responders.isEmpty()) {
            Iterator<Responder> iterator = responders.iterator();
            while (iterator.hasNext()) {
                Responder responder = iterator.next();
                if (responder.isForState(draft.flow_id(), step)) {
                    iterator.remove();
                    responder.destroy();
                    break;
                }
            }
        }
    }

    public static void hide() {
        if(responders != null && !responders.isEmpty()) {
            Iterator<Responder> iterator = responders.iterator();
            while (iterator.hasNext()) {
                Responder responder = iterator.next();
                iterator.remove();
                responder.destroy();
            }
        }
    }

    public static void hide(ContentType contentType) {
        if(responders != null && !responders.isEmpty()) {
            Iterator<Responder> iterator = responders.iterator();
            while (iterator.hasNext()) {
                Responder responder = iterator.next();
                if (contentType == responder.type()) {
                    iterator.remove();
                    responder.destroy();
                }
            }
        }
        if (ContentType.flow.equals(contentType)
                && OverlayUtil.isPrimaryWindow()) {
            BackUpFinder.hide();
        }
        
    }
    
    private static void disposePanels() {
        if (spotlights == null)
            return;
        for (PopupPanel e : spotlights) {
            e.hide();
        }
    }
    
    public static void dispose() {
        disposePanels();
        AutoExecGlass.destroyAutoExecGlass();
    }

    public static void onTheme() {
        makeSpotlights();
    }

    public static PopupPanel[] makeSpotlights() {
        double opacity = Double.parseDouble(Themer.defaultValue("opacity"));
        try {
            opacity = Double.parseDouble(Themer.opacity());
        } catch (Exception e) {
            // Ignore, mostly due to wrongly configured opacity
        }
        return setupSpotlightWithOpacity(opacity);
    }

    public static PopupPanel[] setupSpotlightWithOpacity(double opacity) {
        int spotlightLayers = Customizer.getSpotlightLayers();
        int layers = spotlightLayers > 0 ? 2 : 1;
        double initialOpacity = opacity;
        if (layers > 1) {
            // This is the portion of light which should come out. opacity 0.7
            // means 0.3 amount of light should come out of the glass
            double penetration = 1 - opacity;
            // 2 layer system. hence calculate penetration/layer
            penetration = Math.sqrt(penetration);
            opacity = 1 - penetration;
        }
        // with the feature of spotlight at step level, we need to construct the
        // spotlight again as new
        if (null != spotlights) {
            for (int i = 0; i < spotlights.length; i++) {
                spotlights[i].removeFromParent();
            }
        }
        spotlights = new PopupPanel[layers * 4 + 1];
        for (int i = 0; i < spotlights.length; i++) {
            PopupPanel spotlight = new NoContentPopup();
            spotlight.setStyleName(Overlay.CSS.spotlight());
            spotlight.addStyleName(Overlay.CSS.spotlightTransition());
            spotlight.getElement().getStyle().setOpacity(opacity);
            if (i == spotlights.length - 1) {
                // last element is on top of the element for smooth
                // transition.This opacity remains same
                spotlight.getElement().getStyle().setOpacity(initialOpacity);
            }
            spotlight.setAutoHideEnabled(false);
            spotlights[i] = spotlight;
        }

        return spotlights;
	}

    static class DirectObserver implements WrapObserver {
        private Responder responder;
        private ResponderHelper helper;
        private boolean showFocus = true;

        public Responder getResponder() {
            return responder;
        }

        public DirectObserver(Responder responder) {
            this.responder = responder;
            this.helper = responder.getHelper();
        }

        @Override
        public void onInit(Element source) {
            if (!ValidationUtil.isSupportedInput(source) || StringUtils
                    .isBlank(ValidationUtil.getValue(source))) {
                return;
            }
            validate(source, Common.hasFocus(source));
        }

        @Override
        public void onAction(Element element) {
            LOGGER.log(WfxLogLevel.DEBUG, DirectObserver.class, "onAction",
                    WfxModules.JIT, "Setting Action Element");
            helper.setActionElement(element);
            responder.onNext();
        }
        
        @Override
        public void onNearOut() {
            helper.showEngagers();
        }

        @Override
        public void onNearOver() {
            helper.hideEngagers();
        }

        @Override
        public void onActivate(Element element) {
            helper.placePop(element);
        }

        @Override
        public void onDeactivate(Element element) {
            helper.hidePopper();
        }

        @Override
        public void onEmphasize(Element source) {
            if (!helper.isValid()) {
                helper.showErrorPopup(source);
            }
        }

        @Override
        public void onKeyDown(Element source) {
            if (showFocus) {
                helper.handleKeyDownListener();
                helper.setWcagListener(source);
            }
            showFocus = !showFocus;
        }

        @Override
        public void onFocusOut(Element source) {
            if (!ValidationUtil.isSupportedInput(source)) {
                return;
            }
            validate(source, false);
        }

        private void validate(Element source, boolean showErrorPopup) {
            helper.validate(source);
            if (!helper.isValid()) {
                helper.onValidationError();
                helper.showErrorEngagers(source);
                if (showErrorPopup) {
                    helper.showErrorPopup(source);
                }
            } else {
                helper.disposeErrorPopper(false);
            }
        }

        @Override
        public void onDeEmphasize() {
            helper.hideErrorPopup();
        }

        @Override
        public void moveState() {
            CrossMessager.sendMessage(Action.DISABLE_MOVE_STATE,
                    Boolean.FALSE.toString());
        }

        @Override
        public void onStepCompletion(String action) {
            helper.onStepCompletion(action);
        }
    }

    static class CrossObserver implements WrapObserver {
        private Responder responder;
        private boolean showFocus = true;

        public Responder getResponder() {
            return responder;
        }

        public CrossObserver(Responder responder) {
            this.responder = responder;
        }

        @Override
        public void onInit(Element source) {
            if (!ValidationUtil.isSupportedInput(source) || StringUtils
                    .isBlank(ValidationUtil.getValue(source))) {
                return;
            }
            validate(source, Common.hasFocus(source));
        }

        @Override
        public void onAction(Element element) {
            responder.getHelper().setActionElement(element);
            String message = responder.getState();
            Set<String> type = new HashSet<String>();
            type.add("action_element_selector");
            type.add("action_element_text");
            type.add("element_selector");
            type.add("element_text");
            JavaScriptObjectExt branchingEvaluatedData = ((SourceResponder) responder)
                    .getBranchingEvaluatedData();
            evaluateAndFillElementConditions(responder.getFullStep(),
                    branchingEvaluatedData, type);
            if (!branchingEvaluatedData.isEmpty()) {
                message += SEPERATOR
                        + StringUtils.stringifyObject(branchingEvaluatedData);
            }
            CrossMessager.sendMessageToTop("actioner_next", message);
        }

        @Override
        public void onNearOver() {
            CrossMessager.sendMessageToTop("actioner_hide_eng",
                    responder.getState());
        }

        @Override
        public void onNearOut() {
            CrossMessager.sendMessageToTop("actioner_show_eng",
                    responder.getState());
        }

        @Override
        public void onActivate(Element source) {
            Coordniates coords = DataUtil.create();
            coords.init(source, 0);
            CrossMessager.sendMessageToParent("actioner_static_show",
                    responder.getState(coords));
        }

        @Override
        public void onDeactivate(Element source) {
            CrossMessager.sendMessageToTop("actioner_over",
                    responder.getState());
        }

        @Override
        public void onEmphasize(Element source) {
            if (!responder.getHelper().isValid()) {
                Coordniates coords = DataUtil.create();
                coords.init(source, 0);
                CrossMessager.sendMessageToParent("actioner_static_show_error",
                        responder.getState(coords));
            }
        }

        @Override
        public void onDeEmphasize() {
            CrossMessager.sendMessageToTop("actioner_static_hide_error",
                    responder.getState());
        }

        @Override
        public void onFocusOut(Element source) {
            if (!ValidationUtil.isSupportedInput(source)) {
                return;
            }
            validate(source, false);
        }

        @Override
        public void onKeyDown(Element source) {
            if (showFocus) {
                CrossMessager.sendMessageToTop("send_focus_on_tip",
                        responder.getState());
                responder.getHelper().setWcagListener(source);
            }
            showFocus = !showFocus;
        }

        private void validate(Element source, boolean showErrorPopup) {
            responder.getHelper().validate(source);
            Coordniates coords = DataUtil.create();
            coords.init(source, 0);
            if (!responder.getHelper().isValid()) {
                CrossMessager.sendMessageToTop(
                        "actioner_static_validation_error",
                        responder.getState());
                CrossMessager.sendMessageToParent(
                        "actioner_static_show_error_engager",
                        responder.getState(coords));
                if (showErrorPopup) {
                    CrossMessager.sendMessageToParent(
                            "actioner_static_show_error",
                            responder.getState(coords));
                }
            } else {
                CrossMessager.sendMessageToTop("actioner_static_validation_end",
                        responder.getState(coords));

            }
        }

        @Override
        public void moveState() {
            CrossMessager.sendMessage(Action.DISABLE_MOVE_STATE,
                    Boolean.FALSE.toString());
        }

        @Override
        public void onStepCompletion(String action) {
            CrossMessager.sendMessageToTop("actioner_step_completion",
                    responder.getState(action));
        }
    }

    static class DirectReller implements Reller {
        private Draft draft;
        private Step step;
        protected ResponderHelper helper;

        public DirectReller(Draft draft, Step step, Responder responder) {
            this.draft = draft;
            this.step = step;
            this.helper = responder.getHelper();
        }

        @Override
        public void gone() {
            LOGGER.log(WfxLogLevel.DEBUG, DirectReller.class, "gone",
                    WfxModules.JIT, "Flow: {0}, Step No.: {1}", draft.title(),
                    step.step());
            show(draft, step.step());
        }

        @Override
        public void moved(Element element) {
            if (OverlayUtil.isOnScreen(element)) {
                helper.movePop(element);
            } else {
                helper.hidePopper();
            }
        }

        @Override
        public void scrolled() {
            helper.hideOnScroll();
        }

        @Override
        public boolean scrollAsMoved() {
            return false;
        }

        @Override
        public boolean same(Element element) {
            return element == Finder.findBestOne(Document.get(), draft, step);
        }

        @Override
        public void overLapped(Element element) {
            helper.hideOnOverlap(element);
        }

        @Override
        public void overCleared(Element element) {
            helper.movePop(element);
        }

        @Override
        public boolean trackUnload() {
            return false;
        }

        @Override
        public boolean trackOverlap() {
            return true;
        }

        @Override
        public boolean scrollOnHideEnabled(Element element) {
            return helper.hideOnScrollEnabled(element);
        }
        
        @Override
        public void stabilized(Element element) {
            helper.onStabilized(element);
        }
    }

    static class DirectErrorReller extends DirectReller {

        public DirectErrorReller(Draft draft, Step step, Responder responder) {
            super(draft, step, responder);
        }

        @Override
        public void moved(Element element) {
            super.moved(element);
            if (OverlayUtil.isOnScreen(element)) {
                showErrorPopupOnMove(element);
            } else {
                helper.hideErrorEngagers();
                helper.hideErrorPopup();
            }
        }

        @Override
        public void overLapped(Element element) {
            super.overLapped(element);
            helper.hideErrorEngagers();
            helper.hideErrorPopup();
        }

        @Override
        public void overCleared(Element element) {
            super.overCleared(element);
            showErrorPopupOnMove(element);
        }

        private void showErrorPopupOnMove(Element element) {
            if (!helper.isValid()) {
                helper.showErrorEngagers(element);
                if (Common.hasFocus(element)) {
                    helper.showErrorPopup(element);
                }
            }
        }
    }

    static class SourceReller implements Reller {
        protected int parent;
        protected Responder responder;

        public SourceReller(int parent, Responder responder) {
            this.parent = parent;
            this.responder = responder;
        }

        @Override
        public void gone() {
            CrossMessager.sendMessageToTop("actioner_reshow",
                    responder.getState());
        }

        @Override
        public void moved(Element element) {
            Coordniates coords = DataUtil.create();
            coords.init(element, 0);
            CrossMessager.sendMessageToParent("actioner_move",
                    responder.getState(coords));
        }

        @Override
        public boolean scrollAsMoved() {
            return true;
        }

        @Override
        public boolean same(Element element) {
            return element == Finder.findBestOne(Document.get(),
                    responder.getDraft(), responder.getFullStep());
        }

        @Override
        public void overLapped(Element element) {
            CrossMessager.sendMessageToTop("actioner_over",
                    responder.getState());
        }

        @Override
        public void overCleared(Element element) {
            moved(element);
        }

        @Override
        public boolean trackUnload() {
            return true;
        }

        @Override
        public boolean trackOverlap() {
            return true;
        }

        @Override
        public void scrolled() {
            CrossMessager.sendMessageToTop("actioner_hide_on_move",
                    responder.getState());
        }

        @Override
        public boolean scrollOnHideEnabled(Element element) {
            return responder.getHelper().hideOnScrollEnabled(element);
        }
        
        @Override
        public void stabilized(Element element) {
            responder.getHelper().onStabilized(element);
        }
    }

    static class SourceErrorReller extends SourceReller {

        public SourceErrorReller(int parent, Responder responder) {
            super(parent, responder);
        }

        @Override
        public void moved(Element element) {
            super.moved(element);
            Coordniates coords = DataUtil.create();
            coords.init(element, 0);
            if (!responder.getHelper().isValid()) {
                CrossMessager.sendMessageToParent(
                        "actioner_static_show_error_engager",
                        responder.getState(coords));
                if (Common.hasFocus(element)) {
                    CrossMessager.sendMessageToParent(
                            "actioner_static_show_error",
                            responder.getState(coords));
                }
            }
        }

        @Override
        public void overLapped(Element element) {
            CrossMessager.sendMessageToTop("actioner_static_hide_error_engager",
                    responder.getState());
            CrossMessager.sendMessageToTop("actioner_static_hide_error",
                    responder.getState());
        }

    }

    static class MiddleReller extends SourceReller {
        public MiddleReller(int parent, Responder responder) {
            super(parent, responder);
        }

        @Override
        public void moved(Element element) {
            gone();
        }

        @Override
        public boolean same(Element element) {
            Element best = Finder.findBest(Document.get(),
                    responder.getFullStep(), parent);
            return best == null ? element == null : element == best;
        }

        @Override
        public void overLapped(Element element) {
            // Very complex to handle overlap for middle iframes, Ignoring.
        }

        @Override
        public void overCleared(Element element) {
        }

        @Override
        public boolean trackOverlap() {
            return false;
        }
        
        @Override
        public void stabilized(Element element) {
            //DO nothing
        }
    }

    static class TopReller extends MiddleReller {
        public TopReller(int parent, Responder responder) {
            super(parent, responder);
        }

        @Override
        public boolean scrollAsMoved() {
            return false;
        }

        @Override
        public boolean trackUnload() {
            return false;
        }

        @Override
        public boolean trackOverlap() {
            /*
             * when element is inside the iframe, on video full screen the
             * element overlap not identified by the sourceReller. Need to check
             * overlap at top level as well
             */
            return true;
        }

    }

    public static class PartState extends StepState {
        protected PartState() {
        }

        public final native int parent() /*-{
			return this.parent;
        }-*/;

        public final native void parent(int parent) /*-{
			this.parent = parent;
        }-*/;
    }

    // the configurable entities
    static ActionerListener listener;
    static ActionerImpl impl;

    public static void setActionerListener(ActionerListener l) {
        listener = l;
        BackUpFinder.setBackUpListener((BackUpListener) l);
    }
    
    public static void setImpl(ActionerImpl i) {
        impl = i;
    }

    public static interface PopperListener extends PopListener {
        public void onMiss(JsArray<JavaScriptObjectExt> reason);

        public void onShowed(Element shown);

        public void onOptionalNext();
    }

    public static interface ActionerListener extends PopperListener {
        public void onNext(Step previousStep);

        public void onBack(Step action);

        public boolean branchBeforeShow(Step step, Draft draft);
        
        default void updateRecaptureStep(Step step, Element shown,
                AsyncCallback<Void> executeCb) {
          // overridden in TopGlue  
        }

    }

    public static class ActionerImpl extends PopoverExtender
            implements WrapScheduler {
        public void scheduleFixedDelay(RepeatingCommand cmd, int delayMs) {
            Scheduler.get().scheduleFixedDelay(cmd, delayMs);
        }
    }

    // scroll element if needed
    static int OVERLAY_AVG_HEIGHT = 80;
    static int OVERLAY_AVG_WIDTH = 220;

    static void alignPopover(ActionerPopover popover, boolean alignTop,
            Boolean hasSingleScroll, Element element) {
        Element popoverElement = null;
        if (popover != null) {
            if (null != popover.popover()) {
                popoverElement = popover.popover().getElement();
            }
        }
        if (null != popoverElement
                && (OverlayUtil.needScroll(popoverElement, 0, 0)
                        || !OverlayUtil.isOnScreen(popoverElement))) {
            hasSingleScroll = (hasSingleScroll == null)
                    ? OverlayUtil.hasSingleVerticalScroll(element,
                            Customizer.ignoreScrollOnBody())
                    : hasSingleScroll;
            if (!hasSingleScroll) {
                Common.scrollIntoView(popoverElement, alignTop);
            } else {
                scrollIntoCenterOfViewPort(popoverElement, "r", false);
            }

        }
    }

    static boolean alignTop(String placement) {
        return placement.indexOf('b') != -1;
    }

    static boolean alignRight(String placement) {
        return placement.indexOf('r') == 0;
    }

    public static void scrollIntoCenterOfViewPort(Element element,
            String placement, boolean horizontalScroll) {
        int verticalMidPoint;
        int horizontalMidPoint = 0;
        if (placement.contains("t")) {
            // scroll top of the element to the middle of view port
            verticalMidPoint = Actioner.getAbsoluteTop(element)
                    - (Window.getClientHeight()) / 2;

            horizontalMidPoint = Actioner.getAbsoluteLeft(element)
                    - (Window.getClientWidth()) / 2;

        } else if (placement.contains("b")) {
            // scroll bottom of the element to the middle of view port
            verticalMidPoint = Actioner.getAbsoluteTop(element)
                    + element.getClientHeight()
                    - (Window.getClientHeight()) / 2;

            horizontalMidPoint = Actioner.getAbsoluteLeft(element)
                    - (Window.getClientWidth() / 2) + element.getClientWidth();

        } else {
            // scroll middle of the element to the middle of view port
            verticalMidPoint = Actioner.getAbsoluteTop(element)
                    + (element.getClientHeight() - Window.getClientHeight())
                            / 2;

            horizontalMidPoint = Actioner.getAbsoluteLeft(element)
                    - (Window.getClientWidth() / 2)
                    + element.getClientWidth() / 2;
        }
        Window.scrollTo(horizontalScroll ? horizontalMidPoint : 0,
                verticalMidPoint);
    }

    public static final int getAbsoluteTop(Element element) {
        return element.getAbsoluteTop();
    }

    public static final int getAbsoluteLeft(Element element) {
        return element.getAbsoluteLeft();
    }

    public final static int getAbsoluteBottom(Element element) {
        return getAbsoluteTop(element) + element.getOffsetHeight();
    }

    public final static int getAbsoluteRight(Element element) {
        return getAbsoluteLeft(element) + element.getOffsetWidth();
    }

    public static Settings settings() {
        return settings;
    }

    protected static void settings(Settings value) {
        settings = value;
    }

    public static interface Settings {
        public int staticShowTime();

        public void staticShowTime(int staticShowTime);

        public int staticCloseTime();

        public void staticCloseTime(int staticCloseTime);

        public boolean staticClose();

        public void staticClose(boolean staticClose);
    }

    private static class CustomizerSettings implements Settings {

        @Override
        public int staticShowTime() {
            if (Customizer.getStaticShowTime() != -1) {
                return Customizer.getStaticShowTime();
            } else {
                return Integer
                        .parseInt(Themer.value(Themer.SMART_TIP.APPEAR_AFTER));
            }
        }

        public void staticShowTime(int staticShowTime) {
        }

        @Override
        public int staticCloseTime() {
            if (Customizer.getStaticCloseTime() != -1) {
                return Customizer.getStaticCloseTime();
                        
            } else {
                return Integer
                        .parseInt(
                                Themer.value(Themer.SMART_TIP.DISAPPEAR_AFTER));
            }
        }

        @Override
        public void staticCloseTime(int staticCloseTime) {
        }

        @Override
        public boolean staticClose() {
            if (Browser.isMobile()) {
                return true;
            } else {
                return Themer.value(Themer.SMART_TIP.CLOSE)
                    .equalsIgnoreCase("show");
            }
        }

        @Override
        public void staticClose(boolean staticClose) {
        }

    }

    static void evaluateAndFillElementConditions(Step step,
            JavaScriptObjectExt branchingEvaluatedData, Set<String> type) {
        if (step == null) {
            return;
        }
        JsArray<JsArray<Condition>> conditions = step.branch_conditions();
        if (!JsUtils.isNotEmpty(conditions)) {
            return;
        }
        for (int section = 0; section < conditions.length(); section++) {
            JsArray<Condition> conditionSet = conditions.get(section);
            JavaScriptObjectExt result = DataUtil.create();
            for (int index = 0; index < conditionSet.length(); index++) {
                Condition cond = conditionSet.get(index);
                if (type == null || type.contains(cond.type())) {
                    JsArray<Condition> condition = DataUtil.createArray()
                            .cast();
                    condition.push(cond);
                    result.put(String.valueOf(index),
                            String.valueOf(Strategies.isFit(step, condition)));
                }
            }
            if (!result.isEmpty()) {
                branchingEvaluatedData.put(String.valueOf(section), result);
            }
        }
    }

    public static void branchViaUI(final Step previousStep,
            final AsyncCallback<String[]> callback) {
        long sTime = System.currentTimeMillis();
        CrossMessager.removeAllListener(ELEMENT_FOUND);
        JsArray<JsArray<Condition>> conditions = previousStep
                .branch_conditions();
        if (conditions == null || conditions.length() == 0) {
            callback.onSuccess(null);
            return;
        }
        JavaScriptObjectExt evaluationData = previousStep
                .branch_element_evaluation();
        if (evaluationData == null) {
            evaluationData = DataUtil.create();
        }
        CrossListener found_listner = null;

        Set<String> pendingResponse = new HashSet<String>();
        for (int section = 0; section < conditions.length(); section++) {
            JsArray<Condition> conditionSet = conditions.get(section);
            if (conditionSet == null || conditionSet.length() == 0) {
                continue;
            }
            for (int index = 0; index < conditionSet.length(); index++) {
                Condition condition = conditionSet.get(index);
                if ("other_element".equals(condition.type())
                        && condition.operand(1).contains("<<")) {
                    int split = condition.operand(1).lastIndexOf("<<");
                    String selector = condition.operand(1).substring(split + 2);
                    JsArray<Element> elements = ExistStrategy
                            .getElements(condition.operator(), selector.trim());
                    if (elements == null || elements.length() == 0) {
                        String eval = condition.operator().startsWith("!")
                                ? "true"
                                : "false";
                        fillEvalData(String.valueOf(section),
                                String.valueOf(index), eval, evaluationData);
                    } else {
                        if (found_listner == null) {
                            found_listner = CrossMessager.addListener(
                                    getElementFoundListener(pendingResponse,
                                            evaluationData, previousStep,
                                            callback),
                                    ELEMENT_FOUND);
                        }
                        JavaScriptObjectExt data = DataUtil.create();
                        data.put("section", "" + section);
                        data.put("index", "" + index);
                        data.put("operator", condition.operator());
                        data.put("selector",
                                condition.operand(1).substring(0, split));
                        pendingResponse.add(section + ":" + index);
                        CrossMessager.sendMessageToFrame(elements.get(0),
                                "find_element",
                                StringUtils.stringifyObject(data));
                    }
                }
            }
        }
        if (pendingResponse.isEmpty()) {
            String[] flowAndKeyToBranch = decidebranchViaUi(evaluationData,
                    previousStep);
            NFRLogger.addLog(NFRProperty.BRANCHING,
                    System.currentTimeMillis() - sTime);
            callback.onSuccess(flowAndKeyToBranch);
        }
    }

    private static void fillEvalData(String section, String index, String eval,
            JavaScriptObjectExt evaluationData) {
        JavaScriptObjectExt itr_obj = evaluationData.value(section).cast();
        if (itr_obj == null) {
            itr_obj = DataUtil.create();
        }
        itr_obj.put(index, eval);
        evaluationData.put(section, itr_obj);
    }

    private static String[] decidebranchViaUi(JavaScriptObjectExt evaluationData,
            Step previousStep) {
        JsArray<JsArray<Condition>> conditions = previousStep
                .branch_conditions();
        if (!evaluationData.isEmpty()) {
            conditions = StringUtils.copyArray(conditions);
            for (String index : evaluationData.keys()) {
                JavaScriptObjectExt eval = evaluationData.value(index).cast();
                JsArray<Condition> conditionSet = conditions
                        .get(Integer.parseInt(index));
                for (String i : eval.keys()) {
                    Condition cond = conditionSet.get(Integer.parseInt(i));
                    cond.evaluated_value(eval.valueAsString(i));
                }
            }
        }
        for (int index = 0; index < conditions.length(); index++) {
            JsArray<Condition> conditionSet = conditions.get(index);
            if (Strategies.isFit(previousStep, conditionSet)) {
                String destination = previousStep.branch_targets().get(index);
                int colonIndex = destination.indexOf(":");
                if (colonIndex == -1) {
                    return null;
                }
                String flow_id = destination.substring(0, colonIndex);
                if (StringUtils.isBlank(flow_id) || "-".equals(flow_id)) {
                    return null;
                }
                String step_key = destination.substring(colonIndex + 1);
                return (new String[] { flow_id, "", step_key });
            }
        }
        return null;
    }

    private static CrossListener getElementFoundListener(
            Set<String> pendingResponse,
            final JavaScriptObjectExt evaluationData, final Step previousStep,
            final AsyncCallback<String[]> callback) {
        return new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                JavaScriptObjectExt data = DataUtil.create(content);
                String section = data.valueAsString("section");
                String index = data.valueAsString("index");
                pendingResponse.remove(section + ":" + index);
                boolean eval = Boolean.valueOf(data.valueAsString("found"));
                String operator = data.valueAsString("operator");
                eval = operator.startsWith("!") ? !eval : eval;
                fillEvalData(section, index, String.valueOf(eval),
                        evaluationData);
                if (pendingResponse.isEmpty()) {
                    CrossMessager.removeAllListener(ELEMENT_FOUND);
                    String[] flowAndKeyToBranch = decidebranchViaUi(
                            evaluationData, previousStep);
                    callback.onSuccess(flowAndKeyToBranch);
                }
            }
        };
    }

    public static void showUserActions(JsArray<Flow> userActions) {
        for (int i = 0; i < userActions.length(); i++) {
            show(userActions.get(i), 1, 0, false);
        }
    }

}
