package co.quicko.whatfix.overlay;


import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.data.EmbedData.Settings;

public abstract class Launcher extends FixedPopup implements ResizeHandler,
        ClickHandler, CloseHandler<PopupPanel> {
    protected Settings settings;
    protected String prefix;

    protected Frame frame;
    protected FixedPopup widgetPopup;
    protected HandlerRegistration resizeRegn;
    public static final String MOBILE = "mobile";
    public static final String FULL = "full";
    private static final String FRAME_TITLE = "Task List";

    public Launcher(Settings settings, String prefix) {
        this(settings);
        this.prefix = prefix;

        frame = getFrame();
        frame.addStyleName(Overlay.CSS.widget());
        frame.setTitle(FRAME_TITLE);

        widgetPopup = new FixedPopup() {
            @Override
            public void show() {
                super.show();
                refreshWidgetPosition();
            }
        };
        widgetPopup.setStyleName(Overlay.CSS.widget());
        widgetPopup.addCloseHandler(this);
        widgetPopup.setModal(false);
        widgetPopup.setAutoHideEnabled(true);
        widgetPopup.setAnimationEnabled(true);
        widgetPopup.setAutoHideOnHistoryEventsEnabled(true);
        widgetPopup.setWidget(frame);

        Common.setId(this, getWidgetId());
        Common.setId(widgetPopup, getLauncherId());

        onResize(null);
        resizeRegn = Window.addResizeHandler(this);

    }

    public Launcher(Settings settings) {
        this.settings = settings;
        if (settings.position() == null) {
            settings.position("rtm");
        }
        setStyleName(Overlay.CSS.widget());

        setModal(false);
        setAutoHideEnabled(false);
        setAnimationEnabled(false);
        setAutoHideOnHistoryEventsEnabled(false);

        Widget launcher = launcher(settings);
        setWidget(launcher);
    }

    protected Widget launcher(Settings settings) {
        String label = settings.label();
        if (label == null || label.length() == 0) {
            label = Overlay.CONSTANTS.widgetLauncherLabel(false);
        }

        Anchor selfHelp = new Anchor(label);
        selfHelp.addClickHandler(this);
        selfHelp.setStyleName(Overlay.CSS.widgetLauncher());

        String position = settings.position();
        if (position.startsWith("t") || position.startsWith("l")) {
            selfHelp.addStyleName(Overlay.CSS.widgetLauncherTop());
        } else {
            selfHelp.addStyleName(Overlay.CSS.widgetLauncherBottom());
        }

        String color = settings.color();
        if (color != null) {
            OverlayUtil.setStyle(selfHelp, OverlayUtil.BACKGROUND, color);
        }
        return selfHelp;
    }

    protected abstract Frame getFrame();

    protected String mode() {
        return FULL;
    }

    protected void destroy() {
        closeWidget();

        hide();

        resizeRegn.removeHandler();

    }

    protected void closeWidget() {
        processBeforeClose();
        widgetPopup.hide();
        show();
    }

    @Override
    public void onResize(ResizeEvent event) {
        refreshLauncherPosition();
        refreshWidgetPosition();
    }

    @Override
    public void onClose(CloseEvent<PopupPanel> event) {
        closeWidget();
    }

    @Override
    public void onClick(ClickEvent event) {
        hide();
        widgetPopup.show();
    }

    protected String prefix(String type) {
        return prefix + type;
    }

    protected void refreshLauncherPosition() {
        int w = getLauncherWidth();
        int h = getLauncherHeight();
        if (isShowing()) {
            w = getOffsetWidth();
            h = getOffsetHeight();
        }
        setPosition(this, w, h, true);
    }

    protected void refreshWidgetPosition() {
        setPosition(widgetPopup, getWidgetWidth(), getWidgetHeight(), false);
    }

    protected abstract void showDeck(String content);

    protected abstract int getLauncherWidth();

    protected abstract int getLauncherHeight();

    protected abstract int getWidgetWidth();

    protected abstract int getWidgetHeight();

    protected abstract String getLauncherId();

    protected abstract String getWidgetId();

    protected void setPosition(PopupPanel popup, int width, int height,
            boolean transform) {
        if (settings.target() == null) {
            setFixedPosition(popup, width, height, transform);
        } else {
            setRelPosition(popup, width, height);
        }
    }

    protected void setRelPosition(PopupPanel popup, int width, int height) {
        Element relative = null;
        try {
            relative = relativeElement(Document.get(), settings.target());
        } catch (Exception e) {
        }
        if (relative == null) {
            return;
        }

        Style popStyle = popup.getElement().getStyle();
        OverlayUtil.clearPosition(popStyle);
        setTop(popStyle, relative.getAbsoluteBottom());
        if ("tl-bl".equals(settings.position())) {
            setLeft(popStyle, relative.getAbsoluteLeft());
        } else {
            setLeft(popStyle, relative.getAbsoluteRight() - width);
        }
    }

    protected native static Element relativeElement(Document doc,
            String selector) /*-{
		return doc.querySelector(selector);
    }-*/;

    protected void setFixedPosition(PopupPanel popup, int width, int height,
            boolean transform) {
        int totalWidth = clientWidth();
        int totalHeight = clientHeight();

        Element elem = popup.getElement();
        Style style = elem.getStyle();
        OverlayUtil.clearPosition(style);

        boolean supportTransform = false;
        for (String property : TRANSFORM_PROPS) {
            if (style.getProperty(property) != null) {
                supportTransform = true;
                break;
            }
        }

        String position = settings.position();
        if (supportTransform) {
            clearProperty(style, TRANSFORM_PROPS);
            clearProperty(style, TRANSFORM_ORIGIN_PROPS);
        } else if (position.startsWith("l") || position.startsWith("r")) {
            position = "b";
        }

        if (position.startsWith("t")) {
            setTop(style, 0);
            setLeft(style, start(position, totalWidth, width, 0, "rm"));
        } else if (position.startsWith("b")) {
            setBottom(style, 0);
            setLeft(style, start(position, totalWidth, width, 0, "rm"));
        } else if (position.startsWith("l")) {
            setLeft(style, 0);
            if (transform) {
                setProperty(style, "rotate(270deg)", TRANSFORM_PROPS);
                setProperty(style, height + "px " + height + "px",
                        TRANSFORM_ORIGIN_PROPS);
            }
            setTop(style, start(position, totalHeight, height, 0, "bm"));
        } else {
            setRight(style, 0);
            int adjust = 0;
            if (transform) {
                setProperty(style, "rotate(270deg)", TRANSFORM_PROPS);
                setProperty(style, width + "px " + height + "px",
                        TRANSFORM_ORIGIN_PROPS);
                adjust = (height / 2) + (width / 2);
            }
            setTop(style, start(position, totalHeight, height, adjust, "bm"));
        }
    }

    @Override
    public void setPopupPosition(int left, int top) {
    }

    protected void processBeforeClose() {
        // Process to be executed before widget is destoryed
    }

    protected int start(String position, int total, int pixels, int adjust,
            String threeForth) {
        int start;
        if (position.length() == 1) {
            start = (total - pixels) / 2;
        } else if (position.endsWith(threeForth)) {
            start = total - (total / 4) - (pixels / 2);
        } else {
            start = (total / 4) - (pixels / 2);
        }

        start = start - adjust;

        if (start < 0) {
            return 0;
        } else if (start + pixels > total) {
            return total - pixels;
        } else {
            return start;
        }
    }

    private static String[] TRANSFORM_PROPS = new String[] { "webkitTransform",
            "MozTransform", "msTransform", "OTransform" };
    private static String[] TRANSFORM_ORIGIN_PROPS = new String[] {
            "webkitTransformOrigin", "MozTransformOrigin", "msTransformOrigin",
            "OTransformOrigin" };

    protected void clearProperty(Style style, String... names) {
        for (String property : names) {
            style.clearProperty(property);
        }
    }

    protected void setProperty(Style style, String value, String... names) {
        for (String name : names) {
            style.setProperty(name, value);
        }
    }

    protected void setLeft(Style style, int left) {
        style.setLeft(left, Unit.PX);
    }

    protected void setTop(Style style, int top) {
        style.setTop(top, Unit.PX);
    }

    protected void setRight(Style style, int right) {
        style.setRight(right, Unit.PX);
    }

    protected void setBottom(Style style, int bottom) {
        style.setBottom(bottom, Unit.PX);
    }

    protected int clientWidth() {
        return Window.getClientWidth();
    }

    protected int clientHeight() {
        return Window.getClientHeight();
    }


}
