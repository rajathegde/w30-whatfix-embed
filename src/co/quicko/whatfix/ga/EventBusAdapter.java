package co.quicko.whatfix.ga;

import static co.quicko.whatfix.ga.GaUtil.analyticsCache;

import com.google.gwt.core.client.JsArray;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.contextualinfo.StepCompletionInfo;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.Beacon;
import co.quicko.whatfix.overlay.Beacon.BeaconContent;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.player.beacon.event.BeaconClosedEvent;
import co.quicko.whatfix.player.beacon.event.BeaconShowedEvent;
import co.quicko.whatfix.player.exitpopup.event.ExitPopupCloseEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitCloseEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitDefaultEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitInfoTipShownEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitMaximizeEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitMinimizeEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitShownEvent;
import co.quicko.whatfix.player.gracefulexit.event.HideGracefulExitEvent;
import co.quicko.whatfix.player.tip.SmartTipPlayer;
import co.quicko.whatfix.player.tip.event.SmartTipAutoCompletedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipClosedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipMissedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipShowedEvent;
import co.quicko.whatfix.player.tip.event.ValidationTipErrorEvent;
import co.quicko.whatfix.player.tip.event.ValidationTipRectifiedEvent;
import co.quicko.whatfix.player.tip.event.ValidationTipValidEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.workflowengine.data.finder.FinderLogger;

/**
 * This class is used only to send Analytics Events
 */
public class EventBusAdapter {

    public EventBusAdapter() {
        EventBus.addHandler(BeaconClosedEvent.class, this::onBeaconClosedEvent);
        EventBus.addHandler(BeaconShowedEvent.class, this::onBeaconShowedEvent);
        
        EventBus.addHandler(SmartTipShowedEvent.class, this::onStaticShowed);
        EventBus.addHandler(SmartTipMissedEvent.class, this::onStaticMiss);
        EventBus.addHandler(SmartTipClosedEvent.class, this::onStaticClose);
        EventBus.addHandler(SmartTipAutoCompletedEvent.class,
                this::onStaticAutoCompleted);
        
        EventBus.addHandler(ValidationTipErrorEvent.class, this::onValidationError);
        EventBus.addHandler(ValidationTipRectifiedEvent.class, this::onValidationRectified);
        EventBus.addHandler(ValidationTipValidEvent.class, this::onValidationValid);

        EventBus.addHandler(GracefulExitCloseEvent.class,
                this::onGracefulExitCloseEvent);
        EventBus.addHandler(GracefulExitMinimizeEvent.class,
                this::onGracefulExitMinimizeEvent);
        EventBus.addHandler(GracefulExitMaximizeEvent.class,
                this::onGracefulExitMaximizeEvent);
        EventBus.addHandler(GracefulExitDefaultEvent.class,
                this::onGracefulExitDefaultEvent);
        EventBus.addHandler(GracefulExitShownEvent.class,
                this::onGracefulExitShownEvent);
        EventBus.addHandler(HideGracefulExitEvent.class,
                this::onHideGracefulExitEvent);
        EventBus.addHandler(ExitPopupCloseEvent.class,
                this::onExitPopupCloseEvent);
        EventBus.addHandler(GracefulExitInfoTipShownEvent.class,
                this::onGracefulExitInfoTipShownEvent);
    }

    public void onValidationError(Event e) {
        ValidationTipErrorEvent event = (ValidationTipErrorEvent) e;
        Draft draft = event.getTipCollection();
        Step step = event.getStep();
        
        String validationCondition = GaUtil
                .extractConditions(step.data_validations());
        Common.tracker().onEvent(EmbedEvent.TIP_VALIDATE_ERROR.builder()
                .flow(draft.flow_id(), draft.title())
                .segment(Enterpriser.getActiveSmartTipSegment().segment_id(),
                        Enterpriser.getActiveSmartTipSegmentName(draft))
                .parseStep(step).validationCondition(validationCondition)
                .validationMessage(
                        JsUtils.getTextFromHtml(step.validation_errors_md()))
                .srcId(TrackerEventOrigin.SMART_TIPS).build());
    }

    public void onValidationRectified(Event e) {
        ValidationTipRectifiedEvent event = (ValidationTipRectifiedEvent) e;
        Draft draft = event.getTipCollection();
        Step step = event.getStep();
        
        String validationCondition = GaUtil
                .extractConditions(step.data_validations());
        Common.tracker().onEvent(EmbedEvent.TIP_VALIDATE_RECTIFIED.builder()
                .flow(draft.flow_id(), draft.title())
                .segment(Enterpriser.getActiveSmartTipSegment().segment_id(),
                        Enterpriser.getActiveSmartTipSegmentName(draft))
                .parseStep(step).validationCondition(validationCondition)
                .validationMessage(
                        JsUtils.getTextFromHtml(step.validation_errors_md()))
                .srcId(TrackerEventOrigin.SMART_TIPS).build());
    }

    public void onValidationValid(Event e) {
        ValidationTipValidEvent event = (ValidationTipValidEvent) e;
        Draft draft = event.getTipCollection();
        Step step = event.getStep();
        
        Common.tracker().onEvent(EmbedEvent.TIP_VALIDATE_VALID.builder()
                .flow(draft.flow_id(), draft.title())
                .segment(Enterpriser.getActiveSmartTipSegment().segment_id(),
                        Enterpriser.getActiveSmartTipSegmentName(draft))
                .parseStep(step).srcId(TrackerEventOrigin.SMART_TIPS).build());
    }
    
    public void onStaticShowed(Event e) {
        SmartTipShowedEvent event = (SmartTipShowedEvent) e;
        Step step = event.getStep();
        Draft flow = event.getDraft();

        PlayerBase.setCurrentPageTags();
        Common.tracker().onEvent(EmbedEvent.SMART_TIP_SHOW.builder()
                .flow(flow.flow_id(), flow.title())
                .segment(Enterpriser.getActiveSmartTipSegmentId(flow),
                        Enterpriser.getActiveSmartTipSegmentName(flow))
                .step(step.stepId(), step.description_md()).stepNumber(step.step())
                .currentPageTags(PlayerBase.getCurrentPageTags())
                .srcId(TrackerEventOrigin.SMART_TIPS.getSrcName())
                .disableInteraction()
                .fillFinderTimingData(SmartTipPlayer.getStepFindData())
                .foundElementScore(StringUtils.stringifyArray(FinderLogger
                        .getFinderSuccessComponent(step.step(), step.type())))
                .build());
    }

    public void onStaticAutoCompleted(Event e) {
        SmartTipAutoCompletedEvent event = (SmartTipAutoCompletedEvent) e;
        Step step = event.getStep();
        Draft flow = event.getDraft();
        String completionAction = event.getCompletionAction();
        StepCompletionInfo stepCompletionInfo = DataUtil.create();
        stepCompletionInfo.completion_action(completionAction);
        stepCompletionInfo.completion_type(StepCompletionInfo.AUTO);
        ContextualInfo contextualStepComplInfo = StepCompletionInfo
                .getStepCompletionInfo(ContextualInfo.OuterKey.STEP_DETAIL,
                        stepCompletionInfo);

        Common.tracker().onEvent(EmbedEvent.SMART_TIP_AUTO_COMPLETE.builder()
                .flow(flow.flow_id(), flow.title())
                .segment(Enterpriser.getActiveSmartTipSegmentId(flow),
                        Enterpriser.getActiveSmartTipSegmentName(flow))
                .step(step.stepId(), step.description_md()).stepNumber(step.step())
                .srcId(TrackerEventOrigin.SMART_TIPS.getSrcName())
                .contextualInfo(contextualStepComplInfo)
                .disableInteraction().build());
    }

    public void onStaticMiss(Event e) {
        SmartTipMissedEvent event = (SmartTipMissedEvent) e;
        Step step = event.getStep();
        Draft flow = event.getDraft();
        JsArray<JavaScriptObjectExt> reason = event.getReason();
        PlayerBase.setCurrentPageTags();
        Common.tracker().onEvent(EmbedEvent.SMART_TIP_MISS.builder()
                .flow(flow.flow_id(), flow.title())
                .segment(Enterpriser.getActiveSmartTipSegmentId(flow),
                        Enterpriser.getActiveSmartTipSegmentName(flow))
                .step(step.stepId(), step.description_md()).stepNumber(step.step())
                .currentPageTags(PlayerBase.getCurrentPageTags())
                .srcId(TrackerEventOrigin.SMART_TIPS.getSrcName())
                .disableInteraction().fillElementData(null, reason).build());
    }

    public void onStaticClose(Event e) {
        SmartTipClosedEvent event = (SmartTipClosedEvent) e;
        Step step = event.getStep();
        Draft flow = event.getDraft();
        
        PlayerBase.setCurrentPageTags();
        Common.tracker().onEvent(EmbedEvent.SMART_TIP_CLOSE.builder()
                .flow(flow.flow_id(), flow.title())
                .segment(Enterpriser.getActiveSmartTipSegmentId(flow),
                        Enterpriser.getActiveSmartTipSegmentName(flow))
                .step(step.stepId(), step.description_md()).stepNumber(step.step())
                .currentPageTags(PlayerBase.getCurrentPageTags())
                .srcId(TrackerEventOrigin.SMART_TIPS.getSrcName())
                .disableInteraction().build());
    }
    
    private void onBeaconClosedEvent(Event event) {
        final BeaconClosedEvent e = (BeaconClosedEvent) event;
        final Step step = e.getStep();
        final Draft draft = e.getDraft();

        String contentType;
        BeaconContent beaconContent = (BeaconContent) step
                .custom_data(Beacon.id(step));
        if (beaconContent == null || beaconContent.contentId() == null) {
            contentType = "none";
        } else {
            contentType = beaconContent.contentType();
        }
        GaUtil.startInteraction();
        AnalyticsInfo beaconInfo = AnalyticsInfo.create(
                Enterpriser.getActiveBeaconSegment(), draft.title(),
                TrackerEventOrigin.BEACON, Beacon.analyticsId(step),
                step.description_md(), draft.step_note_md(step.step()));
        Common.tracker().onEvent(EmbedEvent.BEACON_CLICKED.builder()
                .widget(TrackerEventOrigin.BEACON)
                .replaceInPage("contentType", contentType)
                .flow(beaconInfo.flow_id(), beaconInfo.flow_name())
                .segment(beaconInfo.segment_id(), beaconInfo.segment_name())
                .playId(beaconInfo.play_id())
                .step(beaconInfo.step_id(), beaconInfo.step_name())
                .stepNumber(step.step()).srcId(beaconInfo.src_id()).build());
    }

    private void onBeaconShowedEvent(Event event) {
        final BeaconShowedEvent e = (BeaconShowedEvent) event;
        final int step = e.getStep();
        final Draft beacon = e.getDraft();

        AnalyticsInfo beaconInfo = AnalyticsInfo.create(
                Enterpriser.getActiveBeaconSegment(), beacon.title(),
                TrackerEventOrigin.BEACON,
                Beacon.analyticsId(beacon.step(step)),
                beacon.step(step).description_md(), beacon.step_note_md(step));

        Common.tracker().onEvent(EmbedEvent.BEACON_LOADED.builder()
                // .widget(TrackerEventOrigin.BEACON)
                .flow(beaconInfo.flow_id(), beaconInfo.flow_name())
                .segment(beaconInfo.segment_id(), beaconInfo.segment_name())
                .step(beaconInfo.step_id(), beaconInfo.step_name())
                .srcId(TrackerEventOrigin.BEACON)
                .fillFinderTimingData(SmartTipPlayer.getStepFindData())
                .foundElementScore(StringUtils.stringifyArray(FinderLogger
                        .getFinderSuccessComponent(step, beacon.type())))
                .build());
    }

    private void onGracefulExitCloseEvent(Event e) {
        final GracefulExitCloseEvent event = (GracefulExitCloseEvent) e;
        if (!event.isDestroyModal()) {
            Common.tracker().onEvent(event.getEmbebEvent().builder()
                    .analyticsInfo(analyticsCache).stepNumber(event.getStep())
                    .playId(analyticsCache.play_id()).build());
        }
    }

    private void onGracefulExitMinimizeEvent(Event e) {
        final GracefulExitMinimizeEvent event = (GracefulExitMinimizeEvent) e;
        Common.tracker().onEvent(EmbedEvent.GRACEFUL_EXIT_MINIMIZE.builder()
                .analyticsInfo(analyticsCache).stepNumber(event.getStepNo())
                .playId(analyticsCache.play_id()).build());
    }

    public void onGracefulExitMaximizeEvent(Event e) {
        final GracefulExitMaximizeEvent event = (GracefulExitMaximizeEvent) e;
        Common.tracker().onEvent(EmbedEvent.GRACEFUL_EXIT_MAXIMIZE.builder()
                .analyticsInfo(analyticsCache).stepNumber(event.getStepNo())
                .playId(analyticsCache.play_id()).build());
    }

    public void onGracefulExitDefaultEvent(Event e) {
        final GracefulExitDefaultEvent event = (GracefulExitDefaultEvent) e;
        Common.tracker().onEvent(EmbedEvent.GRACEFUL_EXIT_DEFAULT.builder()
                .analyticsInfo(analyticsCache).stepNumber(event.getStepNo())
                .playId(analyticsCache.play_id()).build());
    }

    public void onGracefulExitShownEvent(Event e) {
        final GracefulExitShownEvent event = (GracefulExitShownEvent) e;
        Common.tracker().onEvent(EmbedEvent.GRACEFUL_EXIT_SHOWN.builder()
                .analyticsInfo(analyticsCache).stepNumber(event.getStepNo())
                .playId(analyticsCache.play_id()).build());
    }

    public void onHideGracefulExitEvent(Event e) {
        final HideGracefulExitEvent event = (HideGracefulExitEvent) e;
        EmbedEvent embedEvent;
        if (event.isNext()) {
            embedEvent = EmbedEvent.GRACEFUL_EXIT_CLOSE_NEXT;
        } else if (event.isFlowChange()) {
            embedEvent = EmbedEvent.GRACEFUL_EXIT_CLOSE_FLOW_CHANGE;
        } else {
            embedEvent = EmbedEvent.GRACEFUL_EXIT_CLOSE_STEP_FOUND;
        }
        Common.tracker()
                .onEvent(embedEvent.builder().analyticsInfo(analyticsCache)
                        .stepNumber(event.getStepNo())
                        .playId(analyticsCache.play_id()).build());
    }

    public void onExitPopupCloseEvent(Event e) {
        final ExitPopupCloseEvent event = (ExitPopupCloseEvent) e;
        Common.tracker().onEvent(event.getEmbedEvent().builder()
                .analyticsInfo(analyticsCache).playId(analyticsCache.play_id())
                .srcId(analyticsCache.src_id()).build());
    }

    private void onGracefulExitInfoTipShownEvent(Event e) {
        Common.tracker()
                .onEvent(EmbedEvent.GRACEFUL_EXIT_INFO_TIP_SHOWN.builder()
                        .analyticsInfo(analyticsCache)
                        .playId(analyticsCache.play_id()).build());
    }
}
