package co.quicko.whatfix.service.cdn;

import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.offline.TipServiceOffline;
import co.quicko.whatfix.service.online.TipServiceOnline;

public class TipServiceCdn extends FlowServiceCdn {
    @Override
    protected FlowService getOfflineService() {
        return new TipServiceOffline();
    }

    @Override
    protected FlowService getOnlineService() {
        return new TipServiceOnline();
    }

    @Deprecated
    @Override
    public void flows(String flowId1, String flowId2,
            AsyncCallback<Flow> callback) {
        OFFLINE_SERVICE.flows(flowId1, flowId2, callback);
    }
}
