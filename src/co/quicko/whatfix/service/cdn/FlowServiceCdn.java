package co.quicko.whatfix.service.cdn;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.data.EmbedData.TaskerNudgeCountersData;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.FlowAndEnt;
import co.quicko.whatfix.data.Segment;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.security.UserInfo;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.Service.DirectResponse;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.service.offline.FlowServiceOffline;
import co.quicko.whatfix.service.online.FlowServiceOnline;

public class FlowServiceCdn extends FlowService {
    protected FlowService OFFLINE_SERVICE = getOfflineService();
    protected FlowService ONLINE_SERVICE = getOnlineService();

    @Override
    public void flow(String flow_id, final AsyncCallback<Flow> callback) {
        OFFLINE_SERVICE.flow(flow_id, callback);
    }

    @Override
    public void flow(String flow_id, String type,
            final AsyncCallback<Flow> callback) {
        OFFLINE_SERVICE.flow(flow_id, type, callback);
    }

    @Override
    public void flowWithTags(String flow_id, AsyncCallback<Flow> callback) {
        OFFLINE_SERVICE.flowWithTags(flow_id, callback);
    }

    @Override
    public void flowWithTags(String ent_id, String flow_id, AsyncCallback<Flow> callback) {
        OFFLINE_SERVICE.flowWithTags(ent_id, flow_id, callback);
    }

    @Override
    public void viewed(String user_id, String flow_id, String unq_id,
            final AsyncCallback<Void> callback) {
        ONLINE_SERVICE.viewed(user_id, flow_id, unq_id, callback);
    }

    @Override
    public void run(String user_id, String flow_id, String unq_id,
            final AsyncCallback<Void> callback) {
        ONLINE_SERVICE.run(user_id, flow_id, unq_id, callback);
    }

    @Override
    public void miss(String user_id, String flow_id, String unq_id,
            final AsyncCallback<Void> callback) {
        ONLINE_SERVICE.miss(user_id, flow_id, unq_id, callback);
    }

    @Override
    public void completed(String user_id, String flow_id, UserInfo user,
            final AsyncCallback<Void> callback, boolean isTaskerPresent) {
        ONLINE_SERVICE.completed(user_id, flow_id, user, callback, isTaskerPresent);
    }

    @Override
    public void flowCounter(String flow_id, String cntr,
            final AsyncCallback<Double> callback) {
        ONLINE_SERVICE.flowCounter(flow_id, cntr, callback);
    }

    @Override
    public void embed_exist(String url, final AsyncCallback<Void> callback) {
        ONLINE_SERVICE.embed_exist(url, callback);
    }

    @Override
    public void flowAndEnt(String flow_id,
            final AsyncCallback<FlowAndEnt> callback) {
        OFFLINE_SERVICE.flowAndEnt(flow_id, callback);
    }

    @Override
    public void flowAndEnt(String flow_id, String locale,
            final AsyncCallback<FlowAndEnt> callback,long versionNum) {
        OFFLINE_SERVICE.flowAndEnt(flow_id, locale, callback, ZERO);
    }
    // SEGMENTS
    @Override
    public void getAllSegments(AsyncCallback<JsArray<Segment>> callback,
            TrackerEventOrigin widgetType) {
        OFFLINE_SERVICE.getAllSegments(callback, widgetType);
    }

    @Override
    public void dontShowPopup(String segment_id, TrackerEventOrigin widgetType,
            AsyncCallback<Void> callback) {
        ONLINE_SERVICE.dontShowPopup(segment_id, widgetType, callback);
    }

    @Override
    public void getPopupViewCount(String segment_id,
            TrackerEventOrigin widgetType, String popupKey,
            AsyncCallback<Double> callback) {
        ONLINE_SERVICE.getPopupViewCount(segment_id, widgetType, popupKey,
                callback);
    }
    
    @Override
    public void getPopupLastSeen(String segment_id, TrackerEventOrigin widgetType,
            AsyncCallback<Double> callback) {
        ONLINE_SERVICE.getPopupLastSeen(segment_id, widgetType, callback);
    }

    @Override
    public void incrementPopupViewCount(String segment_id,
            TrackerEventOrigin widgetType, String popupKey,
            AsyncCallback<Void> callback) {
        ONLINE_SERVICE.incrementPopupViewCount(segment_id, widgetType, popupKey,
                callback);
    }

    @Override
    public void completedContents(String[] flowIds, String userId,
            UserInfo user, JavaScriptObject flowGoals,
            AsyncCallback<DirectResponse<JsArrayString>> callback) {
        ONLINE_SERVICE.completedContents(flowIds, userId, user, flowGoals,
                callback);
    }

    @Override
    public void getNudgeCounters(String segment_id,
            final AsyncCallback<TaskerNudgeCountersData> callback) {
        ONLINE_SERVICE.getNudgeCounters(segment_id, callback);
    }
    
    @Override
    public void updateNudgeCounters(Where counters, String segment_id,
            AsyncCallback<String> callback) {
        ONLINE_SERVICE.updateNudgeCounters(counters, segment_id, callback);
    }

    protected FlowService getOfflineService() {
        return new FlowServiceOffline();
    }

    protected FlowService getOnlineService() {
        return new FlowServiceOnline();
    }
}
