package co.quicko.whatfix.service.offline;

import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.service.TipService;

public class TipServiceOffline extends FlowServiceOffline {

    @Override
    protected String type() {
        return ContentType.tip.name();
    }

    @Override
    @Deprecated
    public void flows(final String flowId1, final String flowId2,
            final AsyncCallback<Flow> callback) {
        TipService.getFlows(flowId1, flowId2, callback);
    }
}
