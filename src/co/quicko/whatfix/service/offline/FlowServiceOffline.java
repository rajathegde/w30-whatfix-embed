package co.quicko.whatfix.service.offline;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.data.CompletedContentOfflineResponse;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.FlowAndEnt;
import co.quicko.whatfix.data.Tag;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.player.TaskerLauncher.ExternalTracker.CompletedTasksTracker;
import co.quicko.whatfix.player.TaskerLauncher.ExternalTracker.CustomTaskListTracker;
import co.quicko.whatfix.player.TaskerLauncher.ExternalTracker.LocalStorageTaskListTracker;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.security.UserInfo;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.Service;
import co.quicko.whatfix.service.Service.DirectResponse;
import co.quicko.whatfix.service.ServiceUtil;
import co.quicko.whatfix.service.TagService;

public class FlowServiceOffline extends FlowService {
    
    private PopupCounter popupCounter;
    
    @Override
    public void flow(String flow_id, final AsyncCallback<Flow> callback) {
        flow(flow_id, type(), callback);
    }
    
    @Override
    public void flow(String flow_id, String type, final AsyncCallback<Flow> callback) {
        AsyncCallback<Flow> wrapperCb = new AsyncCallback<Flow>() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess(Flow result) {
                ServiceUtil.decodeURLs(result);
                result.trust_id_code(Enterpriser.enterprise().trust_id_code());
                callback.onSuccess(result);
            }
        };
        Service.fallBack(flow_id, type, wrapperCb, folder());
    }

    @Override
    public void flowWithTags(String flow_id, AsyncCallback<Flow> callback) {
        AsyncCallback<Flow> wrapperCb = new AsyncCallback<Flow>() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess(Flow flow) {
                TagService.IMPL.all(flow.ent_id(), new AsyncCallback<JsArray<Tag>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        callback.onSuccess(flow);
                    }

                    @Override
                    public void onSuccess(JsArray<Tag> tags) {
                        flow.all_tags(tags);
                        callback.onSuccess(flow);
                    }
                });
            }
        };
        Service.fallBack(flow_id, "flow", wrapperCb, folder());
    }

    @Override
    public void flowWithTags(String ent_id, String flow_id, AsyncCallback<Flow> callback) {
        flowWithTags(flow_id, callback);
    }

    @Override
    public void flowAndEnt(String flow_id,
            final AsyncCallback<FlowAndEnt> callback) {
        flowAndEnt(flow_id, null, callback,ZERO);
    }

    @Override
    public void flowAndEnt(String flow_id,
            final AsyncCallback<FlowAndEnt> callback, Long versionNum) {
        flowAndEnt(flow_id, null, callback, ZERO);
    }

    @Override
    public void flowAndEnt(String flow_id, String locale,
            AsyncCallback<FlowAndEnt> callback, long versionNum) {
        AsyncCallback<JavaScriptObject> flowAndEntCb = new AsyncCallback<JavaScriptObject>() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess(JavaScriptObject result) {
                FlowAndEnt flowAndEnt = getFlowAndEnt(result);
                flowAndEnt.flow().trust_id_code(
                        flowAndEnt.enterprise().trust_id_code());
                ServiceUtil.decodeURLs(flowAndEnt.flow());
                callback.onSuccess(flowAndEnt);
            }

            private final native FlowAndEnt getFlowAndEnt(
                    JavaScriptObject result) /*-{
                                             var object = {};
                                             object.enterprise = result.ent;
                                             delete result["ent"];
                                             object.flow = result;
                                             return object;
                                             }-*/;
        };
        Service.fallBack(flow_id, type(), locale, flowAndEntCb, folder());
    }

    @Override
    public void incrementPopupViewCount(String segment_id,
            TrackerEventOrigin widgetType, String popupKey,
            AsyncCallback<Void> callback) {
        PopupCounter popupCounter = getPopupCounter();
        popupCounter.incrementPopupViewCount(segment_id, widgetType,
                Security.unq_id(AppUser.getUser()), popupKey);
    }

    @Override
    public void dontShowPopup(String segment_id, TrackerEventOrigin widgetType,
            AsyncCallback<Void> callback) {
        PopupCounter popupCounter = getPopupCounter();
        popupCounter.dontShowPopup(segment_id, widgetType,
                Security.unq_id(AppUser.getUser()));
    }

    @Override
    public void getPopupViewCount(String segment_id,
            TrackerEventOrigin widgetType, String popupKey,
            final AsyncCallback<Double> callback) {
        PopupCounter popupCounter = getPopupCounter();
        popupCounter.getPopupViewCount(segment_id, widgetType,
                Security.unq_id(AppUser.getUser()), popupKey, callback);
    }
    
    @Override
    public void getPopupLastSeen(String segment_id, TrackerEventOrigin widgetType,
            final AsyncCallback<Double> callback) {
        PopupCounter popupCounter = getPopupCounter();
        popupCounter.getPopupTimestamp(segment_id, widgetType,
                Security.unq_id(AppUser.getUser()), callback);
    }

    private PopupCounter getPopupCounter() {
        if (popupCounter == null) {
            if (Customizer.hasGetPopupViewCount()) {
                popupCounter = new CustomPopupCounter();
            } else {
                popupCounter = new DefaultPopupCounter();
            }
        }
        return popupCounter;
    }
    
    protected String type() {
        return ContentType.flow.name();
    }
    
    protected String folder() {
        return type();
    }
    
    @Override
    public void completedContents(String[] flowIds, String userId,
            UserInfo user, JavaScriptObject flowGoals,
            AsyncCallback<DirectResponse<JsArrayString>> callback) {
        CompletedTasksTracker completedTracker;
        if (Customizer.hasCompletedTasks()) {
            completedTracker = new CustomTaskListTracker();
        } else {
            completedTracker = new LocalStorageTaskListTracker();
        }

        JsArrayString flowIdsArrayString = JavaScriptObject.createArray()
                .cast();
        for (String flowId : flowIds) {
            flowIdsArrayString.push(flowId);
        }

        // call the service to fetch the jsArray of completed contents
        JsArrayString completionData = completedTracker
                .completedTasks(flowIdsArrayString);

        CompletedContentOfflineResponse response = JavaScriptObject
                .createObject().cast();

        response.setData(completionData);
        if (completionData != null) {
            response.setResult("successful");
            DirectResponse<JsArrayString> directResponse = response.cast();
            callback.onSuccess(directResponse);
        } else {
            response.setResult("failed");
            callback.onFailure(
                    new Throwable("unable to fetch completed contents"));
        }
    }

    @Override
    public void completed(String userId, String flowId, UserInfo user,
            AsyncCallback<Void> callback, boolean isTaskerPresent) {
        if (!isTaskerPresent) {
            CompletedTasksTracker completedTracker;
            if (Customizer.hasCompletedTasks()) {
                completedTracker = new CustomTaskListTracker();
            } else {
                completedTracker = new LocalStorageTaskListTracker();
            }
            completedTracker.onFlowCompleted(flowId);
        } else {
            // no processing done here tasker will update the offline completed
            // contents
        }
    }
    
}
