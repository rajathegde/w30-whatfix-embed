package co.quicko.whatfix.embed;

import static co.quicko.whatfix.common.logger.LOGConstants.LOGGING;
import static co.quicko.whatfix.security.Enterpriser.ent_id;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.DirectPlayer;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.FrameUrl;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.HTMLSanitizer;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.NativeMobileLogger;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.common.ResizeHandler;
import co.quicko.whatfix.common.StateKeys;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.TimeStampTracker;
import co.quicko.whatfix.common.WindowCloseManager;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo.OuterKey;
import co.quicko.whatfix.common.logger.LOGConstants;
import co.quicko.whatfix.common.logger.NFRLogger;
import co.quicko.whatfix.common.logger.NFRProperty;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.AssistantConfig;
import co.quicko.whatfix.data.AutoExecutionConfig;
import co.quicko.whatfix.data.BeaconSegment;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.EmbedData.SelfHelpWidgetSettings;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.EmbedData.TagEnabledWidgetSettings;
import co.quicko.whatfix.data.EmbedData.TaskerSettings;
import co.quicko.whatfix.data.Enterprise;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.data.Integration;
import co.quicko.whatfix.data.JavaScriptExecutableObject;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.PreviousStates;
import co.quicko.whatfix.data.TagCache;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.data.WidgetType;
import co.quicko.whatfix.embed.debugpanel.DebugFunctions;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.extension.util.WidgetSettings;
import co.quicko.whatfix.ga.AnalyticsLogger;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.Beacon;
import co.quicko.whatfix.overlay.Beacon.BeaconSetting;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.CrossMessager.SecureCrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.FramesData;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.overlay.data.WfxData;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.player.assistant.event.AssistantTipStopEvent;
import co.quicko.whatfix.player.assistant.event.ShowAssistantEvent;
import co.quicko.whatfix.player.tip.event.EvaluateSmartTipSegment;
import co.quicko.whatfix.player.tip.event.RefreshSmartTipEvent;
import co.quicko.whatfix.player.tip.event.ShowSmartTipEvent;
import co.quicko.whatfix.player.tip.event.StopSmartTipEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.security.UserInfo;
import co.quicko.whatfix.security.WFCookies;
import co.quicko.whatfix.security.WFLocalStorage;
import co.quicko.whatfix.service.AdminService;
import co.quicko.whatfix.service.BeaconService;
import co.quicko.whatfix.service.ContentManager;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.LocaleUtil;
import co.quicko.whatfix.service.WfxInfo;
import co.quicko.whatfix.service.WfxInfo.Domains;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.event.EventPayload;
import co.quicko.whatfix.userjourneys.TrackUserActions;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.AppController.RefreshListener;
import co.quicko.whatfix.workflowengine.app.AppFactory;
import co.quicko.whatfix.workflowengine.app.SalesforceApp;

public class EmbedEntry extends PlayerBase implements EntryPoint {
    private static final String STATE_VALUES_DELIM = "::";
    
    private static final String COOKIE_ROOT = "/";

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    private static HandlerRegistration hashChangeRegister;
    private static boolean openerPlaying;
    private static EmbedImpl IMPL = GWT.create(EmbedImpl.class);
    private static boolean hasEditorWon;
    private static Set<PageInfoTracker> trackPageInfoCmd = new HashSet<PageInfoTracker>();
    private static RefreshListener refreshListener = new RefreshListener() {
        @Override
        public final void refresh() {
            EmbedEntry.refresh(null);
        }
    };

    private static ContentManager manager = GWT.create(ContentManager.class);
    
    @Override
    public void onModuleLoad() {
        TimeStampTracker.setTime(TimeStampTracker.Event.EMBED_START_TIME, System.currentTimeMillis());
        FrameUrl.preserveEmbedScriptSrc();
        IMPL.init(new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                Console.debug("Unable to initialize Embed");
            }

            @Override
            public void onSuccess(Boolean result) {
                // am i the only embed?
                if (hasEmbed()) {
                    return;
                }
                
                // Extension case
                if(result == true && OverlayUtil.isTopWindow()) {
                    CrossMessager.addListener(new CrossListener() {
                        @Override
                        public void onMessage(String type, String content) {
                            CrossMessager.removeListener(this);

                            // set EXTENSION_USER_ID in EMBED local storage
                            Console.debug("EmE - extId " + content);
                            ExtensionHelper.setExtensionUserId(content);
                            WFLocalStorage.set(WFLocalStorage.KEY.EXTENSION_USER_ID, content);
                        }
                    }, Action.EMBED_SET_EXT_USERID);

                    // send a message to ext foreground to fetch EXTENSION_USER_ID
                    CrossMessager.sendMessage(Action.EMBED_GET_EXT_USERID, "");

                    CrossMessager.addListener(new CrossListener() {
                        @Override
                        public void onMessage(String type, String content) {
                            refreshPlay();
                        }
                    }, Action.PLAY_REFRESH);
                }

                if (shouldEditorInitiate()) {
                    waitForEditorToInitiate();
                    return;
                }

                // Start Embed Execution
                startEmbed();
            }
        }, this);
    }
    
    private void startEmbed() {
        long sTime = System.currentTimeMillis();
        if (hasEditorWon || hasEmbed()) {
            return;
        }
        initEmbed();
        
        // need to expose this api for both embed and editor
        exportFinderDebug();
        // to enable logger
        exportLogger();
        // to enable debug panel
        DebugFunctions.exportDebugPanel();
        
        // If embed is for a frame then let the player be just initialized.
        if (!OverlayUtil.isPrimaryWindow()) {
            Actioner.initialize();
            initWidgets();
            return;
        }
        
        initPayloadListener();
        initializeActioner();
        initLogListener();

        // inform server that i am alive.
        informExistence();
        
        addFlowPlayListeners();
        
        // Find if integration is configured.
        executeIntegration(false);
        if (OverlayUtil.isDesktopDevice()) {
			initDesktopListener();
		}
        Console.debug("EU-performance:" + EmbedEntry.class.getSimpleName()
                + "startEmbed Time taken to execute this method "
                + (System.currentTimeMillis() - sTime) + "ms");
    }

    protected void initEmbed() {
        saveEmbed();
        exportPublicFunctions();
    }

    private void waitForEditorToInitiate() {
        Console.debug("WR: Waiting for Editor to initiate: "
                + System.currentTimeMillis());
        Scheduler.get().scheduleDeferred(new ScheduledCommand() {
            
            private boolean sendMessageToBackground = true;
            private int MAX_TRIES = 8;
            
            @Override
            public void execute() {
                final CrossListener listener = new CrossListener() {
                    @Override
                    public void onMessage(String type, String content) {
                        sendMessageToBackground = false;
                        CrossMessager.removeListener(this);
                        Console.debug(
                                "WR: Widget Settings Obtained from Editor: "
                                        + System.currentTimeMillis() + " - "
                                        + content);
                        WidgetSettings widgetSettings = DataUtil.create(content);
                        boolean widgetInit = Boolean.parseBoolean(
                                widgetSettings.preview_mode_enabled());
                        if (!widgetInit) {
                            startEmbed();
                        } else {
                            // Editor has won. Don't initiliaze Embed
                            hasEditorWon = true;
                        }
                    }
                };
                CrossMessager.addListener(listener, "widget_settings");
                Scheduler.get().scheduleFixedPeriod(new RepeatingCommand() {
                    private int currentTries = 0;
                    @Override
                    public boolean execute() {
                        currentTries++;
                        if (sendMessageToBackground
                                && currentTries < MAX_TRIES) {
                            CrossMessager.sendMessage("widget_settings_get",
                                    Location.getHostName());
                            return true;
                        }
                        return false;
                    }
                }, 200);

                // Wait for 1sec for Editor To Initiate
                Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
                    @Override
                    public boolean execute() {
                        sendMessageToBackground = false;
                        CrossMessager.removeListener(listener);
                        if(!hasEditorWon) {
                            Console.debug(
                                    "WR: Editor message not received. Embed has Won:"
                                            + System.currentTimeMillis());
                        }
                        startEmbed();
                        return false;
                    }
                }, 1500);
            }
        });
    }
    
    protected void addFlowPlayListeners() {
        CrossMessager.addListener(new SecureCrossListener() {
            @Override
            public void onMessage(String type, String content) {
                PlayState state = DataUtil.create(content);
                stater.setState(StateKeys.INJECT_PLAYER, "true",
                        StateKeys.PLAY_STATE,
                        StringUtils.stringifyObject(state),
                        StateKeys.PLAY_POSITION, "0", StateKeys.SKIPPED_STEPS,
                        "0", StateKeys.LAST_TRACKED_STEP, "0");
                clearLastTrackedStep();
                clearPreviousStates();
                clearPlayStarted();
                play();
            }
        }, "embed_run_secure");
        
        CrossMessager.addListener(new SecureCrossListener() {
            @Override
            public void onMessage(String type, String content) {
                PlayState state = DataUtil.create(content);
                entry.launchLiveHerePopup(state);
            }
        }, "embed_run_popup_secure");
        
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String message) {
                JavaScriptObjectExt parsedMsg = DataUtil.create(message);
                String flowId = parsedMsg.valueAsString("flowId");
                String source = parsedMsg.valueAsString("srcId");
                FlowService.IMPL.flow(flowId, new LogFailureCb<Flow>() {

                    public void onSuccess(Flow result) {
                        /* On step completion of beacon check if the
                         * flow has play as pop up system tag
                         */
                        if (result.isPlayAsPopup()) {
                            launchLiveHerePopup(result, source);
                            return;
                        }
                        clearPreviousStates();
                        clearPlayStarted();
                        GaUtil.resetFlowInCache(source, result);
                        play(result, source);
                    };

                });
            }
        }, "embed_run_from_id");
        addExtensionTagCallback();
    }

    /*
     * SUCC-3512 Extension system tag does not load AC, Exposing onStart
     * callback on parent window.
     */
    private void addExtensionTagCallback() {
        if (OverlayUtil.isTopWindow()) {
            CrossMessager.addListener(new CrossListener() {
                @Override
                public void onMessage(String type, String playState) {
                    PlayState state = DataUtil.<PlayState> create(playState);
                    Customizer.onStart(state);
                }
            }, "extension_tag_callback");
        }
    }

    protected void onDisableMoveState(String type, String content) {
        stater.setState(StateKeys.DISABLE_MOVE_STATE, content);
    }
    
    /**
     * @param state
     *            checks for any invalid html elements as per our specified
     *            collection
     */
    private boolean sanitizedHTML(Draft state) {
        if (null == state) {
            return true;
        }
        if (!HTMLSanitizer.isSanitizedHTML(state.title())
                || !HTMLSanitizer.isSanitizedHTML(state.description_md())
                || !HTMLSanitizer.isSanitizedHTML(state.footnote_md())) {
            return false;
        }
        if (state.steps() > 0) {
            int steps = state.steps() <= 50 ? state.steps() : 50;
            for (int i = 1; i <= steps; i++) {
                if (!HTMLSanitizer.isSanitizedHTML(state.step_description_md(i))
                        || !HTMLSanitizer.isSanitizedHTML(state.step_note_md(i))
                        || !HTMLSanitizer.isSanitizedHTML(
                                state.step_validation_errors_md(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    private void executeIntegration(final boolean refresh) {
        final String ent_id = entId();
        Security.COOKIES.handleCookies();

        if (ent_id == null) {
            LOGGER.log(WfxLogLevel.DEBUG, EmbedEntry.class,
                    LOGConstants.EXECUTE_INTEGRATION, WfxModules.JIT,
                    "ent id is null");
            ExtensionHelper.initialize(false);
            GaUtil.initGA();
            long startTime = System.currentTimeMillis();
            
            EventPayload.Builder event = EmbedEvent.EMBED_LOADED.builder()
                    .srcId(TrackerEventOrigin.DIRECT);
            if (Customizer.isDebugMode()) {
                event.contextualInfo(OuterKey.ANALYTICS_DEBUG_PAYLOAD,
                        AnalyticsLogger.getInstance().getDebugPayload());
            }
            Common.tracker().onEvent(event.build());
            
            LOGGER.log(WfxLogLevel.PERFORMANCE, EmbedEntry.class,
                    LOGConstants.EXECUTE_INTEGRATION, WfxModules.JIT,
                    "Time taken for executeIntegration Analytics Event : {0} ms",
                    (System.currentTimeMillis() - startTime));
            initAfterSanity();
        } else {
            nativeExportIntegrationCb();
            long sTime3 = System.currentTimeMillis();
            AdminService.IMPL.executeIntegration(ent_id,
                    new Callback<Void, Exception>() {
                        @Override
                        public void onFailure(Exception caught) {
                            LOGGER.log(WfxLogLevel.DEBUG, EmbedEntry.class,
                                    LOGConstants.EXECUTE_INTEGRATION,
                                    WfxModules.JIT,
                                    "onFailure callback received after {0} ms",
                                    (System.currentTimeMillis() - sTime3));
                            ExtensionHelper.initialize(false);
                            integrationFailed();
                        }

                        @Override
                        public void onSuccess(Void result) {
                            LOGGER.log(WfxLogLevel.DEBUG,
                                    EmbedEntry.class,
                                    LOGConstants.EXECUTE_INTEGRATION,
                                    WfxModules.JIT,
                                    "onSuccess callback received after {0} ms",
                                    (System.currentTimeMillis() - sTime3));
                            long sTime = System.currentTimeMillis();
                            ExtensionHelper.initialize(false);
                            Overlay.CONSTANTS.use(Enterpriser
                                    .properties(LocaleUtil.locale(true)));
                            GaUtil.initGA();
                            Common.tracker()
                                    .onEvent(EmbedEvent.EMBED_LOADED.builder()
                                            .srcId(TrackerEventOrigin.DIRECT)
                                            .build());
                            if (refresh) {
                                refresh(null);
                            } else {
                                initAfterSanity();
                            }
                            // init enterprise in iframes
                            FramesData framesData = FramesData.getData();
                            CrossMessager.sendMessageToFrames(
                                    "enterprise_settings_all_frames",
                                    StringUtils.stringifyObject(framesData));
                            deleteIntegrationCb();
                            LOGGER.log(WfxLogLevel.PERFORMANCE,
                                    EmbedEntry.class,
                                    LOGConstants.EXECUTE_INTEGRATION,
                                    WfxModules.JIT,
                                    "onSuccess, Time taken to execute this method: {0} ms",
                                    (System.currentTimeMillis() - sTime));
                        }
                    });
        }
    }
    
    protected void integrationFailed() {
        initAfterSanity();
        deleteIntegrationCb();        
    }
    
    @SuppressWarnings("deprecation")
    protected String entId() {
        Customizer.setIfEmpty();
        // Expects format to be
        // xxxxwhatfix.com/prod/<ent_id>/embed/embed.nocache.js
        String embedURL = com.google.gwt.core.client.GWT.getModuleBaseURL();
        String entIdFromURL = Common.getPotentialEntIdFromURL(embedURL);
        if(StringUtils.isNotBlank(entIdFromURL)) {
        	return entIdFromURL;
        }
        Console.debug("No valid ent id present in URL");
        
        String offlineEntId = offlineEntId();
        if (Common.isGUID(offlineEntId)) {
            Console.debug("return ent id from script - " + offlineEntId);
            return offlineEntId;
        }
        Console.debug(
                "No valid ent id present in script also and returning _wfx_settings.ent_id");
        
        
        // check for all scriptTags and get The EntId
        // As in SSO Mode modulebaseURL is same as the host url
        String entId = getEntIdFromScriptTags();
        if (entId != null) {
            return entId;
        }
        
        return Customizer.getEntId();
    }
    
    private String getEntIdFromScriptTags() {
        String embedStr = GWT.getModuleName() + ".nocache.js";
        NodeList<Element> elements = Document.get()
                .getElementsByTagName(ScriptElement.TAG);
        int len = elements.getLength();
        for (int i = 0; i < len; i++) {
            ScriptElement scriptElement = (ScriptElement) elements.getItem(i);
            if (scriptElement != null
                    && StringUtils.isNotBlank(scriptElement.getSrc())) {
                String src = scriptElement.getSrc();
                if (src.contains(embedStr)) {
                    // TODO: Check on On Premise app, need to get remove
                    // hardcoded whatfix.com
                    String[] urlParts = src
                            .split(WfxInfo.getDomain(Domains.API));
                    String[] potentialEntIds = urlParts[1].split("/");
                    for (String potentialEntId : potentialEntIds) {
                        if (Common.isGUID(potentialEntId)) {
                            Console.log("Obtained EntId: " + potentialEntId);
                            Console.debug("return ent id from url - "
                                    + potentialEntId);
                            if (src.contains(
                                    "test." + WfxInfo.getDomain(
                                            Domains.API))
                                    || src.contains(WfxInfo
                                            .getDomain(Domains.PRODSCRIPT))) {
                                Framers.FRAME_URL.setCdnBaseUrl(potentialEntId);
                            } else {
                                Framers.FRAME_URL.setBaseUrl("https://"
                                        + WfxInfo.getDomain(Domains.DEVSCRIPT)
                                        + "/" + potentialEntId + "/");
                            }
                            return potentialEntId;
                        }
                    }
                }
            }
        }
        return null;
    }

    private static void integrationCb(JavaScriptObject result) {
        Integration integration = (Integration) result;
        Enterpriser.fillAndInitialize((Enterprise) result);
        
        if (integration.script_on_hash()) {
            if (hashChangeRegister == null) {
                hashChangeRegister = History
                        .addValueChangeHandler(new ScriptHandler());
            }
        } else {
            if (hashChangeRegister != null) {
                hashChangeRegister.removeHandler();
                hashChangeRegister = null;
            }
        }
        Actioner.onTheme();
    }

    private native static void deleteIntegrationCb() /*-{
		delete $wnd["_wfx_integration_cb"];
    }-*/;

    private static class ScriptHandler implements ValueChangeHandler<String> {

        public ScriptHandler() {
        }

        @Override
        public void onValueChange(ValueChangeEvent<String> event) {
            entry.executeIntegration(true);
        }
    }

    private void initAfterSanity() {
        DIRECTOR.checkStateSanity(stater, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                clearState();
                onSuccess(null);
            }

            @Override
            public void onSuccess(Void result) {
                init();
            }
        });
    }

    private void init() {
        // tagCache will be used for tag condition evaluation
        // and false positive identification
        long sTime = System.currentTimeMillis();
        Enterprise ent = Enterpriser.enterprise();
        TagCache.initializeSynchronously(ent.tags());
        LOGGER.log(WfxLogLevel.PERFORMANCE, EmbedEntry.class, "init",
                WfxModules.WORKFLOW_ENGINE,
                "Time taken to initialize tag cache: {0} ms",
                (System.currentTimeMillis() - sTime));
        stater.getState(StateKeys.INJECT_PLAYER, new AsyncCallback<String>() {
            @Override
            public void onSuccess(String result) {
                // already the play is over?
                if (!"false".equals(result)) {
                    IMPL.tryToPlay();
                }
            }

            @Override
            public void onFailure(Throwable caught) {
            }
        });

        initWidgets();

        // if flow_id is already initialized.
        tryLive();
        tryLivePopup();

        // refresh listener won't be registered if disable_page_refresh
        // is set true from monitor
        if (!Enterpriser.enterprise().disable_page_refresh()) {
            AppController.registerRefreshListener(refreshListener);
        }

        if (Enterprise.enterprise().track_user_actions()) {
            try {
                TrackUserActions userActionsTracker = new TrackUserActions();
                userActionsTracker.initialize();
            } catch (Exception error) {
                Console.debug("Failed to initilaize user actions tracker " + error);
            }
        }

        // Not using this as of now so turning it off
        // trackPageInfo();
    }

    private void initWidgets() {
        long sTime = System.currentTimeMillis();
        
        startUserActionsTracker();
        
        // resize blog frame
        ResizeHandler.initResizer();
        refreshWidgets();
        invokeOnload();
        LOGGER.log(WfxLogLevel.PERFORMANCE, EmbedEntry.class, "initWidgets",
                WfxModules.JIT, "Time taken to execute this method: {0} ms",
                (System.currentTimeMillis() - sTime));
    }

    private static boolean tryNativeStatic() {
        String static_flow_id = natStaticId();
        if (static_flow_id == null || static_flow_id.length() == 0) {
            return false;
        } else {
            EventBus.fire(ShowSmartTipEvent.fromFlowId(static_flow_id));
            return true;
        }
    }

    public static native String natStaticId() /*-{
		return $wnd._wfx_smart_tips;
    }-*/;

    @Override
    protected void getBeaconDraft(final AsyncCallback<Draft> beaconStartCb) {
    	long sTime = System.currentTimeMillis();
        BeaconSetting beaconSettings = (BeaconSetting) beaconSettings();
        if(beaconSettings != null) {
            Draft draft = Beacon.wrapIntegration(beaconSettings);
            Enterpriser.enterprise().theme().value(Themer.BEACON.BEACON_COLOR,
                    beaconSettings.color());
            beaconStartCb.onSuccess(draft);
        } else {
        	UserInfo user= Security.unq_id_with_enc_status(AppUser.getUserId());
            BeaconSegment result = Enterpriser.getBeaconSegment();
            if (result != null) {
				Enterpriser.setActiveBeaconSegment(result);
				result.custom_enabled(Enterpriser.hasCustomTime(result.conditions()));
				BeaconService.IMPL.fullBeacon(result.times_to_show(), result.segment_id(), user , beaconStartCb);
            }
        }
        NFRLogger.addLog(NFRProperty.BEACON,
                System.currentTimeMillis() - sTime);
    }
    
    public void tryToPlay() {
        // see if play has to be resumed from opener.
        openerPlaying = openerPlaying();
        if (openerPlaying) {
            askPlayState();
            return;
        }

        // play across subdomains & protocols.
        String flow_next = getPersistedState();
        if (flow_next != null) {
            String[] flow_next_data = flow_next.split(STATE_VALUES_DELIM);
            autoExecutionConfig = AutoExecutionConfig.fromString(flow_next_data[11]);
            GaUtil.setAnalytics(flow_next_data[5], flow_next_data[4],
                    flow_next_data[10], flow_next_data[0]);
            determinePlay(flow_next_data[0], flow_next_data[1],
                    flow_next_data[2], flow_next_data[6], flow_next_data[7],
                    flow_next_data[8], flow_next_data[9]);
            WFCookies.deserialize(flow_next_data[3]);
            return;
        }
        playFromState();
    }
    
    private void determinePlay(String flow_id, String position, String skipped,
            String src_id, String dest_flow_id, String dest_position,
            String dest_step_key) {
        if (StringUtils.isNotBlank(dest_flow_id)) {
            if (!dest_flow_id.equals(flow_id)) {
                skipped = "0";
            }

            if (StringUtils.isNotBlank(dest_position)) {
                directPlay(dest_flow_id, dest_position, skipped, src_id);
            }

            if (StringUtils.isNotBlank(dest_step_key)) {
                branchTo(dest_flow_id, dest_step_key, skipped, src_id);
            }
        } else {
            directPlay(flow_id, position, skipped, src_id);
        }
    }

    public void playFromState() {
        stater.getState(StateKeys.PLAY_STATE, new AsyncCallback<String>() {

            @Override
            public void onSuccess(String result) {
                if (result.length() == 0) {
                    // see if argument is present for direct play.
                    if (canDirectPlay()) {
                        IMPL.schDirectPlay();
                        return;
                    }

                    // no opener, no play
                    JavaScriptObject opener = opener();
                    if (opener == null) {
                        return;
                    }

                    // lets try to get state
                    fetchStateAndPlay();
                } else {
                    // middle of play
                    play();
                }
            }

            @Override
            public void onFailure(Throwable caught) {
            }
        });
    }

    private static DirectPlayer DIRECTOR = DirectPlayer.getInstance();

    private void fetchStateAndPlay() {
        DIRECTOR.receive(opener(), stater, new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(String content) {
                PlayState state = DataUtil.create(content);
                GaUtil.setAnalytics(state.analyticsInfo());
                stater.setState(StateKeys.PLAY_STATE,
                        StringUtils.stringifyObject(state));
                clearPlayStarted();
                play();
            }
        });
    }

    public void schDirectPlay() {
        if (!ExtensionHelper.isSupported()) {
            // Extension is not supported. Can play directly.
            directPlay();
            return;
        }

        // Initiating direct play after a sec delay. Assuming that we will get
        // to know about extension by then. Leaving extension to initiate direct
        // play.
        scheduler.scheduleFixedDelay(new RepeatingCommand() {
            @Override
            public boolean execute() {
                if (!ExtensionHelper.isInstalled()) {
                    directPlay();
                }
                return false;
            }
        }, 1000);
    }

    // we want to make sure opener here refers to a different tab and not the
    // same one.
    // in case window.open opener refers to the same tab, in that case we want
    // to return null.
    protected native JavaScriptObject openerTop() /*-{
		return (($wnd.opener && $wnd.opener != $wnd.top) ? $wnd.opener.top
				: null);
    }-*/;

    // we want to make sure opener here refers to a different tab and not the
    // same one.
    // in case window.open opener refers to the same tab, in that case we want
    // to return null.
    private native JavaScriptObject opener() /*-{
		return (($wnd.opener && $wnd.opener != $wnd.top) ? $wnd.opener : null);
    }-*/;

    // Support making play work across domains.
    private HandlerRegistration cookieRegn;
    
    @Override
    protected void clearAll() {
        super.clearAll();
        clearCookies();
    }
    
    protected void clearCookies() {
        if(IMPL.shouldPersistCookies() && cookieRegn != null) {
            // Cookie registration exists if we anticipated a page reload & it
            // didn't happen.
                cookieRegn.removeHandler();
                cookieRegn = null;
        }
    }
    
    @Override
    protected void savePosition(final PlayState state, final int next,
            final int skipped, final String[] branchDest,
            final PreviousStates previousStates) {
        // Cookie registration exists if we anticipated a page reload & it
        // didn't happen.
        clearCookies();
        
        if(IMPL.shouldPersistCookies()) {
            try {
                final Draft flow = state.flow();
                if (!state.draft() && !state.test() && next != 0
                        && next != flow.steps()) {
                    final StatePersister persister = getPersister(state.flow(),
                            next);
                    if (persister != null) {
                        ClosingHandler closer = new ClosingHandler() {
                            @Override
                            public void onWindowClosing(ClosingEvent event) {
                                String destFlowId = "";
                                String destPosition = "";
                                String destStepKey = "";
                                if(branchDest != null) {
                                    destFlowId = branchDest[0];
                                    destPosition = branchDest[1];
                                    destStepKey = branchDest[2];
                                }

                                String src_id = state.src_id();
                                persister.saveState(flow.flow_id()
                                        + STATE_VALUES_DELIM + next
                                        + STATE_VALUES_DELIM + skipped
                                        + STATE_VALUES_DELIM
                                        + WFCookies.serialize()
                                        + STATE_VALUES_DELIM
                                        + GaUtil.analyticsCache.segment_name()
                                        + STATE_VALUES_DELIM
                                        + GaUtil.analyticsCache.segment_id()
                                        + STATE_VALUES_DELIM
                                        + (src_id == null ? state.src_id()
                                                : src_id)
                                        + STATE_VALUES_DELIM + destFlowId
                                        + STATE_VALUES_DELIM + destPosition
                                        + STATE_VALUES_DELIM + destStepKey
                                        + STATE_VALUES_DELIM
                                        + GaUtil.analyticsCache.source_flow_id() + STATE_VALUES_DELIM
                                        + serializeAutoExecutionDetails(flow));
                                clearState();
                            }
                        };
                        cookieRegn = WindowCloseManager
                                .addWindowClosingHandler(closer);
                    }
                }
            } catch (Exception error) {
                // Ignore the error.
            }
        }
        super.savePosition(state, next, skipped, branchDest, previousStates);
    }
    
    private String serializeAutoExecutionDetails(Draft flow) {
        return StringUtils.stringifyObject(autoExecutionConfig);
    }
    
    private static StatePersister getPersister(Draft flow, final int next) {
        String url1 = flow.step_url(next);
        String url2 = flow.step_url(next + 1);
        int needCookie = needCookie(url1, url2);
        if (needCookie == 0) {
            return new CookiePersister();
        } else if (needCookie == 1 && Customizer.getHandleDomains()) {
            return new NamePersister();
        } else {
            return null;
        }
    }

    private static String getPersistedState() {
        String state = CookiePersister.fetchState();
        if (state != null) {
            return state;
        }
        return NamePersister.fetchState();
    }

    /**
     * Supported combinations: https://drive.google.com & https://google.com
     * https://google.com & http://google.com https://drive.google.com &
     * https://mail.google.com
     */
    private static int needCookie(String url1, String url2) {
        if (Customizer.getHandleSubDomain()) {
            return 0;
        }
        String current = Common.host(url1);
        String future = Common.host(url2);
        if (current.equals(future)) {
            return (protocol(url1).equals(protocol(url2)) ? 2 : 0);
        } else if (current.endsWith(future) || future.endsWith(current)) {
            return 0;
        } else if (domain(current).equals(domain(future))) {
            return 0;
        } else {
            return 1;
        }
    }

    private static String domain(String host) {
        int dotOne = host.lastIndexOf('.');
        int dotTwo = host.lastIndexOf('.', dotOne - 1);
        if (dotOne - dotTwo <= 3) {
            dotTwo = host.lastIndexOf('.', dotTwo - 1);
        }
        return host.substring(dotTwo + 1);
    }

    private static String protocol(String url) {
        int protoEnd = url.indexOf("://");
        return url.substring(0, protoEnd);
    }

    private static interface StatePersister {
        public void saveState(String state);
    }

    private static class CookiePersister implements StatePersister {
        private static final String SUB_DOMAIN_COOKIE = "wfxpp";

        @Override
        public void saveState(String state) {
            placeCookie(state);
        }

        public static String fetchState() {
            String persistedState = Cookies.getCookie(SUB_DOMAIN_COOKIE);
            if (!Customizer.delayStateClearance()) {
                if (persistedState != null) {
                    removeCookie();
                }
            }
            return persistedState;
        }

        private static void placeCookie(String value) {
            Date expiry = new Date(System.currentTimeMillis() + 2 * 60 * 1000);
            Cookies.setCookie(SUB_DOMAIN_COOKIE, value, expiry,
                    domain(Window.Location.getHostName()), COOKIE_ROOT,
                    Common.isHttps());
        }

        private static void removeCookie() {
            Cookies.removeCookieNative(SUB_DOMAIN_COOKIE, COOKIE_ROOT
                    + ";domain=" + domain(Window.Location.getHostName()));
        }
        
        public static void clearState() {
            String persistedState = Cookies.getCookie(SUB_DOMAIN_COOKIE);
            if (persistedState != null) {
                removeCookie();
            }
        }

    }

    private static class NamePersister implements StatePersister {
        private static final String START = "wfx_play_state:";

        @Override
        public void saveState(String state) {
            setWindowName(START + state);
        }

        public static String fetchState() {
            String windowName = getWindowName();
            if (windowName == null || windowName.length() == 0
                    || !windowName.startsWith(START)) {
                return null;
            }
            if (!Customizer.delayStateClearance()) {
                setWindowName("");
            }
            return windowName.substring(START.length());
        }

        private static native void setWindowName(String name) /*-{
			$wnd.name = name;
        }-*/;

        private static native String getWindowName() /*-{
			return $wnd.name;
        }-*/;
        
        public static void clearState() {
            String windowName = getWindowName();
            if (StringUtils.isNotBlank(windowName)
                    && windowName.startsWith(START)) {
                setWindowName("");
            }
        }
    }

    private static long SEEN_GAP = 7 * 24 * 60 * 60 * 1000l;
    private static String SEEN = "seen";

    // let the server know that i am alive, informed once in a week
    protected void informExistence() {
        if (Framers.IGNORE_EXTN.equals(Customizer.getIgnoreExtension())) {
            return;
        }
        final Storage local = Storage.getLocalStorageIfSupported();
        if (local == null) {
            return;
        }
        String value = local.getItem(SEEN);
        if (value != null) {
            long time = Long.parseLong(value);
            if (System.currentTimeMillis() - time > SEEN_GAP) {
                value = null;
            }
        }

        if (value == null) {
            FlowService.IMPL.embed_exist(
                    Window.Location.getProtocol() + "//"
                            + Window.Location.getHost(),
                    new AsyncCallback<Void>() {
                        @Override
                        public void onSuccess(Void result) {
                            local.setItem(SEEN,
                                    Long.toString(System.currentTimeMillis()));
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                        }
                    });
        }
    }

    protected native boolean hasEmbed() /*-{
		return $wnd.___embed;
    }-*/;

    private native void saveEmbed() /*-{
		$wnd.___embed = true;
    }-*/;

    protected void refreshWidgets() {
        if (isAutoTest()) {
            //don't load the widgets in case of auto test
            return;
        }
        long sTime5 = System.currentTimeMillis();
        NodeList<Element> divs = Document.get()
                .getElementsByTagName(DivElement.TAG);
        int size = divs.getLength();
        for (int index = 0; index < size; index++) {
            Element element = divs.getItem(index);

            Element child = element.getFirstChildElement();
            if (child == null) {
                continue;
            }

            // checking explicitly for iframe also as FreshDesk editor replacing
            // anchor with iframe.
            String tag = child.getTagName();
            if (!(AnchorElement.TAG.equalsIgnoreCase(tag)
                    || IFrameElement.TAG.equalsIgnoreCase(tag))) {
                continue;
            }

            String flow = element.getAttribute("data-flow");
            if (flow == null || flow.length() == 0) {
                continue;
            }

            String href = element.getAttribute("data-href");
            String widgetType = element.getAttribute("data-widget-type");
            if ((StringUtils.isNotBlank(href) && href.contains("blog.html"))
                    || "blog".equals(widgetType)) {
                element.appendChild(Framers.blog(flow, element).build());
            } else if ((StringUtils.isNotBlank(href)
                    && href.contains("deck.html"))
                    || "deck".equals(widgetType)) {
                IFrameElement deckIframe = Framers.deck(flow, element).build();
                deckIframe.setClassName(Common.CSS.deckBorderRadius());
                element.appendChild(deckIframe);
            } else {
                continue;
            }
            element.removeChild(child);
        }

        initLaunchers();
        LOGGER.log(WfxLogLevel.PERFORMANCE, EmbedEntry.class,
                "refreshWidgets", WfxModules.JIT,
                "Time taken to execute this method: {0} ms",
                (System.currentTimeMillis() - sTime5));
    }

    // Script based play trigger.
    private static EmbedEntry entry;

    public static EmbedEntry getEntry() {
        return entry;
    };

    private void exportPublicFunctions() {
        entry = this;
        nativeExportDirectPlay();
        nativeExportRefresh();
        nativeExportLive();
        nativeExportResumePlay();
        nativeExportList();
        nativeExportOpen();
        nativeWidgetsClose();
        nativeAutoRunFlow();
        nativeExportDirectPlayWithPosition();
        nativeAutoTestFlow();
        nativeAutoTestLastStep();
        WfxData.exportMethods();
        nativeExportAppUser();
        NativeMobileLogger.nativeDeviceLogger();
        nativeAssistantPlay();
        nativeAssistantStop();
    }
    
    private void exportFinderDebug() {
        nativeDebugFinder();
        nativeSelectorGetter();
        nativeSelectorValidator();
        nativeAutoTestDebugFinder();
    }

    private static boolean tryLive() {
        String flow_id = natLiveId();
        if (flow_id == null || flow_id.length() == 0) {
            return false;
        } else {
            GaUtil.resetAnalyticsCache();
            entry.clearPlayStarted();
            GaUtil.startInteraction();
            entry.play(flow_id, new AsyncCallback<Flow>() {
                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(Flow result) {
                    directPlay(result);
                }
            });
            return true;
        }
    }
    
    // Live Tour Case
    private static void directPlay(Flow flow) {
        TrackerEventOrigin liveTourSrc = TrackerEventOrigin.LIVE_TOUR;
        GaUtil.setAnalytics(flow, liveTourSrc);
        /* Flow played via live tour - check if the flow is tagged with
         * play as a pop-up tag
         */
        if (flow.isPlayAsPopup()) {
            entry.launchLiveHerePopup(flow, liveTourSrc.getSrcName());
            return;
        }

        entry.play(flow, "0", "0", liveTourSrc.getSrcName());
    }

    private static boolean tryLivePopup() {
        String flow_id = natLivePopupId();
        if (flow_id == null || flow_id.length() == 0) {
            return false;
        } else {
            FlowService.IMPL.flow(flow_id, new AsyncCallback<Flow>() {
                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(Flow result) {
                    PlayState state = DataUtil.create();
                    state.flow(result);
                    AnalyticsInfo info = AnalyticsInfo.create(result,
                            TrackerEventOrigin.GUIDED_POPUP);
                    state.src_id(TrackerEventOrigin.GUIDED_POPUP.getSrcName());
                    entry.livePopup(state, info, TrackerEventOrigin.GUIDED_POPUP);
                }
            });
            return true;
        }
    }
    
    private static void directPlay(String flow_id) {
        String src_id = TrackerEventOrigin.LIVE_TOUR.getSrcName();
        entry.play(flow_id, new AsyncCallback<Flow>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Flow result) {
                // Direct play play as popup : _wfx_run("<flow_id>") case
                GaUtil.setAnalytics(result, src_id);
                if (result.isPlayAsPopup()) {
                    entry.launchLiveHerePopup(result, src_id);
                    return;
                }
                entry.clearPlayStarted();
                entry.play(result, "0", "0", src_id);
            }
        });
    }
    
    private static void directPlay(String flow_id, String position,
            String skipped, String src_id) {
      entry.jumpTo(flow_id, position, skipped, src_id);
    }

    //triggered from _wfx_close_live()
    private static void stopPlay() {
        entry.onClose(true);
    }

    private static void stopStaticTips() {
        EventBus.fire(new StopSmartTipEvent());
    }

    private static void refresh(boolean widgetExists, final WidgetType widget,
            final AsyncCallback<Void> cb) {
        // entry null-check for Extension tag flows
        if (widgetExists && entry != null) {
            // Close existing launchers.
            String[] destroyActions;
            if (widget != null) {
                destroyActions = widget.getDestroyAction();
            } else {
                // add destroy actions for all visible widgets
                ArrayList<String> actions = new ArrayList<String>();
                for (WidgetType widgetType : WidgetType.values()) {
                    if (entry.hasLaunchers(widgetType)) {
                        for (String action : widgetType.getDestroyAction()) {
                            actions.add(action);
                        }
                    }
                }
                destroyActions = actions.toArray(new String[actions.size()]);
            }
            String crossMsgContent = WidgetType.POP_UP == widget
                    ? GaUtil.PopupCloseOption.cross.toString() + "_"
                    : "";
            destroyLaunchers(destroyActions, crossMsgContent);
            if (AppFactory.isDesktopApp() && (Window.getClientHeight() < 400
                    || Window.getClientWidth() < 500)) {
                // dont add the launchers in case the screen size is too small
                return;
            }
            Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
                private int refreshRetries = 30;

                @Override
                public boolean execute() {
                    if (entry.hasLaunchers(widget)) {
                        // returning after some retries as launchers might get
                        // initialized by a previous call to _wfx_refresh()
                        refreshRetries = refreshRetries - 1;
                        return (refreshRetries != 0);
                    } else {
                        cb.onSuccess(null);
                        return false;
                    }
                }
            }, 100);
        } else {
            cb.onSuccess(null);
        }

    }

    private static void refresh(String type) {
        if (type == null) {
            // refresh all widgets
            AsyncCallback<Void> cb = new AsyncCallback<Void>() {
                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(Void result) {
                    if (entry != null) {
                        entry.refreshWidgets();
                    }
                }
            };
            refresh(true, null, cb);
        } else if ("page_tags".equals(type)) {
            // re-evaluate page_tags
            Enterprise ent = Enterpriser.enterprise();
            if (ent.auto_segment_enabled()) {
                PlayerBase.setCurrentPageTags();
            }
        } else {
            // refresh specific widget
            refreshWidget(type);
        }
        // Not using this as of now so turning it off
        //trackPageInfo();
    }
    
    private static void refreshSmartTips(Boolean refreshCurrent) {
        if (Boolean.TRUE.equals(refreshCurrent)) {
            EventBus.fire(new RefreshSmartTipEvent());
        } else {
            EventBus.fire(new EvaluateSmartTipSegment());
        }
    }

    private static void refreshWidget(String type) {
        final WidgetType widget = WidgetType.getWidgetFromType(type);
        if (widget == null) {
            return;
        }
        boolean widgetExists;
        switch (widget) {
            case SELF_HELP:
                widgetExists = entry.hasWidgetLauncher();
                break;
            case TASK_LIST:
                widgetExists = entry.hasTaskerLauncher();
                break;
            case POP_UP:
                widgetExists = entry.isPopupActive();
                break;
            default:
                widgetExists = false;
        }
        AsyncCallback<Void> cb = new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Void result) {
                switch (widget) {
                    case SELF_HELP:
                        entry.initWidgetLauncher();
                        break;
                    case TASK_LIST:
                        entry.initTaskerLauncher();
                        break;
                    case POP_UP:
                        entry.tryOnboarding();
                        break;
                    case BEACON:
                        entry.tryBeaconCollectionSegments();
                        break;
                    default:
                }
            }
        };
        refresh(widgetExists, widget, cb);
    }

    private static void openWidget() {
        CrossMessager.sendMessage("widget_open", "");
    }

    private static void openTasker() {
        CrossMessager.sendMessage("tasker_open", "");

    }
    
    private static void closeWidgets() {
        CrossMessager.sendMessage("close_widgets","");
    }

    private static native void markOpen(String widget) /*-{
		if ($wnd[widget]) {
			$wnd[widget]['state'] = "open";
		}
    }-*/;

    private static native String offlineEntId() /*-{
        var ent_id = "_wfx_ent_id_replace";
        return ent_id;
    }-*/;
    
    // Older style javascript embed.
    private native void nativeExportDirectPlay() /*-{
                                                 $wnd._wfx_run = $entry(@co.quicko.whatfix.embed.EmbedEntry::directPlay(Ljava/lang/String;));
                                                 }-*/;

 // Older style javascript embed.
    private native void nativeExportDirectPlayWithPosition() /*-{
                                     $wnd._wfx_run_position = $entry(@co.quicko.whatfix.embed.EmbedEntry::directPlay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;));
                                     }-*/;

    private native void nativeExportLive() /*-{
		$wnd._wfx_live = $entry(@co.quicko.whatfix.embed.EmbedEntry::tryLive());
		$wnd._wfx_live_popup = $entry(@co.quicko.whatfix.embed.EmbedEntry::tryLivePopup());
		$wnd._wfx_is_live = $entry(@co.quicko.whatfix.embed.EmbedEntry::wfx_is_playing());
		$wnd._wfx_close_live = $entry(@co.quicko.whatfix.embed.EmbedEntry::stopPlay());
		$wnd._wfx_start_smart_tips = $entry(@co.quicko.whatfix.embed.EmbedEntry::tryNativeStatic());
		$wnd._wfx_stop_smart_tips = $entry(@co.quicko.whatfix.embed.EmbedEntry::stopStaticTips());
		$wnd._wfx_complete_step = $entry(this.@co.quicko.whatfix.embed.EmbedEntry::completeStep());
        $wnd._wfx_notify = $entry(@co.quicko.whatfix.player.notification.SelfHelpNotification::showNotificationAdapter(*));
        $wnd._wfx_notification_hide = $entry(@co.quicko.whatfix.player.notification.SelfHelpNotification::hideNotification(*));
        $wnd._wfx_search = $entry(this.@co.quicko.whatfix.embed.EmbedEntry::contentSearchJSAPI(*));
    }-*/;

    /**
     * this is a search function aimed to provide search functionality outside of SelfHelp Widget in Export mode only
     * Sample usage : execute following in browser console on page load
     *
     * _wfx_search('SearchString', {
     *    onSuccess: function(data) {
     *        console.log(data);
     *    },
     *    onFailure: function(error) {
     *        console.error(error);
     *    }
     * });
     */
    private void contentSearchJSAPI(String query, JavaScriptExecutableObject searchResultCB) {
        try {
            manager.initialize(ent_id());
            Settings widgetSettings = widgetSettings();
            if(widgetSettings != null) {
                String filter_by_tag = widgetSettings.filter_by_tags() != null ? widgetSettings.filter_by_tags().join("&") : null;
                manager.searchAPI(query, ent_id(), 25, filter_by_tag, searchResultCB, Enterpriser.noindex_tag());
            }
            else{
                searchResultCB.onFailure(new Throwable("No active segment found. Please create/enable a segment."));
            }
        }catch (JavaScriptException e){
            Console.log("Arguments are not as per required format : "+ e);
        }
    }

    private native void nativeExportRefresh() /*-{
        $wnd._wfx_refresh = $entry(@co.quicko.whatfix.embed.EmbedEntry::refresh(Ljava/lang/String;));
        $wnd._wfx_refresh_tips = $entry(@co.quicko.whatfix.embed.EmbedEntry::refreshSmartTips(Ljava/lang/Boolean;));
    }-*/;

    private native void nativeExportOpen() /*-{
		$wnd._wfx_widget_open = $entry(@co.quicko.whatfix.embed.EmbedEntry::openWidget());
		$wnd._wfx_tasker_open = $entry(@co.quicko.whatfix.embed.EmbedEntry::openTasker());
    }-*/;
    
    private native void nativeWidgetsClose() /*-{
        $wnd._wfx_widgets_close = $entry(@co.quicko.whatfix.embed.EmbedEntry::closeWidgets());
    }-*/;

    private native void nativeExportIntegrationCb() /*-{
                                                    $wnd._wfx_integration_cb = $entry(@co.quicko.whatfix.embed.EmbedEntry::integrationCb(Lcom/google/gwt/core/client/JavaScriptObject;));
                                                    }-*/;

    private native void nativeExportAppUser() /*-{
        $wnd._wfx_app_user = $entry(@co.quicko.whatfix.overlay.data.AppUser::getUser());
    }-*/;

    private native void nativeAssistantPlay() /*-{
        $wnd._wfx_assistant = $entry(@co.quicko.whatfix.embed.EmbedEntry::playAssistant(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;));
    }-*/;

    private native void nativeAssistantStop() /*-{
        $wnd._wfx_stop_assistant = $entry(@co.quicko.whatfix.embed.EmbedEntry::stopAssistant());
    }-*/;

    /**
     * Invoked from native method nativeAssistantPlay.
     */
    private static void playAssistant(String position, Boolean isVertical,
            String flowToPlay, String buttonText, String mockFlowId,
            Boolean newTab) {
        AssistantConfig config = AssistantConfig.create(mockFlowId, position,
                flowToPlay, buttonText, isVertical, newTab);
        LOGGER.log(WfxLogLevel.DEBUG, EmbedEntry.class, "playAssistant",
                WfxModules.JIT, JsonUtils.stringify(config));
        EventBus.fire(ShowAssistantEvent.fromJsApi(config));
    }

    /**
     * Invoked from native method nativeAssistantStop.
     */
    private static void stopAssistant() {
        EventBus.fire(new AssistantTipStopEvent());
    }

    /**
     * 1. evaluate self help segment settings
     * 2. merge role settings if applicable
     * 3. merge auto segment setting if applicable
     */
    @Override
    protected Settings widgetSettings() {
        long startTime = System.currentTimeMillis();
        Settings widgetSetting = evaluatedWidgetSettings();
        if (null != widgetSetting) {
            JavaScriptObjectExt widgetEvaluationTime = DataUtil.create();
            widgetEvaluationTime.put("evaluation_time",
                    String.valueOf(System.currentTimeMillis() - startTime));
            NFRLogger.addLog(NFRProperty.SELF_HELP,
                    System.currentTimeMillis() - startTime);
            widgetSetting.metrics(widgetEvaluationTime);
        }
        return widgetSetting;
    }
    
    protected Settings evaluatedWidgetSettings() {
        Customizer.onBeforeGetPageOptions(null);
        SelfHelpWidgetSettings settings = getRoleMergedwidgetSettings();
        if (settings == null || settings.no_initial_flows()) {
            // return widget settings if no valid role
            return settings;
        }
        Enterprise ent = Enterpriser.enterprise();

        SelfHelpWidgetSettings autoSettings = null;
        if (ent.auto_segment_enabled() || ent.auto_skip_on_launch()) {
            autoSettings = Enterpriser.getPageSegmentSettings();
            if (ent.auto_skip_on_launch()) {
                PlayerBase.setCurrentPageTags(autoSettings);
            }
        }
        // apply auto segmentation in case of no page tag condition in segment
        if (settings != null && !settings.has_page_condition()
                && ent.auto_segment_enabled()) {
            settings.is_segment_auto_filtered(true);
            if (autoSettings == null || autoSettings.tag_ids() == null
                    || autoSettings.tag_ids().length() == 0) {
                // if no page tag is true, self_help should show no content
                settings.no_initial_flows(true);
            } else {
                settings.no_initial_flows(false);
                // merge auto segment
                // copy object is required in case of integration script
                SelfHelpWidgetSettings mergedSettings = StringUtils.copyObject(settings);
                Settings nativeSettings = natSettings("_wfx_widget");
                if (nativeSettings != null && nativeSettings.order() != null) {
                    // give priority to integration script ordering
                    mergedSettings.order(nativeSettings.order());
                }
                SelfHelpWidgetSettings.mergeSettings(autoSettings, mergedSettings);
                mergedSettings.page_tags(autoSettings.page_tags());
                Console.debugOn("All mergerd Settings");
                Console.debug(mergedSettings);
                Console.debugOff();
                return mergedSettings;
            }
        }
        return settings;
    }

    private SelfHelpWidgetSettings getDefaultwidgetSettings() {
        // tagCache has been initialized in init method
        SelfHelpWidgetSettings settings = (SelfHelpWidgetSettings)super.widgetSettings();
        if (settings == null) {
            settings = (SelfHelpWidgetSettings)natSettings("_wfx_widget");

            String[] typeValue = (settings != null) ? settings.typeValue()
                    : new String[] { null, null };
            if (typeValue[0] == null) { // Wfx Widget has no tags,host,tag_ids
                // Go for Segments
                SelfHelpWidgetSettings segSettings = Enterpriser.getWidgetSettings(settings);
                if (segSettings == null) {
                    return settings; // Native Wfx Widget
                }
                return segSettings;
            }
        } else {
            return settings;
        }
        return settings;
    }

    protected SelfHelpWidgetSettings getRoleMergedwidgetSettings() {
        SelfHelpWidgetSettings settings = getDefaultwidgetSettings();
        // apply role tags in case of no role tag condition in segment
        if (shouldMergeRoleSettings(settings)) {
            SelfHelpWidgetSettings roleSettings = (SelfHelpWidgetSettings) Enterpriser
                    .getRoleSegmentSettings(WidgetType.SELF_HELP);
            if (roleSettings != null) {
                // copy object is required in case of integration script
                SelfHelpWidgetSettings roleMergedSettings = StringUtils.copyObject(settings);
                SelfHelpWidgetSettings.mergeSettings(roleSettings, roleMergedSettings);
                roleMergedSettings.role_tags(roleSettings.role_tags());
                return roleMergedSettings;
            } else {
                // current user is not having any valid role
                Console.debug(
                        "No valid role found. SelfHelp might be empty or hidden");
                return getNoValidRoleSegment(settings);
            }
        }
        return settings;
    }

    private boolean shouldMergeRoleSettings(TagEnabledWidgetSettings settings) {
        return settings != null && !settings.has_role_condition()
                && Enterpriser.enterprise().role_filtering_enabled()
                && !TagCache.getRoleTags().isEmpty();
    }

    protected SelfHelpWidgetSettings getNoValidRoleSegment(
            SelfHelpWidgetSettings settings) {
        // don't show widget if no valid role
        return null;
    }

    private Settings getDefaultTLSettings() {
        Settings settings = super.taskerSettings();
        if (settings == null) {
            settings = natSettings("_wfx_tasker");

            String[] typeValue = (settings != null) ? settings.typeValue()
                    : new String[] { null, null };
            if (typeValue[0] == null) {
                // Go for Segments
                Settings segSettings = Enterpriser
                        .getTasklistSettings(settings);
                if (segSettings == null) {
                    // do not allow native TL creation without ent id for
                    // business accounts
                    if (!Enterpriser.community()
                            && (settings != null && !Enterpriser.ent_id()
                                    .equals(settings.ent_id()))) {
                        return null;
                    }
                    return settings; // Native Wfx Tasker
                }
                segSettings.groups(getGroupsForSegment(segSettings.order()));
                return segSettings;
            }
        } else {
            return settings;
        }
        return settings;

    }
    
    @Override
    protected Settings taskerSettings() {
        Settings settings = getDefaultTLSettings();
        if(settings != null) {
            Settings mergedSettings = mergedRoleSettings(
                    (TaskerSettings) settings);
            if (mergedSettings != null) {
                mergedSettings.groups(
                        getGroupsForSegment(mergedSettings.order()));
            }
            return mergedSettings;        
        }
        return settings;
    }

    private Settings mergedRoleSettings(TaskerSettings input) {

        if (shouldMergeRoleSettings(input)) {
            // copy object is required in case of integration script
            TaskerSettings dest = (TaskerSettings) StringUtils
                    .copyObject(input);
            TaskerSettings src = (TaskerSettings) Enterpriser
                    .getRoleSegmentSettings(WidgetType.TASK_LIST);
            if (src != null) {
                TaskerSettings.mergeSettings(src, dest);
                dest.role_tags(src.role_tags());
                Console.debug(
                        "Role tags found for TaskList and going to be used");
                return dest;
            } else {
                // current user is not having any valid role
                Console.debug(
                        "No valid role found. TaskList might be empty or hidden");
                return null;
            }
        }
        return input;


    }

    private JavaScriptObjectExt getGroupsForSegment(JsArrayString order) {
        JavaScriptObjectExt groups = DataUtil.create().cast();
        if (order != null) {
            for (int i = 0; i < order.length(); i++) {
                String contentId = order.get(i);
                if (Enterpriser.enterprise().groups().hasKey(contentId)) {
                    Group group = (Group) Enterpriser.enterprise()
                            .group(contentId);
                    groups.put(group.flow_id(), group);
                }
            }
        }
        return groups;
    }

    @Override
    protected Settings beaconSettings() {
        Settings settings = super.beaconSettings();
        if (settings == null) {
            return natSettings(Beacon.BEACON_SETTINGS);
        } else {
            return settings;
        }
    }

    @Override
    protected boolean canLiveHere() {
        return true;
    }

    private native Settings natSettings(String widget) /*-{
		return $wnd[widget];
    }-*/;

    private static native String natLiveId() /*-{
		return $wnd._wfx_flow;
    }-*/;

    // For continuing play across windows.
    private native void nativeExportResumePlay() /*-{
                                                 $wnd.wfx_is_playing__ = $entry(@co.quicko.whatfix.embed.EmbedEntry::wfx_is_playing());
                                                 $wnd.wfx_send_play_state__ = $entry(@co.quicko.whatfix.embed.EmbedEntry::wfx_send_play_state(Lcom/google/gwt/core/client/JavaScriptObject;));
                                                 $wnd.wfx_set_play_state__ = $entry(@co.quicko.whatfix.embed.EmbedEntry::wfx_set_play_state(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;));
                                                 }-*/;

    private boolean openerPlaying() {
        try {
            if (DirectPlayer.useOpener()) {
                return false;
            }
            return invokeOpenerPlaying(openerTop());
        } catch (Exception error) {
            return false;
        }
    }

    private native boolean invokeOpenerPlaying(
            JavaScriptObject openerTopWindow) /*-{
		return openerTopWindow && openerTopWindow.wfx_is_playing__();
    }-*/;

    private static boolean wfx_is_playing() {
        return !entry.isStopped();
    }

    private void askPlayState() {
        try {
            askForPlayState(openerTop());
        } catch (Exception error) {
        }
    }

    private native void askForPlayState(JavaScriptObject openerTopWindow) /*-{
		openerTopWindow.wfx_send_play_state__($wnd);
    }-*/;

    private static void wfx_send_play_state(JavaScriptObject toWindow) {
        entry.sendPlayState(toWindow);
    }

    private void sendPlayState(final JavaScriptObject toWindow) {
        fetchState(0, new String[7], new AsyncCallback<String[]>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(String[] result) {
                if (StringUtils.isBlank(result[0])
                        || StringUtils.isBlank(result[3])) {
                    // result[0]:blank means move state not required
                    // result[3]:blank means flow not playing in opener tab
                    return;
                }
                try {
                    invokeSetPlayState(toWindow, result[1], result[2],
                            result[3], result[4], result[5]);
                    stater.clearState(StateKeys.PLAY_STATE,
                            StateKeys.PLAY_POSITION, StateKeys.INJECT_PLAYER,
                            StateKeys.SKIPPED_STEPS,
                            StateKeys.LAST_TRACKED_STEP,
                            StateKeys.PREVIOUS_STATES,
                            StateKeys.DISABLE_MOVE_STATE);
                    refreshPlay();
                } catch (Throwable error) {
                }
            }
        }, StateKeys.DISABLE_MOVE_STATE, StateKeys.INJECT_PLAYER,
                StateKeys.PLAY_POSITION, StateKeys.PLAY_STATE,
                StateKeys.SKIPPED_STEPS, StateKeys.LAST_TRACKED_STEP,
                StateKeys.PREVIOUS_STATES);
    }

    private void fetchState(final int current, final String[] out,
            final AsyncCallback<String[]> cb, final String... keys) {
        stater.getState(keys[current], new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                cb.onFailure(caught);
            }

            @Override
            public void onSuccess(String result) {
                out[current] = result;
                if (current == keys.length - 1) {
                    cb.onSuccess(out);
                } else {
                    fetchState(current + 1, out, cb, keys);
                }
            }
        });
    }

    private native void invokeSetPlayState(JavaScriptObject toWindow,
            String inject_player, String play_position, String play_state,
            String skipped_steps, String last_tracked_step) /*-{
                toWindow.wfx_set_play_state__(inject_player, play_position, play_state,
                skipped_steps, last_tracked_step);
            }-*/;

    private static void wfx_set_play_state(String inject_player,
            String play_position, String play_state, String skipped_steps,
            String last_tracked_step) {
        entry.setPlayState(inject_player, play_position, play_state,
                skipped_steps, last_tracked_step);
    }

    private void setPlayState(String inject_player, String play_position,
            String play_state, String skipped_steps,
            String last_tracked_step) {
        stater.setState(StateKeys.INJECT_PLAYER, inject_player,
                StateKeys.PLAY_POSITION, play_position, StateKeys.PLAY_STATE,
                play_state, StateKeys.SKIPPED_STEPS, skipped_steps,
                StateKeys.LAST_TRACKED_STEP, last_tracked_step,
                StateKeys.DISABLE_MOVE_STATE, String.valueOf(false));
        refreshPlay();
    }

    // Support for giving flow list out.
    private static void sendTopFlows(Settings settings,
            final JavaScriptObject callback) {
        String[] typeValue = settings.typeValue();
        ContentManager manager = GWT.create(ContentManager.class);
        manager.initialize(settings.ent_id());
        manager.topFlows(typeValue[0], typeValue[1], settings.order(),
                new AsyncCallback<JsArray<Flow>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(JsArray<Flow> result) {
                        int length = result.length();
                        JsArray<SimpleFlow> simpleArray = JavaScriptObject
                                .createArray().cast();
                        for (int index = 0; index < length; index++) {
                            simpleArray
                                    .push(SimpleFlow.create(result.get(index)));

                        }
                        invokeCallback(callback, simpleArray);
                    }
                });
    }
    
    @Override
    protected void initWindowCloseHandlers() {
        super.initWindowCloseHandlers();
        if (openerPlaying) {
            // may have to shift back play to opener.
            WindowCloseManager.addWindowClosingHandler(new ClosingHandler() {
                @Override
                public void onWindowClosing(ClosingEvent event) {
                    if (cookieRegn != null) {
                        return;
                    }
                    JavaScriptObject openerTop = openerTop();
                    if (openerTop != null && !isStopped()) {
                        wfx_send_play_state(openerTop);
                    }
                }
            });
        }
    }
    
    public void initPayloadListener() {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                GaUtil.processPayload(content, PlayerBase.getCurrentPageTags());
            }
        }, GaUtil.PAYLOAD);
    }

    public void initLogListener() {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                LogPayload logPayload = DataUtil.create(content);
                WfxLogLevel logLevel;
                switch (logPayload.log_level()) {
                    case "ALL" :
                        logLevel = WfxLogLevel.ALL;
                        break;
                    case "DEBUG" :
                        logLevel = WfxLogLevel.DEBUG;
                        break;
                    case "WARNING" :
                        logLevel = WfxLogLevel.WARNING;
                        break;
                    case "PERFORMANCE" :
                        logLevel = WfxLogLevel.PERFORMANCE;
                        break;
                    case "ERROR" :
                        logLevel = WfxLogLevel.ERROR;
                        break;
                    default:
                        logLevel = WfxLogLevel.PERFORMANCE;
                        break;
                }
                LOGGER.log(logLevel,
                        logPayload.class_name(), logPayload.method_name(),
                        WfxModules.valueOf(logPayload.module()),
                        logPayload.content());

            }
        }, LOGGING);
    }

    private static class LogPayload extends JavaScriptObject {
        protected LogPayload() {
        }

        public final native String log_level() /*-{
            return this.log_level;
        }-*/;

        public final native String content() /*-{
            return this.content;
        }-*/;

        public final native String class_name() /*-{
			return this.class_name;
        }-*/;

        public final native String module() /*-{
			return this.module;
        }-*/;
        public final native String method_name() /*-{
			return this.method_name;
        }-*/;
    }

    private static class SimpleFlow extends JavaScriptObject {
        protected SimpleFlow() {
        }

        public final native String flow_id() /*-{
			return this.flow_id;
        }-*/;

        public final native void flow_id(String flow_id) /*-{
			this.flow_id = flow_id;
        }-*/;

        public final native String title() /*-{
			return this.title;
        }-*/;

        public final native void title(String title) /*-{
			this.title = title;
        }-*/;

        public final native void authored_at(double authored_at) /*-{
			this.authored_at = authored_at;
        }-*/;

        public final native double authored_at() /*-{
			return this.authored_at;
        }-*/;

        public final native void published_at(double published_at) /*-{
			this.published_at = published_at;
        }-*/;

        public final native double published_at() /*-{
			return this.published_at;
        }-*/;

        public final native JsArrayString tags() /*-{
			return this.tags;
        }-*/;

        public final native void tags(JsArrayString tags) /*-{
			this.tags = tags;
        }-*/;

        static SimpleFlow create(Flow flow) {
            SimpleFlow simple = DataUtil.create();
            simple.flow_id(flow.flow_id());
            simple.title(flow.title());
            simple.authored_at(flow.authored_at());
            simple.published_at(flow.published_at());
            simple.tags(flow.tags());
            return simple;
        }
    }

    private static native void invokeCallback(JavaScriptObject callback,
            JsArray<SimpleFlow> result) /*-{
		if (callback) {
			callback.apply(null, [ result ]);
		}
    }-*/;

    private native void nativeExportList() /*-{
                                           $wnd._wfx_flow_list = $entry(@co.quicko.whatfix.embed.EmbedEntry::sendTopFlows(Lco/quicko/whatfix/data/EmbedData$Settings;Lcom/google/gwt/core/client/JavaScriptObject;));
                                           }-*/;

    // Support for whatfix onload call.
    private void invokeOnload() {
        try {
            onLoad();
        } catch (Throwable error) {
            // Ignore any error while loading.
        }
    }

    private native void onLoad() /*-{
		if ($wnd._wfx_onload) {
			$wnd._wfx_onload();
		}
    }-*/;

    protected boolean shouldEditorInitiate() {
        if (hasEditorLoaded()) {
            return true;
        }

        // Fallback mechanism uses LocalStorage
        Storage storage = Storage.getLocalStorageIfSupported();
        if (storage == null) {
            return false;
        }

        String settings = storage.getItem("__wfx_widget_settings");
        if (StringUtils.isBlank(settings)) {
            return false;
        }

        WidgetSettings widgetSettings = DataUtil.create(settings);
        if (widgetSettings != null) {
            return Boolean.parseBoolean(widgetSettings.preview_mode_enabled());
        }
        return false;
    }
    
    private native boolean hasEditorLoaded() /*-{
		return $wnd.__wfx_editor && $wnd.__wfx_editor == true;
    }-*/;
    
    private native void nativeAutoRunFlow() /*-{
        $wnd._wfx_autoexecute_flow = $entry(@co.quicko.whatfix.embed.EmbedEntry::autoexecuteFlow(Ljava/lang/String;Ljava/lang/String;Lco/quicko/whatfix/data/AutoExecutionConfig;));
    }-*/;
    
    private native void nativeAutoTestFlow() /*-{
        $wnd._wfx_autotest_flow = $entry(@co.quicko.whatfix.embed.EmbedEntry::autoTestFlow(Ljava/lang/String;Ljava/lang/String;Lco/quicko/whatfix/data/AutoExecutionConfig;));
    }-*/;
    
    private native void nativeAutoTestLastStep() /*-{
        $wnd._wfx_autotest_last_step = $entry(@co.quicko.whatfix.embed.EmbedEntry::autoTestLastStep());
    }-*/;
    
    private native void nativeSelectorGetter() /*-{
    $wnd._wfx_get_selector = $entry(@co.quicko.whatfix.workflowengine.alg.SelectorHelper::getWhatfixCssSelector(Lcom/google/gwt/dom/client/Element;));
    }-*/;
    
    private native void nativeSelectorValidator() /*-{
    $wnd._wfx_validate_selector = $entry(@co.quicko.whatfix.workflowengine.alg.SelectorHelper::getWhatfixElement(Ljava/lang/String;));
    }-*/;
    /*
    /*
     * this method is used to auto run a flow using default inputs
     * 
     * {@link AutoexecuteInputs} for more details about inputs
     */
    
    private static void autoexecuteFlow(String flow_id, String position,
            AutoExecutionConfig config) {
        if (config == null) {
            config = AutoExecutionConfig.def();
            config.type(AutoExecutionConfig.AUTO_EXEC);
        }
        entry.autoExecuteFlow(flow_id, position, config);
    }
    
    private static void autoTestFlow(String flow_id, String position,
            AutoExecutionConfig config) {
        if(config == null) {
            config = AutoExecutionConfig.def();
            config.type(AutoExecutionConfig.AUTO_TEST);
        }
        entry.autoExecuteFlow(flow_id, position, config);
    }
    
    private static String autoTestLastStep() {
        return StringUtils.stringifyObject(entry.autoExecutionConfig);  
    }
     
    private static native boolean isAutoTest() /*-{
                                               var auto_test = $wnd._wfx_auto_test;
                                               if (auto_test == true){
                                               return true;
                                               }
                                               else{
                                               return false;
                                               }
                                               }-*/;

    private native void exportLogger() /*-{
        $wnd._wfx_add_logger = $entry(@co.quicko.whatfix.common.logger.WfxLogFactory::addLoggers(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;));
    }-*/;

    /**
     * This method is used to print debug logs for a specific content type
     * ex: _wfx_debug_finder('flow') - prints the debug logs for all the steps of the last ran flow
     *     _wfx_debug_finder('flow',1) - prints only the debug logs for step 1 of the last ran flow
     *     _wfx_debug_finder('tip')
     */
    private native void nativeDebugFinder() /*-{
        $wnd._wfx_debug_finder = $entry(@co.quicko.whatfix.workflowengine.data.finder.FinderLogger::printFinderLog(Ljava/lang/String;Ljava/lang/String;));
    }-*/;
    
    private native void nativeAutoTestDebugFinder() /*-{
        $wnd._wfx_autotest_debug_finder = $entry(@co.quicko.whatfix.workflowengine.data.finder.FinderLogger::getFinderLogs(Ljava/lang/String;Ljava/lang/String;));
    }-*/;
    
    /*
     * Used to track page related information for analytics
     * 
     * Tracked whenever there is an integration call or widgets are refreshed
     */
    public static void trackPageInfo() {

        /*
         * For apps like oracle there might be a race condition between the
         * embed and the evaluation of the page tracking information. A
         * scheduler repeatedly checks for the page information.
         * 
         * Event is sent on change of page data. For some applications where the
         * rendering is slow, the page info is sent with a sequence number.
         * 
         * Sequence format: Time stamp at which the first event is sent -
         * sequence number
         * 
         */

        for (PageInfoTracker rptCmd : trackPageInfoCmd) {
            rptCmd.initShutdown();
        }
        PageInfoTracker rptCmd = new PageInfoTracker();
        trackPageInfoCmd.add(rptCmd);
        Scheduler.get().scheduleFixedDelay(rptCmd, 250);

        // Try to find the Help Element in SF every 500ms for 5 seconds.
        final SalesforceApp salesforceApp = (SalesforceApp) AppController
                .detectSalesforceApp();
        if (null != salesforceApp) {
            Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
                int timerCounter = 0;
                
                @Override
                public boolean execute() {
                    if (timerCounter == 10) {
                        return false;
                    }
                    
                    timerCounter += 1;
                    final Element helpElement = salesforceApp.getHelpElement();
                    if (null != helpElement) {
                        Common.sendInAppHelpEvent(helpElement);
                        return false;
                    }
                    return true;
                }                
            }, 500);
        }
    }
    
    public static class PageInfoTracker implements RepeatingCommand {
        int maxRetries = 100;
        int eventId = 0;
        JavaScriptObjectExt lastPageInfo = DataUtil.create().cast();
        long eventTimeStamp = System.currentTimeMillis();

        boolean shouldStop = false;

        @Override
        public boolean execute() {
            if (shouldStop) {
                trackPageInfoCmd.remove(this);
                return false;
            }
            maxRetries--;
            JavaScriptObjectExt pageInfo = AppController.getPageInformation();

            if (null != pageInfo && !pageInfo.isEmpty()) {
                eventId++;
                if (!lastPageInfo.isJsonEqual(pageInfo)) {
                    lastPageInfo = pageInfo;
                    Common.tracker().onEvent((EmbedEvent.WFX_REFRESH.builder())
                            .srcId(TrackerEventOrigin.DIRECT)
                            .pageTagsMeta(StringUtils.stringifyObject(pageInfo))
                            .playId(eventTimeStamp + "-" + eventId).build());
                }
                return eventId <= 5;
            }
            return maxRetries > 0 ? true : false;
        }

        public void initShutdown() {
            this.shouldStop = true;
        }
    }
    
    @Override
    protected void clearIntermediateState() {
        super.clearIntermediateState();
        CookiePersister.clearState();
        NamePersister.clearState();
    }

    private void completeStep() {
        GaUtil.analyticsCache.step_completion_action("api");
        entry.onNext(entry.getCurrentStep());
    }
    
}
