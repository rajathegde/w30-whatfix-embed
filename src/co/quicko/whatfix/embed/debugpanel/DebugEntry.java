package co.quicko.whatfix.embed.debugpanel;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public abstract class DebugEntry {
    public abstract String getName();

    public abstract VerticalPanel getNextPanel(VerticalPanel previous);

    public static VerticalPanel getPanelFromDebugEntry(
            final VerticalPanel previous, DebugEntry... entries) {
        final VerticalPanel current = new VerticalPanel();
        for (DebugEntry entry : entries) {
            HorizontalPanel hp = new HorizontalPanel();
            hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT,
                    Unit.PX);
            hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
            Anchor a = new Anchor(entry.getName());
            a.addClickHandler(event -> doNext(current, entry));
            hp.add(a);
            current.add(hp);
        }
        addBackAnchor(previous, current);
        return current;
    }

    private static void doNext(final VerticalPanel current, DebugEntry entry) {
        CellPanel parent = (CellPanel) current.getParent();
        VerticalPanel next = entry.getNextPanel(current);
        if (next != null) {
            current.removeFromParent();
            parent.add(next);
        }
    }

    protected static void addBackAnchor(final VerticalPanel previous,
            final VerticalPanel current) {
        if (previous != null) {
            HorizontalPanel hp = new HorizontalPanel();
            hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT,
                    Unit.PX);
            hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
            Anchor back = new Anchor("<-- Back");
            back.addClickHandler(event -> doBack(current, previous));
            hp.add(back);
            current.add(hp);
        }
    }

    protected static void doBack(final VerticalPanel current,
            final VerticalPanel previous) {
        CellPanel parent = (CellPanel) current.getParent();
        current.removeFromParent();
        parent.add(previous);
    }
}
