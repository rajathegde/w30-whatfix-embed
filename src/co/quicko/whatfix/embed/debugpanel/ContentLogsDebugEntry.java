package co.quicko.whatfix.embed.debugpanel;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import co.quicko.whatfix.workflowengine.data.finder.FinderLogger;

public class ContentLogsDebugEntry extends DebugEntry {
    public String getName() {
        return "Finder Logs";
    }

    public VerticalPanel getNextPanel(VerticalPanel previous) {
        return getPanelFromDebugEntry(previous, new FlowLogs(), new TipLogs(),
                new BeaconLogs());
    }

    private static class FlowLogs extends DebugEntry {
        public String getName() {
            return "Finder Logs for Flow";
        }

        public VerticalPanel getNextPanel(VerticalPanel previous) {
            VerticalPanel next = new VerticalPanel();
            HorizontalPanel hp = new HorizontalPanel();
            hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT,
                    Unit.PX);
            hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
            Label logs = new Label(FinderLogger.getFinderLogs("flow", null));
            hp.add(logs);
            next.add(hp);
            addBackAnchor(previous, next);
            return next;
        }
    }

    private static class TipLogs extends DebugEntry {
        public String getName() {
            return "Finder Logs for Tip";
        }

        public VerticalPanel getNextPanel(VerticalPanel previous) {
            VerticalPanel next = new VerticalPanel();
            HorizontalPanel hp = new HorizontalPanel();
            hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT,
                    Unit.PX);
            hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
            Label logs = new Label(FinderLogger.getFinderLogs("tip", null));
            hp.add(logs);
            next.add(hp);
            addBackAnchor(previous, next);
            return next;
        }
    }

    private static class BeaconLogs extends DebugEntry {
        public String getName() {
            return "Finder Logs for Beacon";
        }

        public VerticalPanel getNextPanel(VerticalPanel previous) {
            VerticalPanel next = new VerticalPanel();
            HorizontalPanel hp = new HorizontalPanel();
            hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT,
                    Unit.PX);
            hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
            Label logs = new Label(FinderLogger.getFinderLogs("beacon", null));
            hp.add(logs);
            next.add(hp);
            addBackAnchor(previous, next);
            return next;
        }
    }
}