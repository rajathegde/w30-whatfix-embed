package co.quicko.whatfix.embed.debugpanel;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import co.quicko.whatfix.common.StringUtils;

public class FlowStateDebugEntry extends DebugEntry {
    public String getName() {
        return "FlowState related data";
    }

    public VerticalPanel getNextPanel(VerticalPanel previous) {
        return getPanelFromDebugEntry(previous, new FlowDetails());
    }

    private static class FlowDetails extends DebugEntry {
        public String getName() {
            return "Current Playing Step Details";
        }

        public VerticalPanel getNextPanel(VerticalPanel previous) {
            VerticalPanel next = new VerticalPanel();
            HorizontalPanel hp = new HorizontalPanel();
            hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT,
                    Unit.PX);
            hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
            hp.add(new Label(StringUtils
                    .stringifyObject(DebugFunctions.getPlayState())));
            next.add(hp);
            addBackAnchor(previous, next);
            return next;
        }
    }
}