package co.quicko.whatfix.embed.debugpanel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import co.quicko.whatfix.common.JsArrayIterator;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.data.CustomMark.PivotMark;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.RuleFinder;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.alg.filler.AttributesFiller;
import co.quicko.whatfix.workflowengine.alg.filler.FillerUtil;
import co.quicko.whatfix.workflowengine.alg.filler.PropertyFiller;
import co.quicko.whatfix.workflowengine.alg.filter.ElementProperties;
import co.quicko.whatfix.workflowengine.data.strategy.rules.Rule;

public class FinderConfigCoverage extends DebugEntry {

    private static final int[] DIRECT_STRONG = { 3, 127 };
    private static final int[] DERIVATIVE_STRONG = { 2, 255 };

    @Override
    public String getName() {
        return "Show Strong elements";
    }

    @Override
    public VerticalPanel getNextPanel(VerticalPanel previous) {
        String message = "Finder rules are not configured.";
        if (DebugFunctions.showFinderConfig()) {
            message = "Please refresh the app before capture.";
        }
        VerticalPanel next = new VerticalPanel();
        HorizontalPanel hp = new HorizontalPanel();
        hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT, Unit.PX);
        hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
        hp.add(new Label(message));
        next.add(hp);
        addBackAnchor(previous, next);
        return next;
    }

    public static boolean showStrongElements() {
        JsArray<JsArray<JsArray<Rule>>> rules = Enterpriser.enterprise()
                .rule_config_finder();
        if (JsUtils.isNotEmpty(rules)) {
            showStrongElements(rules);
            return true;
        }
        return false;
    }

    private static void showStrongElements(
            JsArray<JsArray<JsArray<Rule>>> rules) {
        Map<Element, JsArray<PivotMark>> map = new HashMap<>();
        Set<Element> strong = fillDirectStrongElements(rules, map);
        Set<Element> derivativeStrong = fillDerivativeStrongElements(strong);
        for (Element elm : strong) {
            setEngager(elm, DIRECT_STRONG[0], DIRECT_STRONG[1]);
        }
        for (Element elm : derivativeStrong) {
            setEngager(elm, DERIVATIVE_STRONG[0], DERIVATIVE_STRONG[1]);
        }
    }

    private static Set<Element> fillDerivativeStrongElements(
            Set<Element> strong) {
        Set<Element> derivativeStrong = new HashSet<>();
        for (Element elm : strong) {
            NodeList<Element> children = DOMUtil.getElementsByQuerySelector(elm,
                    "*");
            for (int i = 0; i < children.getLength(); i++) {
                Element child = children.getItem(i);
                if (isTagNameUnique(elm, child)
                        || isUniquePropertyExists(elm, child)) {
                    derivativeStrong.add(child);
                }
            }
        }
        return derivativeStrong;
    }

    private static Set<Element> fillDirectStrongElements(
            JsArray<JsArray<JsArray<Rule>>> rules,
            Map<Element, JsArray<PivotMark>> map) {
        Set<Element> strong = new HashSet<>();
        Element debugPanel = DOMUtil
                .getUniqueElementForQuerySelector("#wfxDebugPanel");

        List<Element> allElements = DOMUtil
                .getAllElements(Document.get().getBody());
        for (int i = 0; i < allElements.size(); i++) {
            Element elm = allElements.get(i);
            if (debugPanel != null && debugPanel.isOrHasChild(elm)) {
                continue;
            }
            JsArray<PivotMark> pivotMarks = RuleFinder
                    .getPivotMarksFromRules(elm, rules);
            if (JsUtils.isNotEmpty(pivotMarks)) {
                map.put(elm, pivotMarks);
                for (int j = 0; j < pivotMarks.length(); j++) {
                    PivotMark mark = pivotMarks.get(j);
                    if (mark.depth() == 0) {
                        strong.add(elm);
                        break;
                    }
                }
            }
        }
        return strong;
    }

    private static boolean isTagNameUnique(Element source, Element child) {
        String tagName = child.getTagName().toLowerCase();
        NodeList<Element> elms = DOMUtil.getElementsByQuerySelector(source,
                tagName);
        return elms.getLength() == 1;
    }

    private static boolean isUniquePropertyExists(Element source,
            Element child) {
        JsArray<ElementProperties> properties = FillerUtil.getUniqueProperties(
                source, child, new AttributesFiller[] { new PropertyFiller() });
        JsArrayIterator<ElementProperties> itr = new JsArrayIterator<>(
                properties);
        while (itr.hasNext()) {
            ElementProperties property = itr.next();
            if (property.count() == 1
                    && !"class".equalsIgnoreCase(property.name())) {
                return true;
            }
        }
        return false;
    }

    private static native void setEngager(Element elm, int width, int colour)/*-{
        elm.style.outline = width + "px solid rgb(0, " + colour + " ,0)"
    }-*/;

}