package co.quicko.whatfix.embed.debugpanel;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public final class DebugPanel {

    private DebugPanel() {

    }

    private static FlowPanel mainPanel;
    private static final int Z_INDEX = 999999;
    private static final int PANEL_HEIGHT = 300;
    private static final int PANEL_WIDTH = 500;
    private static final int FONT_SIZE = 20;
    public static final int LINE_HEIGHT = 30;
    public static final int MARGIN = 10;

    public static void initialize() {
        if (mainPanel == null) {
            mainPanel = createPanel();
        }
    }

    private static FlowPanel createPanel() {
        final FlowPanel mainPanel = new FlowPanel();
        RootPanel.get().add(mainPanel);
        mainPanel.getElement().setId("wfxDebugPanel");
        Style style = mainPanel.getElement().getStyle();
        style.setPosition(Position.FIXED);
        style.setRight(0, Unit.PX);
        style.setBottom(0, Unit.PX);
        style.setZIndex(Z_INDEX);
        style.setBackgroundColor("#01bcd4");
        style.setFontSize(FONT_SIZE, Unit.PX);
        HorizontalPanel header = new HorizontalPanel();
        header.getElement().getStyle().setHeight(LINE_HEIGHT, Unit.PX);
        Label label = new Label(Document.get().getURL());
        header.add(label);
        Anchor close = new Anchor("X - close");
        close.getElement().getStyle().setPosition(Position.FIXED);
        close.getElement().getStyle().setRight(0, Unit.PX);
        close.addClickHandler(event -> removePanel());
        header.add(close);
        HorizontalPanel hp = new HorizontalPanel();
        hp.getElement().getStyle().setHeight(PANEL_HEIGHT, Unit.PX);
        hp.getElement().getStyle().setWidth(PANEL_WIDTH, Unit.PX);
        hp.add(header);
        VerticalPanel vp = initialEntries();
        hp.add(vp);
        mainPanel.add(header);
        mainPanel.add(hp);
        return mainPanel;
    }

    private static void removePanel() {
        mainPanel.removeFromParent();
        mainPanel = null;
    }

    private static VerticalPanel initialEntries() {
        return DebugEntry.getPanelFromDebugEntry(null,
                new FlowStateDebugEntry(), new VisualAppCoverage(),
                new ContentLogsDebugEntry(), new DetectSmartContextApp());
    }

}
