package co.quicko.whatfix.embed.debugpanel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import co.quicko.whatfix.common.JsArrayIterator;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft.Condition.ConditionsSet;
import co.quicko.whatfix.data.Tag;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import co.quicko.whatfix.workflowengine.app.App;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class SCconfigCoverage extends DebugEntry {

    private static final List<String> COLOURS = Arrays.asList("green", "red",
            "blue", "yellow", "black", "brown", "cyan", "indigo");

    @Override
    public String getName() {
        return "Show SC config Coverage";
    }

    @Override
    public VerticalPanel getNextPanel(VerticalPanel previous) {
        VerticalPanel next = new VerticalPanel();
        HorizontalPanel hp = new HorizontalPanel();
        hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT, Unit.PX);
        hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
        next.add(hp);
        JsArray<Tag> pageTags = DebugFunctions.showSmartContextConfig();
        if (JsUtils.isEmpty(pageTags)) {
            hp.add(new Label("SC rules are not configured."));
        } else {
            hp.add(new Label("Please refresh the app before capture."));
            JsArrayIterator<Tag> itr = new JsArrayIterator<>(pageTags);
            while (itr.hasNext()) {
                Tag pageTag = itr.next();
                HorizontalPanel hp1 = new HorizontalPanel();
                hp1.add(new Label(pageTag.description() + " : " + pageTag.name()
                        + " : "
                        + StringUtils.stringifyArray(pageTag.conditions())));
                next.add(hp1);
            }
        }
        addBackAnchor(previous, next);
        return next;
    }

    public static JsArray<Tag> showSCConfig() {
        JsArray<Tag> ret = DataUtil.createArray().cast();
        Map<Element, Tag> tagMapping = new HashMap<>();
        if (JsUtils.isNotEmpty(Enterpriser.enterprise().app_rules())) {
            App app = AppFactory.getMultiRuleConfiguredApp();
            Element debugPanel = DOMUtil
                    .getUniqueElementForQuerySelector("#wfxDebugPanel");

            List<Element> allElements = DOMUtil
                    .getAllElements(Document.get().getBody());

            for (int i = 0; i < allElements.size(); i++) {
                Element elm = allElements.get(i);
                if (debugPanel != null && debugPanel.isOrHasChild(elm)) {
                    continue;
                }
                Tag pageTag = AppController.getPageTag(app,
                        app.getSegmenter("2"), elm, null, null);
                if (null != pageTag) {
                    tagMapping.put(elm, pageTag);
                }
            }
            Map<ConditionsSet, Tag> highlight = highlight(tagMapping);
            if (!highlight.isEmpty()) {
                for (Tag pageTag : highlight.values()) {
                    ret.push(pageTag);
                }
            }
        }
        return ret;
    }

    private static Map<ConditionsSet, Tag> highlight(
            Map<Element, Tag> tagMapping) {
        int colourCount = 0;
        Map<ConditionsSet, Tag> colours = new HashMap<>();
        for (Entry<Element, Tag> entry : tagMapping.entrySet()) {
            Tag pageTag = entry.getValue();
            ConditionsSet conditionsSet = new ConditionsSet(
                    pageTag.getConditionSet());
            Tag tag = colours.computeIfAbsent(conditionsSet, k -> pageTag);
            if (tag.equals(pageTag)) {
                tag.description(COLOURS.get(colourCount));
                colourCount++;
                colours.put(conditionsSet, tag);
            }
            setEngager(entry.getKey(), tag.description());
        }
        return colours;
    }

    public static String getColour(int colourCount) {
        return COLOURS.size() < colourCount ? COLOURS.get(colourCount) : "pink";
    }

    private static native void setEngager(Element elm, String colour)/*-{
        elm.style.border = 2 + "px solid " + colour;
    }-*/;

}