package co.quicko.whatfix.embed.debugpanel;

import com.google.gwt.user.client.ui.VerticalPanel;

public class VisualAppCoverage extends DebugEntry {

    @Override
    public String getName() {
        return "Show Visaul App Coverage";
    }

    @Override
    public VerticalPanel getNextPanel(VerticalPanel previous) {
        return getPanelFromDebugEntry(previous, new FinderConfigCoverage(),
                new SCconfigCoverage());
    }

}