package co.quicko.whatfix.embed.debugpanel;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import co.quicko.whatfix.common.StringUtils;

public class DetectSmartContextApp extends DebugEntry {

    @Override
    public String getName() {
        return "Detect SmartContext App";
    }

    @Override
    public VerticalPanel getNextPanel(VerticalPanel previous) {
        HorizontalPanel hp = new HorizontalPanel();
        VerticalPanel next = new VerticalPanel();
        hp.getElement().getStyle().setHeight(DebugPanel.LINE_HEIGHT, Unit.PX);
        hp.getElement().getStyle().setMargin(DebugPanel.MARGIN, Unit.PX);
        hp.add(new Label(StringUtils
                .stringifyObject(DebugFunctions.detectSmartContextApp())));
        next.add(hp);
        addBackAnchor(previous, next);
        return next;
    }  

}
