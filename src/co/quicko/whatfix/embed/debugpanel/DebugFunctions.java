package co.quicko.whatfix.embed.debugpanel;

import com.google.gwt.core.client.JsArray;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.Pair;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.data.AppRules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft.Condition;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Tag;
import co.quicko.whatfix.embed.EmbedEntry;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.app.App;
import co.quicko.whatfix.workflowengine.app.Segmenter;
import co.quicko.whatfix.workflowengine.app.AppFactory.APP_NAMES;
import co.quicko.whatfix.workflowengine.configured.MultiRuleConfiguredAppV2;

public final class DebugFunctions {

    private DebugFunctions() {
        
    }

    public static void exportDebugPanel() {
        nativeDebugPanel();
        nativeExportPlayState();
        nativeFinderConfig();
        nativeSmartContextConfig();
        nativeSmartContextAppName();
        nativePrintNFRLogs();
    }

    private static native void nativeDebugPanel() /*-{
        $wnd._wfx_debug_panel = $entry(@co.quicko.whatfix.embed.debugpanel.DebugFunctions::initialize());
    }-*/;

    private static native void nativeExportPlayState() /*-{
        $wnd._wfx_play_state = $entry(@co.quicko.whatfix.embed.debugpanel.DebugFunctions::getPlayState());
    }-*/;

    private static native void nativeFinderConfig() /*-{
        $wnd._wfx_show_finder_config = $entry(@co.quicko.whatfix.embed.debugpanel.DebugFunctions::showFinderConfig());
    }-*/;

    private static native void nativeSmartContextConfig() /*-{
        $wnd._wfx_show_sc_config = $entry(@co.quicko.whatfix.embed.debugpanel.DebugFunctions::showSmartContextConfig());
    }-*/;
    
    private static native void nativeSmartContextAppName() /*-{
        $wnd._wfx_sc_app_name = $entry(@co.quicko.whatfix.embed.debugpanel.DebugFunctions::detectSmartContextApp());
    }-*/;

    public static void initialize() {
        DebugPanel.initialize();
    }

    public static JavaScriptObjectExt getPlayState() {
        JavaScriptObjectExt state = DataUtil.create();
        EmbedEntry entry = EmbedEntry.getEntry();
        PlayState playState = entry.getState();
        if (playState != null && playState.flow() != null) {
            state.put("flow_id", playState.flow().flow_id());
            state.put("step", entry.getCurrentStep().step() + "");
        } else {
            state.put("message", "Nothing is playing right now");
        }
        return state;
    }
    
    public static JavaScriptObjectExt detectSmartContextApp() {
        String key = "App for Smart Context";
        JavaScriptObjectExt state = DataUtil.create();
        Pair<App, Segmenter> appSegmenter = AppController.detectSegmenter();
        if (null == appSegmenter || appSegmenter.getLeft() == null) {
            state.put(key, "App not found for Smart Context");
            return state;
        }
        if (APP_NAMES.MULTI_RULE_CONFIGURED_APP
                .equals(appSegmenter.getLeft().getIdentifier())) {
            Pair<AppRules, JsArray<Condition>> maxAppRulesAndAppConditions = MultiRuleConfiguredAppV2
                    .getMaxAppRulesAndAppValidationConditionsPair();
            String appName = maxAppRulesAndAppConditions == null
                    || maxAppRulesAndAppConditions.getLeft() == null
                            ? "Smart context not defined."
                            : maxAppRulesAndAppConditions.getLeft().app_name();
            state.put(key, appName);

        } else {
            state.put(key, appSegmenter.getLeft().getIdentifier());
        }
        return state;
    }
    
    public static boolean showFinderConfig() {
        boolean showStrongElements = FinderConfigCoverage.showStrongElements();
        if (showStrongElements) {
            Console.log("Please refresh the app before capture.");
        } else {
            Console.log("Finder rules are not configured.");
        }
        return showStrongElements;
    }

    public static JsArray<Tag> showSmartContextConfig() {
        JsArray<Tag> tags = SCconfigCoverage.showSCConfig();
        if (JsUtils.isEmpty(tags)) {
            Console.log("SC rules are not configured.");
        } else {
            Console.log("Please refresh the app before capture.");
            Console.log(tags);
        }
        return tags;
    }
    
    public static native JsArray<JavaScriptObjectExt> nativePrintNFRLogs() /*-{
        $wnd._wfx_nfr_logs = $entry(@co.quicko.whatfix.common.logger.NFRLogger::printNFRLogs());
    }-*/;
}
