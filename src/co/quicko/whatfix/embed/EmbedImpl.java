package co.quicko.whatfix.embed;

import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.overlay.CrossMessager;

/**
 * @author anoosh
 *
 */
public class EmbedImpl {
    
    protected static EmbedEntry embed;
    
    public void init(AsyncCallback<Boolean> cb, EmbedEntry embed) {
        EmbedImpl.embed = embed;
        cb.onSuccess(false);
        CrossMessager.addListener(EmbedImpl.embed::onDisableMoveState,
                Action.DISABLE_MOVE_STATE);
    }
    
    public void tryToPlay() {
        embed.tryToPlay();
    }
    
    public void schDirectPlay() {
        embed.schDirectPlay();
    }
    
    public boolean shouldPersistCookies() {
        return true;
    }
}
