package co.quicko.whatfix.embed;

import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.extension.util.ExtensionUtil;
import co.quicko.whatfix.overlay.CrossMessager;

public class ExtensionEmbedImpl extends EmbedImpl {
    
    // Extension case
    // Initializes the baseUrl in Framers
    @Override
    public void init(final AsyncCallback<Boolean> cb, EmbedEntry embed) {
        ExtensionEmbedImpl.embed = embed;
        Action.setPrefix(actionPrefix());
        Framers.FRAME_URL.initBaseUrl(new AsyncCallback<Void>() {
            @Override
            public void onSuccess(Void result) {
                cb.onSuccess(true);
            }

            @Override
            public void onFailure(Throwable caught) {
                cb.onFailure(caught);
            }
        }, ExtensionUtil.WHATFIX_DOMAIN + "/");
        CrossMessager.addListener(ExtensionEmbedImpl.embed::onDisableMoveState,
                Action.DISABLE_MOVE_STATE);
    }
    
    @Override
    public void tryToPlay() {
        embed.playFromState();
    }
    
    @Override
    public void schDirectPlay() {
        embed.directPlay();
    }
    

    protected String actionPrefix() {
        return ExtensionUtil.CUSTOM_EXT_PREFIX;
    }
   
    @Override
    public boolean shouldPersistCookies() {
        return false;
    }
}
