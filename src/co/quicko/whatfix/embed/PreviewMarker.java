package co.quicko.whatfix.embed;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.Alert;
import co.quicko.whatfix.common.ConfirmationPopUp;
import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.common.Pair;
import co.quicko.whatfix.common.PopUpCallback;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.ToggleCheckbox;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.workflowengine.app.AppFactory;

class PreviewMarker extends NoContentPopup {

    /**
     * It shows alert marker on PreviewDomains when widget realization is
     * enabled.
     */
    private final EditorEmbed editorEmbed;
    private boolean top = true;
    private FlowPanel mainPanel;
    private ConfirmationPopUp previewModePopup;

    public static final String ID_ATTRIBUTE = "wfx-preview-marker-popup";

    public PreviewMarker(EditorEmbed editorEmbed) {
        this.editorEmbed = editorEmbed;
        Common.setId(this, ID_ATTRIBUTE);
        setModal(false);
        setAutoHideEnabled(false);
        setAnimationEnabled(false);
        setAutoHideOnHistoryEventsEnabled(false);
        addPreviewLabelDisplayListeners();
        setWidget(previewPanel());
    }

    @Override
    public void show() {
        super.show();
        setPosition();
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                hide();
            }
        }, Action.WIDGET_SETTINGS_SET);
    }

    private FlowPanel previewPanel() {
        setStyleName(Overlay.CSS.previewMarkerPopup());
        mainPanel = new FlowPanel();
        mainPanel.setStyleName(Overlay.CSS.previewMarkerPanel());
        FlowPanel innerPanel = new FlowPanel();
        innerPanel.setStyleName(Overlay.CSS.previewMarker());
        innerPanel.add(getToggleCheckboxPanel());

        mainPanel.add(innerPanel);
        return mainPanel;
    }

    private void addPreviewLabelDisplayListeners() {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                setVisible(("show").equals(content));
            }
        }, Action.PREVIEW_MODE_LABEL);
    }
    
    private FlowPanel getToggleCheckboxPanel() {
        Pair<FlowPanel, ToggleCheckbox> pair = Common.getWidgetPreview(
                Common.i18nConstants.previewMarker(), true,
                editorEmbed.userId(), new AsyncCallback<Void>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        Common.showMessage("Preview Mode not updated",
                                Alert.ERROR);
                    }

                    @Override
                    public void onSuccess(Void result) {
                        CrossMessager.sendMessage(
                                Action.WIDGET_SETTINGS_SET.toString(),
                                editorEmbed.entId() + "." + false + "."
                                        + false);
                        String alertMsg = AppFactory.isDesktopApp()
                                ? Common.i18nConstants
                                        .nonPreviewModeAlertMessageDesktop()
                                : Common.i18nConstants
                                        .nonPreviewModeAlertMessage();
                        showPreviewModePopup(
                                Common.i18nConstants.nonPreviewModeAlert(),
                                alertMsg);
                    }
                });
        FlowPanel toggleCheckboxPanel = pair.getLeft();
        toggleCheckboxPanel.addStyleName(Overlay.CSS.toggleCheckboxPanel());
        return toggleCheckboxPanel;
    }

    private Anchor getArrowAnchor() {
        Image arrow;
        if (top) {
            arrow = Common.image(Overlay.BUNDLE.arrowDown(),
                    Overlay.CSS.previewMarkerImg());
        } else {
            arrow = Common.image(Overlay.BUNDLE.arrowUp(),
                    Overlay.CSS.previewMarkerImg());
        }

        Anchor anchor = Common.imageAnchor(arrow);
        anchor.setStyleName(Overlay.CSS.previewMarkerArrow());
        anchor.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                top = !top;
                EditorEmbed.stater.setState(getWidgetPositionKey(),
                        Boolean.toString(top));
                setPosition();
            }
        });
        return anchor;
    }

    private void setPosition() {
        EditorEmbed.stater.getState(getWidgetPositionKey(),
                new AsyncCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        if (StringUtils.isBlank(result)) {
                            result = "true";
                            //default position to top null/true
                        }
                        top = Boolean.parseBoolean(result);
                        setPositionByLocation();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        setPositionByLocation();
                    }
                });
    }

    /**
     * Use the location variable top to figure out where to place the marker
     */
    private void setPositionByLocation() {
        Element elem = getElement();
        Style style = elem.getStyle();
        OverlayUtil.clearPosition(style);
        int margin = 0;
        if (top) {
            OverlayUtil.setTop(style, margin);
            removeStyleName(Overlay.CSS.previewMarkerBottom());
            addStyleName(Overlay.CSS.previewMarkerTop());
        } else {
            OverlayUtil.setBottom(style, margin);
            removeStyleName(Overlay.CSS.previewMarkerTop());
            addStyleName(Overlay.CSS.previewMarkerBottom());
        }

        if (mainPanel.getWidgetCount() > 1) {
            mainPanel.remove(1);
        }
        mainPanel.add(getArrowAnchor());
    }
    
    public void showPreviewModePopup(String title, String text) {
        int zIndex = Overlay.CSS.maxZindex();
        previewModePopup = new ConfirmationPopUp(null, title, text,
                Common.i18nConstants.ok(), null, zIndex, new PopUpCallback() {

                    @Override
                    public void onPositiveAction() {
                        previewModePopup.close();
                    }

                    @Override
                    public void onNegativeAction() {
                        previewModePopup.close();
                    }
                });
        previewModePopup.showPopup();
        previewModePopup.addStyleName(Common.CSS.previewModePopup());
    }
    
    /**
     * Key generator for storing the position of the preview mode as top or
     * bottom, top is null/true , bottom is false
     * 
     * <br>
     * 
     * We need this to ensure our preview marker stays where it was last marked
     * by the user, either top or bottom but also based on the application.
     * 
     * 
     * @return a key of type __wfx_widget_position_domainhostname_userid
     * 
     */
    private String getWidgetPositionKey() {
        return "__wfx_widget_position_"
                + Location.getHostName().replaceAll(".", "_") + "_"
                + editorEmbed.userId();
    }
}