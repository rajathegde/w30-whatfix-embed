package co.quicko.whatfix.embed;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * @author madhura 
 * This class stores the stage,state and embed timestamp
 * information that's passed from top page single script to single
 * script in iframes of the page.
 *
 */
public class CLMStateInfo extends JavaScriptObject{
    protected CLMStateInfo() {}
    
    public final native String get_stage()/*-{
        return this.stage;
    }-*/;
    
    public final native String get_state()/*-{
        return this.state;
    }-*/;
    
    public final native String get_embed_url()/*-{
        return this.embed_url;
    }-*/;
    
    public final native void set_info(String stage,String state,String embedUrl)/*-{
        this.stage = stage;
        this.state = state;
        this.embed_url = embedUrl;
    }-*/;
    
}
