package co.quicko.whatfix.embed;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.SelfHelpWidgetSettings;
import co.quicko.whatfix.data.Enterprise;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.extension.util.WidgetSettings;
import co.quicko.whatfix.ga.AnalyticsTracker;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.FramesData;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.EnterpriseService;

public class EditorEmbed extends EmbedEntry {

    private boolean widgetInit;
    private String entId;
    private String userId;
    private boolean enableEditSegment;
    private boolean isAutoTesting = false;
    private boolean isExtensionTagAttached = false;

    @Override
    public void onModuleLoad() {
        if (!OverlayUtil.isTopWindow()) {
            superOnModuleLoad();
            return;
        }

        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                CrossMessager.removeListener(this);
                WidgetSettings widgetSettings = DataUtil.create(content).cast();
                entId = widgetSettings.ent_id();
                userId = widgetSettings.user_id();
                widgetInit = Boolean
                        .parseBoolean(widgetSettings.preview_mode_enabled());
                enableEditSegment = widgetSettings.enable_edit_segment();
                isAutoTesting = widgetSettings.is_auto_testing();
                placePreviewMarker();
                disableAnalytics();
                superOnModuleLoad();
            }
        }, Action.EDITOR_WIDGET_SETTINGS);
        CrossMessager.sendMessage(Action.GET_EDITOR_WIDGET_SETTINGS,
                Location.getHostName());
        flowRunThroughEditor = true;
        ExtensionHelper.setPreviewMode(true);
    }

    private void superOnModuleLoad() {
        super.onModuleLoad();
    }

    @Override
    protected String entId() {
        if (!widgetInit) {
            return null;
        }
        Customizer.setIfEmpty();
        return entId;
    }

    @Override
    protected boolean shouldEditorInitiate() {
        return false;
    }

    @Override
    protected void refreshWidgets() {
        if (isAutoTesting) {
            return;
        }
        if (widgetInit) {
            super.refreshWidgets();
        }
    }

    @Override
    protected void initEmbed() {
        if (widgetInit) {
            super.initEmbed();
        }
    }

    @Override
    protected boolean hasEmbed() {
        if (widgetInit || isExtensionTagAttached) {
            return super.hasEmbed();
        }
        return false;
    }

    @Override
    protected void informExistence() {
        // Nothing to inform here
    }

    @Override
    protected void addFlowPlayListeners() {
        if (widgetInit) {
            super.addFlowPlayListeners();
        }
    }
    
    private void disableAnalytics() {
        if(widgetInit) {
            AnalyticsTracker.disableAnalyticsTracker();
        }
    }
    
    // Fallback Mechanism When Integration Fails
    // Enteprise is obtained via API and Initialized
    // No Integration script gets executed
    @Override
    protected void integrationFailed() {
        if (widgetInit && StringUtils.isNotBlank(entId)) {
            EnterpriseService.IMPL.enterpriseById(entId,
                    new AsyncCallback<Enterprise>() {
                        @Override
                        public void onSuccess(Enterprise result) {
                            Enterpriser.reset(result);
                            FramesData framesData = FramesData.getData();
                            CrossMessager.sendMessageToFrames(
                                    "enterprise_settings_all_frames",
                                    StringUtils.stringifyObject(framesData));
                            superIntegrationFailed();
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            superIntegrationFailed();
                        }
                    });
            return;
        }
        superIntegrationFailed();
    }
    
    private void superIntegrationFailed() {
        super.integrationFailed();
    }

    // Filling Settings with editorRealised Flag
    @Override
    protected SelfHelpWidgetSettings getRoleMergedwidgetSettings() {
        SelfHelpWidgetSettings settings = super.getRoleMergedwidgetSettings();
        if (settings != null) {
            settings.is_editor_realized(true);
            settings.enable_edit_segment(enableEditSegment);
        }
        return settings;
    }

    @Override
    protected SelfHelpWidgetSettings getNoValidRoleSegment(
            SelfHelpWidgetSettings settings) {
        // show empty widget if no valid role
        settings.no_initial_flows(true);
        settings.custom_message(Common.getNoValidRoleMessage());
        return settings;
    }

    private void placePreviewMarker() {
        if (isAutoTesting) {
            return;
        }
        if (widgetInit) {
            new PreviewMarker(this).show();

            // Retry creating preview marker if removed from DOM by a script in
            // client page - Refer SUCC-1905
            final EditorEmbed editorEmbed = this;
            Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
                private final int MAX_TRIES = 5;
                private int currentTries = 0;

                @Override
                public boolean execute() {
                    currentTries++;
                    if (currentTries <= MAX_TRIES) {
                        if (DOM.getElementById(
                                PreviewMarker.ID_ATTRIBUTE) == null) {
                            new PreviewMarker(editorEmbed).show();
                        }
                        return true;
                    }
                    return false;
                }
            }, 1000);
        }
    }

    public String userId() {
        return userId;
    }

    @Override
    protected String getLoggedInUserId() {
        return userId;
    }
    
    /**
     * Show Graceful Exit popup only if Preview mode is ON.
     */
    @Override
    public void onMiss(JsArray<JavaScriptObjectExt> reason) {
        if (widgetInit) {
            super.onMiss(reason);
        }
        // not changing the logic of above code, separating code execution for
        // EditorTroubleshoot
        if (Enterpriser.isEditorTroubleshootEnabled()) {
            super.sendFailureMsgToEditorTroubleshoot();
        }
    }
    
    /**
     * Show Graceful Exit popup only if Preview mode is ON and troubleshoot is
     * off.
     */
    @Override
    protected boolean shouldDisplayGracefulExit() {
        return widgetInit && Enterpriser.isGracefulFailureEnabled()
                && !Enterpriser.isEditorTroubleshootEnabled();
    }
    
    
    /**
	 * setting ___embed to true so that only editorEmbed handles flow play. Though,
	 * not calling super.refreshWidgets() since editorEmbed will then load the test
	 * content from server rather then the cdn content. Ref : SUCC-3986
	 */
    @Override
    protected void handleExtensionTagFlow() {
    	isExtensionTagAttached = true;
    	super.initEmbed();
    }

}