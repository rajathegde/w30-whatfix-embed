package co.quicko.whatfix.embed;

import static co.quicko.whatfix.service.WfxInfo.getDomain;

import java.util.HashMap;
import java.util.Map;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.ScriptInjector;
import co.quicko.whatfix.common.ScriptInjector.FromUrl;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft.Condition;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.service.WfxInfo;
import co.quicko.whatfix.service.WfxInfo.Domains;
import co.quicko.whatfix.workflowengine.data.strategy.Strategies;

/**
 * Initiator script supports multiple stages by acting as a proxy. It evaluates
 * the stage mapping conditions in order to evaluate the stage and state,
 * injects the appropriate script based on these. Sets this information so that
 * it's accessible by embed.
 * 
 * @author madhura
 *
 */
public class InitiatorEntry implements EntryPoint {

    private static final String EMBED_SCRIPT_ID = "_wfx_embed_injected_via_cdn";
    private static final String STAGE = "stage";
    private static final String STATE = "state";
    private static final String PRODUCTION = "production";
    private static final String DESIGN = "design";
    private static final String READY = "ready";
    private static final String GET_STAGE = "get_stage";
    private static final String SET_STAGE = "set_stage";

    private String entId;
    private CLMStateInfo stageInfo;
    
    @Override
    public void onModuleLoad() {
        if (OverlayUtil.isPrimaryWindow()) {
            injectEmbedInTopFrame();
        }
        else {
            injectEmbedInIframes();
        }
    }

    private void injectEmbedInTopFrame() {
        entId = getEntId();
        if (StringUtils.isNotBlank(entId)) {
            setCLMStageInfo();
            injectScript();
        }
        communicateStageInfoWithIframes();
    }

    private String getEntId() {
        String initStr = GWT.getModuleName() + ".nocache.js";
        NodeList<Element> elements = Document.get()
                .getElementsByTagName(ScriptElement.TAG);
        int len = elements.getLength();
        for (int i = 0; i < len; i++) {
            ScriptElement scriptElement = (ScriptElement) elements.getItem(i);
            if (scriptElement != null
                    && StringUtils.isNotBlank(scriptElement.getSrc())) {
                return getExtractedEntId(initStr, scriptElement);
            }
        }
        Console.error("Ent id not found");
        return null;
    }
    
    private String getExtractedEntId(String initStr,
            ScriptElement scriptElement) {
        String src = scriptElement.getSrc();
        if (src.contains(initStr)) {
            String[] urlParts = src.split(WfxInfo.getDomain(Domains.API));
            String[] potentialEntIds = urlParts[1].split("/");
            for (String potentialEntId : potentialEntIds) {
                if (Common.isGUID(potentialEntId) && src
                        .contains(WfxInfo.getDomain(Domains.PRODSCRIPT))) {
                    return potentialEntId;
                }
            }
        }
        return null;
    }
    
    private void setCLMStageInfo() {
        JSONObject stageConditionsJson = getConditions();
        JsArray<Condition> designConditions = getConditionsFromResult(
                stageConditionsJson, Common.DESIGN_SCRIPT_CONDITIONS);
        JsArray<Condition> prodConditions = getConditionsFromResult(
                stageConditionsJson, Common.PROD_SCRIPT_CONDITIONS);
        setStageBasedOnConditions(designConditions, prodConditions);
    }
    
    private JSONObject getConditions() {
        JSONObject stageConditionsJson = null;
        try {
            stageConditionsJson = JSONParser.parseStrict(getStageConditions())
                    .isObject();
        } catch (Exception e) {
            Console.error("Stage conditions not found.");
        }
        return stageConditionsJson;
    }

    private void setStageBasedOnConditions(JsArray<Condition> designConditions,
            JsArray<Condition> prodConditions) {
        if (areMappingConditionsValid(prodConditions)) {
            stageInfo.set_info(PRODUCTION, "", getEmbedUrl(getTimeStamp()));
        } else if (areMappingConditionsValid(designConditions)) {
            stageInfo.set_info(DESIGN, READY, getEmbedUrl(getTimeStamp()));
        } else {
            // if no conditions match or no conditions are specified, then
            // don't inject any stage.
            stageInfo = null;
        }
    }

    private void injectEmbedInIframes() {
        CrossListener lisener = new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                CrossMessager.removeListener(this, SET_STAGE);
                stageInfo = DataUtil.create(content);
                injectScript();
            }
        };
        CrossMessager.addListener(lisener, SET_STAGE);
        CrossMessager.sendMessageToTop(GET_STAGE, "");
    }

    private boolean isValidInfo() {
        return stageInfo != null && StringUtils.isNotBlank(stageInfo.get_stage())
                && StringUtils.isNotBlank(stageInfo.get_embed_url());
    }

    private boolean areMappingConditionsValid(JsArray<Condition> conditions) {
        return conditions != null && Strategies.isFit(conditions);
    }
    
    private String getEmbedUrl(String timeStamp) {
        return "https://" + getDomain(Domains.PRODSCRIPT) + "/prod/" + entId
                + "/" + timeStamp + "/embed/embed.nocache.js";
    }
    
    private String getTimeStamp() {
        JSONObject stageMappingsJSON = JSONParser
                .parseStrict(getStageMappings()).isObject();
        return stageMappingsJSON.get(getStageStateKey()).isString()
                .stringValue();
    }

    private static JsArray<Condition> getConditionsFromResult(
            JSONObject detailObj, String fieldName) {
        if (detailObj != null && detailObj.containsKey(fieldName)) {
            JSONArray condArr = detailObj.get(fieldName).isArray();
            if (condArr == null) {
                return null;
            }
            return condArr.getJavaScriptObject().cast();
        }
        return null;
    }

    private void injectScript() {
        if (!isValidInfo()) {
            return;
        }
        FromUrl embedFromUrl = ScriptInjector.fromUrl(stageInfo.get_embed_url());
        setStageInfoAsAttribute(embedFromUrl);
        embedFromUrl.inject();
    }

    private String getStageStateKey() {
        if (StringUtils.isBlank(stageInfo.get_state())) {
            return stageInfo.get_stage() + "_js_timestamp";
        }
        return stageInfo.get_stage() + "_" + stageInfo.get_state()
                + "_js_timestamp";
    }

    private void setStageInfoAsAttribute(FromUrl embedFromUrl) {
        embedFromUrl.setId(EMBED_SCRIPT_ID);
        Map<String, String> attributes = new HashMap<>();
        attributes.put(STAGE, stageInfo.get_stage());
        if (StringUtils.isNotBlank(stageInfo.get_state())) {
            attributes.put(STATE, stageInfo.get_state());
        }
        embedFromUrl.setAttributes(attributes);
    }

    private void communicateStageInfoWithIframes() {
        CrossMessager.sendMessageToFrames(SET_STAGE, stageInfo.toString());
        CrossListener topListener = (String type, String content) -> {
            CrossMessager.sendMessage(SET_STAGE, stageInfo.toString());
        };
        CrossMessager.addListener(topListener, GET_STAGE);
    }

    /**
     * 
     * Stage mappings will get replaced in the backend during packaging. Stage
     * mappings indicates the embed timestamp folder to be used for each stage-
     * production and design ready.
     * 
     * @return
     */
    private static native String getStageMappings() /*-{
        var stage_mappings = "wfx_clm_stage_mappings_replace";
        return stage_mappings;
    }-*/;

    /**
     * Stage conditions get replaced in the backend during packaging. Stage
     * conditions indicate the visibility conditions configured for each stage
     * (production and design ready).
     * 
     * @return
     */
    private static native String getStageConditions() /*-{
        var stage_conditions = "wfx_clm_stage_conditions_replace";
        return stage_conditions;
    }-*/;
}