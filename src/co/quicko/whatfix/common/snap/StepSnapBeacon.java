package co.quicko.whatfix.common.snap;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.Themer.BEACON.BeaconAnimationType;
import co.quicko.whatfix.overlay.Beacon;
import co.quicko.whatfix.overlay.Overlay;


public class StepSnapBeacon extends StepSnap {

    private StepSnap snap;
    private FlowPanel beacon;

    public StepSnapBeacon(StepSnap snap) {
        this.snap = snap;
        structureBeacon();
    }

    public void structureBeacon() {
        beacon = new FlowPanel();
        snap.add(beacon, 40, 150);
    }
    
    private void setBeaconAnimation() {
        beacon.clear();
        SimplePanel beaconLauncher = new SimplePanel();
        BeaconAnimationType animationType = Beacon.getCurrentAnimationType();
        Beacon.setBeaconAnimation(beacon, beaconLauncher, animationType);
        Widget beaconBase = Beacon.getBeaconBase(snap.getValue(),
                animationType);
        beacon.add(beaconBase);
    }

    public void refresh() {
        Step step = snap.getValue();
        int mark_offset = 2 * Overlay.CSS.mark_strength();
        int top = snap.top(step) - mark_offset;
        int left = snap.left(step) - mark_offset;
        int width = snap.width(step) + mark_offset;
        int height = snap.height(step) + mark_offset;
        String placement = step.placement();

        int[] position = Beacon.position(top, left + width + mark_offset,
                top + height + mark_offset, left, Beacon.ICON_HEIGHT,
                Beacon.ICON_WIDTH, placement);
        setBeaconAnimation();
        snap.setWidgetPosition(beacon, position[0], position[1]);

        Label highlight = snap.getHightlight();
        Themer.applyTheme(highlight, Themer.STYLE.BORDER_COLOR,
                Themer.BEACON.BEACON_COLOR);
        highlight.setVisible(true);
        highlight.setPixelSize(width, height);
        snap.setWidgetPosition(highlight, left, top);
    }

}
