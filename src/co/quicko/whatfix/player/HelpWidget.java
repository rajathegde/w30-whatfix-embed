package co.quicko.whatfix.player;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.ShortcutHandler.Shortcut;
import co.quicko.whatfix.common.ShortcutHandler.ShortcutListener;
import co.quicko.whatfix.common.StateHandler;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.TimeStampTracker;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.FullPopover;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.Popover;
import co.quicko.whatfix.overlay.Popover.ClickRegister;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.StepPop;
import co.quicko.whatfix.overlay.StepStaticPop;
import co.quicko.whatfix.overlay.actioner.ActionerPopover;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.player.widget.NotificationRenderer;
import co.quicko.whatfix.player.widget.event.lifecycle.BeforeSelfHelpOpenEvent;
import co.quicko.whatfix.player.widget.event.lifecycle.SelfHelpClosedEvent;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.tracker.event.EventPayload.Builder;

public class HelpWidget extends BaseWidget implements ShortcutListener {
    protected FlowPanel frameOuterContainer;
    protected FlowPanel frameContainer;

    private Widget progressor;
    private AsyncCallback<Void> callback;
    private boolean animationSupported = false;
    private boolean showingFull = false;
    private boolean srcSet = false;
    private ActionerPopover popover;

    public static StateHandler stater = GWT.create(StateHandler.class);
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    public HelpWidget(PlayerBase player, Settings settings,
            final RePositionCB rePositionCB) {
        super(player, settings, rePositionCB);

        frameOuterContainer = new FlowPanel();
        frameContainer = new FlowPanel();
        renderer = new NotificationRenderer(frameOuterContainer, getElement(), settings, TrackerEventOrigin.SELF_HELP);

        progressor = Common.getProgressor(Common.CSS.widgetProgressor(),
                Overlay.CSS.progressor());
        callback = callback();

        addShortcut();
    }

    protected void createStructure() {
        frame.addStyleName(Overlay.CSS.helpWidgetFrame());
        frame.addStyleName(widgetExpanded());
        frame.addStyleName(Overlay.CSS.widget());
        frame.setTitle(settings.title());
        frame.getElement().setTabIndex(-1);
        frame.getElement().setAttribute(AriaProperty.LABEL, "Self Help");
        Roles.getPresentationRole().set(frame.getElement());
        Common.setBorderRadius(frame.getElement(), settings.position());

        getContainerElement().setClassName(Overlay.CSS.fillContainer());

        frameOuterContainer.addStyleName(Overlay.CSS.frameOuterContainer());

        frameContainer.addStyleName(Overlay.CSS.frameContainer());

        frameOuterContainer.add(label);
        frameOuterContainer.add(frameContainer);
        frameOuterContainer.add(progressor);

        setWidget(frameOuterContainer);
        frameOuterContainer.addDomHandler(this, ClickEvent.getType());
        
        Common.setId(this, getIdAttribute());
        Common.setId(frameContainer, getLauncherIdAttribute());
    }

    protected String getIdAttribute() {
        return WIDGET_ID;
    }

    protected String getLauncherIdAttribute() {
        return LAUNCHER_ID;
    }

    @Override
    protected void showFull() {
        EventBus.fire(new BeforeSelfHelpOpenEvent());
        super.removeAnchorFocusStyle();
        getElement().setAttribute("draggable", "false");
        label.removeFromParent();

        frameContainer.addStyleName(widgetExpanded());
        getElement().addClassName(widgetExpanded());
        if (isDraggable) {
            getElement().removeClassName(getStyle(settings.position()));
        }
        frameOuterContainer
                .addStyleName(Overlay.CSS.frameOuterContainerExpanded());

        showingFull = true;
        setAutoHideEnabled(true);

        fallback();
    }

    @Override
    protected void closeFull() {
        getElement().setAttribute("draggable", "true");
        label.getElement().setAttribute(AriaProperty.LABEL,
                getWidgetLabel(settings));
        label.getElement().setAttribute(AriaProperty.EXPANDED,
                String.valueOf(false));
        disposeHelpWidgetTip();
        showingFull = false;
        setAutoHideEnabled(false);

        frame.removeStyleName(Overlay.CSS.helpWidgetFrameDisplay());

        if (srcSet) {
            progressor.removeStyleName(Overlay.CSS.progressorDisplay());
            getElement().removeClassName(Overlay.CSS.widgetShadow());
            srcSet = false;
            trackTime();
        }

        getElement().removeClassName(widgetExpanded());
        frameContainer.removeStyleName(widgetExpanded());
        if (isDraggable) {
            addStyleName(getStyle(settings.position()));
        }
        frameOuterContainer
                .removeStyleName(Overlay.CSS.frameOuterContainerExpanded());

        fallback();
    }

    @Override
    public void hide(boolean autoClosed) {
        if (isShowingFull()) {
            closeFull();
            trackWidgetClose(getWidgetCloseBuilder(), "closeBy", "click",
                    "src_id", getWidgetSrcName());
        } else {
            super.hide(false);
        }
    }

    @Override
    public boolean isShowingFull() {
        return showingFull;
    }

    @Override
    protected void postSetup(boolean recreated) {
        setUpAnimation();
        super.postSetup(recreated);
    }

    private void setUpAnimation() {
        addAnimationStyles();
        animationSupported = Common.CssAnimation.registerCssCallback(
                getElement(), "background-color", callback);
    }

    private void fallback() {
        if (!animationSupported) {
            callback.onSuccess(null);
        }
    }

    @Override
    protected void onLoaded() {
        frame.addStyleName(Overlay.CSS.helpWidgetFrameDisplay());
        progressor.removeStyleName(Overlay.CSS.progressorDisplay());
        LOGGER.log(WfxLogLevel.PERFORMANCE, HelpWidget.class, "click-to-render",
                WfxModules.WORKFLOW_ENGINE,
                "Self Help : Time taken by click-to-render action : {0} ms",
                (System.currentTimeMillis() - TimeStampTracker.getTime(TimeStampTracker.Event.SELF_HELP_TRIGGERED_TIME)));
        sendAnalayticsCallforContentLoaded(settings);
    }

    private AsyncCallback<Void> callback() {
        return new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                onSuccess(null);
            }

            @Override
            public void onSuccess(Void result) {
                frame.removeFromParent();
                framer.removeSrc(frame);
                if (!isShowingFull()) {
                    frameOuterContainer.add(label);
                    hideForRelative();
                    if (focusSelfHelp) {
                        label.getElement().setTabIndex(0);
                        label.getElement().focus();
                    }
                    EventBus.fire(new SelfHelpClosedEvent());
                } else {
                    framer.setSrc(frame);
                    frameContainer.add(frame);
                    progressor.addStyleName(Overlay.CSS.progressorDisplay());
                    getElement().addClassName(Overlay.CSS.widgetShadow());
                    srcSet = true;
                    setHelpTip();
                }
            }
        };
    }

    @Override
    protected String mode() {
        return WidgetLauncher.FULL_MODE;
    }

    private void addAnimationStyles() {
        getElement().addClassName(widgetAnimation());
    }

    protected String widgetExpanded() {
        if (Enterpriser.hasFeature(AdditionalFeatures.chat_bot_tab)) {
            return Overlay.CSS.widgetExpandedX();
        } else {
            return Overlay.CSS.widgetExpanded();
        }
    }

    protected String widgetAnimation() {
        return Overlay.CSS.widgetAnimation();
    }

    private void addShortcut() {
        Shortcut shortCut = Themer.widgetShortcut();
        if (shortCut != null) {
            register(shortCut, this);
        }
    }

    @Override
    public void onShortcut(Shortcut shortcut) {
        if (!isShowingFull()) {
            onClick(null);
        } else {
            closeFull();
            trackWidgetClose(getWidgetCloseBuilder(), "closeBy",
                    "shortcut", "src_id", getWidgetSrcName());
        }
    }

    protected String getWidgetSrcName() {
        return TrackerEventOrigin.SELF_HELP.getSrcName();
    }

    private void trackWidgetClose(Builder builder, Object... objects) {
        trackWidgetClose(builder,
                StringUtils.stringifyObject(Where.create(objects)));
    }

    protected void setHelpTip() {
        if (!isTipMessageEnabled()
                || (WidgetActionsTracker.getCount().widgetTipCount() >= 1)) {
            return;
        }

        Step step = DataUtil.create();
        step.placement(getTipPlacement());
        int width = getElement().getOffsetWidth();
        int height = getElement().getOffsetHeight();
        int top = getElement().getOffsetTop();
        int bottom = top + height;
        int left = getElement().getOffsetLeft();
        int right = left + width;

        popover = new ActionerPopover(new HelpWidgetTipExtender(), null, null,
                step, top, right, bottom, left, width, height);
        popover.popover().addStyleName(Overlay.CSS.fixedTip());
        popover.show(top, right, bottom, left, width, height,
                getTipPlacement());
    }

    protected boolean isTipMessageEnabled() {
        return "show".equalsIgnoreCase(Themer.value(Themer.SELFHELP.TIP_MSG))
                ? true : false;
    }

    private String getTipPlacement() {
        String selfHelpPosition = settings.position();
        if (selfHelpPosition != null) {
            if (selfHelpPosition.contains("l")) {
                return "rt";
            }
        }
        return "lt";
    }

    private class HelpWidgetTipExtender extends PopoverExtender {
        @Override
        protected Popover makePopover(Step action, PopListener listener,
                ClickRegister register, boolean alignment) {
            return new HelpWidgetTipPopover(listener, register, alignment);
        }

        @Override
        protected StepPop stepPop(Step step, String footnote,
                String placement, String stepnote) {
            return new HelpWidgetTipPop(step, footnote, placement, true);
        }

        @Override
        protected String footnote(Draft draft, Step action) {
            return " ";
        }
    }

    private void disposeHelpWidgetTip() {
        if (popover != null) {
            popover.dispose();
            popover = null;
            WidgetActionsCount counts = WidgetActionsTracker.getCount();
            counts.widgetTipCount(counts.widgetTipCount() + 1);
            WidgetActionsTracker.onInteraction(counts);
        }
    }

    private String getStyle(String position) {
        if (position.startsWith("l") || position.startsWith("r")) {
            return Overlay.CSS.widgetLauncherDrag();
        } else {
            return Overlay.CSS.widgetLauncherDragHorizontal();
        }
    }

    private class HelpWidgetTipPopover extends FullPopover {

        public HelpWidgetTipPopover(PopListener listener,
                ClickRegister register, boolean isLTR) {
            super(listener, register, true);
        }

        @Override
        protected String getThemeType() {
            return Themer.TIP.TipThemeType.CLASSIC.name;
        }

        @Override
        protected boolean isBackNeeded() {
            return false;
        }

        @Override
        protected void refreshExtra(Pop pop) {
            super.refreshExtra(pop);
            if (null != next) {
                next.setText(Overlay.CONSTANTS.helpWidgetTipButtonText(false));
                Themer.applyTheme(next, Themer.STYLE.BACKGROUD_COLOR,
                        Themer.SELFHELP.TIP_BUTTON_BG_COLOR, Themer.STYLE.COLOR,
                        Themer.SELFHELP.TIP_TEXT_COLOR);
            }
        }

        @Override
        protected void onNext() {
            disposeHelpWidgetTip();
        }

        @Override
        protected PopTheme fetchTheme() {
            return new ActionerPopTheme() {
                @Override
                public String bodybgcolor() {
                    return Themer.SELFHELP.WIDGET_COLOR;
                }

                @Override
                public String titleColor() {
                    return Themer.SELFHELP.WID_HEADER_TEXT_COLOR;
                }
            };
        }
    }

    private class HelpWidgetTipPop extends StepStaticPop {

        public HelpWidgetTipPop(Step step, String footnote, String placement,
                boolean isNextRequired) {
            super(step, footnote, placement, isNextRequired);
        }

        @Override
        public boolean closeable() {
            return false;
        }

        @Override
        public String description_md() {
            return Overlay.CONSTANTS.helpWidgetTipDescription(false);
        }
    }
    
}
