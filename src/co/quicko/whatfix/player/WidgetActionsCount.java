package co.quicko.whatfix.player;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Class to have Count of widget actions. actions like number of times widget is
 * being interacted and attention animation is shown, number of time tip message
 * on the widget is being interacted.
 * 
 * @author mohsin
 *
 */
public class WidgetActionsCount extends JavaScriptObject {
    protected WidgetActionsCount() {

    }

    public final native int widgetAnimationCount() /*-{
        return this.widget_animation_count ? this.widget_animation_count : 0;
    }-*/;

    public final native void widgetAnimationCount(int widget_animation_count) /*-{
        this.widget_animation_count = widget_animation_count;
    }-*/;

    public final native int widgetTipCount() /*-{
        return this.widget_tip_count ? this.widget_tip_count : 0;
    }-*/;

    public final native void widgetTipCount(int widget_tip_count) /*-{
        this.widget_tip_count = widget_tip_count;
    }-*/;

    // geTipCount is for GracefulExit info tip
    public final native int geTipCount() /*-{
        return this.ge_tip_count ? this.ge_tip_count : 0;
    }-*/;

    public final native void geTipCount(int ge_tip_count) /*-{
        this.ge_tip_count = ge_tip_count;
    }-*/;
}
