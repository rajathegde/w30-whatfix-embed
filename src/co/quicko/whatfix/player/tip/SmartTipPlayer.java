package co.quicko.whatfix.player.tip;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.ZMaxer;
import co.quicko.whatfix.common.logger.NFRLogger;
import co.quicko.whatfix.common.logger.NFRProperty;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.SmartTipSegment;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.player.tip.event.EvaluateSmartTipSegment;
import co.quicko.whatfix.player.tip.event.RefreshSmartTipEvent;
import co.quicko.whatfix.player.tip.event.ShowSmartTipEvent;
import co.quicko.whatfix.player.tip.event.SmartTipClosedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipMissedEvent;
import co.quicko.whatfix.player.tip.event.SmartTipShowedEvent;
import co.quicko.whatfix.player.tip.event.StopSmartTipEvent;
import co.quicko.whatfix.player.tip.event.StopStaticSmartTipEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.TipService;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import static co.quicko.whatfix.common.logger.WfxLogFactory.getLogger;

public class SmartTipPlayer {

    private PlayState stateStatic;
    private static JavaScriptObjectExt stepFindData;

    public SmartTipPlayer() {
        EventBus.addHandler(ShowSmartTipEvent.class, this::onShowSmartTipEvent);
        EventBus.addHandler(RefreshSmartTipEvent.class,
                this::onRefreshSmartTips);
        EventBus.addHandler(StopSmartTipEvent.class, this::onStopSmartTips);
        EventBus.addHandler(EvaluateSmartTipSegment.class,
                this::onEvaluateSmartTipSegment);
        EventBus.addHandler(SmartTipShowedEvent.class, this::onStaticShowed);
        EventBus.addHandler(SmartTipMissedEvent.class, this::onStaticMiss);
        EventBus.addHandler(SmartTipClosedEvent.class, this::onStaticClose);
        EventBus.addHandler(StopStaticSmartTipEvent.class,
                this::onStopStaticSmartTips);
    }

    private void onEvaluateSmartTipSegment(Event e) {
        long sTime = System.currentTimeMillis();
        String staticFlowId = natStaticId();
        if (staticFlowId != null && staticFlowId.length() > 0) {
            playSmartTips(staticFlowId);
        }
        SmartTipSegment result = Enterpriser.getSmartTipSettings();
        if (result != null && result.flow_id() != null) {
            playSmartTips(result.flow_id());
        }
        NFRLogger.addLog(NFRProperty.SMART_TIP,
                System.currentTimeMillis() - sTime);
    }
    
    // TODO Remove static code from here.
    public static native String natStaticId() /*-{
        return $wnd._wfx_smart_tips;
    }-*/;

    /***
     * for backward compatibility only. Earlier some smart tips were created as
     * flows
     */
    private void onShowSmartTipEvent(Event e) {
        ShowSmartTipEvent event = (ShowSmartTipEvent) e;
        Draft result = event.getSmartTips();

        if (result != null) {
            startSmartTips(result);
        } else if (StringUtils.isNotBlank(event.getSmartTipCollectionId())) {
            playSmartTips(event.getSmartTipCollectionId());
        } else {
            Console.debug("Can't show SmartTip. Id/Draft not present.");
        }

    }

    private void startSmartTips(Draft result) {
        result.type(ContentType.tip.name());
        stateStatic = PlayerBase.createPlayState(result, "js");
        ZMaxer.init();
        Actioner.showStatic(result);

        Common.tracker().onEvent(EmbedEvent.SMART_TIP_LOADED.builder()
                .flow(result.flow_id(), result.title())
                .widget(TrackerEventOrigin.SMART_TIPS)
                .segment(Enterpriser.getActiveSmartTipSegmentId(result),
                        Enterpriser.getActiveSmartTipSegmentName(result))
                .srcId(TrackerEventOrigin.SMART_TIPS).build());
    }

    private void onRefreshSmartTips(Event e) {
        if (stateStatic != null && stateStatic.flow() != null) {
            startSmartTips(stateStatic.flow());
        }
    }

    private void playSmartTips(final String flowId) {
        String[] tips = flowId.split(",");
        if (tips.length >= 2) {
            // Not clean. For quick support of more than 50 tips
            TipService.IMPL.flows(tips[0], tips[1], new LogFailureCb<Flow>() {

                @Override
                public void onSuccess(Flow result) {
                    startSmartTips(result);
                }
            });
            return;
        }

        TipService.IMPL.flow(flowId, new LogFailureCb<Flow>() {

            @Override
            public void onSuccess(Flow result) {
                startSmartTips(result);
            }
        });
    }

    private void onStopSmartTips(Event e) {
        Actioner.hide(ContentType.tip);
    }

    /*
     * For backward compatibility only. Earlier some smart tips were created as
     * flows (as type)
     */
    private void onStopStaticSmartTips(Event e) {
        StopStaticSmartTipEvent event = (StopStaticSmartTipEvent) e;
        if (stateStatic != null && stateStatic.flow().flow_id() == event
                .getState().flow().flow_id()) {
            onStopSmartTips(e);
        }
    }

    public void onStaticShowed(Event e) {
        SmartTipShowedEvent event = (SmartTipShowedEvent) e;
        Step step = event.getStep();
        getLogger().log(WfxLogLevel.DEBUG, SmartTipPlayer.class,
                "onStaticShowed", WfxModules.JIT, "Step number is: {0}",
                step.step());
        Customizer.onStaticShowed(stateStatic, step.step());
    }

    public void onStaticMiss(Event e) {
        SmartTipMissedEvent event = (SmartTipMissedEvent) e;
        Step step = event.getStep();
        getLogger().log(WfxLogLevel.DEBUG, SmartTipPlayer.class, "onStaticMiss",
                WfxModules.JIT, "Step number is: {0}", step.step());
        Customizer.onStaticMiss(stateStatic, step.step());
    }

    public void onStaticClose(Event e) {
        SmartTipClosedEvent event = (SmartTipClosedEvent) e;
        Step step = event.getStep();
        getLogger().log(WfxLogLevel.DEBUG, SmartTipPlayer.class,
                "onStaticClose", WfxModules.JIT, "Step number is: {0}",
                step.step());
        Customizer.onStaticClose(stateStatic, step.step());
    }

    public static void setStepFindData(JavaScriptObjectExt stepFindData) {
        SmartTipPlayer.stepFindData = stepFindData;
    }

    public static JavaScriptObjectExt getStepFindData() {
        return stepFindData;
    }
}
