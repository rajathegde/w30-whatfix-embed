package co.quicko.whatfix.player.tip.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;

public class ValidationTipRectifiedEvent implements Event {

    private Draft tipCollection;
    private Step step;

    public ValidationTipRectifiedEvent(Step step, Draft draft) {
        this.step = step;
        this.tipCollection = draft;
    }

    public Draft getTipCollection() {
        return tipCollection;
    }

    public Step getStep() {
        return step;
    }

}
