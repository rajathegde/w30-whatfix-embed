package co.quicko.whatfix.player.tip.event;

import com.google.gwt.core.client.JsArray;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Step;

public class SmartTipMissedEvent implements Event {
    private Step step;
    private Draft draft;
    private JsArray<JavaScriptObjectExt> reason;

    public SmartTipMissedEvent(Draft draft, Step step, JsArray<JavaScriptObjectExt> reason) {
        this.draft = draft;
        this.step = step;
        this.reason = reason;
    }

    public Step getStep() {
        return step;
    }

    public Draft getDraft() {
        return draft;
    }
    
    public JsArray<JavaScriptObjectExt> getReason() {
        return this.reason;
    }
}
