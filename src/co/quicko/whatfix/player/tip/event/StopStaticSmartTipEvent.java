package co.quicko.whatfix.player.tip.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.PlayState;

public class StopStaticSmartTipEvent implements Event {

    private PlayState state;

    public StopStaticSmartTipEvent(PlayState state) {
        this.state = state;
    }

    public PlayState getState() {
        return state;
    }

}
