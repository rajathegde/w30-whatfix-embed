package co.quicko.whatfix.player.tip.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.ga.EventBusAdapter;

/**
 * This class captures data for sending smart tip auto complete analytics. Its
 * handler is OnStaticAutoCompleted in {@link EventBusAdapter}
 * 
 * @author prajwal
 *
 */

public class SmartTipAutoCompletedEvent implements Event {

    private Draft draft;
    private Step step;
    private String completionAction;

    public SmartTipAutoCompletedEvent(Draft draft, Step step,
            String completionAction) {
        this.draft = draft;
        this.step = step;
        this.completionAction = completionAction;
    }

    public Step getStep() {
        return step;
    }

    public Draft getDraft() {
        return draft;
    }

    public String getCompletionAction() {
        return completionAction;
    }
}
