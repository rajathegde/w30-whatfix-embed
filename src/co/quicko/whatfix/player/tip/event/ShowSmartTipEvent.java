package co.quicko.whatfix.player.tip.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

public class ShowSmartTipEvent implements Event {

    private Draft smartTips;
    private String unresolvedFlowId;

    private ShowSmartTipEvent(Draft smartTips) {
        this.smartTips = smartTips;
    }

    public static ShowSmartTipEvent fromDraft(Draft smartTips) {
        ShowSmartTipEvent event = new ShowSmartTipEvent(smartTips);
        return event;
    }

    public static ShowSmartTipEvent fromFlowId(String flowId) {
        ShowSmartTipEvent event = new ShowSmartTipEvent(null);
        event.unresolvedFlowId = flowId;
        return event;
    }

    public Draft getSmartTips() {
        return smartTips;
    }

    public String getSmartTipCollectionId() {
        return this.unresolvedFlowId;
    }

}
