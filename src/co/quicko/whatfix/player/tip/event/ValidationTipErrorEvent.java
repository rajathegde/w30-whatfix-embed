package co.quicko.whatfix.player.tip.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;

public class ValidationTipErrorEvent implements Event {

    private Draft tipCollection;
    private Step step;

    public ValidationTipErrorEvent(Step step, Draft draft) {
        this.step = step;
        this.tipCollection = draft;
    }

    public Draft getTipCollection() {
        return tipCollection;
    }

    public Step getStep() {
        return step;
    }

}
