package co.quicko.whatfix.player;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Frame;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.player.widget.NotificationRenderer;

public class MobileHelpWidget extends BaseWidget {

    private NoContentPopup widgetPopup;
    private boolean showingFull = false;

    public MobileHelpWidget(PlayerBase player, Settings settings,
            RePositionCB rePositionCB) {
        super(player, settings, rePositionCB);
    }

    @Override
    protected void createStructure() {
        FlowPanel container = new FlowPanel();
        container.add(label);
        setWidget(container);

        renderer = new NotificationRenderer(container, getElement(), settings, TrackerEventOrigin.SELF_HELP);

        frame.addStyleName(Overlay.CSS.widget());
        frame.addStyleName(Common.CSS.radiusAll());
        frame.getElement().getStyle().setHeight(HEIGHT, Unit.PX);

        widgetPopup = new NoContentPopup();
        widgetPopup.setStyleName(Overlay.CSS.widget());
        widgetPopup.setModal(false);
        widgetPopup.setAnimationEnabled(false);
        widgetPopup.setGlassEnabled(true);
        widgetPopup.setGlassStyleName(Common.CSS.glass());
        widgetPopup.setWidget(frame);

        Common.setId(this, WIDGET_ID);
        Common.setId(widgetPopup, LAUNCHER_ID);
    }

    @Override
    protected Anchor launcher() {
        Anchor selfHelp = super.launcher();
        selfHelp.addClickHandler(this);
        return selfHelp;
    }

    @Override
    public void show() {
        super.show();
        setOverFlow();
        setPosition();
        hideForRelative();
    }

    @Override
    protected void unhideForRelative() {
        // The container for launcher and popup are different.
        // No need to unhide the launcher like in case of desktop where the
        // container is the same
    }

    private double setWidth(Frame frame) {
        double width = Window.getClientWidth();
        width = width * 0.8;
        if (width < 256) {
            width = 256;
        } else if (width > 470) {
            width = 470;
        }
        frame.getElement().getStyle().setWidth(width, Unit.PX);
        return width;
    }

    @Override
    protected void showFull() {
        super.removeAnchorFocusStyle();
        hide();
        setWidth(frame);
        framer.setSrc(frame);
        widgetPopup.center();
        showingFull = true;
    }

    protected void adjustWidget() {
        int width = (int) setWidth(frame);
        int left = (Window.getClientWidth() - width) >> 1;
        int top = (Window.getClientHeight() - height) >> 1;
        widgetPopup.setPopupPosition(Math.max(Window.getScrollLeft() + left, 0),
                Math.max(Window.getScrollTop() + top, 0));
    }

    @Override
    protected void closeFull() {
        showingFull = false;
        trackTime();
        widgetPopup.hide();
        show();
    }

    @Override
    public boolean isShowingFull() {
        return showingFull;
    }

    @Override
    protected void onLoaded() {
        sendAnalayticsCallforContentLoaded(settings);
    }

    @Override
    protected String mode() {
        return WidgetLauncher.MOBILE_MODE;
    }

    @Override
    public void handleResize(ResizeEvent event) {
        if (!isShowing()) {
            return;
        }
        super.handleResize(event);
    }
}
