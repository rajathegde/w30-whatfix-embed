package co.quicko.whatfix.player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.Enterprise;
import co.quicko.whatfix.data.Group;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.SelfHelpSegment;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.ContentManager;

public class NewNotification {
    private static ContentManager manager = GWT.create(ContentManager.class);
    static MyFactory factory = GWT.create(MyFactory.class);

    private static Storage localStorage;

    private static final String SEEN_STATE = "seen_state";

    private static int noOfGroups = 0;

    private static List<String> groupContentList;

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    private static void initIfRequired() {
        if (!isInitialized()) {
            localStorage = Storage.getLocalStorageIfSupported();
        }
    }

    public static boolean isInitialized() {
        return localStorage != null;
    }

    public static void hasUnseenContent(Settings settings,
            AsyncCallback<Set<String>> hasUnseenContent) {
        LOGGER.log(WfxLogLevel.DEBUG, NewNotification.class, "hasUnseenContent",
                WfxModules.NEW_NOTIFICATION,
                "Checking if user has unseen content...");
        String[][] typeValue = settings.mixedTypeValue();
        String[] type = typeValue[0];
        String[] value = typeValue[1];
        JsArrayString order = settings.order();
        Enterprise ent = Enterpriser.enterprise();
        String filterOrderType = null;
        if (ent != null && ent.auto_segment_enabled()
                && ent.show_all_applicable_content()) {
            filterOrderType = "OR_FIRST";
        }
        initializeGroups(settings);
        manager.initialize(settings.ent_id());
        if (settings.no_initial_flows()) {
            onFlowsReceived(settings, new ArrayList<String>(),
                    hasUnseenContent);
        } else {
            manager.topHelp(type, value, order, filterOrderType,
                    new AsyncCallback<JsArray<Content>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            manager.topHelp(type, value, null, null,
                                    new LogFailureCb<JsArray<Content>>() {

                                        @Override
                                        public void onSuccess(
                                                JsArray<Content> topHelpResult) {
                                            List<String> topHelpFlowIds = toFlowIds(
                                                    topHelpResult);
                                            if (settings.isGroupPresent()) {
                                                addGroupContents(settings,
                                                        hasUnseenContent,
                                                        topHelpFlowIds);
                                            } else {
                                                onFlowsReceived(settings,
                                                        topHelpFlowIds,
                                                        hasUnseenContent);
                                            }
                                        }
                                    });
                        }

                        @Override
                        public void onSuccess(JsArray<Content> topHelpResult) {
                            List<String> topHelpFlowIds = toFlowIds(
                                    topHelpResult);
                            if (settings.isGroupPresent()) {
                                addGroupContents(settings, hasUnseenContent,
                                        topHelpFlowIds);
                            } else {
                                onFlowsReceived(settings, topHelpFlowIds,
                                        hasUnseenContent);
                            }
                        }
                    });
        }
    }

    private static void initializeGroups(Settings settings) {
        JsArrayString order = settings.order();
        if (order == null || order.length() == 0) {
            return;
        }
        JavaScriptObjectExt segmentGroups = DataUtil.create().cast();
        for (int index = 0; index < order.length(); index++) {
            String contentId = order.get(index);
            if (Enterpriser.enterprise().groups().hasKey(contentId)) {
                Group segmentGroup = Enterpriser.enterprise().group(contentId);
                segmentGroups.put(contentId, segmentGroup);
            }
        }
        settings.groups(segmentGroups);
        noOfGroups = settings.groups().keys().length;
        LOGGER.log(WfxLogLevel.DEBUG, NewNotification.class, "initializeGroups",
                WfxModules.NEW_NOTIFICATION,
                "{0} groups are present in Self Help", noOfGroups);
    }

    private static void addGroupContents(Settings settings,
            AsyncCallback<Set<String>> hasUnseenContent,
            List<String> topHelpFlowIds) {
        JavaScriptObjectExt groups = settings.groups();
        String[][] typeValue = settings.mixedTypeValue();
        int tagIndex = -1;
        if (typeValue[0] != null) {
            for (int i = 0; i < typeValue[0].length; i++) {
                if ("tag_ids".equals(typeValue[0][i])) {
                    tagIndex = i;
                    break;
                }
            }
        }

        for (String key : groups.keys()) {
            List<String> type = new ArrayList<>();
            List<String> value = new ArrayList<>();
            Group group = (Group) groups.value(key);
            type.add("flow_ids");
            value.add(group.content_ids().join());
            if (tagIndex != -1) {
                type.add("tag_ids");
                value.add(typeValue[1][tagIndex]);
            }
            LOGGER.log(WfxLogLevel.DEBUG, NewNotification.class,
                    "addGroupContents", WfxModules.NEW_NOTIFICATION,
                    "Adding contents of groups: {0}", group.title());
            manager.groupContents(type.toArray(new String[type.size()]),
                    value.toArray(new String[value.size()]),
                    group.content_ids(),
                    groupCallback(settings, topHelpFlowIds, hasUnseenContent));
        }
    }

    private static AsyncCallback<JsArray<Content>> groupCallback(
            Settings settings, List<String> topHelpFlowIds,
            AsyncCallback<Set<String>> hasUnseenContent) {
        return new AsyncCallback<JsArray<Content>>() {

            @Override
            public void onFailure(Throwable caught) {
                Console.error(caught.getMessage());
            }

            @Override
            public void onSuccess(JsArray<Content> result) {
                groupContentList = new ArrayList<>();
                groupContentList.addAll(toFlowIds(result));
                noOfGroups--;
                if (noOfGroups == 0) {
                    topHelpFlowIds.addAll(groupContentList);
                    onFlowsReceived(settings, topHelpFlowIds, hasUnseenContent);
                }
            }
        };
    }

    private static void onFlowsReceived(Settings settings, List<String> flowIds,
            AsyncCallback<Set<String>> hasUnseenContent) {
        initIfRequired();
        String result = localStorage.getItem(SEEN_STATE);
        if (StringUtils.isBlank(result)) {
            SeenState newState = factory.records().as();
            newState.setSeenLongBack(new HashSet<>(flowIds));
            newState.setCurrentlyActive(new HashMap<>());
            putSegmentFlowsInLongSeen(newState);
            AutoBean<SeenState> bean = AutoBeanUtils.getAutoBean(newState);
            localStorage.setItem(SEEN_STATE,
                    AutoBeanCodex.encode(bean).getPayload());
            hasUnseenContent.onSuccess(new HashSet<>());
        } else {
            SeenState seenState = AutoBeanCodex.decode(factory, SeenState.class, result).as();
            Map<String, Long> records = seenState.getCurrentlyActive();
            Set<String> seenLongBack = seenState.getSeenLongBack();

            Iterator<Map.Entry<String, Long>> iterator = records.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, Long> current = iterator.next();
                if (current.getValue() < System.currentTimeMillis()
                        - settings.new_label_time_configuration()) {
                    // already expired, move this to seenLongBack
                    seenLongBack.add(current.getKey());
                    iterator.remove();
                }
            }

            // remove seen long back from records because that's of no use
            // we are not going to mark it new anyway
            flowIds.removeIf(seenLongBack::contains);

            Set<String> actualActiveInSegment = new HashSet<>();
            for (String flow : flowIds) {
                if (!records.containsKey(flow)) {
                    records.put(flow, System.currentTimeMillis());
                }
                actualActiveInSegment.add(flow);
            }

            SeenState newState = factory.records().as();
            newState.setSeenLongBack(seenLongBack);
            newState.setCurrentlyActive(records);
            AutoBean<SeenState> bean = AutoBeanUtils.getAutoBean(newState);
            localStorage.setItem(SEEN_STATE,
                    AutoBeanCodex.encode(bean).getPayload());

            hasUnseenContent.onSuccess(actualActiveInSegment);
        }
    }

    private static void putSegmentFlowsInLongSeen(SeenState newState) {
        // Get the flowIds from enterprise and put in seen long back.
        Enterprise entInstance = Enterprise.enterprise();
        JsArray<SelfHelpSegment> shSegments = entInstance.sh_segments();
        for (int i = 0; i < shSegments.length(); i++) {
            SelfHelpSegment selectedSegment = shSegments.get(i);
            JsArrayString flowIds = selectedSegment.flow_ids();
            if (flowIds != null && flowIds.length() != 0) {
                Set<String> enterpriseFlowIds = new HashSet<>(Arrays.asList(
                        (flowIds.toString()).split(",")));
                newState.getSeenLongBack().addAll(enterpriseFlowIds);
            }
        }
    }

    private static List<String> toFlowIds(JsArray<Content> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < result.length(); i++) {
            list.add(result.get(i).flow_id());
        }
        return list;
    }

    public static void addSeenLongBack(String contentId) {
        initIfRequired();
        String result = localStorage.getItem(SEEN_STATE);
        if (result == null) {
            return;
        }
        SeenState seenState = AutoBeanCodex
                .decode(factory, SeenState.class, result).as();
        Map<String, Long> records = seenState.getCurrentlyActive();
        records.remove(contentId);
        Set<String> seenLongBack = seenState.getSeenLongBack();
        seenLongBack.add(contentId);
        SeenState updatedState = factory.records().as();
        updatedState.setSeenLongBack(seenLongBack);

        updatedState.setCurrentlyActive(records);

        AutoBean<SeenState> bean = AutoBeanUtils.getAutoBean(updatedState);
        localStorage.setItem(SEEN_STATE,
                AutoBeanCodex.encode(bean).getPayload());
    }

    interface SeenState {
        Map<String, Long> getCurrentlyActive();

        void setCurrentlyActive(Map<String, Long> records);

        Set<String> getSeenLongBack();

        void setSeenLongBack(Set<String> seenLongBack);
    }

    interface Response {
        List<Map<String, Object>> getDetail();

        String getResult();

        void setDetail(List<Map<String, Object>> detail);

        void setResult(String value);

    }

    interface MyFactory extends AutoBeanFactory {
        AutoBean<SeenState> records();

        AutoBean<Response> response();
    }


}
