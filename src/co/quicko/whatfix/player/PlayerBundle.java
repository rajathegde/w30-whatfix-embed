package co.quicko.whatfix.player;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource.Import;

public interface PlayerBundle extends ClientBundle {
    @Source("player.css")
    @Import(value = { PlayerCss.class })
    PlayerCss css();
}