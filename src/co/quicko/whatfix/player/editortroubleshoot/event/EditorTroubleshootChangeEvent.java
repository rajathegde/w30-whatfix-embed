package co.quicko.whatfix.player.editortroubleshoot.event;

import co.quicko.whatfix.common.EventBus.Event;

public class EditorTroubleshootChangeEvent implements Event {
    private String position = "";
    private Boolean isMinimized = null;
    private Boolean isClosed = null;
    
    public EditorTroubleshootChangeEvent(String position, Boolean isMinimized, Boolean isClosed) {
        this.position = position;
        this.isMinimized = isMinimized;
        this.isClosed = isClosed;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    
    public Boolean isMinimized() {
        return isMinimized;
    }

    public void isMinimized(boolean isMinimized) {
        this.isMinimized = isMinimized;
    }
    
    public Boolean isClosed() {
        return isClosed;
    }

    public void isClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }
}
