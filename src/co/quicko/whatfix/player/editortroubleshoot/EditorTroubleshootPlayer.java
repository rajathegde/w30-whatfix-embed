package co.quicko.whatfix.player.editortroubleshoot;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EditorTroubleshootoModalData;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.UserPreferences;
import co.quicko.whatfix.player.editortroubleshoot.event.EditorTroubleshootChangeEvent;
import co.quicko.whatfix.service.AdminService;

public class EditorTroubleshootPlayer implements CrossListener {

    private static FixedPopup editorTroubleshootModal = new FixedPopup();
    private Frame frame;
    private PlayState playState;
    private int currentStep;
    private String position = EditorTroubleshootPosition.BOTTOM_RIGHT.getPosition();
    private HandlerRegistration onMouseMove;
    private boolean mouseEventsCleared = true;
    private double left=0, top=0;
    private EditorTroubleshootLauncher launcher;
    Anchor dragAnchor;

    public EditorTroubleshootPlayer() {
        clear();
        CrossMessager.addListener(this, getAllListenerEvents());
        initializeLauncher();        
    }
    
    @Override
    public void onMessage(String type, String content) {
        if (Events.EDITOR_TROUBLESHOOT_FRAME_DATA.getEvent().equals(type)) {
            sendModalData();
        } else if (Events.EDITOR_TROUBLESHOOT_LOADED.getEvent().equals(type)) {
            onLoadActions();
        } else if (Events.INCREASE_FEEDBACK_COUNT.getEvent().equals(type)) {
            increaseFeedbackCount();
        } else if (Events.MODAL_CLOSE.getEvent().equals(type)) {
            takeAction(content);
        }
    }

    private void sendModalData() {
        CrossMessager.sendMessageToFrame(frame.getElement(),
                Events.FRAME_DATA.getEvent(),
                StringUtils.stringifyObject(EditorTroubleshootoModalData
                        .getModalData(playState, currentStep)));
    }

    private void increaseFeedbackCount() {
        int feedbacks = playState.stepPlayStatus().feedback_count() + 1;
        if (StringUtils.isBlank(playState.editor_user_id())) {
            UserPreferences.setEditorTroubleshootKey(playState.editor_user_id(),
                    UserPreferences.EDITOR_TROUBLESHOOT_FEEDBACK_COUNT,
                    Integer.toString(feedbacks));
        } else {
            co.quicko.whatfix.data.UserPreferences preference = DataUtil
                    .create();
            preference.feedback_view_count(Integer.toString(feedbacks));
            AdminService.IMPL.updateEditorTroubleshootPreferences(
                    playState.editor_user_id(), preference,
                    new AsyncCallback<Void>() {

                        @Override
                        public void onSuccess(Void result) {
                            // do nothing
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                            // do nothing
                        }
                    });
        }
    }

    private void onLoadActions() {
        editorTroubleshootModal.setVisible(true);
        launcher.updateFailureCount(getTotalfailures());
        if (null != playState.stepPlayStatus()
                && playState.stepPlayStatus().is_minimized()) {
            editorTroubleshootModal.setVisible(false);
            launcher.setVisible(true);
        }
        if (playState.stepPlayStatus().step_status(currentStep)
                .equals(Events.STEP_FOUND.getEvent())) {
            sendStepFoundEvent(currentStep);
        }
    }

    private void initializeLauncher() {
        launcher = new EditorTroubleshootLauncher() {
            @Override
            public void onClick(ClickEvent event) {
                super.onClick(event);
                editorTroubleshootModal.setVisible(true);
                launcher.setVisible(false);
                EventBus.fire(
                        new EditorTroubleshootChangeEvent("", false, null));
                setModalPosition(editorTroubleshootModal.getElement(), false);
                setModalPosition(launcher.getElement(), true);
            }
        };
        launcher.show();
        launcher.setVisible(false);
        Common.setId(launcher, "editor-troubleshoot-launcher");
    }

    private String[] getAllListenerEvents() {
        return new String[] { Events.MODAL_CLOSE.getEvent(),
                Events.EDITOR_TROUBLESHOOT_FRAME_DATA.getEvent(),
                Events.EDITOR_TROUBLESHOOT_LOADED.getEvent(),
                Events.INCREASE_FEEDBACK_COUNT.getEvent() };
    }

    public void setPlayState(PlayState playState) {
        this.playState = playState;
    }

    public void setCurrentStep(int step) {
        currentStep = step;
    }

    private void clear() {
        editorTroubleshootModal.clear();
        removeListeners();
    }

    public void sendStepStartEvent(int step) {
        if (step <= playState.flow().steps()) {
            sendStepEvent(Events.STEP_START.getEvent(), step);
        }
        setCurrentStep(step);
    }

    public void sendStepFoundEvent(int step) {
        sendStepEvent(Events.STEP_FOUND.getEvent(), step);
    }

    public void sendStepPlayedEvent(int step) {
        sendStepEvent(Events.STEP_PLAYED.getEvent(), step);
    }

    public void sendStepSkippedEvent(int step) {
        sendStepEvent(Events.STEP_SKIPPED.getEvent(), step);
    }

    public void sendStepFailureEvent(int step, String reason) {
        CrossMessager.sendMessageToFrame(frame.getElement(),
                Events.STEP_FAILED.getEvent(),
                Integer.toString(step) + "_" + reason);
        launcher.updateFailureCount(getTotalfailures());
    }

    protected void sendStepEvent(String event, int step) {
        CrossMessager.sendMessageToFrame(frame.getElement(), event,
                Integer.toString(step));
    }

    private int getTotalfailures() {
        int totalSteps = playState.flow().steps();
        int failCount = 0;
        if(null == playState.stepPlayStatus()) {
            return failCount;
        }
        for (int step = 1; step <= totalSteps; step++) {
            if (playState.stepPlayStatus().step_status(step)
                    .equals(Events.STEP_FAILED.getEvent())) {
                failCount++;
            }
        }
        return failCount;
    }

    public void closeEditorTroubleshoot() {
        if (editorTroubleshootModal != null) {
            editorTroubleshootModal.hide();
        }
    }

    private void removeListeners() {
        CrossMessager.removeListener(this, getAllListenerEvents());
    }

    public void makeETModal() {
        editorTroubleshootModal
                .setStyleName(Overlay.CSS.editorTroubleshootModal());
        
        editorTroubleshootModal.setWidget(getDefaultFramePanel());
        editorTroubleshootModal.show();
        launcher.setVisible(false);
        if (null != playState.stepPlayStatus() && StringUtils
                .isNotBlank(playState.stepPlayStatus().position())) {
            position = playState.stepPlayStatus().position();
        }
        makeMovable();
        setModalPosition(editorTroubleshootModal.getElement(), false);
        setModalPosition(launcher.getElement(), true);
    }

    private Widget getDefaultFramePanel() {
        FlowPanel framePanel = new FlowPanel();
        Frame defaultFrame = getDefaultFrame();
        framePanel.add(getDragIconOverlay());
        framePanel.add(defaultFrame);
        return framePanel;
    }

    private Frame getDefaultFrame() {
        if (null != frame) {
            return frame;
        }
        frame = Framers.editorTroubleshoot().buildFrame();
        frame.addStyleName(Overlay.CSS.editorTroubleshootFrame());
        return frame;
    }

    private Widget getDragIconOverlay() {
        FlowPanel iconPanel = new FlowPanel();
        iconPanel.add(getDragIcon());
        iconPanel.addStyleName(Overlay.CSS.editorTroubleshootDragIconPanel());
        return iconPanel;
    }

    private Anchor getDragIcon() {
        dragAnchor = Common.anchor(null);
        dragAnchor.setTitle("Drag");
        Element iconElement = dragAnchor.getElement();
        iconElement.setInnerHTML(SVGElements.DRAG_ICON_EDITOR_TROBLESHOOT);
        iconElement.setAttribute(AriaProperty.HIDDEN, String.valueOf(true));
        return dragAnchor;
    }

    private void makeMovable() {
        editorTroubleshootModal.addDomHandler((MouseDownEvent event) -> {
            event.preventDefault();
            if (mouseEventsCleared) {
                frame.addStyleName(Overlay.CSS.removePointerEvents());
                dragAnchor.addStyleName(Overlay.CSS.editorTroubleshootMoveCursor());
                addMouseMoveEvent();
                mouseEventsCleared = false;
            }
        }, MouseDownEvent.getType());
    }
    
    private void addMouseMoveEvent() {
        onMouseMove = Event.addNativePreviewHandler((event) -> {

            final int eventType = event.getTypeInt();
            switch (eventType) {
                case Event.ONMOUSEMOVE:
                    // corner case for allowed x-y positions
                    int x = event.getNativeEvent().getClientX();
                    int y = event.getNativeEvent().getClientY();
                    if (x > 10 && x < Window.getClientWidth() - 350 && y > 20
                            && y < Window.getClientHeight() - 400) {
                        left = x - 10.0;
                        top = y - 20.0;
                        editorTroubleshootModal.getElement().getStyle()
                                .setLeft(left, Unit.PX);
                        editorTroubleshootModal.getElement().getStyle()
                                .setTop(top, Unit.PX);
                        editorTroubleshootModal.getElement().getStyle()
                                .setProperty("bottom", "unset");
                        editorTroubleshootModal.getElement().getStyle()
                                .setProperty("right", "unset");
                    }
                    break;
                case Event.ONMOUSEUP:
                    removeMouseMoveEvent();
                    updatePosition("" + left + "_" + top);
                    dragAnchor.removeStyleName(Overlay.CSS.editorTroubleshootMoveCursor());
                    break;
                default:
                    // not interested in other events
            }
        });
    }
    
    private void removeMouseMoveEvent() {
        onMouseMove.removeHandler();
        mouseEventsCleared = true;
        frame.removeStyleName(Overlay.CSS.removePointerEvents());
    }

    private void updatePosition(String position) {
        this.position = position;
        EventBus.fire(new EditorTroubleshootChangeEvent(position, null, null));
    }

    private void setModalPosition(Element popupElement, boolean isDefault) {
        Style style = popupElement.getStyle();
        OverlayUtil.clearPosition(style);
        if (isDefault || StringUtils.isBlank(position) || position.equalsIgnoreCase(
                EditorTroubleshootPosition.BOTTOM_RIGHT.getPosition())) {
            OverlayUtil.setRight(style, 12);
            OverlayUtil.setBottom(style, 12);
        } else {
            String[] xAndY = position.split("_");
            OverlayUtil.setLeft(style, Integer.parseInt(xAndY[0]));
            OverlayUtil.setTop(style, Integer.parseInt(xAndY[1]));
        }
    }

    private void takeAction(String content) {
        try {
            Events event = Events.valueOf(content.toUpperCase());
            switch (event) {
                case MINIMIZE:
                    editorTroubleshootModal.setVisible(false);
                    launcher.show();
                    launcher.setVisible(true);
                    launcher.updateFailureCount(getTotalfailures());
                    setModalPosition(launcher.getElement(), true);
                    EventBus.fire(
                            new EditorTroubleshootChangeEvent("", true, null));
                    break;
                case CLOSE:
                    closeEditorTroubleshoot();
                    EventBus.fire(
                            new EditorTroubleshootChangeEvent("", null, true));
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            Console.debug("Tracking Editor Troubleshoot Modal Failed: "
                    + e.getCause());
        }
    }

    public enum Events {
        MODAL_CLOSE("editor_troubleshoot_close"),
        FRAME_DATA("frame_data"),
        EDITOR_TROUBLESHOOT_FRAME_DATA("editor_troubleshoot_frame_data"),
        CLOSE("close"),
        MINIMIZE("minimize"),
        EDITOR_TROUBLESHOOT_LOADED("editor_troubleshoot_loaded"),
        STEP_START("Searching"),
        STEP_FOUND("Displayed"),
        STEP_PLAYED("Completed"),
        STEP_FAILED("Failed"),
        STEP_SKIPPED("Skipped"),
        SPLASH_SCREEN_COUNT("splash_screen_count"),
        INCREASE_FEEDBACK_COUNT("increase_feedback_count");

        private String event;

        Events(String event) {
            this.event = event;
        }

        public String getEvent() {
            return event;
        }
    }
}
