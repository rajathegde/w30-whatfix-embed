package co.quicko.whatfix.player.editortroubleshoot;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;

public class EditorTroubleshootLauncher extends FixedPopup
        implements ClickHandler {
    protected InlineLabel notificationCircle;
    protected Anchor etlauncher;
    protected boolean isModalOpen = false;

    public EditorTroubleshootLauncher() {
        add(getLauncherContainer());
        setStyleName(Overlay.CSS.editorTroubleshootLauncherContainer());
    }

    private FlowPanel getLauncherContainer() {
        FlowPanel launcherContainer = new FlowPanel();
        HorizontalPanel panel = new HorizontalPanel();
        panel.setStyleName(Overlay.CSS.taskerTable());
        Widget img = Common.html(SVGElements.LAUNCHER_ICON_EDITOR_TROUBLESHOOT,
                Overlay.CSS.editorTroubleshootImg());
        img.getElement().setAttribute(AriaProperty.HIDDEN,
                String.valueOf(true));
        Image launcherImage = Image.wrap(img.getElement());
        etlauncher = Common.imagePredAnchor(launcherImage);
        etlauncher.addClickHandler(this);
        etlauncher.setStyleName(Overlay.CSS.editorTroubleshootLauncher());

        etlauncher.addFocusHandler((event) -> {
            etlauncher.getElement().focus();
        });
        notificationCircle = new InlineLabel("0");
        notificationCircle.addClickHandler(this);
        notificationCircle.setStyleName(Overlay.CSS.taskerNotification());
        String launcherBGColor = "#00BCD4";
        String notificationBGColor = "#FF564E";
        String fontColor = "#FFFFFF";
        OverlayUtil.setStyle(notificationCircle,
                new String[] { OverlayUtil.BORDER_COLOR, OverlayUtil.BACKGROUND,
                        OverlayUtil.COLOR },
                new String[] { notificationBGColor, notificationBGColor,
                        fontColor });
        OverlayUtil.setStyle(etlauncher, OverlayUtil.BACKGROUND,
                launcherBGColor);
        panel.add(etlauncher);
        panel.add(notificationCircle);
        launcherContainer.add(panel);

        return launcherContainer;
    }

    protected void updateFailureCount(int count) {
        notificationCircle.setText("" + count);
    }

    @Override
    public void onClick(ClickEvent event) {
        hide();
        isModalOpen = true;
    }
}
