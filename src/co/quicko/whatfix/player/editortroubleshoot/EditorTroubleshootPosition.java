package co.quicko.whatfix.player.editortroubleshoot;

public enum EditorTroubleshootPosition {

    BOTTOM_RIGHT("br"), BOTTOM_LEFT("bl");

    private String position;

    EditorTroubleshootPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

}