package co.quicko.whatfix.player.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.player.PlayerBase;

/**
 * Event triggered when a new flow is played. This event has all the information
 * required for {@link PlayerBase} to start the flow.
 * 
 * Note that this stops any currently playing flow and starts a new flow.
 * Termination events might be fired for the currently playing flow (if any).
 * Check {@link PlayerBase}
 * 
 * @see {@link BranchToFlowEvent} to branch to a new flow
 * @see {@link ResumeFlowPlayEvent} to continue playing a flow from
 *      state/opener/etc
 * 
 * @author Suresh Sarda (suresh@whatfix.com)
 *
 */
public class StartNewFlowEvent implements Event {

    private final Draft draft;
    private final String src;
    private final String launchedFrom;
    private final boolean injectPlayer;

    /**
     * Creates a new StartNewFlowEvent. Recommended to avoid using in favor of
     * {@link Builder}
     * 
     * @param draft
     *            flow/draft object
     * @param src
     *            src string of the widget triggering the flow. See
     *            {@link TrackerEventOrigin}
     * @param launchedFrom
     *            string representing the widget from where the flow is launched
     *            {@link TrackerEventOrigin}
     */
    public StartNewFlowEvent(Draft draft, String src, String launchedFrom) {
        this(draft, src, launchedFrom, true);
    }

    // do not expose
    private StartNewFlowEvent(Draft draft, String src, String launchedFrom,
            boolean injectPlayer) {
        this.draft = draft;
        this.src = src;
        this.launchedFrom = launchedFrom;
        this.injectPlayer = injectPlayer;
    }

    public Draft getDraft() {
        return draft;
    }

    public String getSrc() {
        return src;
    }

    public String getLaunchedFrom() {
        return launchedFrom;
    }

    public String shouldInjectPlayer() {
        return String.valueOf(injectPlayer);
    }

    public static class Builder {
        private Draft draft;
        private String src;
        private String launchedFrom;
        private boolean injectPlayer = true;

        public Builder(Draft draft, String src, String launchedFrom) {
            this.draft = draft;
            this.src = src;
            this.launchedFrom = launchedFrom;
        }

        public Builder injectPlayer() {
            this.injectPlayer = true;
            return this;
        }

        public Builder doNotInjectPlayer() {
            this.injectPlayer = false;
            return this;
        }

        public StartNewFlowEvent build() {
            return new StartNewFlowEvent(draft, src, launchedFrom,
                    injectPlayer);
        }
    }

}
