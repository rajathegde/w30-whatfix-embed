package co.quicko.whatfix.player.event;

import co.quicko.whatfix.common.EventBus.Event;

public class FlowCompletedEvent implements Event {

    private boolean didComplete;

    private FlowCompletedEvent(boolean didComplete) {
        this.didComplete = didComplete;
    }

    public static FlowCompletedEvent whenCompleted() {
        return new FlowCompletedEvent(true);
    }

    public static FlowCompletedEvent asClosedAbruptly() {
        return new FlowCompletedEvent(false);
    }

    public boolean didComplete() {
        return didComplete;
    }
}
