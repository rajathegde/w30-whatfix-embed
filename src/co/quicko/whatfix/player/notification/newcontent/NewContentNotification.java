package co.quicko.whatfix.player.notification.newcontent;

import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.player.notification.BadgeConfig;
import co.quicko.whatfix.player.notification.BannerConfig;
import co.quicko.whatfix.player.notification.Notification;
import co.quicko.whatfix.player.notification.NotificationsTopic;

public class NewContentNotification implements Notification {

    @Override
    public NotificationsTopic getTopic() {
        return NotificationsTopic.NEW_CONTENT;
    }

    @Override
    public BannerConfig getBannerConfig() {
        return null;
    }

    @Override
    public BadgeConfig getBadgeConfig() {
        return new BadgeConfig(SVGElements.NOTIFICATION_CONTENT, "green", "red", "blue", "Content updated");
    }

    @Override
    public String getNotificationId() {
        return null;
    }
}
