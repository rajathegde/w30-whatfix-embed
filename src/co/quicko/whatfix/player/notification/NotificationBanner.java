package co.quicko.whatfix.player.notification;

import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;

public class NotificationBanner extends Composite {

    interface NotificationPanelUiBinder extends UiBinder<FocusPanel, NotificationBanner> {
    }

    private static NotificationPanelUiBinder ourUiBinder = GWT.create(NotificationPanelUiBinder.class);

    @UiField
    FocusPanel container;

    @UiField(provided = true)
    String header;

    @UiField(provided = true)
    String description;


    @UiField(provided = true)
    String callToAction;

    @UiField()
    Label icon;

    @UiField()
    Label cta;

    @UiField()
    Label close;

    public NotificationBanner(String header, String description, String cta, String iconSvg) {
        this.header = header;
        this.description = description;
        this.callToAction = StringUtils.isBlank(cta) ? "See Notification" : cta;

        initWidget(ourUiBinder.createAndBindUi(this));
        this.icon.getElement().setInnerHTML(iconSvg);
        this.close.getElement().setInnerHTML(SVGElements.CLOSE);

        this.container.addClickHandler(DomEvent::stopPropagation);
    }

    public void addCtaClickHandler(ClickHandler handler) {
        this.cta.addClickHandler(handler);
    }

    public void addCloseClickHandler(ClickHandler handler) {
        this.close.addClickHandler(handler);
    }

}
