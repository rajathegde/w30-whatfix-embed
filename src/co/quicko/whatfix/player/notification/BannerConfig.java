package co.quicko.whatfix.player.notification;

import co.quicko.whatfix.data.JavaScriptObjectExt;
import com.google.gwt.core.client.JavaScriptObject;

public class BannerConfig {

    private String icon;
    private String header;
    private String description;
    private String cta;

    private BannerConfig() {

    }

    public BannerConfig(String icon, String header, String description, String cta) {
        this.icon = icon;
        this.header = header;
        this.description = description;
        this.cta = cta;
    }

    public static BannerConfig from(JavaScriptObject obj) {
        if (obj == null) {
            return null;
        }
        JavaScriptObjectExt banner = (JavaScriptObjectExt) obj;

        BannerConfig config = new BannerConfig();

        config.icon = banner.valueAsString("icon");
        config.header = banner.valueAsString("header");
        config.description = banner.valueAsString("description");
        config.cta = banner.valueAsString("cta");

        return config;
    }

    public String getIcon() {
        return icon;
    }

    public String getHeader() {
        return header;
    }

    public String getDescription() {
        return description;
    }

    public String getCta() {
        return cta;
    }
}
