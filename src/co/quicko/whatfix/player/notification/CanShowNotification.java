package co.quicko.whatfix.player.notification;

public interface CanShowNotification {

    void showBannerNotification(Notification notification);

    void hideBannerNotification(NotificationsTopic topic, String closeBy);

    void hideBannerNotification(NotificationsTopic topic);
}
