package co.quicko.whatfix.player.notification;

import co.quicko.whatfix.data.JavaScriptObjectExt;
import com.google.gwt.core.client.JavaScriptObject;

public class BadgeConfig {

    private String badgeIcon;
    private String backgroundColor;
    private String color;
    private String highlightColor;
    private String tooltip;

    public BadgeConfig(String badgeIcon, String backgroundColor, String color, String highlightColor, String tooltip) {
        this.badgeIcon = badgeIcon;
        this.backgroundColor = backgroundColor;
        this.color = color;
        this.highlightColor = highlightColor;
        this.tooltip = tooltip;
    }

    private BadgeConfig() {

    }


    public static BadgeConfig from(JavaScriptObject obj) {
        if (obj == null) {
            return null;
        }
        JavaScriptObjectExt jso = (JavaScriptObjectExt) obj;

        BadgeConfig config = new BadgeConfig();
        config.badgeIcon = jso.valueAsString("icon");
        config.color = jso.valueAsString("color");
        config.backgroundColor = jso.valueAsString("backgroundColor");
        config.highlightColor = jso.valueAsString("highlightColor");
        config.tooltip = jso.valueAsString("tooltip");

        return config;
    }

    public String getBadgeIcon() {
        return badgeIcon;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public String getColor() {
        return color;
    }

    public String getHighlightColor() {
        return highlightColor;
    }

    public String getTooltip() {
        return tooltip;
    }
}
