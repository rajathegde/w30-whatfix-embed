package co.quicko.whatfix.player.notification;

public enum NotificationsTopic {
    NEW_CONTENT("new_content", 100),
    NEW_CONTENT_BADGE("new_content_badge", 90),
    ASK_ME_ANYTHING("ask_me_anything", 50),
    CHAT_BOT("chat_bot", 25);

    /**
     * The nice value like of a process. Higher value is less priority and lower value is high priority.
     * A low priority will preempt a higher value notification
     */
    private final int priority;
    private final String topic;

    NotificationsTopic(String topic, int priority) {
        this.topic = topic;
        this.priority = priority;
    }

    @Override
    public String toString() {
        return this.topic;
    }

    public int getPriority() {
        return priority;
    }

    public String getTopic() {
        return this.topic;
    }

}
