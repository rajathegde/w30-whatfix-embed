package co.quicko.whatfix.player.notification;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.player.notification.event.HideNotificationEvent;
import co.quicko.whatfix.player.notification.event.ShowNotificationEvent;
import com.google.gwt.core.client.Callback;

public class SelfHelpNotification {

    private static SocketConnectionManager connection;

    public static void enableNotifications() {
        connection = new SocketConnectionManager(null, null, new Callback<String, String>() {

            @Override
            public void onFailure(String s) {
                Console.log("Failed to establish connection");
            }

            @Override
            public void onSuccess(String message) {
                JavaScriptObjectExt object = DataUtil.create(message);
                showNotification(object);
            }
        });
    }

    public static void showNotification(JavaScriptObjectExt jso) {
        if ("chat".equals(jso.valueAsString("topic"))) {
            String header = jso.valueAsString("header");
            String description = jso.valueAsString("description");

            // create and send a chat notification
            Notification chat = new Notification() {
                @Override
                public NotificationsTopic getTopic() {
                    return NotificationsTopic.CHAT_BOT;
                }

                @Override
                public BannerConfig getBannerConfig() {
                    return new BannerConfig(SVGElements.CHAT_BADGE, header, description, "See messages");
                }

                @Override
                public BadgeConfig getBadgeConfig() {
                    return new BadgeConfig(SVGElements.CHAT_BADGE, null, null, null, "Click here to see the message");
                }

                @Override
                public String getNotificationId() {
                    return jso.valueAsString("id");
                }
            };
            EventBus.fire(new ShowNotificationEvent(chat));

        }

    }

    public static void showNotificationAdapter(JavaScriptObjectExt jso) {
        NotificationsTopic topic = NotificationsTopic.valueOf(jso.valueAsString("topic").toUpperCase());

        JavaScriptObjectExt banner = (JavaScriptObjectExt) jso.value("banner");
        if (banner != null) {
            banner.put("icon", iconFinder(banner.valueAsString("icon")));
        }


        JavaScriptObjectExt badge = (JavaScriptObjectExt) jso.value("badge");
        if (badge != null) {
            badge.put("icon", iconFinder(badge.valueAsString("icon")));
        }


        EventBus.fire(new ShowNotificationEvent(new Notification() {

            @Override
            public NotificationsTopic getTopic() {
                return topic;
            }

            @Override
            public BadgeConfig getBadgeConfig() {
                return BadgeConfig.from(badge);
            }

            @Override
            public String getNotificationId() {
                return jso.valueAsString("id");
            }

            @Override
            public BannerConfig getBannerConfig() {
                return BannerConfig.from(banner);
            }
        }));

    }

    public static void hideNotification(String topic) {
        EventBus.fire(new HideNotificationEvent(NotificationsTopic.valueOf(topic.toUpperCase())));
    }

    private static String iconFinder(String name) {
        switch (name) {
            case "chat":
                return SVGElements.CHAT_BADGE;
            case "check":
                return SVGElements.CHECK;
            case "text":
                return SVGElements.TEXT_ICON;
            default:
                return SVGElements.UPDATE_CIRCLE;
        }
    }
}
