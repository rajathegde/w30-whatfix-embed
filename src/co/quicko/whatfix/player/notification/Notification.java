package co.quicko.whatfix.player.notification;

public interface Notification {

    NotificationsTopic getTopic();

    BannerConfig getBannerConfig();

    BadgeConfig getBadgeConfig();

    String getNotificationId();
}


