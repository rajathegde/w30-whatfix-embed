package co.quicko.whatfix.player.notification;


import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.security.WFLocalStorage;
import co.quicko.whatfix.service.RootFinder;
import co.quicko.whatfix.service.ServiceUtil;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;

import java.util.Date;

public class SocketConnectionManager extends Timer {

    private enum ConnectionState {
        LEADER, SLAVE, WAITING
    }

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    private ConnectionState state = ConnectionState.WAITING;

    private static final String HEARBEAT = "heartbeat";
    private static final String ON_MESSAGE = "socket_message";
    private final int ACCEPTABLE_HEARTBEAT_DELTA = 3 * 1000; // 30 seconds
    private final Callback<String, String> callback;
    private final SocketConnectionManager INSTANCE;

    public SocketConnectionManager(String url, String topic, Callback<String, String> cb) {
        INSTANCE = this;
        callback = cb;
        log("Created a new connection manager.");
        check(1);
    }

    private void check(int retries) {
        String lastBeat = WFLocalStorage.get(HEARBEAT);
        long current = new Date().getTime();

        if (StringUtils.isNotBlank(lastBeat)
                && Math.abs(Long.parseLong(lastBeat) - current) < ACCEPTABLE_HEARTBEAT_DELTA) {
//            log("Heartbeat is in acceptable delay. Has a leader.");
            if (state == ConnectionState.WAITING) {
                addStorageListener();
            }
        } else {
            if (retries <= 0) {
                log("No pulse. No pending retires");
                becomeLeader();
            } else {
                int i = Random.nextInt(ACCEPTABLE_HEARTBEAT_DELTA);
                log("No pulse. Retrying after: " + i);

                state = ConnectionState.WAITING;
                this.schedule(i);
            }
        }
    }

    private void becomeLeader() {
        state = ConnectionState.LEADER;
        WFLocalStorage.set(HEARBEAT, String.valueOf(new Date().getTime()));
        this.scheduleRepeating((int) (ACCEPTABLE_HEARTBEAT_DELTA * 0.75));
        log("I am the leader. Creating socket connection...");
        enableNotifications();
    }

    private native void actualAddListener(SocketConnectionManager obj) /*-{
        $wnd.addEventListener('storage', function (ev) {
            obj.@co.quicko.whatfix.player.notification.SocketConnectionManager::onStorageMessage(*)(ev.key, ev.newValue);
        })

    }-*/;

    private void onStorageMessage(String key, String value) {
        log("Message Received. Key: {0}, Value: {1}", key, value);
        if (ON_MESSAGE.equalsIgnoreCase(key)) {
            callback.onSuccess(value);
        } else if (HEARBEAT.equalsIgnoreCase(key) && state == ConnectionState.WAITING) {
            this.cancel();
            addStorageListener();
        } else {
            // don't do anything
        }
    }

    private void addStorageListener() {
        log("Adding storage listener");

        state = ConnectionState.SLAVE;
        this.scheduleRepeating((int) (ACCEPTABLE_HEARTBEAT_DELTA * 0.75));

        actualAddListener(this);

    }

    private void enableNotifications() {
        ScriptInjector.fromUrl(RootFinder.getRoot() + "lib/sockets/sockjs.min.js").setCallback(new Callback<Void,
                Exception>() {
            @Override
            public void onFailure(Exception e) {
                Console.error(e.getMessage());
            }

            @Override
            public void onSuccess(Void unused) {
                ScriptInjector.fromUrl(RootFinder.getRoot() + "lib/sockets/stomp.min.js").setCallback(new Callback<Void,
                        Exception>() {
                    @Override
                    public void onFailure(Exception e) {
                        Console.error(e.getMessage());
                    }

                    @Override
                    public void onSuccess(Void unused) {
                        if (StringUtils.isNotBlank(AppUser.getUser())) {
                            String path = "/topic/" + AppUser.getUser() + "/updates";
                            Console.log("Subscribing to path: " + path);
                            nativeSubscribe(ServiceUtil.SERVICE_HOST, path, INSTANCE);
                        } else {
                            Console.log("User is null or blank");
                        }
                    }
                }).inject();
            }
        }).inject();
    }

    private native void nativeSubscribe(String host, String channel, SocketConnectionManager obj) /*-{
        var socket = new SockJS(host + 'whatfix-websocket');
        var stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe(channel, function (greeting) {
                obj.@co.quicko.whatfix.player.notification.SocketConnectionManager::onMessage(*)(greeting.body);
            });
        });
    }-*/;

    private void onMessage(String message) {
        WFLocalStorage.set(ON_MESSAGE, message);
        callback.onSuccess(message);
    }


    @Override
    public void run() {
        switch (state) {
            case SLAVE:
                check(1);
                break;
            case LEADER:
                WFLocalStorage.set(HEARBEAT, String.valueOf(new Date().getTime()));
                break;
            case WAITING:
                log("Wait time over.");
                check(0);
                break;
            default:
                break;
        }
    }


    private static void log(String message, Object... args) {
        LOGGER.log(WfxLogLevel.DEBUG, SocketConnectionManager.class, "connection",
                WfxModules.WS_CONNECTION, message, args);
    }

}
