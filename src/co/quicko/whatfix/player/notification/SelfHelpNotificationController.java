package co.quicko.whatfix.player.notification;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.player.notification.event.CloseBannerNotification;
import co.quicko.whatfix.player.notification.event.HideNotificationEvent;
import co.quicko.whatfix.player.notification.event.ShowNotificationEvent;
import co.quicko.whatfix.player.widget.NotificationRenderer;
import co.quicko.whatfix.player.widget.WidgetExtension;
import co.quicko.whatfix.player.widget.event.lifecycle.BeforeSelfHelpOpenEvent;
import co.quicko.whatfix.player.widget.event.lifecycle.SelfHelpClosedEvent;
import co.quicko.whatfix.player.widget.event.lifecycle.SelfHelpOpenEvent;
import co.quicko.whatfix.player.widget.event.lifecycle.SelfHelpWidgetCreatedEvent;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.event.Event;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.user.client.Timer;

import java.util.HashMap;
import java.util.Map;

public class SelfHelpNotificationController {
    
    private static final WfxLogger LOG = WfxLogFactory.getLogger();

    /**
     * The current banner that is present on screen
     */
    private Notification currentlyShowing;

    /**
     * Current badges present on the screen. One of them is also a banner
     */
    private Map<NotificationsTopic, Notification> notificationsByTopic = new HashMap<>();

    private Timer timer;
    private boolean isSelfHelpOpen = false;

    public SelfHelpNotificationController() {
        EventBus.addHandler(ShowNotificationEvent.class, this::onShowNotification);
        EventBus.addHandler(HideNotificationEvent.class, this::onHideNotification);
        EventBus.addHandler(CloseBannerNotification.class, this::onCloseNotification);
        EventBus.addHandler(BeforeSelfHelpOpenEvent.class, this::onBeforeSelfHelpOpen);
        EventBus.addHandler(SelfHelpClosedEvent.class, this::onAfterSelfHelpClose);
        EventBus.addHandler(SelfHelpWidgetCreatedEvent.class, this::onAfterSelfHelpClose);

        timer = new Timer() {
            @Override
            public void run() {
                EventBus.fire(new CloseBannerNotification("time_out"));
            }
        };
    }

    private void onCloseNotification(EventBus.Event event) {
        PlayerBase.launcher.getWidget().hideBannerNotification(null, ((CloseBannerNotification) event).getCloseBy());
        currentlyShowing = null;
    }

    private void onBeforeSelfHelpOpen(EventBus.Event event) {
        this.isSelfHelpOpen = true;
        PlayerBase.launcher.getWidget().hideBannerNotification(null);
        notificationsByTopic.keySet().forEach(it -> PlayerBase.launcher.getWidget().removeWidgetExtension(it.toString()));
    }

    private void onAfterSelfHelpClose(EventBus.Event event) {
        this.isSelfHelpOpen = false;
        notificationsByTopic.forEach((topic, notification) -> {
            if (notification.getBadgeConfig() != null) {
                Console.debug("Adding Self Help extension");
                PlayerBase.launcher.getWidget().removeWidgetExtension(topic.toString());
                addWidgetExtension(notification);
            }
        });
    }

    private void onHideNotification(EventBus.Event event) {
        HideNotificationEvent e = (HideNotificationEvent) event;
        NotificationsTopic topic = e.getNotification();

        if (currentlyShowing != null && currentlyShowing.getTopic().equals(topic)) {
            // hide the banner
            PlayerBase.launcher.getWidget().hideBannerNotification(topic);
            currentlyShowing = null;
        }

        if (notificationsByTopic.containsKey(topic)) {
            Console.debug("Attempting to remove the badge");
            PlayerBase.launcher.getWidget().removeWidgetExtension(topic.toString());
            notificationsByTopic.remove(topic);
        } else {
            Console.debug("The notification doesn't have any badge present.");
        }
    }

    private void onShowNotification(EventBus.Event event) {
        Console.debug("A new notification has arrived.");
        ShowNotificationEvent e = (ShowNotificationEvent) event;
        Notification notification = e.getNotification();
        LOG.log(WfxLogLevel.DEBUG, SelfHelpNotificationController.class,
                "onShowNotification", WfxModules.NEW_NOTIFICATION,
                "Checking if banner should be shown");
        if (notification.getBannerConfig() != null && !isSelfHelpOpen) {
            Console.debug("Notification has a banner.");
            if (currentlyShowing == null ||
                    notification.getTopic().getPriority() <= currentlyShowing.getTopic().getPriority()) {
                Console.debug("Updating the banner.");
                PlayerBase.launcher.getWidget().hideBannerNotification(notification.getTopic(), "notification_updated");

                currentlyShowing = notification;
                notificationsByTopic.put(notification.getTopic(), notification);
                PlayerBase.launcher.getWidget().showBannerNotification(notification);

                if (timer.isRunning()) {
                    timer.cancel();
                }
                timer.schedule(5000);

            } else {
                Console.debug("A higher priority notification is already there.");
            }
        }

        Console.debug("Checking if badge should be shown");
        if (notification.getBadgeConfig() != null) {
            Console.debug("Adding Self Help extension");
            notificationsByTopic.put(notification.getTopic(), notification);
            if (!isSelfHelpOpen) {
                PlayerBase.launcher.getWidget().removeWidgetExtension(notification.getTopic().toString());
                addWidgetExtension(notification);
            }
        }
    }

    private void addWidgetExtension(Notification notification) {
        BadgeConfig config = notification.getBadgeConfig();
        WidgetExtension extension = new WidgetExtension(
                notification.getTopic().toString(),
                config.getBackgroundColor(),
                config.getBadgeIcon(),
                config.getTooltip()
        );

        extension.addClickHandler(event -> {
            event.preventDefault();
            event.stopPropagation();
            EventBus.fire(new HideNotificationEvent(notification.getTopic()));
            EventBus.fire(new SelfHelpOpenEvent(event, "chat_bot"));
            Common.tracker().onEvent(EmbedEvent.BADGE_CLICKED.builder()
                    .srcId(TrackerEventOrigin.SELF_HELP.getSrcName())
                    .contextualInfo(NotificationRenderer.getContextualInfo(notification))
                    .build());
        });

        extension.addDomHandler(mouseOverEvent -> {
            Common.tracker().onEvent(EmbedEvent.BADGE_MOUSEOVER.builder()
                    .srcId(TrackerEventOrigin.SELF_HELP.getSrcName())
                    .contextualInfo(NotificationRenderer.getContextualInfo(notification))
                    .build());
        }, MouseOverEvent.getType());

        PlayerBase.launcher.getWidget().addWidgetExtension(extension);
    }

}
