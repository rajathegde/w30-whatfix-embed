package co.quicko.whatfix.player.notification.event;

import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.player.notification.Notification;

public class ShowNotificationEvent implements EventBus.Event {

    private Notification notification;

    public ShowNotificationEvent(Notification notification) {
        this.notification = notification;
    }

    public Notification getNotification() {
        return notification;
    }
}
