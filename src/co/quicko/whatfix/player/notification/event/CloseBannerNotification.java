package co.quicko.whatfix.player.notification.event;

import co.quicko.whatfix.common.EventBus;

public class CloseBannerNotification implements EventBus.Event {
    private String closeBy;

    public String getCloseBy() {
        return closeBy;
    }

    public void setCloseBy(String closeBy) {
        this.closeBy = closeBy;
    }

    public CloseBannerNotification(String closeBy) {
        this.closeBy = closeBy;
    }
}
