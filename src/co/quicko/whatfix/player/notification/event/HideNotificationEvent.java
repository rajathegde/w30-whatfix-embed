package co.quicko.whatfix.player.notification.event;

import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.player.notification.Notification;
import co.quicko.whatfix.player.notification.NotificationsTopic;

public class HideNotificationEvent implements EventBus.Event {

    private NotificationsTopic notification;

    public HideNotificationEvent(NotificationsTopic notification) {
        this.notification = notification;
    }

    public NotificationsTopic getNotification() {
        return notification;
    }
}
