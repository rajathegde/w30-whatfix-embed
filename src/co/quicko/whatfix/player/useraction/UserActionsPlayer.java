package co.quicko.whatfix.player.useraction;

import static co.quicko.whatfix.ga.GaUtil.analyticsCache;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.useraction.event.AbstractUserActionEvent;
import co.quicko.whatfix.tracker.useraction.event.PageUserActionsEvent;
import co.quicko.whatfix.tracker.useraction.event.UserActionCompleteEvent;
import co.quicko.whatfix.tracker.useraction.event.UserActionMissEvent;

public class UserActionsPlayer {

    public static final String MESSAGE_UA_COMPLETED = "user_action_completed";
    public static final String SEGMENT_USER_ACTION = "user_action";

    private static final TrackerEventOrigin USER_ACTIONS_SRC = TrackerEventOrigin.DIRECT;

    public UserActionsPlayer() {
        init();
    }

    /**
     * Call this from the class where user actions are being detected and fired
     */
    private void init() {
        EventBus.addHandler(PageUserActionsEvent.class,
                this::onPageUserActions);
        EventBus.addHandler(UserActionCompleteEvent.class, this::onCompleted);
        EventBus.addHandler(UserActionMissEvent.class, this::onMiss);
    }

    /**
     * Call Finder to find elements and latch listeners.
     * 
     * @param e
     */
    private void onPageUserActions(Event e) {
        PageUserActionsEvent event = (PageUserActionsEvent) e;

        Actioner.showUserActions(event.getUserActions());
    }

    /**
     * handle UserActionCompleteEvent
     * 
     * @param event
     */
    private void onCompleted(Event event) {
        track(EmbedEvent.USER_ACTION_COMPLETED,
                (AbstractUserActionEvent) event);
    }

    /**
     * Track UA events
     * 
     * @param type
     * @param event
     */
    private void track(EmbedEvent type, final AbstractUserActionEvent event) {
        co.quicko.whatfix.tracker.event.Event uaEvent = type.builder()
                .flow(event.getDraft().flow_id(), event.getDraft().title())
                .srcId(USER_ACTIONS_SRC).build();

        Common.tracker().onEvent(uaEvent);
    }
    
    private void onMiss(Event event) {
        trackMiss(EmbedEvent.USER_ACTION_MISS, (UserActionMissEvent) event);
    }

    private void trackMiss(EmbedEvent type, final UserActionMissEvent event) {
        co.quicko.whatfix.tracker.event.Event uaMissEvent = type.builder()
                .analyticsInfo(analyticsCache)
                .flow(event.getDraft().flow_id(), event.getDraft().title())
                .srcId(USER_ACTIONS_SRC).build();

        Common.tracker().onEvent(uaMissEvent);
    }

}
