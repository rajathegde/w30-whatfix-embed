package co.quicko.whatfix.player;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.dom.client.DragLeaveEvent;
import com.google.gwt.event.dom.client.DragOverEvent;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.user.client.ui.FlowPanel;

import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.DragDropCallbacks.DropEventCB;

/**
 * Drop zone panel abstract, builds the drop zone/placeholder for drag and
 * drop operations.
 *
 * @author akash
 *
 */
public abstract class DropZonePanel extends NoContentPopup {

    protected String position;
    protected FlowPanel innerPanel = new FlowPanel();

    public DropZonePanel(final String position, final DropEventCB cb) {
        this.position = position;
        buildInnerElement();

        addDomHandler(DragOverEvent::stopPropagation, DragOverEvent.getType());
        addDomHandler(event -> cb.onDrop(event, position), DropEvent.getType());
        addDomHandler(event -> {
            event.stopPropagation();
            event.preventDefault();
            onDragEnter();
        }, DragEnterEvent.getType());
        addDomHandler(event -> {
            event.stopPropagation();
            event.preventDefault();
            onDragLeave();
        }, DragLeaveEvent.getType());
    }

    private void buildInnerElement() {
        innerPanel.setStyleName(Overlay.CSS.dropInnerPanel());
        add(innerPanel);
        setModal(false);
        setAnimationEnabled(false);
        setAutoHideEnabled(false);
        setAutoHideOnHistoryEventsEnabled(false);
        setInnerPanelStyles();
    }

    public String getPosition() {
        return position;
    }

    protected abstract void setInnerPanelStyles();

    protected abstract void setBorder(Element element, String position);

    protected abstract void onDragLeave();

    protected abstract void onDragEnter();

}
