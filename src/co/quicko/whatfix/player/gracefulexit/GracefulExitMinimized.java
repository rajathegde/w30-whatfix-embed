package co.quicko.whatfix.player.gracefulexit;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EndUserWidgetIds;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.WcagUtils;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.exitpopup.event.ShowExitPopupEvent;
import co.quicko.whatfix.player.gracefulexit.GracefulExitPlayer.Events;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitCloseEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitDefaultEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitMaximizeEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitDefaultEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitMaxEvent;
import co.quicko.whatfix.tracker.event.EmbedEvent;

public class GracefulExitMinimized extends NoContentPopup {

    private FlowPanel mainContainer;
    private FlowPanel header;
    private Draft draft;
    private int stepNo;
    private static final String TITLE = "title";
    private Element firstElement;
    private static final String EMPTY_STRING = "";

    public GracefulExitMinimized(Draft draft, int stepNo) {
        this.draft = draft;
        this.stepNo = stepNo;
        mainContainer = new FlowPanel();
        setStyles();
        addHeader();
        mainContainer.add(header);
        setWidget(mainContainer);
    }

    private void setStyles() {
        setStyleName(Overlay.CSS.helpWidget());
        addStyleName(Overlay.CSS.widget());
        addStyleName(Overlay.CSS.minimizeGE());
        setModal(false);
        setAutoHideEnabled(false);
        setAnimationEnabled(true);
        setAutoHideOnHistoryEventsEnabled(false);
    }

    protected void addHeader() {
        header = new FlowPanel();
        Common.setId(header, EndUserWidgetIds.WFX_FOS_HEADER);
        header.addStyleName(Overlay.CSS.header());
        addFlowTitle();
        addButtonsPanel();
        add(header);
        Themer.applyTheme(header, Themer.STYLE.BACKGROUD_COLOR,
                Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR),
                Themer.STYLE.COLOR, Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
    }

    protected void addButtonsPanel() {
        FlowPanel butonsPanel = new FlowPanel();
        butonsPanel.add(getButton(Events.MAXIMIZE.getEvent(),
                SVGElements.MINIMIZED_DEFAULT, Overlay.CONSTANTS.def(),
                event -> closeGEModal(Events.MINIMIZED_DEFAULT.getEvent()),
                getKeyDownHandler(Events.MINIMIZED_DEFAULT.getEvent())));
        butonsPanel.add(getButton(Events.MINIMIZED_DEFAULT.getEvent(),
                SVGElements.MAXIMIZE_GRACEFUL_EXIT,
                Overlay.CONSTANTS.maximize(),
                event -> closeGEModal(Events.MAXIMIZE.getEvent()),
                getKeyDownHandler(Events.MAXIMIZE.getEvent())));
        butonsPanel.add(getButton(Events.CLOSE.getEvent(),
                SVGElements.CLOSE_GRACEFUL_EXIT, Overlay.CONSTANTS.close(),
                event -> closeGEModal(Events.CLOSE.getEvent()),
                getKeyDownHandler(Events.CLOSE.getEvent())));
        butonsPanel.addStyleName(Overlay.CSS.rightPadding());
        butonsPanel.addStyleName(Overlay.CSS.btnPanel());
        header.add(butonsPanel);
    }

    private void closeGEModal(String content) {
        try {
            Events event = Events.valueOf(content.toUpperCase());
            switch (event) {
                case MAXIMIZE:
                    this.hide();
                    EventBus.fire(
                            new ShowGracefulExitMaxEvent(draft, stepNo, false));
                    EventBus.fire(new GracefulExitMaximizeEvent(stepNo));
                    break;
                case MINIMIZED_DEFAULT:
                    this.hide();
                    EventBus.fire(new ShowGracefulExitDefaultEvent(draft,
                            stepNo, false));
                    EventBus.fire(new GracefulExitDefaultEvent(stepNo));
                    break;
                case CLOSE:
                    EventBus.fire(new ShowExitPopupEvent());
                    EventBus.fire(new GracefulExitCloseEvent(stepNo,
                            EmbedEvent.GRACEFUL_EXIT_CLOSE, false));
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            Console.debug(
                    "Tracking Graceful Exit Modal Failed: " + e.getCause());
        }
    }

    private void addFlowTitle() {
        String title = draft.title();
        Label titleLabel = Common.label(title);
        Element titleElement = titleLabel.getElement();
        firstElement = titleElement;
        Common.setId(titleLabel, EndUserWidgetIds.WFX_FOS_HEADER_TEXT);
        titleElement.setAttribute(TITLE, title);
        titleLabel.addStyleName(Overlay.CSS.headerTitle());
        Roles.getHeadingRole().set(titleLabel.getElement());
        titleElement.setAttribute(AriaProperty.LEVEL, "1");
        titleElement.setAttribute(AriaProperty.LABEL, title);
        FlowPanel iconPanel = new FlowPanel();
        iconPanel.add(getDragIcon());
        iconPanel.addStyleName(Overlay.CSS.iconPanel());
        header.add(iconPanel);
        header.add(titleLabel);
        setFirstElementFocus();
    }

    /**
     * @param titleElement
     */
    private void setFirstElementFocus() {
        Scheduler.get()
                .scheduleDeferred(() -> WcagUtils.setFocus(firstElement));
    }

    private Anchor getDragIcon() {
        Anchor identifierIcon = Common.anchor(null);
        Element iconElement = identifierIcon.getElement();
        iconElement.addClassName(Overlay.CSS.transformDragIconSVG());
        iconElement.setInnerHTML(SVGElements.DRAG_ICON);
        iconElement.setAttribute(AriaProperty.HIDDEN, String.valueOf(true));
        iconElement.setTabIndex(-2);
        identifierIcon.addStyleName(Overlay.CSS.flowIcon());
        identifierIcon.setTitle(Overlay.CONSTANTS.flow());
        Themer.applyTheme(identifierIcon, Themer.STYLE.FILL,
                Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
        return identifierIcon;
    }

    private Anchor getButton(String buttonType, String svgElement,
            String buttonTitle, ClickHandler clickHandler,
            KeyDownHandler keyDownHandler) {
        Anchor button = Common.anchor(null);
        Element iconElement = button.getElement();
        iconElement.setInnerHTML(svgElement);
        Roles.getButtonRole().set(iconElement);
        iconElement.addClassName(Overlay.CSS.noOutline());
        iconElement.setTabIndex(0);
        iconElement.setAttribute(AriaProperty.LABEL, buttonTitle);
        button.addStyleName(Overlay.CSS.close());
        Common.setId(button, getId(buttonTitle));
        button.setTitle(buttonTitle);
        button.addClickHandler(clickHandler);
        button.addKeyDownHandler(keyDownHandler);
        Themer.applyTheme(button, Themer.STYLE.STROKE,
                Themer.getThemeKey(Themer.TIP.TITLE_COLOR), Themer.STYLE.FILL,
                Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
        if (buttonType.equalsIgnoreCase(Events.MAXIMIZE.getEvent())) {
            button.addStyleName(Overlay.CSS.minRightPadding());
        }
        return button;
    }
    
    private String getId(String buttonTitle) {
        String id = EMPTY_STRING;
        switch (buttonTitle) {
            case "Maximize":
                id = EndUserWidgetIds.WFX_FOS_MAXIMIZE;
                break;
            case "Close":
                id = EndUserWidgetIds.WFX_FOS_CLOSE;
                break;
            default:
                id = EndUserWidgetIds.WFX_FOS_DEFAULT;
                break;
        }
        return id;
    }

    /**
     * KeyDown event handler for buttons(default, minimise, maximise and close)
     * on default and maximised popup.
     *
     * @param eventType
     * @return
     */
    protected KeyDownHandler getKeyDownHandler(String eventType) {
        return keyDownEvent -> {
            if (KeyCodes.KEY_ENTER == keyDownEvent.getNativeKeyCode()) {
                closeGEModal(eventType);
            }
        };
    }

    public void setFocusIfVisible() {
        if (null != firstElement && firstElement.getOffsetHeight() > 0) {
            setFirstElementFocus();
        }
    }

}
