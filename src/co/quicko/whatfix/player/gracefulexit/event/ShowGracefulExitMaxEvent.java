package co.quicko.whatfix.player.gracefulexit.event;

/*
 * This listener of this event is in class GracefulExitPlayer, this event 
 * is fired when we click on maximize button of GracefulExitModal
 */
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

public class ShowGracefulExitMaxEvent implements Event {
    private Draft draft;
    private int stepNo;
    private boolean isFinderListenerNeeded;

    public ShowGracefulExitMaxEvent(Draft draft, int stepNo,
            boolean isFinderListenerNeeded) {
        this.setDraft(draft);
        this.setStepNo(stepNo);
        this.isFinderListenerNeeded = isFinderListenerNeeded;
    }

    public Draft getDraft() {
        return draft;
    }

    public void setDraft(Draft draft) {
        this.draft = draft;
    }

    public int getStepNo() {
        return stepNo;
    }

    public void setStepNo(int stepNo) {
        this.stepNo = stepNo;
    }

    public boolean isFinderListenerNeeded() {
        return isFinderListenerNeeded;
    }
}
