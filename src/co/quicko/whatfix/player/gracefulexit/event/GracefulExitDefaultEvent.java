package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;

public class GracefulExitDefaultEvent implements Event {
    private int stepNo;

    public GracefulExitDefaultEvent(int stepNo) {
        this.stepNo = stepNo;
    }

    public int getStepNo() {
        return stepNo;
    }
}
