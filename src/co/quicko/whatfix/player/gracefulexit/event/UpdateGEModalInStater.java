package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;

public class UpdateGEModalInStater implements Event {
    private String modalType;

    public UpdateGEModalInStater(String modalType) {
        this.modalType = modalType;
    }

    public String getModalType() {
        return modalType;
    }
}
