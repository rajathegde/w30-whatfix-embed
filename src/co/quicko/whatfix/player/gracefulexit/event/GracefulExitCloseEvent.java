package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.tracker.event.EmbedEvent;

public class GracefulExitCloseEvent implements Event {
    private int step;
    private EmbedEvent embedEvent;
    boolean isDestroyModal;

    public GracefulExitCloseEvent(int step, EmbedEvent embedEvent,
            boolean isDestroyModal) {
        this.setStep(step);
        this.embedEvent = embedEvent;
        this.isDestroyModal = isDestroyModal;
    }

    public GracefulExitCloseEvent(EmbedEvent embedEvent,
            boolean isDestroyModal) {
        this.embedEvent = embedEvent;
        this.isDestroyModal = isDestroyModal;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public EmbedEvent getEmbebEvent() {
        return this.embedEvent;
    }

    public boolean isDestroyModal() {
        return this.isDestroyModal;
    }
}
