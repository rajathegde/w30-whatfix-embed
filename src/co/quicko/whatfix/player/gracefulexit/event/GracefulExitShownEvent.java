package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;

/**
 * This event is used when Graceful Exit Modal is shown
 * 
 * @author arpitamishra
 *
 */

public class GracefulExitShownEvent implements Event {
    private int stepNo;

    public GracefulExitShownEvent(int stepNo) {
        this.stepNo = stepNo;
    }

    public int getStepNo() {
        return stepNo;
    }
}
