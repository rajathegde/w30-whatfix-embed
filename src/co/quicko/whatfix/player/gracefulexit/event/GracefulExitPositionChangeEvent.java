package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;

/**
 * Event to update graceful exit modal position on drag and drop.
 *
 * @author akash
 *
 */
public class GracefulExitPositionChangeEvent implements Event {

    private String position;

    public GracefulExitPositionChangeEvent(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
