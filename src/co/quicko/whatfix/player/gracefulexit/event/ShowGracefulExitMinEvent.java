package co.quicko.whatfix.player.gracefulexit.event;

/*
 * This listener of this event is in class GracefulExitPlayer, this event 
 * is fired when we click on minimize button of GracefulExitModal
 */
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

public class ShowGracefulExitMinEvent implements Event {

    private Draft draft;
    private int stepNo;
    private boolean isFinderListenerNeeded;

    public ShowGracefulExitMinEvent(Draft draft, int stepNo,
            boolean isFinderListenerNeeded) {
        this.draft = draft;
        this.stepNo = stepNo;
        this.isFinderListenerNeeded = isFinderListenerNeeded;
    }

    public int getStepNo() {
        return stepNo;
    }

    public Draft getDraft() {
        return draft;
    }

    public boolean isFinderListenerNeeded() {
        return isFinderListenerNeeded;
    }

}
