package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;

public class GracefulExitMaximizeEvent implements Event {
    private int stepNo;

    public GracefulExitMaximizeEvent(int stepNo) {
        this.stepNo = stepNo;
    }

    public int getStepNo() {
        return stepNo;
    }
}
