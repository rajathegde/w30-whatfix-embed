package co.quicko.whatfix.player.gracefulexit.event;

public enum GracefulExitHideReason {
    ON_NEXT("on_next"),
    ON_STEP_FOUND("on_step_found"),
    ON_FLOW_CHANGE("on_flow_change");

    private String reason;

    private GracefulExitHideReason(String reason) {
        this.reason = reason;
    }

    public static GracefulExitHideReason getReason(String reason) {
        return GracefulExitHideReason.valueOf(reason);
    }

}
