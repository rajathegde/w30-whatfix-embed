package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;

/**
 * This event is used when we close the Graceful Exit Modal either by clicking
 * on "next" (isNext = true in this case) button of the modal or by clicking on
 * close (isNext = false in this case)
 * 
 * @author arpitamishra
 *
 */
public class HideGracefulExitEvent implements Event {
    private GracefulExitHideReason reason;
    private int stepNo;

    public HideGracefulExitEvent(int stepNo, GracefulExitHideReason reason) {
        this.reason = reason;
        this.stepNo = stepNo;
    }

    public boolean isNext() {
        return reason.equals(GracefulExitHideReason.ON_NEXT);
    }

    public boolean isFlowChange() {
        return reason.equals(GracefulExitHideReason.ON_FLOW_CHANGE);
    }

    public int getStepNo() {
        return stepNo;
    }
}
