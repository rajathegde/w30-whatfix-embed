package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;

public class GracefulExitMinimizeEvent implements Event {
    private int stepNo;

    public GracefulExitMinimizeEvent(int stepNo) {
        this.stepNo = stepNo;
    }

    public int getStepNo() {
        return stepNo;
    }
}
