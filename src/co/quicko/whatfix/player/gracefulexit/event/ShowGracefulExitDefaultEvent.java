package co.quicko.whatfix.player.gracefulexit.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

public class ShowGracefulExitDefaultEvent implements Event {
    private Draft draft;
    private int stepNo;
    private boolean isFinderListenerNeeded;

    public ShowGracefulExitDefaultEvent(Draft draft, int stepNo,
            boolean isFinderListenerNeeded) {
        this.draft = draft;
        this.stepNo = stepNo;
        this.isFinderListenerNeeded = isFinderListenerNeeded;
    }

    public Draft getDraft() {
        return draft;
    }

    public int getStepNo() {
        return stepNo;
    }

    public boolean isFinderListenerNeeded() {
        return isFinderListenerNeeded;
    }
}
