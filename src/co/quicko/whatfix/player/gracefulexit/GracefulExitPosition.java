package co.quicko.whatfix.player.gracefulexit;

/**
 * New drop zone positions to be added here in future.
 *
 * @author akash
 */
public enum GracefulExitPosition {

    BOTTOM_RIGHT("br"), BOTTOM_LEFT("bl");

    private String position;

    GracefulExitPosition(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

}
