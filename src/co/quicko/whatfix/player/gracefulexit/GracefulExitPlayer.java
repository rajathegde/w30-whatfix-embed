package co.quicko.whatfix.player.gracefulexit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.DataTransfer;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.DragEndEvent;
import com.google.gwt.event.dom.client.DragStartEvent;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Browser;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EndUserWidgetIds;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.WcagUtils;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.EmbedData.GraceFulExitNextConfig;
import co.quicko.whatfix.data.GracefulExitModalData;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.OnPopupShowEvent;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.actioner.ActionerPopover;
import co.quicko.whatfix.player.DragDropCallbacks.DropEventCB;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.player.GracefulExitDropPanel;
import co.quicko.whatfix.player.WidgetActionsCount;
import co.quicko.whatfix.player.WidgetActionsTracker;
import co.quicko.whatfix.player.exitpopup.event.ExitPopupCloseEvent;
import co.quicko.whatfix.player.exitpopup.event.ShowExitPopupEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitCloseEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitDefaultEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitHideReason;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitInfoTipShownEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitMaximizeEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitMinimizeEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitPositionChangeEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitShownEvent;
import co.quicko.whatfix.player.gracefulexit.event.HideGracefulExitEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitDefaultEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitMaxEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitMinEvent;
import co.quicko.whatfix.player.gracefulexit.event.UpdateGEModalInStater;
import co.quicko.whatfix.tracker.event.EmbedEvent;

public class GracefulExitPlayer implements CrossListener {

    private Frame frame;
    private static FixedPopup gracefulExitModal;
    private Draft draft;
    private int stepNo;
    private int maxModalHeight;
    private int maxModalWidth;
    private PopupPanel spinnerContainer;
    private static ActionerPopover popover;
    private static String infoTipPlacement;
    private boolean isDefault;
    private GracefulExitMinimized minimizedGE;
    private PopupPanel dragImagePanel;
    private static List<String> fixedPositions;
    private String position;
    private static final String DEFAULT_POSITION = GracefulExitPosition.BOTTOM_RIGHT
            .getPosition();
    private static GracefulExitDropPanel[] dropZones;
    private static GraceFulExitNextConfig nextConfig;
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();
    private static final String FLOW_TITLE_0 = "Flow title {0} ";

    static {
        fixedPositions = new ArrayList<>();
        Collections.addAll(fixedPositions,
                GracefulExitPosition.BOTTOM_RIGHT.getPosition(),
                GracefulExitPosition.BOTTOM_LEFT.getPosition());
        nextConfig = Customizer.graceFulExitNextConfig();
    }

    public enum Events {
        MODAL_CLOSE("graceful_exit_close"),
        FRAME_DATA("frame_data"),
        GRACEFUL_EXIT_FRAME_DATA("graceful_exit_frame_data"),
        CLOSE("close"),
        MINIMIZE("minimize"),
        MAXIMIZE("maximize"),
        NEXT("next"),
        GRACEFUL_EXIT_LOADED("graceful_exit_loaded"),
        DEFAULT("default"),
        MINIMIZED_DEFAULT("minimized_default"),
        EXIT_POP_CLOSE("exit_pop_close");

        private String event;

        Events(String event) {
            this.event = event;
        }

        public String getEvent() {
            return event;
        }
    }

    private enum HtmlConstants {
        FRAME_TITLE("Graceful Exit"), MODAL_ID("_gracefulexit_wfx_");

        private String id;

        private HtmlConstants(String id) {
            this.id = id;
        }

        private String getId() {
            return id;
        }
    }

    private enum ModalType {
        DEFAULT("default"), MAX("max"), MIN("min");

        private String type;

        ModalType(String type) {
            this.type = type;
        }

        private String getType() {
            return type;
        }
    }

    private enum TipPlacement {
        LEFT_TOP("lt"), RIGHT_TOP("rt");

        private String type;

        TipPlacement(String type) {
            this.type = type;
        }

        private String getType() {
            return type;
        }
    }

    public GracefulExitPlayer() {
        EventBus.addHandler(ShowGracefulExitDefaultEvent.class,
                this::onShowGracefulExitDefaultModal);
        EventBus.addHandler(ShowGracefulExitMaxEvent.class,
                this::onShowGracefulExitMaxModal);
        EventBus.addHandler(HideGracefulExitEvent.class,
                this::onHideGracefulExitEvent);
        EventBus.addHandler(GracefulExitCloseEvent.class,
                this::onGracefulExitCloseEvent);
        EventBus.addHandler(ShowGracefulExitMinEvent.class,
                this::onShowGracefulExitMinEvent);
        EventBus.addHandler(ExitPopupCloseEvent.class,
                this::onExitPopupCloseEvent);
    }

    private String getPosition() {
        return position;
    }

    /**
     * Order goes like this. The argument position here is from play_state, if
     * position is unset in play_state, check immediate source i.e, Customizer
     * variable, if unset even here fallback to default.
     *
     * @param position
     */
    private void setPosition(String position) {
        if (StringUtils.isNotBlank(position)) {
            updatePosition(position);
        } else if (StringUtils
                .isNotBlank(Customizer.getGracefulExitPosition())) {
            updatePosition(Customizer.getGracefulExitPosition());
        } else {
            updatePosition(DEFAULT_POSITION);
        }
    }

    private void updatePosition(String position) {
        this.position = position;
    }

    public void onShowGracefulExitDefaultModal(Event e) {
        ShowGracefulExitDefaultEvent event = (ShowGracefulExitDefaultEvent) e;
        isDefault = true;
        this.draft = event.getDraft();
        this.stepNo = event.getStepNo();
        setPosition(event.getDraft().gracefulexit_position());
        if (!Browser.isMobile()) {
            makeSpinnerContainer();
            makeGEDefaultModal();
            setInfoTip();
            EventBus.fire(new UpdateGEModalInStater(
                    ModalType.DEFAULT.getType()));
            if (event.isFinderListenerNeeded()) {
                EventBus.fire(new OnPopupShowEvent(
                        this.draft.step(stepNo).action(), stepNo));
                EventBus.fire(new GracefulExitShownEvent(stepNo));
            }
            CrossMessager.addListener(this, Events.MODAL_CLOSE.getEvent(),
                    Events.GRACEFUL_EXIT_FRAME_DATA.getEvent(),
                    Events.GRACEFUL_EXIT_LOADED.getEvent());
        }
    }

    private void onShowGracefulExitMinEvent(Event e) {
        ShowGracefulExitMinEvent event = (ShowGracefulExitMinEvent) e;
        this.draft = event.getDraft();
        this.stepNo = event.getStepNo();
        setPosition(event.getDraft().gracefulexit_position());
        minimizedGE = new GracefulExitMinimized(event.getDraft(),
                event.getStepNo());
        minimizedGE.show();
        EventBus.fire(
                new UpdateGEModalInStater(ModalType.MIN.getType()));
        if (event.isFinderListenerNeeded()) {
            EventBus.fire(new OnPopupShowEvent(this.draft.step(stepNo).action(),
                    stepNo));
        }
        Themer.applyTheme(minimizedGE, Themer.STYLE.BACKGROUD_COLOR,
                Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR),
                Themer.STYLE.COLOR, Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
        setModalPosition(minimizedGE.getElement());
        makeDraggable(minimizedGE);
    }

    private void onExitPopupCloseEvent(Event e) {
        ExitPopupCloseEvent event = (ExitPopupCloseEvent) e;
        CrossMessager.sendMessageToFrame(frame.getElement(),
                Events.EXIT_POP_CLOSE.getEvent(), event.getContent());
        if (null != minimizedGE) {
            minimizedGE.setFocusIfVisible();
        }
    }

    private void makeDraggable(Widget widget) {
        if (Browser.isMobile()) {
            return;
        }
        long sTime = System.currentTimeMillis();
        LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class, "makeDraggable",
                WfxModules.JIT, FLOW_TITLE_0, draft.title());
        RePositionCB rePositionCB = getCallBack(widget);
        widget.getElement().setDraggable(Element.DRAGGABLE_TRUE);
        rePositionCB.setUpDragImagePanel(draft.title());
        // Start handler
        widget.addDomHandler(rePositionCB::onDragStart,
                DragStartEvent.getType());
        // End handler
        widget.addDomHandler(rePositionCB::onDragEnd, DragEndEvent.getType());
        LOGGER.log(WfxLogLevel.PERFORMANCE, GracefulExitPlayer.class,
                "makeDraggable", WfxModules.JIT,
                "added Dom handlers dragstart and dragend, Time taken to execute this method: {0} ms",
                (System.currentTimeMillis() - sTime));
    }

    private RePositionCB getCallBack(Widget widget) {
        return new RePositionCB() {

            @Override
            public void setUpDragImagePanel(String title) {
                GracefulExitPlayer.this.setUpDragImagePanel();
            }

            @Override
            public void onDragStart(DragStartEvent event) {
                LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class,
                        "RePositionCB.onDragStart", WfxModules.JIT,
                        FLOW_TITLE_0, draft.title());
                dragStartHandler(event, widget);
            }

            @Override
            public void onDragEnd(DragEndEvent event) {
                LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class,
                        "RePositionCB.onDragEnd", WfxModules.JIT,
                        FLOW_TITLE_0, draft.title());
                dragEndHandler(event, widget);
            }

        };
    }

    private void dragEndHandler(DragEndEvent event, Widget widget) {
        event.stopPropagation();
        dragImagePanel.hide();
        widget.getElement().removeClassName(Overlay.CSS.disolveGEModal());
        removeDropZones();
    }

    private void dragStartHandler(DragStartEvent event, Widget widget) {
        disposeInfoTip();
        event.stopPropagation();
        widget.getElement().addClassName(Overlay.CSS.disolveGEModal());
        dragImagePanel.show();
        event.setData("text", draft.title());
        event.getDataTransfer().setDragImage(dragImagePanel.getElement(), 10,
                10);
        setEffectAllowed(event.getDataTransfer());
        Scheduler.get().scheduleDeferred(() -> showDropZone(widget));
    }

    // Needed for IE and Firefox
    private final native void setEffectAllowed(DataTransfer dt) /*-{
        dt.effectAllowed = "move";
    }-*/;

    private void setUpDragImagePanel() {
        dragImagePanel = new GracefulExitDragImagePanel(draft, stepNo,
                nextConfig);
    }

    private void showDropZone(Widget widget) {
        setDropZones(new GracefulExitDropPanel[fixedPositions.size() - 1]);
        for (int i = 0; i < fixedPositions.size(); i++) {
            final String dropZonePosition = fixedPositions.get(i);
            final GracefulExitDropPanel dropZone = new GracefulExitDropPanel(
                    dropZonePosition, getOnDropCb(widget));
            setDropZones(dropZone, i);
            Scheduler.get().scheduleDeferred(() -> placeDropZone(dropZone));
            dropZone.show();
            LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class,
                    "showDropZone", WfxModules.JIT,
                    FLOW_TITLE_0, draft.title());
        }
    }

    /**
     * Callback to be passed to the dropzone panel.
     *
     * @param widget
     * @return
     */
    private DropEventCB getOnDropCb(Widget widget) {
        return (DropEvent event, String droppedPosition) -> {
            LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class, "onDropCb",
                    WfxModules.JIT, "Flow title {0}, dropped position {1}",
                    draft.title(), droppedPosition);
            event.stopPropagation();
            event.preventDefault();
            removeDropZones();
            updatePosition(droppedPosition);
            setModalPosition(widget.getElement());
            // send event to update position in play_state
            EventBus.fire(new GracefulExitPositionChangeEvent(droppedPosition));
        };
    }

    private static void setDropZones(GracefulExitDropPanel[] dropZones) {
        GracefulExitPlayer.dropZones = dropZones;
    }

    private static void setDropZones(GracefulExitDropPanel dropZone,
            int index) {
        GracefulExitPlayer.dropZones[index] = dropZone;
    }

    private void placeDropZone(final GracefulExitDropPanel dropZone) {
        Style style = dropZone.getElement().getStyle();
        OverlayUtil.setBottom(style, 0);
        style.clearTop();
        style.clearRight();
        style.clearLeft();
        String dropPosition = dropZone.getPosition();
        if (dropPosition.equalsIgnoreCase(
                GracefulExitPosition.BOTTOM_LEFT.getPosition())) {
            OverlayUtil.setLeft(style, 12);
            return;
        }
        OverlayUtil.setRight(style, 12);
    }

    protected void removeDropZones() {
        LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class,
                "removeDropZones", WfxModules.JIT,
                FLOW_TITLE_0, draft.title());
        if (dropZones != null) {
            for (GracefulExitDropPanel dropZone : dropZones) {
                dropZone.setStyleName(
                        Overlay.CSS.gracefulExitDropableZoneFadeOut());
                dropZone.hide();
            }
        }
    }

    /**
     * Ensures no memory leak from external components.
     */
    private void disposeExternalComponents() {
        LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class,
                "disposeExternalComponents", WfxModules.JIT,
                FLOW_TITLE_0, draft.title());
        if (null != dragImagePanel) {
            dragImagePanel.hide();
            dragImagePanel = null;
        }
        destroyDropZones();
    }

    private static void destroyDropZones() {
        if (null != dropZones) {
            for (GracefulExitDropPanel dropZone : dropZones) {
                dropZone.hide();
            }
        }
        dropZones = null;
    }

    private void makeSpinnerContainer() {
        spinnerContainer = new PopupPanel();
        spinnerContainer.getElement().setInnerHTML(SVGElements.SPINNER);
        spinnerContainer.getElement().getStyle().setHeight(modalHeight(),
                Unit.PX);
        spinnerContainer.getElement().getStyle().setWidth(modalWidth(),
                Unit.PX);
        spinnerContainer.show();
        spinnerContainer.addStyleName(Overlay.CSS.gracefulExitFrame());
        if (isDefault) {
            setModalPosition(spinnerContainer.getElement());
            setSpinnerPosition();
        } else {
            spinnerContainer.addStyleName(Overlay.CSS.spinnerMax());
        }
    }
    
    private void setSpinnerPosition() {
        if (getPosition()
                .startsWith(GracefulExitPosition.BOTTOM_LEFT.getPosition())) {
            spinnerContainer
                    .addStyleName(Overlay.CSS.gracefulExitSpinnerLeft());
        } else {
            spinnerContainer
                    .addStyleName(Overlay.CSS.gracefulExitSpinnerRight());
        }
    }

    private void makeGEDefaultModal() {
        initGracefulExitModal();
        Common.setId(gracefulExitModal, HtmlConstants.MODAL_ID.getId());
        gracefulExitModal.setStyleName(Overlay.CSS.gracefulExitModal());
        makeDraggable(gracefulExitModal);
        gracefulExitModal.setWidget(getDefaultFramePanel());
        removeSpotlightAndAutoExecGlass();
        gracefulExitModal.show();
        setModalPosition(gracefulExitModal.getElement());
    }

    private Widget getDefaultFramePanel() {
        FlowPanel panel = new FlowPanel();
        Frame defaultFrame = getDefaultFrame();
        panel.add(getDragIconOverlay());
        panel.add(defaultFrame);
        return panel;
    }

    private Widget getDragIconOverlay() {
        FlowPanel iconPanel = new FlowPanel();
        iconPanel.add(getDragIcon());
        iconPanel.addStyleName(Overlay.CSS.gracefulExitDefaultDragIconPanel());
        return iconPanel;
    }

    private Anchor getDragIcon() {
        Anchor identifierIcon = Common.anchor(null);
        identifierIcon.setTitle(Overlay.CONSTANTS.flow());
        Element iconElement = identifierIcon.getElement();
        iconElement.addClassName(Overlay.CSS.transformDragIconSVG());
        iconElement.setInnerHTML(SVGElements.DRAG_ICON);
        iconElement.setAttribute(AriaProperty.HIDDEN, String.valueOf(true));
        iconElement.setTabIndex(-2);
        Themer.applyTheme(identifierIcon, Themer.STYLE.FILL,
                Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
        return identifierIcon;
    }

    private static void initGracefulExitModal() {
        gracefulExitModal = new FixedPopup();
    }

    /*
     * Ensures removal of spotlight in case of spotlight tag. Case1: when a step
     * is a miss, timer disposes the spotlight. This will avoid the spotlight if
     * GE default modal shows up early.
     * Case 2: When on auto execution step miss, the glass overlay is also 
     * removed.
     */
    private void removeSpotlightAndAutoExecGlass() {
        Actioner.dispose();
    }

    private void makeGEMaxModal() {
        gracefulExitModal = new FixedPopup();
        Common.setId(gracefulExitModal, HtmlConstants.MODAL_ID.getId());
        gracefulExitModal.addStyleName(Overlay.CSS.gracefulExitMaxModal());
        Common.addGlassToPopup(gracefulExitModal,
                HtmlConstants.MODAL_ID.getId());
        gracefulExitModal.center();
        gracefulExitModal.show();
        gracefulExitModal.setWidget(getMaxFrame());
        gracefulExitModal.addStyleName(Overlay.CSS.gracefulExitModalShadow());
        gracefulExitModal
                .addStyleName(Overlay.CSS.gracefulExitMaxModalRadius());
        WcagUtils.setWCAGPopup(gracefulExitModal);
    }

    private Frame getDefaultFrame() {
        frame = Framers.gracefulExit().buildFrame();
        frame.addStyleName(Overlay.CSS.gracefulExitFrame());
        WcagUtils.setWCAGOnFrame(frame);
        Common.setId(frame, EndUserWidgetIds.WFX_FOS_DEFAULT_FRAME);
        frame.setTitle(HtmlConstants.FRAME_TITLE.getId());
        return frame;
    }

    private Frame getMaxFrame() {
        frame = Framers.gracefulExit().buildFrame();
        WcagUtils.setWCAGOnFrame(frame);
        Common.setId(frame, EndUserWidgetIds.WFX_FOS_MAX_FRAME);
        frame.setTitle(HtmlConstants.FRAME_TITLE.getId());
        frame.getElement().getStyle().setHeight(maxModalHeight, Unit.PX);
        frame.getElement().getStyle().setWidth(maxModalWidth, Unit.PX);
        return frame;
    }

    private void setModalPosition(Element popupElement) {
        Style style = popupElement.getStyle();
        OverlayUtil.clearPosition(style);
        OverlayUtil.setBottom(style, 0);
        if (getPosition().equalsIgnoreCase(
                GracefulExitPosition.BOTTOM_LEFT.getPosition())) {
            OverlayUtil.setLeft(style, 12);
        } else {
            OverlayUtil.setRight(style, 12);
        }
    }

    private void setInfoTip() {
        if (!showAttention()) {
            return;
        }
        Step step = DataUtil.create();
        initTipPlacement();
        step.placement(infoTipPlacement);
        int width = frame.getElement().getOffsetWidth();
        int height = frame.getElement().getOffsetHeight();
        int top = gracefulExitModal.getElement().getOffsetTop();
        int bottom = top + height;
        int left = gracefulExitModal.getElement().getOffsetLeft();
        int right = left + width;
        popover = new ActionerPopover(new GracefulExitInfoTip(), null, null,
                step, top, right, bottom, left, width, height);
        popover.popover().addStyleName(Overlay.CSS.fixedTip());
        popover.show(top, right, bottom, left, width, height, infoTipPlacement);
        EventBus.fire(new GracefulExitInfoTipShownEvent());
    }

    private void initTipPlacement() {
        setInfoTipPlacement(getPosition()
                .equals(GracefulExitPosition.BOTTOM_RIGHT.getPosition())
                        ? TipPlacement.LEFT_TOP.getType()
                        : TipPlacement.RIGHT_TOP.getType());
    }

    private static void setInfoTipPlacement(String placement) {
        infoTipPlacement = placement;
    }

    private boolean showAttention() {
        return WidgetActionsTracker.getCount().geTipCount() < 1;
    }

    public static void disposeInfoTip() {
        if (popover != null) {
            popover.dispose();
            popover = null;
            WidgetActionsCount counts = WidgetActionsTracker.getCount();
            counts.geTipCount(counts.geTipCount() + 1);
            WidgetActionsTracker.onInteraction(counts);
        }
    }

    public void onShowGracefulExitMaxModal(Event e) {
        ShowGracefulExitMaxEvent event = (ShowGracefulExitMaxEvent) e;
        isDefault = false;
        this.draft = event.getDraft();
        this.stepNo = event.getStepNo();
        maxModalHeight = maxModalHeight();
        maxModalWidth = maxModalWidth();
        if (!Browser.isMobile()) {
            makeSpinnerContainer();
            makeGEMaxModal();
            EventBus.fire(new UpdateGEModalInStater(
                    ModalType.MAX.getType()));
            if (event.isFinderListenerNeeded()) {
                EventBus.fire(new OnPopupShowEvent(this.draft.step(stepNo).action(),
                        stepNo));
            }
            CrossMessager.addListener(this, Events.MODAL_CLOSE.getEvent(),
                    Events.GRACEFUL_EXIT_FRAME_DATA.getEvent());
        }
    }

    @Override
    public void onMessage(String type, String content) {
        if (Events.GRACEFUL_EXIT_FRAME_DATA.getEvent().equals(type)) {
            GracefulExitModalData data = GracefulExitModalData.getModalData(
                    draft, stepNo, modalType(), modalHeight(), modalWidth());
            CrossMessager.sendMessageToFrame(frame.getElement(),
                    Events.FRAME_DATA.getEvent(),
                    StringUtils.stringifyObject(data));
        } else if (Events.MODAL_CLOSE.getEvent().equals(type)) {
            spinnerContainer.hide();
            disposeInfoTip();
            takeAction(content);
        } else if (Events.GRACEFUL_EXIT_LOADED.getEvent().equals(type)) {
            gracefulExitModal.setVisible(true);
            spinnerContainer.hide();
        }
    }

    private void takeAction(String content) {
        try {
            Events event = Events.valueOf(content.toUpperCase());
            switch (event) {
                case MAXIMIZE:
                    isDefault = false;
                    gracefulExitModal.hide();
                    removeListeners();
                    EventBus.fire(
                            new ShowGracefulExitMaxEvent(draft, stepNo, false));
                    EventBus.fire(new GracefulExitMaximizeEvent(stepNo));
                    break;
                case DEFAULT:
                    isDefault = true;
                    gracefulExitModal.hide();
                    removeListeners();
                    EventBus.fire(new GracefulExitDefaultEvent(stepNo));
                    EventBus.fire(new ShowGracefulExitDefaultEvent(draft,
                            stepNo, false));
                    break;
                case MINIMIZE:
                    gracefulExitModal.hide();
                    removeListeners();
                    EventBus.fire(
                            new ShowGracefulExitMinEvent(draft, stepNo, false));
                    EventBus.fire(new GracefulExitMinimizeEvent(stepNo));
                    break;
                case NEXT:
                    gracefulExitModal.hide();
                    removeListeners();
                    EventBus.fire(new HideGracefulExitEvent(stepNo,
                            GracefulExitHideReason.ON_NEXT));
                    break;
                case CLOSE:
                    EventBus.fire(new ShowExitPopupEvent());
                    EventBus.fire(new GracefulExitCloseEvent(stepNo,
                            EmbedEvent.GRACEFUL_EXIT_CLOSE, false));
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            Console.debug(
                    "Tracking Graceful Exit Modal Failed: " + e.getCause());
        }
    }

    private void removeListeners() {
        CrossMessager.removeListener(this, Events.MODAL_CLOSE.getEvent());
        CrossMessager.removeListener(this,
                Events.GRACEFUL_EXIT_FRAME_DATA.getEvent());
    }

    private void onHideGracefulExitEvent(Event e) {
        hideModal();
        EventBus.fire(new UpdateGEModalInStater(null));
    }

    private void hideModal() {
        if (minimizedGE != null) {
            minimizedGE.hide();
        }
        if (gracefulExitModal != null) {
            gracefulExitModal.hide();
        }
        removeListeners();
    }

    private void onGracefulExitCloseEvent(Event e) {
        GracefulExitCloseEvent event = (GracefulExitCloseEvent) e;
        if (event.isDestroyModal()) {
            hideModal();
            disposeExternalComponents();
        }
    }

    private int modalWidth() {
        if (isDefault) {
            return Framers.MIN_GRACEFUL_EXIT_WIDTH;
        }
        return maxModalWidth;
    }

    private int modalHeight() {
        if (isDefault) {
            return Framers.MIN_GRACEFUL_EXIT_HEIGHT;
        }
        return maxModalHeight;
    }

    private int maxModalWidth() {
        return getNearestEvenInt((int) (Window.getClientWidth() * 0.9));
    }

    private int maxModalHeight() {
        return getNearestEvenInt((int) (Window.getClientHeight() * 0.9));
    }

    private String modalType() {
        if (isDefault) {
            return ModalType.DEFAULT.getType();
        }
        return ModalType.MAX.getType();
    }

    private int getNearestEvenInt(int i) {
        return (i % 2 == 0) ? i : (i + 1);
    }

}
