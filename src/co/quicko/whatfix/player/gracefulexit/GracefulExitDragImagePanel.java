package co.quicko.whatfix.player.gracefulexit;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.snap.MicroStepSnap;
import co.quicko.whatfix.common.snap.StepSnap;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.EmbedData.GraceFulExitNextConfig;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.PredAnchor;
import co.quicko.whatfix.player.gracefulexit.GracefulExitPlayer.Events;

/**
 * @author akash
 *
 */
public class GracefulExitDragImagePanel extends PopupPanel {

    private Draft draft;
    private int stepNo;
    private FlowPanel mainContainer;
    private GraceFulExitNextConfig nextConfig;

    public enum GracefulExitConstants {
        SHOW, TITLE
    }

    public GracefulExitDragImagePanel(Draft draft, int stepNo,
            GraceFulExitNextConfig nextConfig) {
        this.draft = draft;
        this.stepNo = stepNo;
        this.nextConfig = nextConfig;
        mainContainer = new FlowPanel();
        setStyle();
        addHeader();
        addImageContainer();
        setWidget(mainContainer);
    }

    private void setStyle() {
        mainContainer
                .setStyleName(Overlay.CSS.dragImagePanelGracefulExit());
    }

    private void addHeader() {
        FlowPanel headerWrapper = new FlowPanel();
        headerWrapper.addStyleName(Overlay.CSS.header());
        headerWrapper.addStyleName(Overlay.CSS.headerTitleFont());
        headerWrapper.add(getIconPanel());
        headerWrapper.add(getLabel());
        headerWrapper.add(getButtonsPanel());
        Themer.applyTheme(headerWrapper, Themer.STYLE.BACKGROUD_COLOR,
                Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR),
                Themer.STYLE.COLOR, Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
        mainContainer.add(headerWrapper);
    }

    private FlowPanel getIconPanel() {
        FlowPanel dragIconPanel = new FlowPanel();
        dragIconPanel.add(getDragIcon());
        dragIconPanel.addStyleName(Overlay.CSS.iconPanel());
        return dragIconPanel;
    }

    private Anchor getDragIcon() {
        Anchor identifierIcon = Common.anchor(null);
        Element iconElement = identifierIcon.getElement();
        iconElement.addClassName(Overlay.CSS.transformDragIconSVG());
        iconElement.setInnerHTML(SVGElements.DRAG_ICON);
        identifierIcon.addStyleName(Overlay.CSS.flowIcon());
        Themer.applyTheme(identifierIcon, Themer.STYLE.FILL,
                Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
        return identifierIcon;
    }

    private Label getLabel() {
        Label dragImageLabel = new Label();
        dragImageLabel.setText(draft.title());
        dragImageLabel.addStyleName(Overlay.CSS.headerTitle());
        return dragImageLabel;
    }

    private FlowPanel getButtonsPanel() {
        FlowPanel buttonsPanel = new FlowPanel();
        buttonsPanel.add(getButton(Events.MAXIMIZE.getEvent(),
                SVGElements.MINIMIZED_DEFAULT));
        buttonsPanel.add(getButton(Events.MINIMIZED_DEFAULT.getEvent(),
                SVGElements.MAXIMIZE_GRACEFUL_EXIT));
        buttonsPanel.add(getButton(Events.CLOSE.getEvent(),
                SVGElements.CLOSE_GRACEFUL_EXIT));
        buttonsPanel.addStyleName(Overlay.CSS.rightPadding());
        buttonsPanel.addStyleName(Overlay.CSS.btnPanel());
        return buttonsPanel;
    }

    private Anchor getButton(String buttonType, String svgElement) {
        Anchor button = Common.anchor(null);
        Element iconElement = button.getElement();
        iconElement.setInnerHTML(svgElement);
        button.addStyleName(Overlay.CSS.close());
        Themer.applyTheme(button, Themer.STYLE.STROKE,
                Themer.getThemeKey(Themer.TIP.TITLE_COLOR), Themer.STYLE.FILL,
                Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
        if (buttonType.equalsIgnoreCase(Events.MAXIMIZE.getEvent())) {
            button.addStyleName(Overlay.CSS.minRightPadding());
        }
        return button;
    }

    private void addImageContainer() {
        FlowPanel bodyContainer = new FlowPanel();
        bodyContainer.addStyleName(Overlay.CSS.dragImagePanelWrapper());
        StepSnap snap = new MicroStepSnap();
        Scheduler.get().scheduleDeferred(() -> {
            snap.setValue(draft.step(stepNo));
            snap.refresh(draft.title(), stepNo, draft.steps());
            snap.addStyleName(Overlay.CSS.dragImagePanelShadow());
        });
        bodyContainer.add(snap);
        bodyContainer.add(addFooterPanel());
        mainContainer.add(bodyContainer);
    }

    private FlowPanel addFooterPanel() {
        FlowPanel footerPanel = new FlowPanel();
        footerPanel.addStyleName(Overlay.CSS.dragImagefooterPanel());
        addNextBtn(footerPanel);
        return footerPanel;
    }

    private void addNextBtn(FlowPanel footerPanel) {
        Anchor nextBtn = new PredAnchor(true);
        nextBtn.setStyleName(Overlay.CSS.popoverAnchor());
        if (GracefulExitConstants.SHOW.toString()
                .equalsIgnoreCase(Themer.getThemeValue(Themer.TIP.NEXT_TEXT))) {
            nextBtn.setText(Overlay.CONSTANTS.next());
        }
        if (GracefulExitConstants.SHOW.toString()
                .equalsIgnoreCase(Themer.getThemeValue(Themer.TIP.NEXT_ICON))) {
            nextBtn.getElement().appendChild(makeNextIconAnchor());
        }
        if (null != nextConfig && vaildateCustomizerInput(nextConfig)) {
            nextBtn.getElement().getStyle()
                    .setBackgroundColor(nextConfig.background_color());
            nextBtn.getElement().getStyle().setColor(nextConfig.text_color());
            nextBtn.getElement().getStyle().setProperty(Themer.STYLE.FILL,
                    nextConfig.text_color());
        } else {
            if (!Themer.isToolTipThemeClassic()) {
                Themer.applyTheme(nextBtn, Themer.STYLE.BACKGROUD_COLOR,
                        Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR),
                        Themer.STYLE.COLOR,
                        Themer.getThemeKey(Themer.TIP.NEXT_COLOR),
                        Themer.STYLE.FILL,
                        Themer.getThemeKey(Themer.TIP.NEXT_COLOR));
            } else {
                Themer.applyTheme(nextBtn, Themer.STYLE.BACKGROUD_COLOR,
                        Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR),
                        Themer.STYLE.COLOR,
                        Themer.getThemeKey(Themer.TIP.TITLE_COLOR),
                        Themer.STYLE.FILL,
                        Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
            }
        }
        footerPanel.add(nextBtn);
    }

    private boolean vaildateCustomizerInput(GraceFulExitNextConfig nextConfig) {
        return (Common.validateHexColorCode(nextConfig.background_color())
                && Common.validateHexColorCode(nextConfig.text_color()));
    }

    private Element makeNextIconAnchor() {
        Element spanElement = DOM.createSpan();
        SpanElement arrowElement = SpanElement.as(spanElement);
        arrowElement.addClassName(Overlay.CSS.iconArrowLeft());
        if (GracefulExitConstants.SHOW.toString()
                .equalsIgnoreCase(Themer.getThemeValue(Themer.TIP.NEXT_TEXT))) {
            arrowElement.addClassName(Overlay.CSS.nextIconLeftMargin());
        }
        arrowElement.setInnerHTML(SVGElements.RIGHT_ARROW);
        return arrowElement;
    }

}
