package co.quicko.whatfix.player.gracefulexit;

import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.FullPopover;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.Popover;
import co.quicko.whatfix.overlay.Popover.ClickRegister;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.StepPop;
import co.quicko.whatfix.overlay.StepStaticPop;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;

class GracefulExitInfoTip extends PopoverExtender {
    @Override
    protected Popover makePopover(Step action, PopListener listener,
            ClickRegister register, boolean alignment) {
        return new GracefulExitTipPopover(listener, register, alignment);
    }

    @Override
    protected StepPop stepPop(Step step, String footnote, String placement,
            String stepnote) {
        return new GracefulExitTipPop(step, footnote, placement, true);
    }

    @Override
    protected String footnote(Draft draft, Step action) {
        return " ";
    }
    
    private class GracefulExitTipPopover extends FullPopover {

        public GracefulExitTipPopover(PopListener listener,
                ClickRegister register, boolean isLTR) {
            super(listener, register, true);
        }

        @Override
        public String getThemeType() {
            return Themer.TIP.TipThemeType.CLASSIC.name;
        }

        @Override
        protected boolean isBackNeeded() {
            return false;
        }

        @Override
        protected boolean isEditOrNextNeeded() {
            return false;
        }

        @Override
        protected PopTheme fetchTheme() {
            return new ActionerPopTheme() {
                @Override
                public String bodybgcolor() {
                    return Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR);
                }

                @Override
                public String titleColor() {
                    return Themer.getThemeKey(Themer.TIP.TITLE_COLOR);
                }
            };
        }
    }

    private class GracefulExitTipPop extends StepStaticPop {

        public GracefulExitTipPop(Step step, String footnote, String placement,
                boolean isNextRequired) {
            super(step, footnote, placement, isNextRequired);
        }

        @Override
        public boolean closeable() {
            return false;
        }

        @Override
        public String description_md() {
            return Overlay.CONSTANTS.gracefulExitTipTitle(false);
        }

        @Override
        public String note_md() {
            return Overlay.CONSTANTS.gracefulExitTipDescription(false);
        }
    }
    
}

