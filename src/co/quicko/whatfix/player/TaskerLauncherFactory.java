package co.quicko.whatfix.player;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;

import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.player.TaskerLauncher.MobileTaskerLauncher;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class TaskerLauncherFactory {

    private static final TaskerLauncherFactory factory = new TaskerLauncherFactory();

    public static TaskerLauncherFactory getInstance() {
        return factory;
    }

    public TaskerLauncher getTaskerLauncher(PlayerBase player,
            Settings settings) {
        int currentWidth = Window.getClientWidth();
        int currentHieght = Window.getClientHeight();
        final TaskerLauncher taskerLauncher;

        if (currentWidth > 640 && currentHieght > Framers.MIN_TASKER_HEIGHT 
                && !AppFactory.isMobileApp()) {
            taskerLauncher = TaskerLauncher.getTaskerLauncherInstance(player,
                    settings);
        } else {
            taskerLauncher = MobileTaskerLauncher
                    .getTaskerLauncherInstance(player, settings);
        }
        HandlerRegistration resizeRegn = Window
                .addResizeHandler(getResizeHandler(player, taskerLauncher));
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                resizeRegn.removeHandler();
            }
        }, "tasker_destroy");
        return taskerLauncher;
    }

    private ResizeHandler getResizeHandler(PlayerBase player,
            final TaskerLauncher taskerLauncher) {
        return new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event) {
                onTaskerResize(taskerLauncher);
            }

            private void onTaskerResize(
                    final TaskerLauncher taskerLauncher) {
                int clientWidth = Window.getClientWidth();
                int clientHieght = Window.getClientHeight();
                taskerLauncher.refreshTooltipPositionIfPresent();
                if (clientWidth > 640
                        && clientHieght > Framers.MIN_TASKER_HEIGHT) {
                    // Normal scenario where we show desktop version of
                    // Launcher
                    if (taskerLauncher instanceof MobileTaskerLauncher) {
                        recreate();
                        return;
                    }
                } else if (clientWidth <= 640
                        || clientHieght <= Framers.MIN_TASKER_HEIGHT) {

                    if (taskerLauncher instanceof TaskerLauncher
                            && !(taskerLauncher instanceof MobileTaskerLauncher)) {
                        // This is when width is reduced and earlier
                        // user was shown desktop version and now
                        // switching to mobile
                        recreate();
                        return;
                    } else {
                        // User was viewing mobile version but handling
                        // further zoom
                        ((MobileTaskerLauncher) taskerLauncher)
                                .adjustTasker(false);
                    }

                }
            }

            private void recreate() {
                taskerLauncher.destroy();
                player.initTaskerLauncher(false);
            }
        };
    }

}
