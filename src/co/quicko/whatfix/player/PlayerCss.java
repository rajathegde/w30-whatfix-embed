package co.quicko.whatfix.player;

import com.google.gwt.resources.client.CssResource;

public interface PlayerCss extends CssResource {

    public String endFrame();

    public int closing_retries();
    
}
