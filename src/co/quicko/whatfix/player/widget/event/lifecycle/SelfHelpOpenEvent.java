package co.quicko.whatfix.player.widget.event.lifecycle;

import co.quicko.whatfix.common.EventBus;
import com.google.gwt.event.dom.client.ClickEvent;

public class SelfHelpOpenEvent implements EventBus.Event {

    private final ClickEvent targetClick;
    private final String defaultPage;

    public SelfHelpOpenEvent(ClickEvent targetClick, String defaultPage) {
        this.targetClick = targetClick;
        this.defaultPage = defaultPage;
    }

    public ClickEvent getTargetClick() {
        return targetClick;
    }

    public String getDefaultPage() {
        return defaultPage;
    }
}
