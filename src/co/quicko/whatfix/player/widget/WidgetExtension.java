package co.quicko.whatfix.player.widget;

import co.quicko.whatfix.common.StringUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;

public class WidgetExtension extends Composite {
    interface ThisUiBinder extends UiBinder<FocusPanel, WidgetExtension> {
    }

    private static ThisUiBinder ourUiBinder = GWT.create(ThisUiBinder.class);

    private final String id;

    @UiField
    FocusPanel container;

    @UiField
    Label icon;


    public WidgetExtension(String id, String background, String svg,
                           String tooltip) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.id = id;
        this.icon.getElement().setInnerHTML(svg);

        container.getElement().getStyle().setBackgroundColor(background);
        container.getElement().setTitle(tooltip);
        icon.getElement().getStyle().setColor("#FFFFFF");

    }

    public String getId() {
        return id;
    }

    public void setBackground(String widgetColor) {
        if (StringUtils.isBlank(widgetColor)) {
            return;
        }
        if (widgetColor.startsWith("#") && widgetColor.length() == 7) {
            StringBuilder newColor = new StringBuilder("#");
            for (int i = 1; i < widgetColor.length(); i += 2) {
                newColor.append(reduceColor(widgetColor.substring(i, i + 2)));
            }
            container.getElement().getStyle().setBackgroundColor(newColor.toString());
        }
    }

    public void addClickHandler(ClickHandler handler) {
        this.container.addClickHandler(handler);
    }

    private static String reduceColor(String value) {
        final double PERCENTAGE = 0.75;
        int newValue = (int) (Integer.parseInt(value, 16) * PERCENTAGE);
        return (newValue > 16 ? "" : "0") + Integer.toHexString(newValue);
    }
}
