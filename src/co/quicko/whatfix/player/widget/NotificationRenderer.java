package co.quicko.whatfix.player.widget;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.user.client.ui.FlowPanel;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.SVGElements;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.contextualinfo.NotificationInfo;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.notification.CanShowNotification;
import co.quicko.whatfix.player.notification.Notification;
import co.quicko.whatfix.player.notification.NotificationBanner;
import co.quicko.whatfix.player.notification.NotificationsTopic;
import co.quicko.whatfix.player.notification.event.CloseBannerNotification;
import co.quicko.whatfix.player.notification.event.HideNotificationEvent;
import co.quicko.whatfix.player.widget.event.lifecycle.SelfHelpOpenEvent;
import co.quicko.whatfix.tracker.event.EmbedEvent;

public class NotificationRenderer implements CanShowNotification {

    public static final int WIDGET_EXTENSION_HEIGHT = 30;
    private final Element borderAdjust;
    private final Settings settings;

    private final FlowPanel container;
    protected NotificationBanner currentNotification;
    private final List<WidgetExtension> widgetExtensions = new LinkedList<>();
    private final TrackerEventOrigin origin;
    private Notification notification;
    private ContextualInfo contextualInfo;

    public NotificationRenderer(FlowPanel container, Element borderAdjust,
            Settings settings, TrackerEventOrigin origin) {
        this.container = container;
        this.borderAdjust = borderAdjust;
        this.settings = settings;
        this.origin = origin;
    }

    @Override
    public void showBannerNotification(Notification notification) {
        String header = notification.getBannerConfig().getHeader();
        String description = notification.getBannerConfig().getDescription();
        this.notification = notification;
        this.contextualInfo = getContextualInfo(notification);

        currentNotification = new NotificationBanner(header, description,
                Overlay.CONSTANTS.newContentCta(), SVGElements.UPDATE_CIRCLE);
        currentNotification.setVisible(false);

        container.add(currentNotification.asWidget());

        currentNotification.getElement().getStyle().setPosition(Style.Position.ABSOLUTE);
        if (settings.position().startsWith("r")) {
            currentNotification.getElement().getStyle().setRight(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
        } else if (settings.position().startsWith("l")) {
            currentNotification.getElement().getStyle().setLeft(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
        } else if (settings.position().startsWith("t")){
            currentNotification.getElement().getStyle().setTop(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
        } else if (settings.position().startsWith("b")) {
            currentNotification.getElement().getStyle().setBottom(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
        } else {
            Console.debug("Invalid position found: " + settings.position());
        }

        currentNotification.addCtaClickHandler(e -> {
            EventBus.fire(new SelfHelpOpenEvent(e, notification.getTopic().toString()));
            EventBus.fire(new HideNotificationEvent(notification.getTopic()));
            hideBannerNotification(notification.getTopic());
            Common.tracker().onEvent(EmbedEvent.NOTIFICATION_CLICKED.builder()
                    .srcId(origin.toString())
                    .contextualInfo(this.contextualInfo)
                    .build());
        });

        currentNotification.addDomHandler((mouseOverHandler) -> {
            Common.tracker().onEvent(EmbedEvent.NOTIFICATION_MOUSEOVER.builder()
                    .srcId(origin.toString())
                    .contextualInfo(contextualInfo)
                    .build());
        }, MouseOverEvent.getType());

        currentNotification.addCloseClickHandler(e -> {
            e.preventDefault();
            e.stopPropagation();
            EventBus.fire(new CloseBannerNotification("click"));
        });

        adjustNotificationBannerPosition();
        Common.tracker().onEvent(EmbedEvent.NOTIFICATION_LOADED.builder().
                srcId(origin.getSrcName())
                .contextualInfo(this.contextualInfo)
                .build());
    }

    public static ContextualInfo getContextualInfo(Notification notification) {
        NotificationInfo notificationInfo = DataUtil.create();
        notificationInfo.notification_id(notification.getNotificationId());
        notificationInfo.notification_topic(notification.getTopic().toString());
        notificationInfo.notification_priority(notification.getTopic().getPriority());

        ContextualInfo info = DataUtil.create();
        info.put(ContextualInfo.OuterKey.NOTIFICATION_INFO.toString(),
                notificationInfo);

        return info;
    }

    private void adjustNotificationBannerPosition() {
        if (currentNotification == null) {
            return;
        }

        Scheduler.get().scheduleDeferred(() -> {
            int shWidth = borderAdjust.getClientWidth() + widgetExtensions.size() * WIDGET_EXTENSION_HEIGHT;
            int shHeight = borderAdjust.getClientHeight() + widgetExtensions.size() * WIDGET_EXTENSION_HEIGHT;

            com.google.gwt.user.client.ui.Widget widget = currentNotification.asWidget();
            int bannerWidth = widget.getElement().getClientWidth();
            int bannerHeight = widget.getElement().getClientHeight();

            int xMove = (shWidth - bannerWidth) / 2;
            int yMove = (shHeight - bannerHeight) / 2;

            currentNotification.getElement().getStyle().setPosition(Style.Position.ABSOLUTE);
            if (settings.position().startsWith("r")) {
                currentNotification.getElement().getStyle().setRight(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
                currentNotification.getElement().getStyle().setBottom(yMove, Style.Unit.PX);
            } else if (settings.position().startsWith("l")) {
                currentNotification.getElement().getStyle().setLeft(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
                currentNotification.getElement().getStyle().setBottom(yMove, Style.Unit.PX);
            } else if (settings.position().startsWith("t")){
                currentNotification.getElement().getStyle().setTop(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
                currentNotification.getElement().getStyle().setLeft(xMove, Style.Unit.PX);
            } else if (settings.position().startsWith("b")) {
                currentNotification.getElement().getStyle().setBottom(WIDGET_EXTENSION_HEIGHT, Style.Unit.PX);
                currentNotification.getElement().getStyle().setLeft(xMove, Style.Unit.PX);
            } else {
                Console.debug("Invalid position found: " + settings.position());
            }
            currentNotification.setVisible(true);
        });
    }

    @Override
    public void hideBannerNotification(NotificationsTopic topic) {
        if (currentNotification != null) {
            currentNotification.removeFromParent();
            currentNotification = null;
        }
    }

    @Override
    public void hideBannerNotification(NotificationsTopic topic, String closeby) {
        if (currentNotification != null) {
            currentNotification.removeFromParent();
            if (StringUtils.isNotBlank(closeby)) {
                Common.tracker().onEvent(EmbedEvent.NOTIFICATION_CLOSED.builder()
                        .replaceInPage("closedBy", closeby)
                        .srcId(origin.getSrcName())
                        .contextualInfo(this.contextualInfo)
                        .build());
            }
            currentNotification = null;
        }
    }

    public void addWidgetExtension(WidgetExtension w) {
        widgetExtensions.add(w);
        reLayoutWidgetExtensions();
    }

    public void removeWidgetExtension(String w) {
        Console.debug("Removing widget extension for id: " + w);
        Iterator<WidgetExtension> iterator = widgetExtensions.iterator();

        while (iterator.hasNext()) {
            WidgetExtension next = iterator.next();
            if (next.getId().equals(w)) {
                iterator.remove();
            }
            next.removeFromParent();
        }
        reLayoutWidgetExtensions();
    }

    private void reLayoutWidgetExtensions() {
        final int BORDER_RADIUS = 4;

        for (int i = 1; i <= widgetExtensions.size(); i++) {
            WidgetExtension ext = widgetExtensions.get(i - 1);
            ext.setBackground(getIconBgColor());
            ext.getElement().getStyle().setPosition(Style.Position.ABSOLUTE);

            if (settings.position().startsWith("r") || settings.position().startsWith("l")) {
                ext.getElement().getStyle().setTop((double) -WIDGET_EXTENSION_HEIGHT * i, Style.Unit.PX);
            } else {
                ext.getElement().getStyle().setTop(0, Style.Unit.PX);
                ext.getElement().getStyle().setRight((double) -WIDGET_EXTENSION_HEIGHT * i, Style.Unit.PX);
            }
            container.add(ext);
            ext.getElement().getStyle().clearProperty(getRadiusValueBasedOnPosition(settings.position()));
        }

        if (!widgetExtensions.isEmpty()) {
            WidgetExtension last = widgetExtensions.get(widgetExtensions.size() - 1);

            last.getElement().getStyle().setProperty(getRadiusValueBasedOnPosition(settings.position()), BORDER_RADIUS,
                    Style.Unit.PX);

            // adjust the border radius of the main widget
            Common.unsetBorderRadius(borderAdjust, settings.position());
            borderAdjust.getStyle().setProperty(getMainElementBorderRadius(settings.position()), BORDER_RADIUS,
                    Style.Unit.PX);
        } else {
            Common.setBorderRadius(borderAdjust, settings.position());
        }
        adjustNotificationBannerPosition();
    }

    private static String getMainElementBorderRadius(String position) {
        switch (position.charAt(0)) {
            case 'r':
            case 't':
                return "border-bottom-left-radius";
            case 'b':
                return "border-top-left-radius";
            case 'l':
                return "border-bottom-right-radius";

            default:
                return "border-radius";
        }
    }

    private static String getRadiusValueBasedOnPosition(String position) {
        switch (position.charAt(0)) {
            case 'r':
                return "border-top-left-radius";
            case 'b':
            case 'l':
                return "border-top-right-radius";
            case 't':
                return "border-bottom-right-radius";

            default:
                return "border-radius";
        }
    }


    protected String getIconBgColor() {
        return Themer.value(Themer.SELFHELP.ICON_BG_COLOR,
                settings.color());
    }
}
