package co.quicko.whatfix.player;

import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.DragDropCallbacks.DropEventCB;

/**
 * SelfHelp DropZone Panel
 * 
 * @author Sakti Kumar Chourasia
 */
public class SelfHelpDropPanel extends DropZonePanel {

    public SelfHelpDropPanel(final String position, final DropEventCB cb) {
        super(position, cb);
        setStyles();
    }

    @Override
    protected void setInnerPanelStyles() {
        Common.setBorderRadius(innerPanel.getElement(), position);
        setBorder(getElement(), position);
        Themer.applyTheme(innerPanel, Themer.STYLE.BACKGROUD_COLOR,
                Themer.SELFHELP.ICON_BG_COLOR);
    }

    private void setStyles() {
        if (position.startsWith("t") || position.startsWith("b")) {
            addStyleName(Overlay.CSS.horizontalDropableZone());
        } else if (position.startsWith("r") || position.startsWith("l")) {
            addStyleName(Overlay.CSS.verticalDropableZone());
        }
    }

    @Override
    protected void onDragEnter() {
        addStyleName(Overlay.CSS.scaleDropZone());
        addStyleName(getStyle());
    }

    @Override
    protected void onDragLeave() {
        removeStyleName(Overlay.CSS.scaleDropZone());
        removeStyleName(getStyle());
    }

    private String getStyle() {
        if (position.startsWith("r")) {
            return Overlay.CSS.scaleDropZoneRight();
        } else if (position.startsWith("l")) {
            return Overlay.CSS.scaleDropZoneLeft();
        } else if (position.startsWith("t")) {
            return Overlay.CSS.scaleDropZoneTop();
        } else {
            return Overlay.CSS.scaleDropZoneBottom();
        }
    }

    @Override
    public void setBorder(Element element, String position) {
        if (position == null) {
            return;
        } else if (position.startsWith("t")) {
            element.addClassName(Overlay.CSS.borderNoTop());
        } else if (position.startsWith("b")) {
            element.addClassName(Overlay.CSS.borderNoBottom());
        } else if (position.startsWith("l")) {
            element.addClassName(Overlay.CSS.borderNoLeft());
        } else if (position.startsWith("r")) {
            element.addClassName(Overlay.CSS.borderNoRight());
        }
    }

}
