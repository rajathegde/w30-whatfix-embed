package co.quicko.whatfix.player;

import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;

import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.Framers.Framer;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.InPlaceSegment;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.TipService;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.event.EventPayload.Builder;
import co.quicko.whatfix.workflowengine.alg.DataCollector;

public class IPWidget extends HelpWidget {

    private static final String INPLACE = "inplace";
    public static final String WIDGET_ID = "_inplace_widget_wfx_";
    public static final String LAUNCHER_ID = "_inplace_widget_launcher_wfx_";
    private Flow smartTip;

    public IPWidget(PlayerBase player, Settings settings,
            RePositionCB rePositionCB) {
        super(player, settings, rePositionCB);
    }

    @Override
    protected String getIdAttribute() {
        return WIDGET_ID;
    }

    @Override
    protected String getLauncherIdAttribute() {
        return LAUNCHER_ID;
    }

    protected String getWidgetSrcName() {
        return TrackerEventOrigin.INPLACE_WIDGET.getSrcName();
    }

    protected Builder getWidgetCloseBuilder() {
        return EmbedEvent.INPLACE_WIDGET_CLOSE.builder();
    }

    @Override
    protected String[] getAllListenerKeys() {
        return new String[] { prefix("_close"), prefix("_loaded"),
                prefix("_frame_data"), prefix("_open"), prefix("_data") };
    }

    protected String widgetLauncherLabel() {
        return Overlay.CONSTANTS.inplaceLauncherLabel(false);
    }

    @Override
    public void onClick(ClickEvent event) {
        if (!isShowingFull()) {
            if (smartTip == null) {
                // init smartTip - first Time
                final InPlaceSegment segment = Enterpriser
                        .getActiveInplaceSegment();
                String smartTipId = segment.smart_tip_collection_id();

                TipService.IMPL.flow(smartTipId,
                        new AsyncCallback<Flow>() {
                            @Override
                            public void onFailure(Throwable caught) {
                            }

                            @Override
                            public void onSuccess(Flow result) {
                                smartTip = result;
                                showFull();
                            }
                        });
            } else {
                showFull();
            }
        }
    }

    @Override
    public void onMessage(String type, final String flow_id) {
        super.onMessage(type, flow_id);
        if (prefix("_data").equals(type)) {
            // Collect data -- form a JSO
            final InPlaceSegment segment = Enterpriser
                    .getActiveInplaceSegment();
            JavaScriptObjectExt stepMapper = segment.step_mapper();
            JsArrayString flowStepMapper = stepMapper.value(flow_id).cast();
            if (flowStepMapper != null) {
                int len = flowStepMapper.length();
                JsArrayString content = DataUtil.createArray().cast();
                for (int i = 0; i < len; i++) {
                    String map = flowStepMapper.get(i);
                    String[] stepKeyData = map.split(":");
                    String smarTipstepKey = stepKeyData[1];
                    String value = DataCollector.collect(smartTip,
                            smartTip.stepNo(smarTipstepKey));
                    content.push(stepKeyData[0] + ":" + value);
                }
                CrossMessager.sendMessageToFrame(frame.getElement(),
                        "inplace_input_data",
                        StringUtils.stringifyArray(content));
            }
        }
    }

    @Override
    protected String prefix(String type) {
        return INPLACE + type;
    }

    protected String getIconBgColor() {
        return Themer.value(Themer.INPLACE.ICON_BG_COLOR);
    }

    protected void applyTextTheme(Anchor selfHelp) {
        Themer.applyTheme(selfHelp, Themer.STYLE.FONT_SIZE,
                Themer.INPLACE.ICON_TEXT_SIZE + ";px", Themer.STYLE.COLOR,
                Themer.INPLACE.ICON_TEXT_COLOR, Themer.STYLE.FONT, Themer.FONT);
    }

    @Override
    protected boolean isAttentionEnabled() {
        return false;
    }

    @Override
    protected Framer createWidgetFrame() {
        return Framers.inplaceWidget();
    }
    
    @Override
    protected void makeDraggable() {
        // Nothing as of now
    }
}
