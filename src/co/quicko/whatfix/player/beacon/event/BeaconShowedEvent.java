package co.quicko.whatfix.player.beacon.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

public class BeaconShowedEvent implements Event {

    private int step;
    private Draft draft;

    public BeaconShowedEvent(Draft draft, int step) {
        this.step = step;
        this.draft = draft;
    }

    public int getStep() {
        return step;
    }

    public Draft getDraft() {
        return draft;
    }

}
