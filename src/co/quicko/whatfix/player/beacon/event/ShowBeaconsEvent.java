package co.quicko.whatfix.player.beacon.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

public class ShowBeaconsEvent implements Event {

    private Draft beaconCollection;

    public ShowBeaconsEvent(Draft draft) {
        this.beaconCollection = draft;
    }
    
    public Draft getBeaconCollection() {
        return this.beaconCollection;
    }
}
