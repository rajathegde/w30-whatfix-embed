package co.quicko.whatfix.player.beacon.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;

public class BeaconClosedEvent implements Event {

    private Step step;
    private Draft draft;

    public BeaconClosedEvent(Draft draft, Step step) {
        this.draft = draft;
        this.step = step;
    }

    public Step getStep() {
        return step;
    }

    public Draft getDraft() {
        return draft;
    }
}
