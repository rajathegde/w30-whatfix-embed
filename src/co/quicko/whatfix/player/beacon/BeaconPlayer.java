package co.quicko.whatfix.player.beacon;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.ZMaxer;
import co.quicko.whatfix.data.BeaconSegment;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.Beacon;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.player.beacon.event.BeaconClosedEvent;
import co.quicko.whatfix.player.beacon.event.ShowBeaconsEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.security.UserInfo;
import co.quicko.whatfix.service.BeaconService;
import co.quicko.whatfix.service.Callbacks;

public class BeaconPlayer {

    private static final String BEACON_ID_DELIM = ":";

    public BeaconPlayer() {
        EventBus.addHandler(ShowBeaconsEvent.class, this::onShowBeaconEvent);
        EventBus.addHandler(BeaconClosedEvent.class, this::onBeaconClosed);
    }

    private void onShowBeaconEvent(Event e) {
        ShowBeaconsEvent event = (ShowBeaconsEvent) e;

        Draft result = event.getBeaconCollection();
        ZMaxer.init();
        result.type(ContentType.beacon.name());
        Actioner.showBeacons(result);
    }

    public void onBeaconClosed(Event event) {
        final BeaconClosedEvent e = (BeaconClosedEvent) event;
        final Step step = e.getStep();

        if (!step.flow_id().equals("_wfx_beacon")) {
            UserInfo user = Security
                    .unq_id_with_enc_status(AppUser.getUserId());

            BeaconSegment activeSegment = Enterpriser.getActiveBeaconSegment();
            String component_id = activeSegment.segment_id() + BEACON_ID_DELIM
                    + Beacon.id(step);
            
            AnalyticsInfo info = AnalyticsInfo.create(activeSegment,
                    TrackerEventOrigin.BEACON);
            GaUtil.setAnalytics(info);
            
            BeaconService.IMPL.seen(component_id, user,
                    Callbacks.emptyVoidCb());
        }
    }

}
