package co.quicko.whatfix.player;

import co.quicko.whatfix.overlay.Customizer;
import com.google.gwt.storage.client.Storage;

import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;

public class WidgetActionsTracker {
    private static Storage localStorage;
    public static final String KEY = "_wfx_widget_actions_count";

    private static void initIfRequired() {
        if (!isInitialized()) {
            localStorage = Storage.getLocalStorageIfSupported();
        }
    }
    
    public static boolean isInitialized() {
        return localStorage != null;
    }
    
    public static WidgetActionsCount getCount() {
        initIfRequired();
        WidgetActionsCount count = DataUtil.create();
        if (localStorage != null) {
            String counts = localStorage.getItem(KEY);
            if (counts != null) {
                count = parseCounts(counts);
            }
        }
        return count;
    }

    public static void onInteraction(WidgetActionsCount value) {
        initIfRequired();
        if (localStorage != null) {
            localStorage.setItem(KEY, StringUtils.stringifyObject(value));
            Customizer.onWidgetLocalStorageChange(KEY,StringUtils.stringifyObject(value));
        }
    }
    
    private static native WidgetActionsCount parseCounts(String counts) /*-{
        return JSON.parse(counts);
    }-*/;
    
    public static void destroy() {
        localStorage = null;
    }
}
