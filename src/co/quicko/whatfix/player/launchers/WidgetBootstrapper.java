package co.quicko.whatfix.player.launchers;

import co.quicko.whatfix.player.PlayerBase;

public class WidgetBootstrapper implements ComponentBootstrapper{

    @Override
    public void bootstrapComponent(PlayerBase playerbase) {
        playerbase.initWidgetLauncher();
    }
    
}
