package co.quicko.whatfix.player.launchers;

import co.quicko.whatfix.player.PlayerBase;

public class PopupBootstrapper implements ComponentBootstrapper{

    @Override
    public void bootstrapComponent(PlayerBase playerbase) {
        playerbase.tryOnboarding();
    }
    
}
