package co.quicko.whatfix.player.launchers;

import co.quicko.whatfix.player.PlayerBase;

public class TaskerBootstrapper implements ComponentBootstrapper{

    @Override
    public void bootstrapComponent(PlayerBase playerbase) {
        playerbase.initTaskerLauncher();
    }
    
}
