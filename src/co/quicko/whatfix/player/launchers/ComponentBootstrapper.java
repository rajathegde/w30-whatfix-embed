package co.quicko.whatfix.player.launchers;

import co.quicko.whatfix.player.PlayerBase;

/**
 * @author anuragsharma
 * This is common interface to bootstrap components asynchronously
 * after checking if there is any segmentation based on end user data 
 */
public interface ComponentBootstrapper {
        public void bootstrapComponent(PlayerBase playerbase);
}
