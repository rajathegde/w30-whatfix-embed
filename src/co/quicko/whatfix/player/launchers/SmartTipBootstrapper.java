package co.quicko.whatfix.player.launchers;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.player.tip.event.EvaluateSmartTipSegment;
import co.quicko.whatfix.player.tip.event.StopSmartTipEvent;

public class SmartTipBootstrapper implements ComponentBootstrapper {

    @Override
    public void bootstrapComponent(PlayerBase playerbase) {
        try {
            EventBus.fire(new StopSmartTipEvent());
            EventBus.fire(new EvaluateSmartTipSegment());
        } catch (Exception e) {
            Console.debug("Error in SmartTips: " + e.getMessage());
        }
    }

}
