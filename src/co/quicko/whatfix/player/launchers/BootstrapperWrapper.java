package co.quicko.whatfix.player.launchers;

import com.google.gwt.core.client.JsArray;

public class BootstrapperWrapper {
    
    public BootstrapperWrapper(ComponentBootstrapper componentBootstrapper,
            JsArray segments) {
        this.componentBootstrapper = componentBootstrapper;
        this.segments = segments;
    }
    
    public ComponentBootstrapper componentBootstrapper;
    public JsArray segments;
}
