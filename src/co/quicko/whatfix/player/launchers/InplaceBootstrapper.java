package co.quicko.whatfix.player.launchers;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.player.InplaceLauncher;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.security.Enterpriser;

public class InplaceBootstrapper implements ComponentBootstrapper{

    @Override
    public void bootstrapComponent(PlayerBase playerbase) {
        try {
            if (playerbase.hasInplaceLauncher()) {
                return;
            }
            Settings settings = Enterpriser.getInplaceSettings();
            if (settings != null) {
                InplaceLauncher launcher = new InplaceLauncher(playerbase, settings);
                launcher.showWidget(settings);
            }
        } catch (Exception e) {
            Console.debug("Error in SelfHelp: " + e.getMessage());
        }
    }
    
}
