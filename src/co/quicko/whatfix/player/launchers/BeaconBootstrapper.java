package co.quicko.whatfix.player.launchers;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.player.PlayerBase;

public class BeaconBootstrapper implements ComponentBootstrapper{

    @Override
    public void bootstrapComponent(PlayerBase playerbase) {
        try {
            Actioner.hide(ContentType.beacon);
            playerbase.tryBeaconCollectionSegments();
        } catch (Exception e) {
            Console.debug("Error in Beacon Collection: " + e.getMessage());
        }
    }
    
}
