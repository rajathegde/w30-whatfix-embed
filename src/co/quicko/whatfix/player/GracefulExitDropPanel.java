package co.quicko.whatfix.player;

import com.google.gwt.dom.client.Element;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.DragDropCallbacks.DropEventCB;
import co.quicko.whatfix.player.gracefulexit.GracefulExitPosition;

/**
 * Current target positions [br, bl]
 * 
 * @author akash
 *
 */
public class GracefulExitDropPanel extends DropZonePanel {

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    public GracefulExitDropPanel(final String position, final DropEventCB cb) {
        super(position, cb);
        addStyleName(Overlay.CSS.gracefulExitDropableZone());
    }

    @Override
    protected void setInnerPanelStyles() {
        Common.setBorderRadius(innerPanel.getElement(), position);
        setBorder(getElement(), position);
        applyTheme();
    }

    protected void applyTheme() {
        Themer.applyTheme(innerPanel, Themer.STYLE.BACKGROUD_COLOR,
                Themer.getThemeKey(Themer.TIP.BODY_BG_COLOR),
                Themer.STYLE.COLOR, Themer.getThemeKey(Themer.TIP.TITLE_COLOR));
    }

    @Override
    protected void onDragEnter() {
        addLog(WfxLogLevel.DEBUG, "onDragEnter");
        addStyleName(getScaleDropZoneStyle());
        addStyleName(getScaleDropZoneOriginStyle());
    }

    @Override
    protected void onDragLeave() {
        addLog(WfxLogLevel.DEBUG, "onDragLeave");
        removeStyleName(getScaleDropZoneStyle());
    }
    
    protected void addLog(WfxLogLevel level, String event) {
        LOGGER.log(level, GracefulExitDropPanel.class, event,
                WfxModules.JIT, "");
    }

    private String getScaleDropZoneStyle() {
        return Overlay.CSS.scaleDropZone();
    }

    protected String getScaleDropZoneOriginStyle() {
        if (GracefulExitPosition.BOTTOM_RIGHT.getPosition()
                .equalsIgnoreCase(position)) {
            return Overlay.CSS.scaleDropZoneBottomRight();
        }
        // case of BOTTOM_LEFT
        return Overlay.CSS.scaleDropZoneBottomLeft();

    }

    /**
     * Setting border to animate the outer element of the panel. For smoother
     * transition.
     *
     * @param element
     * @param position
     */
    @Override
    protected void setBorder(Element element, String position) {
        if (position == null) {
            return;
        }
        if (GracefulExitPosition.BOTTOM_RIGHT.getPosition()
                .equalsIgnoreCase(position)) {
            element.addClassName(Overlay.CSS.borderNoRightAndBottom());
            return;
        }
        // case of BOTTOM_LEFT
        element.addClassName(Overlay.CSS.borderNoLeftAndBottom());
    }

}
