package co.quicko.whatfix.player;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DragEndEvent;
import com.google.gwt.event.dom.client.DragStartEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Browser;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.Framers.Framer;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.NoContentPopup;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.TimeStampTracker;
import co.quicko.whatfix.common.VideoContentPopup;
import co.quicko.whatfix.common.WidgetAttentionAnimation;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Content.Video;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.EmbedData.TagEnabledWidgetSettings;
import co.quicko.whatfix.data.EnterpriseSettings;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.player.notification.CanShowNotification;
import co.quicko.whatfix.player.notification.Notification;
import co.quicko.whatfix.player.notification.NotificationsTopic;
import co.quicko.whatfix.player.notification.SelfHelpNotificationController;
import co.quicko.whatfix.player.notification.event.HideNotificationEvent;
import co.quicko.whatfix.player.widget.NotificationRenderer;
import co.quicko.whatfix.player.widget.WidgetExtension;
import co.quicko.whatfix.player.widget.event.lifecycle.SelfHelpOpenEvent;
import co.quicko.whatfix.player.widget.event.lifecycle.SelfHelpWidgetCreatedEvent;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.event.EventPayload.Builder;

public abstract class BaseWidget extends NoContentPopup
        implements ClickHandler, CrossListener, CanShowNotification {
    public static final String WIDGET_ID = "_widget_wfx_";
    public static final String LAUNCHER_ID = "_widget_launcher_wfx_";
    public static final String VERTICLE_RIGHT_LEFT = "vertical-rl";
    public static final String VERTICLE_LEFT_RIGHT = "vertical-lr";
    public static final String STYLE = "style";

    private static final String PREFIX = "widget";

    protected final int WIDTH = 470;
    protected final int HEIGHT = 400;

    protected int width;
    protected int height;

    protected Settings settings;
    protected Widget label;

    protected Framer framer;
    protected Frame frame;
    protected PlayerBase player;

    private long startAt = 0;

    private static boolean interacted = false;

    protected boolean isDraggable = false;
    private RePositionCB rePositionCB;
    protected boolean focusSelfHelp = false;
    private Set<String> unseenContent;
    private String defaultWidgetTab;

    private static final SelfHelpNotificationController notificationController = new SelfHelpNotificationController();
    protected static NotificationRenderer renderer;
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    public BaseWidget(PlayerBase player, Settings settings,
            final RePositionCB rePositionCB) {
        this.settings = settings;
        this.player = player;
        this.rePositionCB = rePositionCB;

        setStyleName(Overlay.CSS.helpWidget());
        addStyleName(Overlay.CSS.widget());
        Common.setBorderRadius(getElement(), settings.position());

        setModal(false);
        setAutoHideEnabled(false);
        setAnimationEnabled(true);
        setAutoHideOnHistoryEventsEnabled(false);

        label = launcher();

        boolean isNewNotificationEnabled = Enterpriser
                .hasFeature(AdditionalFeatures.NEW_NOTIFICATION);
        if (isNewNotificationEnabled && settings.unseenContent() != null
                && settings.unseenContent().length() > 0) {
            unseenContent = new HashSet<>();
            for (int i = 0; i < settings.unseenContent().length(); i++) {
                unseenContent.add(settings.unseenContent().get(i));
            }
        }

        framer = createWidgetFrame();
        frame = Framer.buildOnlyStyle();

        // remember to removeListener in destroy()
        CrossMessager.addListener(this, getAllListenerKeys());
        if (isAttentionEnabled() && !interacted) {
            setAttentionAnimation();
        }

        if (!Browser.isMobile() && settings.target() == null) {
            makeDraggable();
            isDraggable = true;
        }

        EventBus.addHandler(SelfHelpOpenEvent.class, this::openWidget);
    }

    private void openWidget(EventBus.Event event) {
        SelfHelpOpenEvent e = (SelfHelpOpenEvent) event;
        defaultWidgetTab = e.getDefaultPage();
        onClick(e.getTargetClick());
    }

    protected String[] getAllListenerKeys() {
        return new String[] { prefix("_close"), prefix("_run"),
                prefix("_loaded"), prefix("_frame_data"), prefix("_open"),
                prefix("_video"), prefix("_content"), prefix("_scroll"), 
                prefix("_desktop_content"), "updateUnseenContent" };
    }
    
    protected Framer createWidgetFrame() {
        return Framers.widget();
    }

    protected Anchor launcher() {
        String label = getWidgetLabel(settings);
        Anchor selfHelp = new Anchor(label);
        selfHelp.getElement().setTabIndex(0);
        selfHelp.addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                getElement().addClassName(Overlay.CSS.helpWidgetFocus());
            }
        });

        selfHelp.addBlurHandler(event -> getElement()
                .removeClassName(Overlay.CSS.helpWidgetFocus()));

        selfHelp.getElement().setAttribute(AriaProperty.LABEL, label);
        selfHelp.getElement().setAttribute(AriaProperty.EXPANDED,
                String.valueOf(false));
        selfHelp.setStyleName(Overlay.CSS.widgetLauncher());
        applyTextTheme(selfHelp);
        if (Enterpriser.hasFeature(AdditionalFeatures.open_sh_mouseover)) {
            selfHelp.addMouseOverHandler(mouseOverEvent -> onClick(null));
            addStyleForMouseOver(selfHelp);
        }
        return selfHelp;
    }

    private static void addStyleForMouseOver(Widget widget) {
        addProperty(widget, "margin", "0px 14px !important");
        addProperty(widget, "padding", "7px 0px !important");
    }
    
    protected void applyTextTheme(Anchor selfHelp) {
        Themer.applyTheme(selfHelp, Themer.STYLE.FONT_SIZE,
                Themer.SELFHELP.ICON_TEXT_SIZE + ";px", Themer.STYLE.COLOR,
                Themer.SELFHELP.ICON_TEXT_COLOR, Themer.STYLE.FONT,
                Themer.FONT);
    }

    public String getWidgetLabel(Settings settings) {
        String label = settings.label();
        if (label == null || label.length() == 0) {
            label = widgetLauncherLabel();
        }
        return label;
    }
    
    private String getTextOrientation() {
        return settings.text_orientation();
    }

    protected String widgetLauncherLabel() {
        return Overlay.CONSTANTS.widgetLauncherLabel(false);
    }

    protected abstract void createStructure();

    public void showFirstTime(final boolean recreated) {
        super.show();
        getElement().getStyle().setVisibility(Visibility.HIDDEN);

        Scheduler.get().scheduleDeferred(() -> setDimensions(recreated));
    }

    private void setDimensions(final boolean recreated) {
        String position = settings.position();
        Style style = label.getElement().getStyle();

        boolean supportTransform = false;
        for (String property : TRANSFORM_PROPS) {
            if (style.getProperty(property) != null) {
                supportTransform = true;
                break;
            }
        }

        if (!supportTransform
                && (position.startsWith("l") || position.startsWith("r"))) {
            position = "brm";
            settings.position(position);
        }

        final boolean swap = (position.startsWith("l")
                || position.startsWith("r"));
        Scheduler.get().scheduleDeferred(() -> {
            int labelHeight = label.getOffsetHeight();
            int labelWidth = getWidth(label);
            if (swap) {
                width = labelHeight;
                height = labelWidth;
            } else {
                width = labelWidth;
                height = labelHeight;
            }
            getElement().getStyle().setHeight(height, Unit.PX);
            getElement().getStyle().setWidth(width, Unit.PX);
            setUp(recreated);
        });
    }

    /**
     * This is method is implement to get the total width of the widget including the margin and padding.
     * This is only applicable if widget have margin in pixel Unit.
     * @param w The widget for extracting the total width.
     * @return Total width of widget.
     */
    private static int getWidth(Widget w) {
        int totalWidth = w.getOffsetWidth();
        String leftMarginInPixel = w.getElement().getStyle().getMarginLeft();
        String rightMarginInPixel = w.getElement().getStyle().getMarginRight();
        return totalWidth + marginInInt(leftMarginInPixel) + marginInInt(rightMarginInPixel);
    }

    private static int marginInInt(String margin) {
        int marginInInt = 0;
        final int suffixLength = Unit.PX.getType().length();
        if (StringUtils.isNotBlank(margin) && margin.endsWith(Unit.PX.getType())) {
            marginInInt += Integer.parseInt(margin.substring(0, margin.length() - suffixLength));
        }
        return marginInInt;
    }

    private void setUp(boolean recreated) {
        setOverFlow();
        setBackground();
        setZindex();
        setPosition();
        rotate();
        postSetup(recreated);
    }

    protected void postSetup(boolean recreated) {
        getElement().getStyle().setVisibility(Visibility.VISIBLE);

        EventBus.fire(new SelfHelpWidgetCreatedEvent());

        hideForRelative();
        if (recreated) {
            return;
        }

        // we use relative_to for backward compatibility where we open self help
        // automatically if relative to is present
        @SuppressWarnings("deprecation")
        String relative_to = settings.relative_to();
        if (relative_to == null) {
            if ("open".equals(settings.state())) {
                settings.state(null);
                onClick(null);
            }
        } else {
            onClick(null);
        }
    }

    protected void hideForRelative() {
        if (settings.target() != null && !isShowingFull()) {
            getElement().addClassName(Overlay.CSS.widgetHidden());
        }
    }

    protected void unhideForRelative() {
        if (settings.target() != null) {
            getElement().removeClassName(Overlay.CSS.widgetHidden());
            if (label.isAttached()) {
                // it is removed so that there is no momentary display of self
                // help label when widget is opening
                label.removeFromParent();
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(ClickEvent event) {

        TimeStampTracker.setTime(TimeStampTracker.Event.SELF_HELP_TRIGGERED_TIME, System.currentTimeMillis());

        CrossMessager.sendMessageToParent(GaUtil.PAYLOAD,
                StringUtils.stringifyObject(Where.create("type",
                        TrackerEventOrigin.SELF_HELP.toString(),
                        "event_type", "widget_open",
                        "segment_name", settings.segment_name(),
                        "segment_id", settings.segment_id(),
                        "role_tag",
                        ((TagEnabledWidgetSettings) settings).role_tags(),
                        "src_id", TrackerEventOrigin.SELF_HELP.getSrcName())));

        preventDefault(event);
        boolean settingsUpdated = Customizer.onBeforeWidgetShow(settings);
        if (settingsUpdated) {
            framer = createWidgetFrame();
        } else {
            Settings newSettings = player.widgetSettings();
            // no self help segment. but self help is present already.
            // show empty self help
            if (newSettings == null) {
                Settings noFlowSettings = DataUtil.create();
                noFlowSettings.ent_id(settings.ent_id());
                noFlowSettings.label(settings.label());
                noFlowSettings.title(settings.title());
                noFlowSettings.mode(settings.mode());
                noFlowSettings.position(settings.position());
                noFlowSettings.relative_to(settings.relative_to());
                noFlowSettings.target(settings.target());
                noFlowSettings.no_initial_flows(true);
                settings = noFlowSettings;
                framer = createWidgetFrame();
            } else if (settings.no_initial_flows()) {
                settings = newSettings;
                framer = Framers.widget();
            } else if (!equalConfiguration(settings, newSettings)) {
                if (newSettings.position() == null) {
                    newSettings.position(settings.position());
                }
                settings = newSettings;
                framer = Framers.widget();
            }
        }
        unhideForRelative();
        Scheduler.get().scheduleDeferred(() -> {
            if (!isShowingFull()) {
                if (isAttentionEnabled() && !interacted) {
                    removeStyleName(getAnimationStyle());
                    WidgetActionsCount counts = WidgetActionsTracker.getCount();
                    counts.widgetAnimationCount(
                            counts.widgetAnimationCount() + 1);
                    WidgetActionsTracker.onInteraction(counts);
                    interacted = true;
                }
                showFull();
                LOGGER.log(WfxLogLevel.PERFORMANCE, BaseWidget.class, "widget-open",
                        WfxModules.WORKFLOW_ENGINE,
                        "Time taken for opening the Self help widget : {0} ms",
                        (System.currentTimeMillis() - TimeStampTracker.getTime(TimeStampTracker.Event.SELF_HELP_TRIGGERED_TIME)));
            }
        });
    }

    private void preventDefault(ClickEvent event) {
        if (event != null) {
            event.preventDefault();
        }
    }

    private boolean equalConfiguration(Settings settings1, Settings settings2) {
        String s1 = StringUtils.stringifyObject(settings1);
        String s2 = StringUtils.stringifyObject(settings2);
        return s1.equals(s2);
    }

    protected abstract void showFull();

    protected abstract void closeFull();

    public abstract boolean isShowingFull();

    protected void removeAnchorFocusStyle() {
        /*
         * In IE blur() method does not get triggered while widget launches.
         * because of which the styling of the widget doesn't change in IE. So
         * need to explicitly call this method to remove styling.
         */
        getElement().removeClassName(Overlay.CSS.helpWidgetFocus());
    }

    public void handleResize(ResizeEvent event) {
        setPosition();
    }

    @Override
    public void onMessage(String type, String content) {
        if (!shouldLaunchSelfHelp(type)
                && !shouldCloseSelfHelp(type, content)) {
            if ((prefix("_content").equals(type))
                    || prefix("_desktop_content").equals(type)) {
                markContentAsCompleted(content);
            } else if ((prefix("_scroll").equals(type))) {
                trackWidgetScroll(content);
            } else if ("updateUnseenContent".equals(type)) {
                NewNotification.addSeenLongBack(content);
                unseenContent.remove(content);
                if (unseenContent.isEmpty()) {
                    EventBus.fire(new HideNotificationEvent(
                            NotificationsTopic.NEW_CONTENT));
                    EventBus.fire(new HideNotificationEvent(
                            NotificationsTopic.NEW_CONTENT_BADGE));
                }
            } else {
                Console.log("Unidentified message received: " + type);
            }
        }
    }

    private boolean shouldCloseSelfHelp(String type, String content) {
        if ((prefix("_close")).equals(type)) {
            closeWidget(content);
            focusSelfHelp = keepFocusOnSelfHelp(content);
            return true;
        } else if ((prefix("_run")).equals(type)) {
            showDeck(content);
            return true;
        } else if ((prefix("_video")).equals(type)) {
            showVideoPopup(type, content);
            return true;
        } else {
            return false;
        }
    }

    private boolean shouldLaunchSelfHelp(String type) {
        if (prefix("_loaded").equals(type)) {
            if (isShowingFull()) {
                onLoaded();
                if (StringUtils.isNotBlank(defaultWidgetTab)) {
                    CrossMessager.sendMessageToFrame(frame.getElement(),
                            "routing", defaultWidgetTab);
                }
                startAt = System.currentTimeMillis();
            }
            return true;
        } else if (prefix("_frame_data").equals(type)) {
            if (unseenContent != null) {
                settings.unseenContent(JsUtils
                        .toJsArray(unseenContent.toArray(new String[0])));
            }
            EnterpriseSettings enterpriseSettings = EnterpriseSettings
                    .getEnterpriseSettings(settings);
            GaUtil.setNewInteraction(enterpriseSettings);
            enterpriseSettings
                    .isMobile(WidgetLauncher.MOBILE_MODE.equals(mode()));
            enterpriseSettings.setPreviewMode(ExtensionHelper.isPreviewMode());
            
            if (Enterpriser.hasFeature(AdditionalFeatures.personalisation_content_reorder)) {
            	JavaScriptObjectExt personalisationConfig = (JavaScriptObjectExt) JavaScriptObjectExt.createObject();
            	personalisationConfig.put("userId", AppUser.getPersonalisationUserID());
            	personalisationConfig.put("config", Customizer.getPersonalisationConfig());
            	enterpriseSettings.personalisationConfig(personalisationConfig);
            }
            enterpriseSettings.settings().user(AppUser.getUser());
            
            CrossMessager.sendMessageToFrame(frame.getElement(), "frame_data",
                    StringUtils.stringifyObject(enterpriseSettings));
            return true;
        } else if (prefix("_open").equals(type)) {
            onClick(null);
            return true;
        } else {
            return false;
        }
    }

    private boolean keepFocusOnSelfHelp(String content) {
        if (!Enterpriser.hasFeature(AdditionalFeatures.wcag_enable)) {
            return false;
        }
        String closeBy = ((Where) DataUtil.create(content))
                .getString("closeBy");
        return closeBy.equals("escape") || closeBy.equals("shortcut")
                || closeBy.equals("cross");
    }

    private void markContentAsCompleted(String msg) {
        Content content = DataUtil.create(msg);
        player.markContentCompleted(content.flow_id());
    }

    protected void showVideoPopup(String type, final String content) {
        final Video video = DataUtil.create(content);
        new VideoContentPopup(Common.getLaunchVideoInfo(video));
        markContentAsCompleted(content);
    }

    protected abstract void onLoaded();

    protected abstract String mode();

    protected Builder getWidgetCloseBuilder() {
        return EmbedEvent.SELF_HELP_CLOSE.builder();
    }

    private void closeWidget(String content) {
        closeFull();
        trackWidgetClose(getWidgetCloseBuilder(), content);
    }

    private void showDeck(String content) {
        player.showDeck(content, PREFIX, Callbacks.<String> emptyCb());
    }

    public void destroy() {
        closeFull();
        hide(false);

        CrossMessager.removeListener(this, getAllListenerKeys());
        WidgetActionsTracker.destroy();
    }

    protected void trackTime() {
        if (startAt != 0) {
            final long currentTime = System.currentTimeMillis();
            Common.tracker().trackTimeInWidget(currentTime - startAt);
            startAt = 0;
        }
    }
    
    protected void trackWidgetClose(Builder builder, String content) {
        GaUtil.trackClose(builder, content, TrackerEventOrigin.SELF_HELP);
    }
    
    private void trackWidgetScroll(String content) {
        Where where = DataUtil.create(content);
        String segment_name = where.getString(Common.SEGMENT_NAME);
        String segment_id = where.getString(Common.SEGMENT_ID);
        String src_id = where.getString(Common.SRC_ID);
        ContextualInfo contextualInfo = (ContextualInfo) where.getObject(Common.CONTEXTUAL_INFO);

        Common.tracker()
                .onEvent(EmbedEvent.SELF_HELP_SCROLL.builder()
                        .segment(segment_id, segment_name).srcId(src_id)
                        .contextualInfo(contextualInfo).build());
    }

    protected void setOverFlow() {
        getElement().getStyle().setProperty("overflow", "hidden");
    }

    protected void setPosition() {
        if (settings.target() == null) {
            setFixedPosition();
        } else {
            setRelPosition();
        }
    }

    public String getPosition() {
        return settings.position();
    }

    public static String[] TRANSFORM_PROPS = new String[] { "webkitTransform",
            "MozTransform", "msTransform", "OTransform" };

    private void rotate() {
        String position = settings.position();
        rotate(label, position);
    }

    public void rotate(Widget label, String position) {
        Style labelStyle = label.getElement().getStyle();
        if (position.startsWith("l")) {
            if (getTextOrientation().equalsIgnoreCase(VERTICLE_RIGHT_LEFT)) {
                setProperty(labelStyle, "ltr", "direction");
                setProperty(labelStyle, "rotate(90deg)", TRANSFORM_PROPS);
            } else {
            setProperty(labelStyle, "rtl", "direction");
            setProperty(labelStyle, "rotate(270deg)", TRANSFORM_PROPS);
            }
        } else if (position.startsWith("r")) {
            if (getTextOrientation().equalsIgnoreCase(VERTICLE_RIGHT_LEFT)) {
                setProperty(labelStyle, "ltr", "direction");
                setProperty(labelStyle, "rotate(90deg)", TRANSFORM_PROPS);
            }else {
                setProperty(labelStyle, "rtl", "direction");
                setProperty(labelStyle, "rotate(-90deg)", TRANSFORM_PROPS);
            }
            
        }
    }

    private void setRelPosition() {
        Element elem = getElement();
        Style style = elem.getStyle();
        clearPosition(style);

        int totalWidth = clientWidth();
        int totalHeight = clientHeight();

        Element relative = null;
        try {
            relative = relativeElement(Document.get(), settings.target());
        } catch (Exception e) {
        }
        if (relative == null) {
            return;
        }

        if (settings.position().startsWith("t")) {
            setTop(style, relative.getAbsoluteBottom());
        } else {
            setBottom(style, totalHeight - relative.getAbsoluteBottom()
                    + relative.getOffsetHeight());
        }

        if (settings.position().endsWith("l")) {
            setLeft(style, relative.getAbsoluteLeft());
        } else {
            setRight(style, totalWidth - relative.getAbsoluteRight());
        }
    }

    private native static Element relativeElement(Document doc,
            String selector) /*-{
		return doc.querySelector(selector);
    }-*/;

    private void clearPosition(Style style) {
        style.clearLeft();
        style.clearRight();
        style.clearTop();
        style.clearBottom();
    }

    protected String prefix(String type) {
        return PREFIX + type;
    }

    protected void setFixedPosition() {
        Element elem = getElement();
        Style style = elem.getStyle();
        clearPosition(style);
        String position = settings.position();
        positionElement(elem, position, height, width, false);
    }

    public void positionElement(Element elem, String position, int height,
            int width, Boolean isDropZone) {

        int totalWidth = clientWidth();
        int totalHeight = clientHeight();

        Style style = elem.getStyle();
        clearPosition(style);

        if (position.startsWith("t") || position.startsWith("b")) {
            if (position.endsWith("rm")) {
                setRight(style, totalWidth - (start(position, totalWidth, width,
                        0, "rm", isDropZone) + width));
            } else {
                setLeft(style, start(position, totalWidth, width, 0, "rm",
                        isDropZone));
            }
        }

        if (position.startsWith("l") || position.startsWith("r")) {
            if (position.endsWith("bm")) {
                setBottom(style, clientHeight() - (start(position, totalHeight,
                        height, 0, "bm", isDropZone) + height));
            } else {
                setTop(style, start(position, totalHeight, height, 0, "bm",
                        isDropZone));
            }
        }

        if (position.startsWith("t")) {
            setTop(style, 0);
        } else if (position.startsWith("b")) {
            setBottom(style, 0);
        } else if (position.startsWith("l")) {
            setLeft(style, 0);
        } else {
            setRight(style, 0);
        }
    }

    protected int start(String position, int total, int pixels, int adjust,
            String threeForth, Boolean isDropZone) {
        if (isDropZone) {
            adjust = 0;
        }
        int start;
        if (position.length() == 1) {
            start = (total - pixels) / 2;
        } else if (position.endsWith(threeForth)) {
            start = total - (total / 4) - (pixels / 2);
        } else {
            start = (total / 4) - (pixels / 2);
        }

        start = start - adjust;

        if (start < 0) {
            return 0;
        } else if (start + pixels > total) {
            return total - pixels;
        } else {
            return start;
        }
    }

    private void setProperty(Style style, String value, String... names) {
        for (String name : names) {
            style.setProperty(name, value);
        }
    }

    private static void addProperty(Widget widget, String name, String value) {
        StringBuilder styles = new StringBuilder(widget.getElement()
                .getAttribute(STYLE));
        styles.append(name).append(":").append(value).append(";");
        widget.getElement().setAttribute(STYLE, styles.toString());
    }

    private void clearProperty(Style style, String... names) {
        for (String property : names) {
            style.clearProperty(property);
        }
    }

    private void setLeft(Style style, int left) {
        style.setLeft(left, Unit.PX);
    }

    private void setTop(Style style, int top) {
        style.setTop(top, Unit.PX);
    }

    private void setRight(Style style, int right) {
        style.setRight(right, Unit.PX);
    }

    private void setBottom(Style style, int bottom) {
        style.setBottom(bottom, Unit.PX);
    }

    private int clientWidth() {
        return Window.getClientWidth();
    }

    private int clientHeight() {
        return Window.getClientHeight();
    }

    private void setBackground() {
        String color = getIconBgColor();
        if (color != null) {
            String style = getElement().getAttribute(STYLE);
            if (style != null) {
                style = style + " " + OverlayUtil.BACKGROUND + ":" + color
                        + " !important;";
                getElement().setAttribute(STYLE, style);
            }
        }
    }
    
    protected String getIconBgColor() {
        return Themer.value(Themer.SELFHELP.ICON_BG_COLOR,
                settings.color());
    }

    private void setZindex() {
        String z_index = getZindex();
        if (z_index != null) {
            String style = getElement().getAttribute(STYLE);
            if (style != null) {
                style = style + " " + OverlayUtil.Z_INDEX + ":" + z_index
                        + " !important;";
                getElement().setAttribute(STYLE, style);
            }
        }
    }

    private String getZindex() {
        return settings.z_index() == null ? "2147483640" : settings.z_index();
    }

    private void setAttentionAnimation() {
        if (needAnimation()) {
            addStyleName(getAnimationStyle());
            Themer.applyTheme(this, Themer.STYLE.COLOR,
                    Themer.SELFHELP.ICON_BG_COLOR);
        }
    }

    protected boolean isAttentionEnabled() {
        return "show".equalsIgnoreCase(Themer.value(Themer.SELFHELP.ANIMATE));
    }

    /**
     * This method checks if Animation is required or no. Multiple scenarios
     * where animation is not requires are below. 1. when disable from theme 2.
     * When self help is relative to element on the page 3. when animation count
     * exausted.
     * 
     * @return
     */
    private boolean needAnimation() {
        boolean hasTarget = settings.target() == null;
        boolean countExaust = WidgetActionsTracker.getCount()
                .widgetAnimationCount() < Integer
                        .valueOf(Themer.value(Themer.SELFHELP.ANIMATE_COUNT));
        return hasTarget && countExaust;
    }

    private String getAnimationStyle() {
        String animation = Themer.value(Themer.SELFHELP.ANIMATION);
        WidgetAttentionAnimation animationType = WidgetAttentionAnimation
                .getType(animation);
        if (WidgetAttentionAnimation.RIPPLE == animationType) {
            return getRippleAnimationStyle(settings.position());
        } else if (WidgetAttentionAnimation.RADIAL == animationType) {
            return Overlay.CSS.radialAnimation();
        } else {
            return Overlay.CSS.zoomInOutAnimation();
        }
    }

    private String getRippleAnimationStyle(String position) {
        if (position.startsWith("t")) {
            return Overlay.CSS.rippleAnimationTop();
        } else if (position.startsWith("r")) {
            return Overlay.CSS.rippleAnimationRight();
        } else if (position.startsWith("b")) {
            return Overlay.CSS.rippleAnimationBottom();
        } else {
            return Overlay.CSS.rippleAnimationLeft();
        }
    }

    protected void makeDraggable() {
        getElement().setDraggable(Element.DRAGGABLE_TRUE);
        rePositionCB.setUpDragImagePanel(getWidgetLabel(settings));

        // Drag End Handler to remove the dropZone
        addDomHandler(event -> rePositionCB.onDragEnd(event),
                DragEndEvent.getType());

        // Start Drag handler
        addDomHandler(event -> rePositionCB.onDragStart(event),
                DragStartEvent.getType());

        String position = settings.position();
        if (position.startsWith("l") || position.startsWith("r")) {
            addStyleName(Overlay.CSS.widgetLauncherDrag());
        } else {
            addStyleName(Overlay.CSS.widgetLauncherDragHorizontal());
        }
    }

    public void setUnseenContent(Set<String> result) {
        this.unseenContent = result;
    }

    public Widget getLabel() {
        return label;
    }

    public void addWidgetExtension(WidgetExtension w) {
        renderer.addWidgetExtension(w);
    }

    public void removeWidgetExtension(String id) {
        renderer.removeWidgetExtension(id);
    }

    @Override
    public void showBannerNotification(Notification notification) {
        renderer.showBannerNotification(notification);
    }

    @Override
    public void hideBannerNotification(NotificationsTopic topic) {
        renderer.hideBannerNotification(topic);
    }

    @Override
    public void hideBannerNotification(NotificationsTopic topic, String closeBy) {
        renderer.hideBannerNotification(topic, closeBy);
    }

    protected void sendAnalayticsCallforContentLoaded(Settings settings) {
        Common.tracker().onEvent(EmbedEvent.WIDGET_CONTENT_LOADED.builder()
                .segment(settings.segment_id(), settings.segment_name())
                .widget(TrackerEventOrigin.SELF_HELP).currentPageTags(PlayerBase.getCurrentPageTags())
                .userDetails(((TagEnabledWidgetSettings) settings).role_tags())
                .srcId(TrackerEventOrigin.SELF_HELP).build());
    }
}
