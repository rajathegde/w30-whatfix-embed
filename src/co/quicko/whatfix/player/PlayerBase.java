package co.quicko.whatfix.player;

import static co.quicko.whatfix.ga.GaUtil.analyticsCache;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayMixed;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.ContentUtil;
import co.quicko.whatfix.common.CrossScheduler;
import co.quicko.whatfix.common.Environment;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.FlowUrlUtil;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.Pair;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.common.PlayState.BranchingInfo;
import co.quicko.whatfix.common.StateHandler;
import co.quicko.whatfix.common.StateKeys;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.Url;
import co.quicko.whatfix.common.WindowCloseManager;
import co.quicko.whatfix.common.ZMaxer;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.contextualinfo.StepCompletionInfo;
import co.quicko.whatfix.common.editor.StepCompletionActions;
import co.quicko.whatfix.common.logger.LOGConstants;
import co.quicko.whatfix.common.logger.NFRLogger;
import co.quicko.whatfix.common.logger.NFRProperty;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.AutoExecutionConfig;
import co.quicko.whatfix.data.AutoExecutionConfig.AutoExecuteInput;
import co.quicko.whatfix.data.AutoExecutionConfig.AutoExecutionResult;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.DesktopData;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.Enterprise;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.FlowAndEnt;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.PreviousStates;
import co.quicko.whatfix.data.PreviousStates.PlayStateWithPosition;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.StepPlayStatus;
import co.quicko.whatfix.data.Tag;
import co.quicko.whatfix.data.TagCache;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.data.WidgetType;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.ga.EventBusAdapter;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.Actioner.ActionerImpl;
import co.quicko.whatfix.overlay.Actioner.ActionerListener;
import co.quicko.whatfix.overlay.AutoExecGlass;
import co.quicko.whatfix.overlay.AutoFlowStopEvent;
import co.quicko.whatfix.overlay.AutoFlowSwitchToManualEvent;
import co.quicko.whatfix.overlay.BackUpFinder;
import co.quicko.whatfix.overlay.BackUpFinder.BackUpListener;
import co.quicko.whatfix.overlay.CloseableStepPop;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.Customizer.EventResponse;
import co.quicko.whatfix.overlay.Customizer.PopupKeyResponse;
import co.quicko.whatfix.overlay.LaunchTasker;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.Popover;
import co.quicko.whatfix.overlay.Popover.ClickRegister;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.StepPop;
import co.quicko.whatfix.overlay.StepStaticPop;
import co.quicko.whatfix.overlay.UserPreferences;
import co.quicko.whatfix.overlay.actioner.BodyHacks;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.player.assistant.AssistantTipPlayer;
import co.quicko.whatfix.player.beacon.BeaconPlayer;
import co.quicko.whatfix.player.beacon.event.ShowBeaconsEvent;
import co.quicko.whatfix.player.editortroubleshoot.EditorTroubleshootPlayer;
import co.quicko.whatfix.player.editortroubleshoot.event.EditorTroubleshootChangeEvent;
import co.quicko.whatfix.player.event.FlowCompletedEvent;
import co.quicko.whatfix.player.event.StartNewFlowEvent;
import co.quicko.whatfix.player.event.StepCompleteActionInState;
import co.quicko.whatfix.player.exitpopup.ExitPopPlayer;
import co.quicko.whatfix.player.exitpopup.event.ExitPopupCloseEvent;
import co.quicko.whatfix.player.gracefulexit.GracefulExitPlayer;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitPositionChangeEvent;
import co.quicko.whatfix.player.gracefulexit.event.HideGracefulExitEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitDefaultEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitMaxEvent;
import co.quicko.whatfix.player.gracefulexit.event.ShowGracefulExitMinEvent;
import co.quicko.whatfix.player.gracefulexit.event.UpdateGEModalInStater;
import co.quicko.whatfix.player.popup.NativePopupPlayer;
import co.quicko.whatfix.player.popup.PopupPlayer;
import co.quicko.whatfix.player.popup.end.EndPopupPlayer;
import co.quicko.whatfix.player.popup.end.EndPopupSeenEvent;
import co.quicko.whatfix.player.popup.end.HideEndPopupEvent;
import co.quicko.whatfix.player.popup.end.ShowEndPopupEvent;
import co.quicko.whatfix.player.popup.event.PopupEvaluateEvent;
import co.quicko.whatfix.player.popup.event.ShowLivePopupEvent;
import co.quicko.whatfix.player.popup.guided.GuidedPopupPlayer;
import co.quicko.whatfix.player.popup.guided.event.EvaluateGuidedPopupEvent;
import co.quicko.whatfix.player.popup.guided.event.NoGuidedPopupSegmentEvent;
import co.quicko.whatfix.player.popup.smart.SmartPopupPlayer;
import co.quicko.whatfix.player.popup.smart.event.EvaluateSmartPopupEvent;
import co.quicko.whatfix.player.popup.smart.event.NoSmartPopupSegmentEvent;
import co.quicko.whatfix.player.tip.SmartTipPlayer;
import co.quicko.whatfix.player.tip.event.ShowSmartTipEvent;
import co.quicko.whatfix.player.tip.event.StopStaticSmartTipEvent;
import co.quicko.whatfix.player.useraction.UserActionsPlayer;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.security.UserInfo;
import co.quicko.whatfix.service.AdminService;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.FlowTestingService;
import co.quicko.whatfix.tracker.ExitPoint;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.event.EventPayload.Builder;
import co.quicko.whatfix.tracker.useraction.event.PageUserActionsEvent;
import co.quicko.whatfix.tracker.useraction.event.UserActionCompleteEvent;
import co.quicko.whatfix.workflowengine.AppController;
import co.quicko.whatfix.workflowengine.FindListener;
import co.quicko.whatfix.workflowengine.Finder;
import co.quicko.whatfix.workflowengine.app.App;
import co.quicko.whatfix.workflowengine.app.AppFactory;
import co.quicko.whatfix.workflowengine.app.Segmenter;
import co.quicko.whatfix.workflowengine.data.finder.FinderLogger;

public class PlayerBase
        implements ActionerListener,BackUpListener, ClosingHandler, CloseHandler<Window> {
    private static final int PLAY_GAP_IN_MS = 500;
    private static final int CLOSING_RETRIES = Player.CSS.closing_retries();

    public static StateHandler stater = GWT.create(StateHandler.class);
    public static CrossScheduler scheduler = GWT.create(CrossScheduler.class);
    private PlayState state;

    private int next;
    private int skipped = 0;
    private int lastTrackedStep = 0;
    private Element lastTrackedElement = null;
    private boolean stopped = true;

    private int closingRetries = 0;
    private int playGapInMs = PLAY_GAP_IN_MS;
    private boolean closed = false;
    private boolean listeningEvents = false;
    private boolean moveOnRefresh = false;
    protected TaskerLauncher taskerLauncher;
    private boolean backButtonNeeded = false;
    private boolean isLastStepOptional = false;

    private static String shPageTags;

    private StepFindListener stepFindListener = new StepFindListener(this);
    private PreviousStates previousStates = DataUtil.create();

    protected AutoExecutionConfig autoExecutionConfig = AutoExecutionConfig.def();

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    private static final PopupPlayer popupPlayer = new PopupPlayer();
    private static final NativePopupPlayer nativePopupPlayer = new NativePopupPlayer();
    private static final SmartPopupPlayer smartPopupPlayer = new SmartPopupPlayer();
    private static final GuidedPopupPlayer guidedPopupPlayer = new GuidedPopupPlayer();
    private static final EndPopupPlayer endPopupPlayer = new EndPopupPlayer();
    private static final BeaconPlayer beaconPlayer = new BeaconPlayer();
    private static final SmartTipPlayer smartTipPlayer = new SmartTipPlayer();
    private static final EventBusAdapter ebs = new EventBusAdapter();
    private static final UserActionsPlayer userActionsPlayer = new UserActionsPlayer();
    private static final AssistantTipPlayer assistantTipPlayer = new AssistantTipPlayer();

    // Below are for Graceful Exit Players
    private static GracefulExitPlayer gracefulExitPlayer = null;
    private static ExitPopPlayer exitPopupPlayer = null;
    private static final String lastStepPlayed = "desktop_last_step_played";
    private EditorTroubleshootPlayer editorTroubleshootPlayer = null;
    //To differentiate between editorembed and embed
    protected boolean flowRunThroughEditor = false;
    private int stepShowedBeforeETInit = -1;

    public static WidgetLauncher launcher = null;

    public PlayerBase() {
        EventBus.addHandler(StartNewFlowEvent.class, this::onStartNewFlow);
        EventBus.addHandler(NoGuidedPopupSegmentEvent.class, this::onNoGuidedPopupEvent);
        EventBus.addHandler(NoSmartPopupSegmentEvent.class, this::onNoSmartPopupEvent);
        EventBus.addHandler(EndPopupSeenEvent.class, this::onEndPopupSeenEvent);
        EventBus.addHandler(FlowCompletedEvent.class, this::onFlowCompletedEvent);
        EventBus.addHandler(FlowCompletedEvent.class, this::onAfterEnd);
        EventBus.addHandler(HideGracefulExitEvent.class, this::onNextGE);
        EventBus.addHandler(ExitPopupCloseEvent.class,
                this::onExitPopupCloseEvent);
        EventBus.addHandler(UpdateGEModalInStater.class, this::onUpdateGEModalInStater);
        EventBus.addHandler(StepCompleteActionInState.class,
                this::onStepCompleteActionInState);
        EventBus.addHandler(GracefulExitPositionChangeEvent.class,
                this::onGracefulExitPositionChange);
        EventBus.addHandler(AutoFlowSwitchToManualEvent.class,
                this::onAutoFlowSwitchToManualEvent);
        EventBus.addHandler(AutoFlowStopEvent.class, this::onAutoFlowStopEvent);
        EventBus.addHandler(UserActionCompleteEvent.class,
                this::onUserActionCompleted);
        EventBus.addHandler(EditorTroubleshootChangeEvent.class,
                this::onEditorTroubleshootChange);
    }

    /**
     * Handler for {@link StartNewFlowEvent} for {@link PlayerBase}
     * Clears the currently playing flow (if any) and start a new flow.
     * 
     * @param event
     */
    public void onStartNewFlow(Event e) {
        StartNewFlowEvent event = (StartNewFlowEvent) e;
        clearAllStates();
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "onStartNewFlow",
                WfxModules.JIT, "Flow title {0}, source {1} ",
                event.getDraft().title(), event.getSrc());
        Flow flow = (Flow) event.getDraft();
        String src = event.getSrc();
        MobileUtil.startMobileWalkthrough(flow);
        // play the new flow
        // inject player is currently hard coded. The value is only modified
        // when directly calling certain methods.
        stater.setState(StateKeys.INJECT_PLAYER, "true");

        // check for flow triggered from wfx-flowid in endmessage when flow
        // has wfx_play_as_popup system tag
        if (event.getLaunchedFrom()
                .equalsIgnoreCase(TrackerEventOrigin.END_POPUP.getSrcName())
                && flow.isPlayAsPopup()) {
            PlayState state = DataUtil.create();
            state.flow(flow);
            state.src_id(src);
            launchLiveHerePopup(state);
            return;
        }
        play(flow, src);
    }

    private void clearAllStates() {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "clearAllStates",
                WfxModules.JIT,
                "Clearing state, play started, intermediate state, previous state");
        clearState();
        clearPlayStarted();
        clearIntermediateState(); // required?
        clearPreviousStates();
    }

    public void onStepCompleteActionInState(Event e) {
        StepCompleteActionInState event = (StepCompleteActionInState) e;
        String stepCompletedAction = event.getAction();
        analyticsCache.step_completion_action(stepCompletedAction);
        updatePlayStateInStater(state);
    }

    public void onNoGuidedPopupEvent(Event e) {
        EventBus.fire(new EvaluateSmartPopupEvent());
        MobileUtil.noGuidedPopup();
    }
    
    public void onNoSmartPopupEvent(Event event) {
        if (Customizer.surveyPopupEnabled()) {
            Customizer.showWfxTasklistSurveyPopup();
        }
        MobileUtil.noSmartPopup();
    }

    public static void setCurrentPageTags() {
        Enterprise ent = Enterpriser.enterprise();
        if (ent.create_page_tags()) {
            Settings settings = Enterpriser.getPageSegmentSettings();
            setCurrentPageTags(settings);
        }
    }

    public static void setCurrentPageTags(Settings settings) {
        String currentPageTags = null;
        if (settings != null) {
            JsArrayMixed filterByTags = settings.filter_by_tags();
            if (filterByTags != null && filterByTags.length() > 0) {
                // page tags are OR conditioned at first position
                currentPageTags = filterByTags.getString(0);
            }
        }
        shPageTags = currentPageTags;
    }

    public static String getCurrentPageTags() {
        return shPageTags;
    }

    public PlayState getState() {
        return state;
    }

    private void setMoveOnRefresh(boolean move) {
        moveOnRefresh = move;
    }

    public void play() {
        stater.getState(StateKeys.PLAY_STATE, new LogFailureCb<String>() {
            @Override
            public void onSuccess(String result) {
                if (StringUtils.isBlank(result)) {
                    directPlay();
                } else {
                    PlayState state = DataUtil.create(result);
                    
                    // Special case, in preview/see live Enterprise isn't
                    // initialised as
                    // the intergation script isn't loaded in this case. Further
                    // investigation needs to be done on Integration script load
                    // in case of preview and see live.
                    if (hasEditorLoaded()) {
                        Enterpriser.initialize(state.enterprise());
                    }

                    if (Enterpriser.hasFeature(
                            AdditionalFeatures.editor_flow_level_properties)) {
                        Themer.flowTheme = state.flow().flow_theme();
                    }
                    // Play as popup when a flow is tagged with
                    // wfx_play_as_popup in Preview mode and when self help's
                    // config set to open on new page with this tag.
                    if (state.flow().isPlayAsPopup()
                            && !state.flow().is_flow_playing()) {
                        launchLiveHerePopup(state);
                        return;
                    }
                    initState(result);
                    if (state.flow().needExtension()) {						
						handleExtensionTagFlow();
					}
                }
            }
        });
    }
    
    /**
	 * In case Extension tag is used with a flow, let editorEmbed handle flow play
	 * without letting embed to interfere. Ref : SUCC-3986
	 */
    protected void handleExtensionTagFlow() {
    	// handled in EditorEmbed
    }

    public boolean canDirectPlay() {
        return FlowUrlUtil.getFlowId() != null;
    }

    private native boolean hasEditorLoaded() /*-{
        return $wnd.__wfx_editor && $wnd.__wfx_editor == true;
    }-*/;

    private native boolean isPlayingTheFlow(String flowId) /*-{
        var oldFlowId = "";
        if ($wnd && '_wfx_flow_id' in $wnd) {
            oldFlowId = $wnd['_wfx_flow_id'];
        }
        $wnd['_wfx_flow_id'] = flowId;
        return oldFlowId == flowId;                                                      
    }-*/;

    public void directPlay() {
        String flowId = FlowUrlUtil.getFlowId();
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "directPlay",
                WfxModules.JIT, "FlowID is {0}", flowId);
        if (hasEditorLoaded() && isPlayingTheFlow(flowId)) {
                LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "directPlay",
                        WfxModules.JIT,
                        "already playing the same flow, flowID: {0}", flowId);
                return;
        }
        if (flowId != null) {
            String inputs = FlowUrlUtil.getParameter("_wfx_input_data");
            if (StringUtils.isNotBlank(inputs)) {
                AutoExecutionConfig config = populateAutoExecutionInput(flowId,
                        inputs);
                autoExecuteFlow(flowId, "0", config);
            } else {
                play(flowId, new AsyncCallback<Flow>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(Flow result) {
                        GaUtil.setAnalytics(result, "url");
                        if (result.isPlayAsPopup()) {
                            launchLiveHerePopup(result, "url");
                            return;
                        }
                        clearPlayStarted();
                        play(result, "url");
                    }
                });
            }
        }
    }

    private AutoExecutionConfig populateAutoExecutionInput(String flow_id,
            String input_data) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class,
                "populateAutoExecutionInput", WfxModules.JIT, "flowID: {0}",
                flow_id);
        AutoExecutionConfig config = null;
        JavaScriptObjectExt inputs = null;
        try {
            input_data = URL.decodeQueryString(input_data);
            inputs = (JavaScriptObjectExt) Common.parseString(input_data);
        } catch (Exception e) {
            return config;
        }
        config = DataUtil.create();
        config.type(AutoExecutionConfig.AUTO_EXEC);
        AutoExecuteInput input = DataUtil.create();

        String[] stepNumbers = inputs.keys();
        for (int index = 0; index < stepNumbers.length; index++) {
            String step = stepNumbers[index];
            String stepData = inputs.valueAsString(step);
            input.input(Integer.parseInt(step), stepData);
        }
        config.input(flow_id, input);
        return config;
    }

    public void play(final String flow_id, final AsyncCallback<Flow> cb) {
        if (AutoExecutionConfig.isAutoTest(autoExecutionConfig)) {
            String template = autoExecutionConfig.valueAsString("template");
            String testcase_id = autoExecutionConfig.valueAsString("testcase_id");
            String ent_id = autoExecutionConfig.valueAsString("ent_id");
            FlowTestingService.IMPL.testFlowAndEnt(flow_id, template, testcase_id, ent_id,
                    new FlowAndEntCallback(autoExecutionConfig, cb));
        } else {
            FlowService.IMPL.flowAndEnt(flow_id,
                    new FlowAndEntCallback(autoExecutionConfig, cb));
        }
    }

    public void play(Flow flow, String src_id) {
        PlayState state = DataUtil.create();
        state.flow(flow);
        play(state, src_id);
    }

    protected void autoExecuteFlow(String flow_id, String position,
            AutoExecutionConfig config) {
        stater.clearState(AutoExecutionConfig.FULFILLMENT_TEXT);

        if (!config.isValid()) {
            Console.debug("Invalid Execution Config...");
            Console.debug(config);
            return;
        }
        autoExecutionConfig = config;
        if (null == position) {
            position = "0";
        }    
        jumpTo(flow_id, position, "0",
                TrackerEventOrigin.AUTO_EXEC.getSrcName());
    }

    public void play(PlayState state, String src_id) {
        Common.tracker().initialize(Security.unq_id());
        if (Enterpriser
                .hasFeature(AdditionalFeatures.editor_flow_level_properties)) {
            Themer.flowTheme = state.flow().flow_theme();
        }
        addPlayStateProperties(state, src_id);
        updatePlayStateInStater(state);
        initState(state);
    }

    public static PlayState createPlayState(Draft flow, String src_id) {
        PlayState state = DataUtil.create();
        state.flow(flow);
        addPlayStateProperties(state, src_id);
        return state;
    }

    public static void addPlayStateProperties(PlayState state, String src_id) {
        state.src_id(src_id);
        state.test(false);
        state.unq_id(Security.unq_id());
        state.user_id(Security.user_id());
        state.user_dis_name(Security.displayName());
        state.user_name(Security.user_name());
        analyticsCache.interaction_id(GaUtil.interaction_id);
        state.enterprise(Enterpriser.enterprise());
    }

    public void refreshPlay() {
        stater.getState(StateKeys.PLAY_STATE, new AsyncCallback<String>() {
            @Override
            public void onSuccess(String result) {
                if (StringUtils.isBlank(result)) {
                    stop();
                } else {
                    initState(result);
                }
            }

            @Override
            public void onFailure(Throwable caught) {
            }
        });
    }

    private void initState(String playState) {
        PlayState state = DataUtil.<PlayState> create(playState);
        GaUtil.setAnalytics(state.analyticsInfo());
        autoExecutionConfig = state.autoExecutionConfig();
        Enterpriser.initialize(state.enterprise());
        Common.tracker().initialize(state.unq_id());
        JsArray<Tag> all_tags = state.flow().all_tags();
        if (JsUtils.isNotEmpty(all_tags)) {
            TagCache.initializeSynchronously(all_tags);
        }
        ContentType contentType = state.flow().getType();
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "initState",
                WfxModules.JIT, "contentType: {0}, title: {1}", contentType,
                state.flow().title());
        if (ContentType.tip == contentType) {
            EventBus.fire(ShowSmartTipEvent.fromDraft(state.flow()));
        } else if (ContentType.beacon == contentType) {
            EventBus.fire(new ShowBeaconsEvent(state.flow()));
        } else {
            initState(state);
        }
    }

    private void initState(PlayState playState) {
        int customPlayGap = Customizer.getPlayGapInMs();
        playGapInMs = customPlayGap >= 100 ? customPlayGap
                : PLAY_GAP_IN_MS;
        if (!stopped && !analyticsCache.has_branching()) {
            analyticsCache.addToFlowPaths(ExitPoint.FLOW_CHANGE.toString(),
                    ExitPoint.FLOW_CHANGE.toString());
            Common.tracker()
                    .onEvent(EmbedEvent.FLOW_LIVE_CHANGE.builder()
                            .changedFlowTitle(playState.flow().title())
                            .analyticsInfo(state.analyticsInfo())
                            .stepNumber(next + 1)
                            .playId(state.analyticsInfo().play_id()).build());
        }
        stopped = false;
        Common.tracker().use(playState.flow().ent_id(), playState.user_id(),
                playState.user_dis_name(), playState.user_name(),
                Enterpriser.enterprise().ga_id());

        GaUtil.setInteractionId(analyticsCache.interaction_id());
        ZMaxer.init();

        if (this.state != null && !previousStates.isEmpty()) {
            // if branching then save previous flow state
            state.clearEnterprise();
            state.deleteBranchingInfo();
            previousStates.addPreviousStateInfo(state);
            stater.setState("previous_states",
                    StringUtils.stringifyArray(previousStates.getStates()));
        }

        this.state = playState;

        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "initState",
                WfxModules.JIT, "title: {0}, source: {1}", state.flow().title(),
                state.src_id());
        Actioner.hide(ContentType.flow);
        EventBus.fire(new StopStaticSmartTipEvent(this.state));
        Overlay.CONSTANTS
                .use(Enterpriser.properties(playState.flow().locale()));

        stater.getState("play_started", new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(String result) {
                if (result.length() == 0) {
                    stater.setState("play_started",
                            Long.toString(System.currentTimeMillis()));
                    if (state.test()) {
                        return;
                    }
                    Customizer.onBeforeStart(state);
                    FlowService.IMPL.run(state.user_id(),
                            state.flow().flow_id(), state.unq_id(),
                            Callbacks.emptyVoidCb());
                    Common.tracker().onEvent(EmbedEvent.LIVE_START.builder()
                            .flow(state.flow().flow_id(), state.flow().title())
                            .userDetails(analyticsCache.role_tag())
                            .segment(analyticsCache.segment_id(),
                                    analyticsCache.segment_name())
                            .playId(analyticsCache.play_id())
                            .group(analyticsCache.group_id(),
                                    analyticsCache.group_title())
                            .contextualInfo(analyticsCache.contextual_info())
                            .srcId(state.src_id()).build());
                }
            }
        });

        analyticsCache.setFlowInformation(state.flow());
        state.autoExecutionConfig(autoExecutionConfig);
        updatePlayStateInStater(state);
        initializeSkipped();
        if (!listeningEvents) {
            initWindowCloseHandlers();
            listeningEvents = true;
        }
    }

    protected void initWindowCloseHandlers() {
        WindowCloseManager.addWindowClosingHandler(this);
        WindowCloseManager.addCloseHandler(this);
    }

    public void initializeActioner() {
        Actioner.initialize();
        Actioner.setActionerListener(this);
        Actioner.setImpl(new CrossActioner(this));
    }

    protected void livePopup(final PlayState state, final AnalyticsInfo info,
            final TrackerEventOrigin srcWidget) {
        PopupKeyResponse popupKey = Customizer.getPopupKey(state.flow().name(),
                srcWidget.getType());
        Enterpriser.checkViewCount(state.flow().flow_id(),
                null != popupKey ? popupKey.key() : null,
                new AsyncCallback<Boolean>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(Boolean result) {
                        if (result == true) {
                            EventBus.fire(new ShowLivePopupEvent(state, info,
                                    srcWidget));
                        }
                    }
                });
    }

    protected void clearPlayStarted() {
        stater.clearState("play_started");
    }

    /**
     * This method is called to clear the lastTrackedstep from embed entry. It
     * should be cleared because if the user starts a flow from self help, step
     * 1 is played and then again the user starts a different flow from self
     * help the step 1 call for the second flow wont go unless we clear it.
     * 
     * @author dhayanand
     */
    protected void clearLastTrackedStep() {
        lastTrackedStep = 0;
        lastTrackedElement = null;
    }

    private void initializeSkipped() {
        stater.getState(StateKeys.SKIPPED_STEPS, new AsyncCallback<String>() {
            @Override
            public void onSuccess(String result) {
                if (result.length() != 0) {
                    skipped = Integer.parseInt(result);
                } else {
                    skipped = 0;
                }
                initializePreviousStates();
            }

            @Override
            public void onFailure(Throwable caught) {

            }
        });
    }

    private void initializePreviousStates() {
        stater.getState(StateKeys.PREVIOUS_STATES, new AsyncCallback<String>() {
            @Override
            public void onSuccess(String result) {
                if (StringUtils.isNotBlank(result)) {
                    previousStates.setStates(DataUtil
                            .<JsArray<PlayStateWithPosition>> create(result));
                }
                
                setBackButtonStatus(state);
                findAndShowStep();
            }

            @Override
            public void onFailure(Throwable caught) {

            }
        });
    }

    private void findAndShowStep() {
        stater.getState(StateKeys.PLAY_POSITION, new AsyncCallback<String>() {
            @Override
            public void onSuccess(String result) {
                if (result.length() != 0) {
                    next = Integer.parseInt(result);
                } else {
                    next = 0;
                }
                if (shouldDisplayEditorTroubleshoot()) {
                    if (StringUtils.isBlank(getLoggedInUserId())) {
                        showEditorTroubleshoot(DataUtil.create());
                    } else {
                        AdminService.IMPL.getEditorTroubleshootPreferences(
                                getLoggedInUserId(),
                                new AsyncCallback<co.quicko.whatfix.data.UserPreferences>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // do nothing
                                    }

                                    @Override
                                    public void onSuccess(
                                            co.quicko.whatfix.data.UserPreferences result) {
                                        // result would be null only if user is
                                        // not part of any ent, in which
                                        // case done show ET
                                        if (null == result) {
                                            return;
                                        }
                                        showEditorTroubleshoot(result);
                                    }
                                });
                    }
                }
                showStep();
            }

            @Override
            public void onFailure(Throwable caught) {
            }
        });
    }
    
    private void showEditorTroubleshoot(
            co.quicko.whatfix.data.UserPreferences preference) {
        if (null == editorTroubleshootPlayer) {
            editorTroubleshootPlayer = new EditorTroubleshootPlayer();
        }
        StepPlayStatus status = state.stepPlayStatus();
        if (null == status) {
            status = DataUtil.create();
        }
        boolean isUserIdEmpty = StringUtils.isBlank(getLoggedInUserId());
        // if userid present, take view count and feedback count from api
        // response, else from localstorage
        String viewCount = isUserIdEmpty
                ? UserPreferences.getEditorTroubleshootKey(getLoggedInUserId(),
                        UserPreferences.EDITOR_TROUBLESHOOT_VIEW_COUNT)
                : preference.view_count();
        String feedbackCount = isUserIdEmpty
                ? UserPreferences.getEditorTroubleshootKey(getLoggedInUserId(),
                        UserPreferences.EDITOR_TROUBLESHOOT_FEEDBACK_COUNT)
                : preference.feedback_view_count();
        String position = UserPreferences.getEditorTroubleshootKey(
                getLoggedInUserId(),
                UserPreferences.EDITOR_TROUBLESHOOT_POSITION);
        int views = StringUtils.isBlank(viewCount) ? 0
                : Integer.parseInt(viewCount);
        int feedbacks = StringUtils.isBlank(feedbackCount) ? 0
                : Integer.parseInt(feedbackCount);
        status.view_count(views);
        status.feedback_count(feedbacks);
        if (StringUtils.isNotBlank(position)) {
            status.position(position);
        }
        state.editor_user_id(getLoggedInUserId());
        state.stepPlayStatus(status);
        editorTroubleshootPlayer.setPlayState(state);
        editorTroubleshootPlayer.setCurrentStep(next + 1);
        editorTroubleshootPlayer.makeETModal();
        // starting step getting shown before ET initialize due to the
        // preference api call
        if(stepShowedBeforeETInit != -1) {
            editorTroubleshootPlayer.sendStepFoundEvent(stepShowedBeforeETInit);
        }
        // using local storage to store counts in case of direct play
        if (isUserIdEmpty) {
            UserPreferences.setEditorTroubleshootKey(getLoggedInUserId(),
                    UserPreferences.EDITOR_TROUBLESHOOT_VIEW_COUNT,
                    Integer.toString(views + 1));
        }
    }

    private void updateAutoTestInfo(Draft flow) {
        Long currentTime = System.currentTimeMillis();
        
        autoExecutionConfig.lastStepTime(currentTime.toString());
        if (!flow.step_auto_recorded(next+1)) {
            autoExecutionConfig.lastTrackedStep(flow.original_step(next+1));
            autoExecutionConfig.failedAtFlow(flow.source_flow_id());
        } else {
            autoExecutionConfig.lastTrackedStep(next+1);
            autoExecutionConfig.failedAtFlow(flow.flow_id());
        }
    }

    private void showStep() {
        sendStepCompletionAnalytics();
        if (stopped) {
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "showStep",
                    WfxModules.JIT, "Flow is stopped");
            return;
        }
        Draft flow = state.flow();
        if (AutoExecutionConfig.isAutoTest(autoExecutionConfig)) {
            updateAutoTestInfo(flow);
        }
        if (isBranched()) {
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "showStep",
                    WfxModules.JIT,
                    "Flow: {0}, stepNo.: {1} is branching on a different flow",
                    flow.title(), (next + 1));
            AutoExecGlass.destroyAutoExecGlass();
            return;
        }

        if (analyticsCache.isNewBranch()) {
            Common.tracker()
                    .onEvent(EmbedEvent.BRANCH_LIVE_START.builder()
                            .analyticsInfo(analyticsCache)
                            .userDetails(analyticsCache.role_tag())
                            .playId(analyticsCache.play_id())
                            .branchFlowTitle(flow.title(), next + 1).build());
            Console.debug("Branching to flow: " + flow.flow_id()
                    + ", Step Number: " + (next + 1) + ", Step key: "
                    + flow.step_key(next + 1));
        }

        if (next == 0 && Enterpriser.enterprise().auto_skip_on_launch()
                && Customizer.autoSkipOnLaunch() && shPageTags != null
                && !shPageTags.isEmpty() && !analyticsCache.isNewBranch()) {
            next = skipToCurrentPageStep(flow);
            if (next != 0) {
                skipped += next;
                stater.setState(StateKeys.SKIPPED_STEPS,
                        Integer.toString(skipped), StateKeys.PLAY_POSITION,
                        Integer.toString(next));
                if(isBranched()) {
                    return;
                }
            }
        }

        if (next == flow.steps()) {
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "showStep",
                    WfxModules.JIT, "Flow completed");

            Actioner.dispose();
            EventBus.fire(FlowCompletedEvent.whenCompleted());
            Builder stepEndBuilderEvent = EmbedEvent.FLOW_LIVE_END.builder()
                    .analyticsInfo(analyticsCache)
                    .userDetails(analyticsCache.role_tag())
                    .playId(analyticsCache.play_id()).srcId(state.src_id());

            if (analyticsCache.has_branching()) {
                stepEndBuilderEvent.addBranchInformation(state.flow().title());
            }

            Common.tracker().onEvent(stepEndBuilderEvent.build());
            // Sending source_flow_id here as in case of branching we consider
            // source_flow_id as the completed flow
            analyticsCache.addToFlowPaths(
                    analyticsCache.source_flow_id() + GaUtil.COLON_SEPARATOR
                            + ExitPoint.END,
                    analyticsCache.source_flow_title() + GaUtil.COLON_SEPARATOR
                            + ExitPoint.END);
        } else {
            LOGGER.groupStart(
                    "Flow: " + state.flow().title() + " Step: " + (next + 1));
            moveOnRefresh = false;
            Actioner.show(flow, next + 1,
                    new FindListener[] { stepFindListener });

            // Fire event
            if (next == 0) {
                Customizer.onStart(state);
            }
                Customizer.onShow(state, next);
        }
    }

    private void sendStepCompletionAnalytics() {
        // firing analytics only once for a step
        if (next != 0 && StringUtils
                .isNotBlank(analyticsCache.step_completion_action())) {
            Step completedStep = state.flow().step(next);
            StepCompletionInfo stepCompletionInfo = DataUtil.create();
            stepCompletionInfo
                    .completion_action(analyticsCache.step_completion_action());
            stepCompletionInfo.completion_type(
                    !AutoExecutionConfig.isAutoExecutableStep(completedStep)
                            || analyticsCache.step_completion_action()
                                    .equals(StepCompletionInfo.GRACEFUL_EXIT)
                                            ? StepCompletionInfo.MANUAL
                                            : StepCompletionInfo.AUTO);
            ContextualInfo contextualStepComplInfo = StepCompletionInfo
                    .getStepCompletionInfo(
                    ContextualInfo.OuterKey.STEP_DETAIL, stepCompletionInfo);
            Common.tracker().onEvent(EmbedEvent.FLOW_LIVE_COMPLETE.builder()
                    .analyticsInfo(analyticsCache)
                    .playId(analyticsCache.play_id())
                    .userDetails(analyticsCache.role_tag())
                    .step(completedStep.stepId(), completedStep.description_md())
                    .contextualInfo(contextualStepComplInfo)
                    .srcId(state.src_id()).stepNumber(next).build());
            analyticsCache.step_completion_action("");
            updatePlayStateInStater(state);
        }
    }

    private boolean isBranched() {
        long sTime = System.currentTimeMillis();
        if (branch()) {
            NFRLogger.addLog(NFRProperty.BRANCHING,
                    System.currentTimeMillis() - sTime);
            backButtonNeeded = true;
            analyticsCache.addToFlowPaths(
                    state.flow().flow_id() + GaUtil.COLON_SEPARATOR
                            + GaUtil.BRANCH,
                    state.flow().title() + GaUtil.COLON_SEPARATOR
                            + GaUtil.BRANCH);
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "isBranched",
                    WfxModules.JIT, "branch is true");
            return true;
        }
        return false;
    }

    private boolean branch() {
        Draft flow = state.flow();
        BranchingInfo info = state.branchingInfo();
        String destFlowId = info.dest_flow_id();
        String destPosition = info.dest_position();
        String destStepKey = info.dest_step_key();

        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "branch",
                WfxModules.JIT, "destFlowId: {0}", destFlowId);
        if (StringUtils.isNotBlank(destFlowId)) {
            if (StringUtils.isNotBlank(destPosition)) {
                return branchTo(flow, new String[] { destFlowId, destPosition });
            }

            if (StringUtils.isNotBlank(destStepKey)) {
                if (flow.flow_id().equals(destFlowId)) {
                    int position = position(flow, destStepKey);
                    if (position == -1) {
                        return true;
                    }
                    branchToSame(flow, position);
                    return false;
                }

                branchTo(destFlowId, destStepKey, "0", state.src_id());
                return true;
            }
        }

        return branchTo(flow, branchViaScript(flow));
    }

    private boolean branchTo(Draft currentFlow, String[] destination) {
        if (destination != null) {
            if (StringUtils.isBlank(destination[0])
                    || currentFlow.flow_id().equals(destination[0])) {
                branchToSame(currentFlow, Integer.valueOf(destination[1]));
                return false;
            } else {
                int position = Integer.valueOf(destination[1]);
                if (position <= -1) {
                    position = 0;
                }
                jumpTo(destination[0], Integer.toString(position), "0",
                        state.src_id());
                return true;
            }
        }
        return false;
    }

    private String[] branchViaScript(Draft flow) {
        EventResponse targetDetails;
        if (next == flow.steps()) {
            targetDetails = Customizer.onBeforeEnd(state);
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "branchViaScript",
                    WfxModules.JIT,
                    "on last step, onBeforeEnd, targetDetails: {0}",
                    targetDetails);
        } else {
            targetDetails = Customizer.onBeforeShow(state, next);
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "branchViaScript",
                    WfxModules.JIT,
                    " on last step, onBeforeShow, targetDetails: {0}",
                    targetDetails);
        }

        if (targetDetails == null) {
            return null;
        }

        return new String[] { targetDetails.flow_id(),
                String.valueOf(targetDetails.position()) };
    }

    private void branchViaUi(Draft flow, final Step previousStep,
            final AsyncCallback<String[]> callback) {
        if (next == 0 || previousStep == null) {
            callback.onSuccess(null);
            return;
        }

        EventResponse targetDetails = Customizer.onStepComplete(state,
                (next - 1));
        if (targetDetails != null) {
            callback.onSuccess(new String[] { targetDetails.flow_id(),
                    String.valueOf(targetDetails.position()), "" });
            return;
        }

        Actioner.branchViaUI(previousStep, callback);
    }

    private void branchToSame(Draft flow, int position) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "branchToSame",
                WfxModules.JIT, "Flow: {0} to step {1}", flow.title(), (next + 1));
        if (position >= 0) {
            if (next == 0 && position > next) {
                skipped += (position - next);
                stater.setState(StateKeys.SKIPPED_STEPS,
                        Integer.toString(skipped));
            }
            next = Math.min(position, flow.steps());
            analyticsCache.addToFlowPaths(
                    state.flow().flow_id() + GaUtil.COLON_SEPARATOR
                            + GaUtil.BRANCH,
                    state.flow().title() + GaUtil.COLON_SEPARATOR
                            + GaUtil.BRANCH);
            Console.debug("Branching to same flow: " + flow.flow_id()
                    + ", Step Number: " + (next + 1) + ", Step key: "
                    + flow.step_key(next + 1));
            stater.setState(StateKeys.PLAY_POSITION, Integer.toString(next));
        }
        state.branchingInfo(null);
        updatePlayStateInStater(state);
    }

    private int position(Draft flow, String stepKey) {
        int steps = flow.steps();
        if (Step.isEndMessageStep(stepKey)) {
            return steps;
        }
        for (int index = 1; index <= steps; index++) {
            String key = String.valueOf(flow.step_key(index));
            if (stepKey.toString().equals(key)) {
                return (index - 1);
            }
        }
        return -1;
    }

    private int skipToCurrentPageStep(Draft flow) {
        try {
            int globalStep = -1;
            Map<String, Tag> tagMap = new HashMap<String, Tag>();
            String[] ids = shPageTags.split(",");
            for (String id : ids) {
                // tag cache has already been initialized while rendering
                // selfhelp for auto_segments with tags having non zero
                // flow_count
                Tag tag = TagCache.tagByIdDirect(id);
                if (tag != null) {
                    tagMap.put(id, tag);
                }
            }
            Pair<App, Segmenter> detectedApp = AppController.detectSegmenter();
            Pair<Integer, List<Step>> globalAndAllMatchingStepsPair = AppController
                    .getGlobalAndAllMatchingStepsPair(flow, tagMap, globalStep,
                            detectedApp);
            globalStep = globalAndAllMatchingStepsPair.getLeft().intValue();
            List<Step> matchingSteps = globalAndAllMatchingStepsPair.getRight();
            if (!matchingSteps.isEmpty()) {
                return AppController.getStepToPlay(matchingSteps, detectedApp);
            }
            // return globalStep if found
            return globalStep > -1 ? globalStep : 0;
        } catch (Throwable e) {
            return 0;
        }
    }

    private void trackLiveStep(final Step action, final Element possibleElement,
            final JavaScriptObjectExt stepFindData) {
        // last_tracked_step is not -1 when flow continues on the same page
        if (lastTrackedStep != 0) {
            // track with out stater data
            trackLiveOncePerStep(action, possibleElement, false, stepFindData);

        } else {
            // refresh or flow continue in new page, update last_tracked_step
            // from stater
            stater.getState(StateKeys.LAST_TRACKED_STEP,
                    new AsyncCallback<String>() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.length() != 0) {
                                lastTrackedStep = Integer.parseInt(result);
                            }
                            trackLiveOncePerStep(action, possibleElement, true,
                                    stepFindData);
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                        }
                    });
        }
    }

    private void trackLiveOncePerStep(Step action, Element possibleElement,
            boolean refresh, JavaScriptObjectExt stepFindData) {
        int step = action.step();
        // check if we should ignore analytics call
        if (lastTrackedStep == step) {
            // ignore if last tracked element is same or page refresh
            if (refresh || (lastTrackedElement != null
                    && lastTrackedElement == possibleElement)) {
                lastTrackedElement = possibleElement;
                return;
            }
        }
      
        LOGGER.log(WfxLogLevel.PERFORMANCE, PlayerBase.class,
                "trackLiveOncePerStep", WfxModules.WORKFLOW_ENGINE,
                "Time taken to find Step: {0} of FlowId: {1} is {2} ",
                action.step(), action.flow_id(),
                stepFindData.valueAsString("step_find_time"));
        NFRLogger.addLog(NFRProperty.FINDER,
                Long.parseLong(stepFindData.valueAsString("step_find_time")));
        // calculate page tags of current page
        setCurrentPageTags();
        if (step < lastTrackedStep) {
            analyticsCache.addToFlowPaths(
                    state.flow().flow_id() + GaUtil.COLON_SEPARATOR
                            + GaUtil.BACK,
                    state.flow().title() + GaUtil.COLON_SEPARATOR
                            + GaUtil.BACK);
            Common.tracker().onEvent(EmbedEvent.FLOW_LIVE_BACK.builder()
                    .analyticsInfo(analyticsCache)
                    .playId(analyticsCache.play_id())
                    .userDetails(analyticsCache.role_tag())
                    .step(action.stepId(), action.description_md())
                    .stepNumber(step).previousStepNumber(lastTrackedStep)
                    .fillStepData(action, false)
                    .fillElementData(possibleElement,
                            FinderLogger.getFinderSuccessComponent(
                                    action.step(), action.type()))
                    .currentPageTags(shPageTags).srcId(state.src_id())
                    .fillAppInfo(AppController.getAppInfo())
                    .fillFinderTimingData(stepFindData).build());

        }

        Builder stepBuilderEvent = EmbedEvent.FLOW_LIVE_STEP.builder()
                .analyticsInfo(analyticsCache).playId(analyticsCache.play_id())
                .userDetails(analyticsCache.role_tag())
                .step(action.stepId(), action.description_md()).stepNumber(step)
                .fillStepData(action, false)
                .fillElementData(possibleElement,
                        FinderLogger.getFinderSuccessComponent(action.step(),
                                action.type()))
                .currentPageTags(shPageTags).srcId(state.src_id())
                .fillAppInfo(AppController.getAppInfo())
                .fillFinderTimingData(stepFindData);

        if (analyticsCache.has_branching()) {
            stepBuilderEvent.addBranchInformation(state.flow().title());
        }
        Common.tracker().onEvent(stepBuilderEvent.build());

        lastTrackedStep = step;
        lastTrackedElement = possibleElement;
        stater.setState(StateKeys.LAST_TRACKED_STEP,
                Integer.toString(lastTrackedStep));
        analyticsCache.addToFlowPaths(action, next);
        updatePlayStateInStater(state);
    }

    protected void clearAll() {
        // end of player
        clearState();
    }

    protected void branchTo(String dest_flow_id, final String dest_step_key,
            final String skipped, final String src_id) {
        if ("-".equals(dest_flow_id)) {
            return;
        }
        play(dest_flow_id, new AsyncCallback<Flow>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Flow result) {
                int position = position(result, dest_step_key);
                if (position != -1) {
                    play(result, Integer.toString(position), skipped, src_id);
                }
            }
        });
    }

    protected void jumpTo(String flow_id, final String position,
            final String skippedSteps, final String src_id) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "jumpTo",
                WfxModules.JIT, "FlowID: {0}, Position: {1}, source: {2}",
                flow_id, position, src_id);
        play(flow_id, new AsyncCallback<Flow>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Flow result) {
                /**
                 * When a flow is ran from integration script
                 * _wfx_run("<flow_id>"), we need to clear analytics cache, as
                 * jumpTo is also called when a flow branches, hence this check
                 */
                if (!GaUtil.analyticsCache.isNewBranch()) {
                    clearPlayStarted();
                    GaUtil.resetAnalyticsCache();
                }
                play(result, position, skippedSteps, src_id);
            }
        });
    }

    protected void play(Flow flow, String position, String skippedSteps,
            final String src_id) {
        stater.setState(StateKeys.INJECT_PLAYER, "true",
                StateKeys.PLAY_POSITION, position, StateKeys.SKIPPED_STEPS,
                skippedSteps);
        play(flow, src_id);
    }

    protected void clearState() {
        skipped = 0;
        clearLastTrackedStep();
        stater.clearState(StateKeys.SKIPPED_STEPS, StateKeys.PLAY_STATE,
                StateKeys.PLAY_POSITION, StateKeys.INJECT_PLAYER,
                StateKeys.LAST_TRACKED_STEP, StateKeys.PREVIOUS_STATES,
                StateKeys.DISABLE_MOVE_STATE);
    }

    public void showDeck(String content, final String src_id,
            AsyncCallback<String> onClose) {
        PlayState state = DataUtil.create(content);
        AnalyticsInfo analyticsInfo = state.analyticsInfo();
        if (analyticsInfo != null) {
            analyticsCache.segment_name(analyticsInfo.segment_name());
            analyticsCache.segment_id(analyticsInfo.segment_id());
        }
        Common.showDeckPopup(
                Framers.deck(state.flow().flow_id(), true, true, true,
                        src_id),
                Framers.MIN_DECK_WIDTH, Framers.MIN_DECK_HEIGHT, onClose);
    }

    private static final BodyHacks HACK = GWT.create(BodyHacks.class);

    @Override
    public void onMiss(final JsArray<JavaScriptObjectExt> reason) {
        if (state.analyticsInfo().isRepeatedMissEvent()) {
            return;
        }
        if (state.test()) {
            return;
        }
        
        String auto_execution_mode = state.flow().auto_execution_mode();
        if (StringUtils.isBlank(auto_execution_mode)) {
            Step missedStep = state.flow().step(next + 1);
            if (AutoExecutionConfig.isAutoExecutableStep(missedStep)) {
                auto_execution_mode = AutoExecutionConfig.AUTO_EXEC;
            }
        }
        
        sendMissEventAnalytics(reason);

        if (!state.flow().manual_execution_mode()
                && null != auto_execution_mode) {

            String status = "failed",
                    output = null != reason ? reason.toString() : "";
            String fulfillmentText = AutoExecutionResult
                    .make(auto_execution_mode, status, output).toJson();
            if (AutoExecutionConfig.isAutoTest(autoExecutionConfig)) {
                setFulfillmentText(AutoExecutionConfig.FULFILLMENT_TEXT,
                        AutoExecutionResult
                                .make(auto_execution_mode, status, output,
                                        autoExecutionConfig.lastTrackedStep(),
                                        autoExecutionConfig.lastStepTime(),
                                        autoExecutionConfig.failedAtFlow(),
                                        autoExecutionConfig.template())
                                .toJson());
            } else {
                stater.setState(AutoExecutionConfig.FULFILLMENT_TEXT,
                        fulfillmentText);
                showGracefulExitPopup();
            }
            return;
        }

        updatePlayStateInStater(state);

        // Fire an event.
        Customizer.onMiss(state, next);
        showGracefulExitPopup();
    }
    
    
    private void showGracefulExitPopup() {
        if (shouldDisplayGracefulExit()
                && Finder.validateContext(state.flow().step(next + 1))) {
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "onMiss",
                    WfxModules.JIT, "Launching Flow-on-standby for stepNo: ",
                    (next + 1));
            tryAlternateWayForStep();
        }
    }

    protected void sendFailureMsgToEditorTroubleshoot() {
        if (null != editorTroubleshootPlayer
                && shouldDisplayEditorTroubleshoot()) {
            String errorReason = getStepMissReason();
            updateStepState(next + 1,
                    EditorTroubleshootPlayer.Events.STEP_FAILED.getEvent(),
                    errorReason);
            editorTroubleshootPlayer.sendStepFailureEvent(next + 1, errorReason);
        }
    }
    
    public String getStepMissReason() {
        String stepUrl = state.flow().step_url(next + 1);
        Url url = new Url(stepUrl);
        if (!Window.Location.getHostName().equals(url.getHost())) {
            return StepFailureReason.DIFFERENT_URL.getString();
        } else if (Enterpriser.enterprise().rule_config_finder()
                .length() == 0) {
            return StepFailureReason.FINDER_NOT_CONFIGURED.getString();
        } else {
            return StepFailureReason.ELEMENT_NOT_IDENTIFIED.getString();
        }
    }

    /**
     * onMiss analytics event is sent using this method
     * 
     * @param reason
     */
    private void sendMissEventAnalytics(
            final JsArray<JavaScriptObjectExt> reason) {
        FlowService.IMPL.miss(state.user_id(), state.flow().flow_id(),
                state.unq_id(), Callbacks.emptyVoidCb());

        // calculate page tags of current page
        setCurrentPageTags();
        EmbedEvent event;
        if (next + 1 < lastTrackedStep) {
            event = EmbedEvent.FLOW_LIVE_BACK_AND_MISS;
        } else {
            event = EmbedEvent.FLOW_LIVE_MISS;
        }
        Builder stepMissBuilderEvent = event.builder()
                .analyticsInfo(analyticsCache).playId(analyticsCache.play_id())
                .stepNumber(next + 1)
                .fillStepData(state.flow().step(next + 1), true)
                .currentPageTags(shPageTags).fillElementData(null, reason)
                .srcId(state.src_id()).fillAppInfo(AppController.getAppInfo());

        if (analyticsCache.has_branching()) {
            stepMissBuilderEvent.addBranchInformation(state.flow().title());
        }
        Common.tracker().onEvent(stepMissBuilderEvent.build());
        analyticsCache.addToFlowPaths(
                state.flow().step(next + 1).stepId() + GaUtil.COLON_SEPARATOR
                        + ExitPoint.MISS,
                state.flow().title() + GaUtil.COLON_SEPARATOR + "step"
                        + (next + 1) + GaUtil.COLON_SEPARATOR + ExitPoint.MISS);
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "onMiss",
                WfxModules.JIT, "Flow: {0}, stepNo: {1}", state.flow().title(),
                (next + 1));
    }

    private void doGracefulExit(Draft flow) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "doGracefulExit",
                WfxModules.JIT, "Flow: {0}, stepNo: {1}", flow.title(),
                (next + 1));
        Console.debug("Graceful Exit Called for flow: " + flow.title()
                + ", step no.: " + (next + 1));
        initializePlayers(flow);
        stater.getState(StateKeys.PLAY_STATE, new AsyncCallback<String>() {
            @Override
            public void onSuccess(String result) {
                PlayState state = DataUtil.create(result);
                String modalType = state.modalType();
                if (modalType == null
                        || modalType == GracefulExitEnum.DEFAULT.getString()) {
                    EventBus.fire(new ShowGracefulExitDefaultEvent(flow,
                            next + 1, true));
                } else {
                    try {
                        GracefulExitEnum type = GracefulExitEnum
                                .valueOf(modalType.toUpperCase());
                        switch (type) {
                            case MAX:
                                EventBus.fire(new ShowGracefulExitMaxEvent(flow,
                                        next + 1, true));
                                break;
                            case MIN:
                                EventBus.fire(new ShowGracefulExitMinEvent(flow,
                                        next + 1, true));
                                break;
                            default:
                                break;
                        }
                    } catch (IllegalArgumentException e) {
                        Console.debug(
                                "Setting Graceful Exit Modal Type Failed: "
                                        + e.getCause());
                    }
                }
            }

            @Override
            public void onFailure(Throwable caught) {
            }
        });
    }

    private static void initializePlayers(Draft flow) {
        if (gracefulExitPlayer == null) {
            gracefulExitPlayer = new GracefulExitPlayer();
        }
        if (exitPopupPlayer == null) {
            exitPopupPlayer = new ExitPopPlayer(flow.locale());
        }
    }

    public void onUpdateGEModalInStater(Event e) {
        UpdateGEModalInStater event = (UpdateGEModalInStater) e;
        if (state != null) {
            state.modalType(event.getModalType());
            stater.setState(StateKeys.PLAY_STATE,StringUtils.stringifyObject(state));
        }
    }

    protected void tryAlternateWayForStep() {
        Draft flow = state.flow();
        BackUpFinder.tryAlternateWayForStep(flow, next + 1);
    }

    @Override
    public void showPopUp(Draft flow) {
        doGracefulExit(flow);
    }

    @Override
    public void onPopupShow() {
        EmbedEvent event = EmbedEvent.FLOW_HEAL_STEP;
        Common.tracker().onEvent(event.builder()
                .analyticsInfo(analyticsCache).playId(analyticsCache.play_id())
                .stepNumber(next + 1).srcId(state.src_id()).build());
        GaUtil.analyticsCache.addToFlowPaths(
                state.flow().step(next + 1).stepId() + GaUtil.COLON_SEPARATOR
                        + "autoheal",
                state.flow().title() + GaUtil.COLON_SEPARATOR + "step"
                        + (next + 1) + GaUtil.COLON_SEPARATOR + "autoheal");
        updatePlayStateInStater(state);

    }

    @Override
    public void onNextAfterMiss(Step step) {
        EmbedEvent event = EmbedEvent.FLOW_HEALED_STEP;
        Common.tracker()
                .onEvent(event.builder().analyticsInfo(analyticsCache)
                        .playId(analyticsCache.play_id()).stepNumber(next + 1)
                        .srcId(state.src_id())
                        .stepMarks(StringUtils.stringifyObject(step.marks()))
                        .build());
        GaUtil.analyticsCache.addToFlowPaths(
                state.flow().step(next + 1).stepId() + GaUtil.COLON_SEPARATOR
                        + "autohealed",
                state.flow().title() + GaUtil.COLON_SEPARATOR + "step"
                        + (next + 1) + GaUtil.COLON_SEPARATOR + "autohealed");
        updatePlayStateInStater(state);
        Draft flow = state.flow();
        onNext(flow.step(next + 1));
    }

    @Override
    public boolean validateStepMarks(Step clickedStep) {
        Step capturedStep = state.flow().step(next + 1);
        return Finder.validateStepMarks(capturedStep, clickedStep);
    }


    @Override
    public void onShowed(Element shown) {
        if (shouldDisplayEditorTroubleshoot()) {
            if (null != editorTroubleshootPlayer) {
                editorTroubleshootPlayer.sendStepFoundEvent(next + 1);
            } else {
                // ET not initialized till now. Save this state to be sent when
                // it gets initialized
                stepShowedBeforeETInit = next + 1;
            }
            updateStepState(next + 1,
                    EditorTroubleshootPlayer.Events.STEP_FOUND.getEvent(),
                    "");
        }
        Customizer.onAfterShow(state, next);
        if (Customizer.delayStateClearance()) {
            clearIntermediateState();
        }
        if (AutoExecutionConfig.isAutoTest(autoExecutionConfig)) {
            Customizer.triggerSnapshot();
        }
        moveOnRefresh = state.flow().step_action(next + 1) == 3;
        if (moveOnRefresh) {
            final String url = Window.Location.getHref();
            scheduler.scheduleFixedDelay(new RepeatingCommand() {
                @Override
                public boolean execute() {
                    if (!url.equals(Window.Location.getHref())) {
                        handleRefresh();
                        return false;
                    } else {
                        return true;
                    }
                }
            }, 1000);
        }
    }

    // This function is called when next on Graceful Exit Modal is clicked, to
    // completion of the step and moving to next step
    private void onNextGE(Event e) {
        HideGracefulExitEvent event = (HideGracefulExitEvent) e;
        if (event.isNext()) {
            EventBus.fire(new StepCompleteActionInState(
                    StepCompletionInfo.GRACEFUL_EXIT));
            onNext();
        }
    }

    @Override
    public void onNext() {
        onNext(null);
    }

    @Override
    public void onBack() {
        onBack(null);
    }

    @Override
    public void onNext(Step previousStep) {
        LOGGER.groupEnd();
        int current = next;

        next = next + 1;
        Actioner.hide(state.flow(), next);
        if (next > 0 && !isLastStepOptional) {
            // save previous play position
            previousStates.push(next - 1);
        }
        final boolean wasLastStepOptional = isLastStepOptional;
        isLastStepOptional = false;
        AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {
            @Override
            public void onSuccess(String[] destination) {
                showStep(destination,getPlayGapinMs());
                // Fire an event
                Customizer.onNext(state, current);
                if (null != editorTroubleshootPlayer
                        && shouldDisplayEditorTroubleshoot()) {
                    if (wasLastStepOptional) {
                        editorTroubleshootPlayer.sendStepSkippedEvent(next);
                        updateStepState(next,
                                EditorTroubleshootPlayer.Events.STEP_SKIPPED
                                        .getEvent(),
                                "");
                    } else if (!state.stepPlayStatus().step_status(next)
                            .equals(EditorTroubleshootPlayer.Events.STEP_FAILED
                                    .getEvent())) {
                        // in case last step failed but was flow proceeded next via
                        // custom api
                        editorTroubleshootPlayer.sendStepPlayedEvent(next);
                        updateStepState(next,
                                EditorTroubleshootPlayer.Events.STEP_PLAYED
                                        .getEvent(),
                                "");
                    }
                    if (!isBranched()) {
                        editorTroubleshootPlayer.sendStepStartEvent(next + 1);
                    } else {
                        editorTroubleshootPlayer.closeEditorTroubleshoot();
                    }
                }
            }

            @Override
            public void onFailure(Throwable caught) {

            }
        };
        branchViaUi(state.flow(), previousStep, callback);
    }
    
    private void showStep(String[] destination,int playGap) {
        savePosition(state, next, skipped, destination, previousStates);

        scheduler.scheduleFixedDelay(new RepeatingCommand() {
            @Override
            public boolean execute() {
                if (closed) {
                    return false;
                } else if (closingRetries != 0) {
                    // the scheduler waits for some time before
                    // displaying the
                    // next step
                    // in case we anticipated a page reload, but it
                    // din't happen
                    // ex: click on an anchor tag
                    closingRetries--;
                    return true;
                } else {
                    showStep();
                }
                return false;
            }
        }, playGap);
    }

    @Override
    public void onBack(Step previousStep) {
        Actioner.hide(state.flow(), next + 1);

        PlayStateWithPosition previousState = previousStates.pop();
        next = previousState.getPlayPosition();
        if (previousState.getPlayState() != null) {
            previousState.getPlayState().enterprise(state.enterprise());
            state = previousState.getPlayState();
            stater.setState("play_state", StringUtils.stringifyObject(state));
        }
        if (null != editorTroubleshootPlayer
                && shouldDisplayEditorTroubleshoot()) {
            editorTroubleshootPlayer.sendStepStartEvent(next + 1);

            StepPlayStatus status = state.stepPlayStatus();
            if (null != status) {
                status.step_status(next + 2, null);
                state.stepPlayStatus(status);
                stater.setState(StateKeys.PLAY_STATE,
                        StringUtils.stringifyObject(state));
            }
        }
        showStep(null,playGapInMs);

    }
    /*
     * This method returns the Applicable Delay before Step show
     */
    private int getPlayGapinMs() {
        int findPlayGap;
        if (this.getCurrentStep() != null
                && this.getCurrentStep().step_delay() > 0) {
            int delay = this.getCurrentStep().step_delay();
            findPlayGap = delay * 1000;
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "getPlayGapinMs",
                    WfxModules.JIT, "Applying Step delay of {0} milliseconds",
                    findPlayGap);
        } else {
            findPlayGap = playGapInMs;
        }
        return findPlayGap;
    }

    @Override
    public void onOptionalNext() {
        saveSkipped();
        onNext();
    }

    protected void savePosition(PlayState state, int next, int skipped,
            String[] branchDest, PreviousStates previousStates) {
        stater.setState(StateKeys.PLAY_POSITION, Integer.toString(next),
                StateKeys.PREVIOUS_STATES,
                StringUtils.stringifyArray(previousStates.getStates()));
        if (branchDest != null) {
            state.branchingInfo(BranchingInfo.create(branchDest));
            stater.setState(StateKeys.PLAY_STATE,
                    StringUtils.stringifyObject(state));
        }
    }

    protected void saveSkipped() {
        isLastStepOptional = true;
        if (next == skipped) {
            skipped = skipped + 1;
            stater.setState(StateKeys.SKIPPED_STEPS, Integer.toString(skipped));
        }
    }

    @Override
    public boolean branchBeforeShow(Step step, Draft draft) {
        EventResponse targetDetails = Customizer.onBeforeTipShow(state, next);
        if (targetDetails == null) {
            return false;
        }
        Actioner.hide(state.flow(), next + 1);
        Draft currentFlow = state.flow();
        branchTo(currentFlow, new String[] { targetDetails.flow_id(),
                String.valueOf(targetDetails.position()) });
        if (currentFlow.flow_id().equals(targetDetails.flow_id())) {
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "branchBeforeShow",
                    WfxModules.JIT, "branching to same flow");
            showStep();
        }
        return true;
    }

    @Override
    public void onWindowClosing(ClosingEvent event) {
        handleRefresh();
        int customClosingRetries = Customizer.getClosingRetries();
        closingRetries = customClosingRetries > 0 ? customClosingRetries
                : CLOSING_RETRIES;
    }

    private void handleRefresh() {
        if (!moveOnRefresh) {
            return;
        }
        moveOnRefresh = false;
        Draft flow = state.flow();
        if (next != flow.steps()) {
            analyticsCache.step_completion_action(
                    StepCompletionActions.ON_PAGE_REFRESH.getAction());
            updatePlayStateInStater(state);
            onNext();
        }
    }

    @Override
    public void onClose(CloseEvent<Window> event) {
        closed = true;
    }

    @Override
    public void onClose() {
        onClose(false);
    }

    public void onClose(boolean apiClose) {
        if (stopped) {
            return;
        }
        stop();

        // onClose called using _wfx_close_live()
        if (apiClose) {

            final Builder stepCloseBuilderEvent = EmbedEvent.FLOW_LIVE_STOP
                    .builder();
            /*
             * lastTrackedElement is needed for GA. But since GA is async with
             * clearState() this may get cleared before GA call. So we store it
             * temporarily.
             */
            final Element persistedLastTrackedElement = lastTrackedElement;

            /*
             * On page refresh, if _wfx_is_live and in onBeforeShow of next step
             * if we call _wfx_close_live(), then lastTrackedStep is 0 as it is
             * initialized only in postFind() of Step. So we handle this case
             * here
             */
            stater.getState(StateKeys.LAST_TRACKED_STEP,
                    new AsyncCallback<String>() {
                        @Override
                        public void onSuccess(String result) {
                            int lastTrackedStep = (StringUtils.isBlank(result)
                                    ? 0
                                    : Integer.parseInt(result));

                            performGACall(stepCloseBuilderEvent,
                                    lastTrackedStep,
                                    persistedLastTrackedElement);
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                        }
                    });
        } else {
            final Builder stepCloseBuilderEvent = EmbedEvent.FLOW_LIVE_CLOSE
                    .builder();
            performGACall(stepCloseBuilderEvent, lastTrackedStep,
                    lastTrackedElement);
        }
        Customizer.onClose(state, next);
        EventBus.fire(FlowCompletedEvent.asClosedAbruptly());        
    }
    
    public void onEndPopupSeenEvent(Event event) {
        closeTheFlow();
    }

    public void onFlowCompletedEvent(Event e) {
        FlowCompletedEvent event = (FlowCompletedEvent) e;
        boolean completed = event.didComplete();
        //for the native desktop part to clear step state from cache
        CrossMessager.sendMessage(lastStepPlayed, "");

        if (isEndPopupConfiguredInTheme() && completed && isNotAutoTest()) {
            EventBus.fire(new ShowEndPopupEvent(completed, state.flow(),
                    state.draft()));
        } else if (isEndPopupPresentInCustomizer()) {
            // irrespective of whether flow completes, if customizer says it,
            // then show
            EventBus.fire(new ShowEndPopupEvent(completed, state.flow(),
                    state.draft()));
        } else {
            EventBus.fire(new HideEndPopupEvent());
            closeTheFlow();
        }
    }
    
    private boolean isEndPopupConfiguredInTheme() {
        return "show".equalsIgnoreCase(Themer.value(Themer.ENDPOP.END_SHOW));
    }
    
    private boolean isEndPopupPresentInCustomizer() {
        return Customizer.getMessageOnClose();
    }
    
    private boolean isNotAutoTest() {
        return !AutoExecutionConfig
                .isAutoTest(state.flow().auto_execution_mode());
    }
    
    public void onAfterEnd(Event e) {
        FlowCompletedEvent event = (FlowCompletedEvent) e;
        afterOnEnd(event.didComplete());
    }

    private void closeTheFlow() {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "closeTheFlow",
                WfxModules.JIT, "Stopping the flow");
        Customizer.onEnd(state);
        stopped = true;
        if (moveOnRefresh) {
            setMoveOnRefresh(false);
        }
        clearAll();
    }

    public void clearPreviousStates() {
        previousStates.clear();
        stater.clearState(StateKeys.PREVIOUS_STATES);
    }

    private void performGACall(Builder stepCloseBuilderEvent,
            int lastTrackedStep, Element lastTrackedElement) {

        stepCloseBuilderEvent.analyticsInfo(analyticsCache)
                .playId(analyticsCache.play_id()).stepNumber(lastTrackedStep)
                .srcId(state.src_id()).currentPageTags(shPageTags)
                .fillStepData(state.flow().step(lastTrackedStep), false)
                .fillElementData(lastTrackedElement, null)
                .fillAppInfo(AppController.getAppInfo());

        if (analyticsCache.has_branching()) {
            stepCloseBuilderEvent.addBranchInformation(state.flow().title());
        }
        Common.tracker().onEvent(stepCloseBuilderEvent.build());

        analyticsCache.addToFlowPaths(getFlowPathIdStr(lastTrackedStep),
                state.flow().title() + GaUtil.COLON_SEPARATOR + "step"
                        + (next + 1) + GaUtil.COLON_SEPARATOR
                        + ExitPoint.CLOSE);
    }

    private String getFlowPathIdStr(int lastTrackedStep) {
        if (lastTrackedStep == 0) {
            return state.flow().flow_id() + ":" + "0" + GaUtil.COLON_SEPARATOR
                    + ExitPoint.CLOSE;
        } else {
            return state.flow().step(lastTrackedStep).stepId()
                    + GaUtil.COLON_SEPARATOR + ExitPoint.CLOSE;
        }
    }

    public boolean isStopped() {
        return stopped;
    }

    public native void removeFlowIdFromWindow() /*-{
        $wnd['_wfx_flow_id'] = "";
    }-*/;
    
    public void stop() {
        removeFlowIdFromWindow();
        stopped = true;
        Actioner.dispose();
        Actioner.hide(state.flow(), next + 1);
        EventBus.fire(new HideEndPopupEvent());
        if (null != editorTroubleshootPlayer) {
            editorTroubleshootPlayer.closeEditorTroubleshoot();
        }
    }
    
    protected void initLaunchers() {
        Bootstrapper.initLaunchers(this);
    }

    public void tryBeaconCollectionSegments() {
        final AsyncCallback<Draft> beaconStartCb = new AsyncCallback<Draft>() {
            @Override
            public void onFailure(Throwable caught) {
                onSuccess(null);
            }

            @Override
            public void onSuccess(final Draft beaconCollection) {
                if (beaconCollection != null) {
                    EventBus.fire(new ShowBeaconsEvent(beaconCollection));
                }
            }
        };

        getBeaconDraft(beaconStartCb);
    }

    protected void getBeaconDraft(final AsyncCallback<Draft> beaconStartCb) {
        beaconStartCb.onSuccess(null);
    }

    protected boolean hasLaunchers() {
        return hasWidgetLauncher() || hasTaskerLauncher();
    }

    protected boolean hasLaunchers(WidgetType type) {
        if (type == null) {
            return hasLaunchers();
        }
        switch (type) {
            case SELF_HELP:
                return hasWidgetLauncher();
            case TASK_LIST:
                return hasTaskerLauncher();
            case POP_UP:
                return isPopupActive();
            default:
                return hasLaunchers();
        }
    }

    public static void destroyLaunchers(String[] types, String content) {
        if (types != null) {
            for (String type : types) {
                /**
                 * Popups require "cross_" to be sent with the message for it to
                 * be closed. Hence adding this for destroy actions of popup
                 */
                String messageContent = Arrays
                        .asList(WidgetType.POP_UP.getDestroyAction())
                        .contains(type)
                                ? GaUtil.PopupCloseOption.cross.toString() + "_"
                                : content;
                CrossMessager.sendMessage(type, messageContent);
            }
        }
    }

    protected boolean hasWidgetLauncher() {
        return Document.get().getElementById(BaseWidget.WIDGET_ID) != null
                || Document.get()
                        .getElementById(BaseWidget.LAUNCHER_ID) != null;
    }

    public boolean hasInplaceLauncher() {
        return Document.get().getElementById(IPWidget.WIDGET_ID) != null
                || Document.get().getElementById(IPWidget.LAUNCHER_ID) != null;
    }

    public void initWidgetLauncher() {
        if (AppFactory.isMobileApp() && !MobileUtil.pageIsForSelfHelp()) {
            return; // Popup is not allowed in this webview of mobile.
        }
        
        try {
            if (hasWidgetLauncher()) {
                return;
            }
            LOGGER.createGroupLogsforKey(
                    LOGConstants.PLAYER_BASE_WIDGET_LAUNCHER_GROUP);
            long sTime = System.currentTimeMillis();
            Settings settings = widgetSettings();
            LOGGER.putGroupLogsforKey(
                    LOGConstants.PLAYER_BASE_WIDGET_LAUNCHER_GROUP,
                    LOGGER.getLogMessage(
                    WfxLogLevel.PERFORMANCE, PlayerBase.class,
                            LOGConstants.PLAYER_BASE_INIT_WIDGET_METHOD,
                            WfxModules.JIT,
                    "widgetSettings, Time taken to get WidgetSettings : {0} ms",
                    (System.currentTimeMillis() - sTime)));

            if (settings != null) {
                MobileUtil.createSelfHelp();
                launcher = new WidgetLauncher(this, settings);
                long sTime1 = System.currentTimeMillis();
                launcher.showWidget(settings);
                LOGGER.putGroupLogsforKey(
                        LOGConstants.PLAYER_BASE_WIDGET_LAUNCHER_GROUP,
                        LOGGER.getLogMessage(WfxLogLevel.PERFORMANCE,
                                PlayerBase.class,
                                LOGConstants.PLAYER_BASE_INIT_WIDGET_METHOD,
                                WfxModules.JIT,
                                "showWidget, Time taken by showWidget : {0} ms",
                                (System.currentTimeMillis() - sTime1)));
            } else {
                MobileUtil.noSelfHelp();
            }
            LOGGER.flushGroupLogsforKey(
                    LOGConstants.PLAYER_BASE_WIDGET_LAUNCHER_GROUP);
        } catch (Exception e) {
            Console.debug("Error in SelfHelp: " + e.getMessage());
        }
    }

    public boolean hasTaskerLauncher() {
        // can't check as hasWidgetLauncher() because taskerLauncher
        // might have got initialized,but may not have been attached to page
        return (taskerLauncher != null && taskerLauncher.isActive());
    }

    public void initTaskerLauncher() {
        initTaskerLauncher(true);
    }
    
    public void initTaskerLauncher(boolean shouldEnableNudge) {
        if (AppFactory.isMobileApp() && !MobileUtil.pageIsForTaskList()) {
            return; // Popup is not allowed in this webview of mobile.
        }

        try {
            if (hasTaskerLauncher()) {
                return;
            }
            long sTime = System.currentTimeMillis();
            Settings settings = taskerSettings();
            NFRLogger.addLog(NFRProperty.TASK_LIST,
                    System.currentTimeMillis() - sTime);
            if (settings != null) {
                taskerLauncher = TaskerLauncherFactory.getInstance()
                        .getTaskerLauncher(this, settings);
                taskerLauncher.initialize(new AsyncCallback<Void>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // Do nothing
                    }

                    @Override
                    public void onSuccess(Void result) {
                        // When nudge is enabled and flow is not present on page
                        if (shouldEnableNudge && isStopped()) {
                            taskerLauncher.handleTaskerNudge();
                        }
                    }
                });
            } else {
                MobileUtil.noTaskList();
            }
        } catch (Exception e) {
            Console.debug("Error in TaskList: " + e.getMessage());
        }
    }
    
    protected boolean isPopupActive() {
        return isGuidedPopupActive() || isSmartPopupActive()
                || isNewPopupActive() || isNativePopupActive();
    }
    
    private boolean isGuidedPopupActive() {
        return Document.get().getElementById(
                Common.POPUP_ID_PREFIX + GuidedPopupPlayer.GUIDED_POPUP_ID_SUFFIX) != null;
    }

    private boolean isSmartPopupActive() {
        return Document.get().getElementById(
                Common.POPUP_ID_PREFIX + SmartPopupPlayer.SMART_POPUP_ID_SUFFIX) != null;
    }
    
    private boolean isNewPopupActive() {
        return Document.get().getElementById(
                Common.POPUP_ID_PREFIX + PopupPlayer.POPUP_ID_SUFFIX) != null;
    }

    private boolean isNativePopupActive() {
        return Document.get().getElementById(Common.POPUP_ID_PREFIX
                + NativePopupPlayer.NATIVE_POPUP_ID_SUFFIX) != null;
    }

    protected Settings widgetSettings() {
        return settings("_widget_wfx_");
    }

    protected Settings taskerSettings() {
        return settings("_tasker_wfx_");
    }

    protected Settings settings(String param) {
        String settings = Window.Location.getParameter(param);
        if (settings == null) {
            return null;
        } else if (settings.length() == 0) {
            return DataUtil.create();
        } else {
            Settings set = DataUtil.create(settings);
            if (!canLiveHere() && "live_here".equals(set.mode())) {
                set.mode("live");
            }
            return set;
        }
    }

    protected boolean canLiveHere() {
        return false;
    }

    protected Settings beaconSettings() {
        return null;
    }
    

    /**
     * <pre>
     * Evaluation of Smart/Guided Popups
     * <b>Algo:</b>
     * 1. If there is Guided Popup, show Guided Popup.
     * 2. Go for SmartPopup only if there is no Evaluated
     *    Guided Popups.
     * 3. Try Smart Popup if '2' satisfies.
     * 4. If '3', then Show evaluated Smart Popup
     * </pre>
     * 
     * @author anoosh
     */
    public void tryOnboarding() {
        if (AppFactory.isMobileApp() && !MobileUtil.pageIsForPopup()) {
            return; // Popup is not allowed in this webview of mobile.
        }
        
        try {
            String flow_id = natLivePopupId();
            if (flow_id != null && flow_id.length() != 0) {
                return; // Native Popup already present
            }
            // fire new popup event if feature enabled
            if (Enterpriser.hasFeature(AdditionalFeatures.ot_newpopup)) {
                EventBus.fire(new PopupEvaluateEvent());
            } else {
                EventBus.fire(new EvaluateGuidedPopupEvent());
            }
        } catch (Exception e) {
            Console.debug("Error in Onboarding Popup: " + e.getMessage());
        }
    }

    protected static native String natLivePopupId() /*-{
        return $wnd._wfx_flow_popup;
    }-*/;

    
    private void afterOnEnd(boolean completed) {
        if (completed) {
            // Ideally source_flow_id must be taken from state, but since
            // source_flow_id in state
            // is not getting set appropriately everywhere, taking it from
            // analyticsCache
            markContentCompleted(state.user_id(),
                    analyticsCache.source_flow_id());
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "afterOnEnd",
                    WfxModules.JIT, "Source Flow ID: {0}",
                    analyticsCache.source_flow_id());
        }
        if(StringUtils.isNotBlank(autoExecutionConfig.type())) {
            setFulfillmentText();
        }
    }

    private void setFulfillmentText() {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "setFulfillmentText",
                WfxModules.JIT, "Source Flow ID: {0}",
                analyticsCache.source_flow_id());
        String fulfillmentText = JsUtils
                .getTextFromHtml(state.flow().description_md());
        if (AutoExecutionConfig.isAutoTest(autoExecutionConfig)) {
            setFulfillmentText(AutoExecutionConfig.FULFILLMENT_TEXT,
                    AutoExecutionResult
                            .make(autoExecutionConfig.type(), "success",
                                    fulfillmentText,
                                    autoExecutionConfig.lastTrackedStep(),
                                    autoExecutionConfig.lastStepTime(),
                                    autoExecutionConfig.failedAtFlow(),
                                    autoExecutionConfig.template())
                            .toJson());
        } else {
            stater.setState(AutoExecutionConfig.FULFILLMENT_TEXT,
                    AutoExecutionResult.make(autoExecutionConfig.type(),
                            "success", fulfillmentText).toJson());
        }
    }

    private native void setFulfillmentText(String key, String value) /*-{
        $wnd.sessionStorage.setItem(key, value);
    }-*/;

    public void markContentCompleted(String contentId) {
        markContentCompleted(Security.user_id(), contentId);
    }

    private void markContentCompleted(String userId, String contentId) {
    	UserInfo unqId=  Security.unq_id_with_enc_status(AppUser.getUserId());
    	
        FlowService.IMPL.completed(userId, contentId,
                unqId , Callbacks.emptyVoidCb(), hasTaskerLauncher());

        if (hasTaskerLauncher()) {
            CrossMessager.sendMessage("tasker_update", contentId);
        }
    }

    private final class FlowAndEntCallback
            implements AsyncCallback<FlowAndEnt> {

        private final AsyncCallback<Flow> cb;
        private final AutoExecutionConfig config;

        private FlowAndEntCallback(AutoExecutionConfig config,
                AsyncCallback<Flow> cb) {
            this.config = config;
            this.cb = cb;
        }

        @Override
        public void onFailure(Throwable caught) {
            cb.onFailure(caught);
        }

        @Override
        public void onSuccess(FlowAndEnt result) {
            Enterpriser.initialize(result.enterprise());
            Flow flow = result.flow();
            if (StringUtils.isNotBlank(autoExecutionConfig.type())) {
                AutoExecutionConfig.setInputsToFlow(config, flow);
            }
            cb.onSuccess(flow);
        }
    }

    private static class CrossActioner extends ActionerImpl {
        private PlayerBase player;

        public CrossActioner(PlayerBase player) {
            this.player = player;
        }

        @Override
        public void scheduleFixedDelay(RepeatingCommand cmd, int delayMs) {
            scheduler.scheduleFixedDelay(cmd, delayMs);
        }

        @Override
        protected Popover makePopover(Step action, PopListener listener,
                ClickRegister register, boolean alignment) {
            if (ContentUtil.isSmartStep(action)) {
                return new Popover(listener, register, alignment);
            } else {
                return super.makePopover(action, listener, register, alignment);
            }
        }

        @Override
        protected StepPop stepPop(Step step, String footnote,
                String placement, String stepnote) {
            if (ContentUtil.isSmartStep(step)) {
                return new StepStaticPop(step, footnote, placement);
            } else {
                return new CloseableStepPop(step, footnote, placement,
                        stepnote);
            }
        }

        @Override
        protected String footnote(Draft draft, Step action) {
            String foot = super.footnote(draft, action);
            if (draft.ent_id() == null) {
                return foot + " - <a href=\"" + Common.referralUrl()
                        + "\" rel=\"nofollow noreferrer\" target=\"_blank\">whatfix.com</a>";
            } else {
                return foot;
            }
        }

        @Override
        protected int skippedSteps() {
            return player.skipped;
        }
    }

    private String getBeaconSegmentName(Draft flow) {
        return Enterpriser.getActiveBeaconSegment().name() == null
                ? flow.title()
                : Enterpriser.getActiveBeaconSegment().name();
    }

    private static class StepFindListener implements FindListener {
        private PlayerBase playerBase;

        public StepFindListener(PlayerBase playerBase) {
            this.playerBase = playerBase;
        }

        @Override
        public final void postFind(Step action, Element element,
                JavaScriptObjectExt stepFindData) {
            if (!playerBase.isStopped()) {
                playerBase.trackLiveStep(action, element, stepFindData);
            }
            if (Finder.validateContext(action)) {
                CrossMessager.sendMessage(Action.DISABLE_MOVE_STATE, "");
                LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "postFind",
                        WfxModules.JIT,
                        "Flow Url & current Url matched, re-setting disable_move_state");
            } else {
                LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "postFind",
                        WfxModules.JIT, "Flow Url & current Url did not match");
            }
        }

        @Override
        public void preFind(Step step) {
        }
    };

    private void updatePlayStateInStater(PlayState state) {
        state.analyticsInfo(analyticsCache);
        stater.setState(StateKeys.PLAY_STATE,
                StringUtils.stringifyObject(state));
    }

    private void setBackButtonStatus(PlayState state) {
        if (backButtonNeeded) {
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class,
                    "setBackButtonStatus", WfxModules.JIT,
                    "is backButtonNeeded: {0}", backButtonNeeded);
            PlayStateWithPosition previousStepData = previousStates.peek();
            if (previousStepData == null
                    || !Enterpriser.isBackButtonEnabled()) {
                return;
            }
            PlayState previousPlayState = previousStepData.getPlayState();
            int previousPlayPosition = previousStepData.getPlayPosition();
            Step parentStep = previousPlayState.flow()
                    .step(previousPlayPosition + 1);

            stater.getState(StateKeys.PLAY_POSITION,
                    new AsyncCallback<String>() {
                        @Override
                        public void onSuccess(String result) {
                            // If page tags not matching,clearing play state
                            int currentPlayPosition = Integer.valueOf(result);
                            long sTime = System.currentTimeMillis();
                            boolean isValidPageTag = isValidPageTag(parentStep,
                                    state, currentPlayPosition);
                            LOGGER.log(WfxLogLevel.PERFORMANCE,
                                    PlayerBase.class, "isValidPageTag",
                                    WfxModules.WORKFLOW_ENGINE,
                                    "Time taken by isValidPageTag method to show back button: {0} ms",
                                    (System.currentTimeMillis() - sTime));
                            if (isValidPageTag) {
                                state.flow().step_back_button(
                                        currentPlayPosition + 1, true);
                                LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class,
                                        "setBackButtonStatus", WfxModules.JIT,
                                        "is a valid page tag");
                            } else {
                                state.flow().step_back_button(
                                        currentPlayPosition + 1, false);
                                clearPreviousStates();
                                LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class,
                                        "setBackButtonStatus", WfxModules.JIT,
                                        "is not a valid page tag");
                            }
                        }

                        @Override
                        public void onFailure(Throwable caught) {
                        }
                    });
            backButtonNeeded = !backButtonNeeded;
        }
    }

    // BackButton branching- If parentStep pageTags + parentPageTags(for Version
    // 2) and currentPage Tags are equal then backButton is visible
    private boolean isValidPageTag(Step previousStep,
            PlayState currentPlayState, int currentPlayPosition) {
        JsArrayString pageTags = previousStep.page_tags();
        JsArray<JsArrayString> parentPageTags = previousStep.parent_page_tags();
        JsArray<JsArrayString> allTags = DataUtil.createArray().cast();
        if (pageTags != null && pageTags.length() > 0) {
            allTags.push(pageTags);
        }
        if (JsUtils.isNotEmpty(parentPageTags)) {
            for (int index = 0; index < parentPageTags.length(); index++) {
                allTags.push(parentPageTags.get(index));
            }
        }
        if (allTags.length() == 0) {
            LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "isValidPageTag",
                    WfxModules.JIT, "all tags is zero");
            return false;
        }
        setCurrentPageTags();
        if (StringUtils.isBlank(shPageTags)) {
            return false;
        }
        for (int i = 0; i < allTags.length(); i++) {
            JsArrayString tags = allTags.get(i);
            for (int j = 0; j < tags.length(); j++) {
                String tag = tags.get(j);
                if (StringUtils.isNotBlank(tag) && !shPageTags.contains(tag)) {
                    return false;
                }
            }
        }

        return comparePreviousAndCurrentStepTags(allTags, currentPlayState,
                currentPlayPosition);
    }

    /**
     * When page load takes time the evaluation for step back button has already
     * exectuted because of which it return true. Thus comparing page tags of
     * previous and current state
     */
    private boolean comparePreviousAndCurrentStepTags(
            JsArray<JsArrayString> previousStepTags, PlayState currentPlayState,
            int currentPlayPosition) {
        Set<String> previousStepTagsSet = new HashSet<String>();
        for (int i = 0; i < previousStepTags.length(); i++) {
            JsArrayString tags = previousStepTags.get(i);
            for (int j = 0; j < tags.length(); j++) {
                previousStepTagsSet.add(tags.get(j));
            }
        }

        Step currentStep = currentPlayState.flow()
                .step(currentPlayPosition + 1);
        JsArrayString currentStepPageTags = currentStep.page_tags();
        if (JsUtils.isNotEmptyArray(currentStepPageTags)) {
            for (int i = 0; i < currentStepPageTags.length(); i++) {
                if (!previousStepTagsSet.contains(currentStepPageTags.get(i))) {
                    return false;
                }
            }
        }

        JsArray<JsArrayString> currentStepParentPageTags = currentStep
                .parent_page_tags();
        if (JsUtils.isNotEmpty(currentStepParentPageTags)) {
            for (int i = 0; i < currentStepParentPageTags.length(); i++) {
                JsArrayString currentParentTags = currentStepParentPageTags
                        .get(i);
                for (int j = 0; j < currentParentTags.length(); j++) {
                    if (!previousStepTagsSet
                            .contains(currentParentTags.get(j))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected void clearIntermediateState() {

    }

    private void onExitPopupCloseEvent(Event e) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "onExitPopupCloseEvent",
                WfxModules.JIT, "Closing Popup of GracefulExit for flow: {0}",
                state.flow().title());
        ExitPopupCloseEvent event = (ExitPopupCloseEvent) e;
        if (event.getContent().equalsIgnoreCase("yes")) {
            removeFlowIdFromWindow();
            onEndPopupSeenEvent(e);
            clearAllStates();
        }
    }

    protected void launchLiveHerePopup(Flow flow, String src_id) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "launchLiveHerePopup",
                WfxModules.JIT, "source: {0}", src_id);
        PlayState state = DataUtil.create();
        state.flow(flow);
        state.src_id(src_id);
        launchLiveHerePopup(state);
    }

    /*
     * Opens up a GUIDED POPUP for the flow, avoids duplicate mounting.
     */
    protected void launchLiveHerePopup(PlayState state) {
        if (isPopupActive()) {
            Console.debug("Guided popup is active on this segment");
            return;
        }
        TrackerEventOrigin srcName = TrackerEventOrigin
                .getWidgetNameFromSrc(state.src_id());
        AnalyticsInfo info = AnalyticsInfo.create((Flow) state.flow(), srcName);
        if (GaUtil.checkSrcIsWidget(state.src_id())) {
            info.segment_id(state.analyticsInfo().segment_id());
            info.segment_name(state.analyticsInfo().segment_name());
        }
        EventBus.fire(new ShowLivePopupEvent(state, info, srcName));
    }

    // This step returns current step object
    public Step getCurrentStep() {
        return getState().flow().step(this.next + 1);
    }

    /**
     * Updating the position in storage per flow. Next appearance of graceful
     * exit modal should respect this.
     *
     * @param positionChangeEvent
     */
    public void onGracefulExitPositionChange(Event e) {
        GracefulExitPositionChangeEvent event = (GracefulExitPositionChangeEvent) e;
        LOGGER.log(WfxLogLevel.DEBUG, GracefulExitPlayer.class,
                "onGracefulExitPositionChange", WfxModules.JIT,
                "Update dropped position {0} in play_state", event.getPosition());
        if (null != state) {
            state.flow().gracefulexit_position(event.getPosition());
            stater.setState(StateKeys.PLAY_STATE,
                    StringUtils.stringifyObject(state));
        }
    }

    private void onAutoFlowSwitchToManualEvent(Event e) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class,
                "onAutoFlowSwitchToManualEvent", WfxModules.JIT,
                "Switching execution of flow: {0} ({1}) from auto to manual",
                state.flow().title(), state.flow().flow_id());
        if (null != state) {
            state.flow().manual_execution_mode(true);
            stater.setState(StateKeys.PLAY_STATE,
                    StringUtils.stringifyObject(state));
        }      
        Common.tracker()
                .onEvent(EmbedEvent.FLOW_LIVE_AUTO_SWITCH.builder()
                        .analyticsInfo(analyticsCache)
                        .flow(state.flow().flow_id(), state.flow().title())
                        .srcId(state.src_id()).stepNumber(next + 1).build());

    }

    private void onAutoFlowStopEvent(Event e) {
        LOGGER.log(WfxLogLevel.DEBUG, PlayerBase.class, "onAutoFlowStopEvent",
                WfxModules.JIT, "Execution of auto flow: {0} ({1}) stopped",
                state.flow().title(), state.flow().flow_id());       
        Common.tracker()
                .onEvent(EmbedEvent.FLOW_LIVE_AUTO_CLOSE.builder()
                        .analyticsInfo(analyticsCache)
                        .flow(state.flow().flow_id(), state.flow().title())
                        .srcId(state.src_id()).stepNumber(next + 1).build());
        stop();
        clearAllStates();
    }
    
    public void onEditorTroubleshootChange(Event e) {
        EditorTroubleshootChangeEvent event = (EditorTroubleshootChangeEvent) e;
        if (null != state) {
            if (StringUtils.isNotBlank(event.getPosition())) {
                UserPreferences.setEditorTroubleshootKey(getLoggedInUserId(),
                        UserPreferences.EDITOR_TROUBLESHOOT_POSITION,
                        event.getPosition());
            }
            if (null != event.isMinimized()) {
                state.stepPlayStatus().is_minimized(event.isMinimized());
            }
            if (null != event.isClosed()) {
                state.stepPlayStatus().is_closed(event.isClosed());
            }
            stater.setState(StateKeys.PLAY_STATE,
                    StringUtils.stringifyObject(state));
        }
    }

    /**
     * Starts the goal tracking process to call Finder to find elements and
     * latch listeners.
     *
     */
    protected void startUserActionsTracker() {
        // TODO : For GA - FIX-12292
        if (Enterpriser.isUserJourneyEnabled()
                && !Environment.EXPORT.equals(GaUtil.getEmbedMode())) {
            Enterprise entInstance = Enterprise.enterprise();
            JsArray<Flow> userActions = entInstance.user_actions();
            if (userActions != null && userActions.length() != 0) {
                EventBus.fire(new PageUserActionsEvent(userActions));
            }
        }
    }

    /**
     * Fire completion message so content can verify user action(UA) association
     * and update accordingly
     *
     * @param busEvent
     */
    private void onUserActionCompleted(Event busEvent) {
        UserActionCompleteEvent uaBusEvent = (UserActionCompleteEvent) busEvent;
        String goalId = uaBusEvent.getDraft().flow_id();

        if (hasTaskerLauncher()) {
            CrossMessager.sendMessage(UserActionsPlayer.MESSAGE_UA_COMPLETED,
                    goalId);
        }
    }
    
    private void updateStepState(int step, String stepStatus,
            String failureReason) {
        StepPlayStatus status = state.stepPlayStatus();
        if (null == status) {
            status = DataUtil.create();
        }
        status.step_status(step, stepStatus);
        if (StringUtils.isNotBlank(failureReason)) {
            status.step_failure_reason(step, failureReason);
        }
        state.stepPlayStatus(status);
        stater.setState(StateKeys.PLAY_STATE,
                StringUtils.stringifyObject(state));
    }
    
    private boolean shouldDisplayEditorTroubleshoot() {
        if (null != state.stepPlayStatus()) {
            boolean isClosed = state.stepPlayStatus().is_closed();
            //flow starting from middle in case of refresh page
            if (isClosed && next + 1 > 0) {
                return false;
            }
        }
        // should not display in case of flow testing as well
        return flowRunThroughEditor
                && Enterpriser.isEditorTroubleshootEnabled()
                && !isAutomatedBrowser();
    }
    
    private static native boolean isAutomatedBrowser() /*-{
        try {
            return navigator.webdriver ? true : false;
        } catch(e) {
            return false;
        }
    }-*/;
    
    protected String getLoggedInUserId() {
        return null;
    }
    
    protected boolean shouldDisplayGracefulExit() {
        return Enterpriser.isGracefulFailureEnabled();
    }
    
    /**
	 * TODO : this code would be moved to a new class DesktopEmbed.java by next 2
	 * sprints
	 */
    protected void initDesktopListener() {
		CrossMessager.addListener(new CrossListener() {
			@Override
			public void onMessage(String type, String content) {
				DesktopData desktopData = DataUtil.create(content);
				hideOrShowWidgets(desktopData.isPopup() == 1);
			}
		}, "desktop_handle_widgets");
	}

	private void hideOrShowWidgets(boolean doHide) {
		Element ele = Document.get().getElementById(BaseWidget.WIDGET_ID);
		if (doHide) {
			if (null != ele) {
				Map<String, String> style = new HashMap<>();
				style.put("display", "none");
				Common.appendToStyle(ele, style);
			}
			if (null != taskerLauncher) {
				// update visibility so that new instance of TL does not get created on popup
				LaunchTasker.updateTaskerVisibility(false);
				taskerLauncher.addStyleName(Overlay.CSS.hide());
			}
		} else {
			if (null != ele) {
				ele.getStyle().setDisplay(Style.Display.INITIAL);
			}
			if (null != taskerLauncher) {
				LaunchTasker.updateTaskerVisibility(true);
				taskerLauncher.removeStyleName(Overlay.CSS.hide());
			}
		}
	}
}
