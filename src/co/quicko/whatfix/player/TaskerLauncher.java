package co.quicko.whatfix.player;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.ShortcutHandler.Shortcut;
import co.quicko.whatfix.common.ShortcutHandler.ShortcutListener;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.VideoContentPopup;
import co.quicko.whatfix.data.Content;
import co.quicko.whatfix.data.Content.Video;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.CustomTaskerNudgeCountersData;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.EmbedData.TaskerSettings;
import co.quicko.whatfix.data.EnterpriseSettings;
import co.quicko.whatfix.data.SegmentNudgeParameters;
import co.quicko.whatfix.data.TaskListNudgeCondition;
import co.quicko.whatfix.data.TaskListNudgeTrigger;
import co.quicko.whatfix.data.TaskerInfo;
import co.quicko.whatfix.data.TaskerInfo.TaskerData;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.ExtensionHelper;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.LaunchTasker;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.player.useraction.UserActionsPlayer;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.security.UserInfo;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.service.ContentManager;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class TaskerLauncher extends LaunchTasker
        implements CrossListener, ShortcutListener {

    private PlayerBase player;
    private StorageHandler storageHandler;
    private AnalyticsInfo info;
    private static final TrackerEventOrigin TASKER_SRC = TrackerEventOrigin.TASK_LIST;
    private static final String SHORTCUT = "shortcut";
    private static final String ESCAPE = "escape";
    private static final String CROSS = "cross";
    
    public static final long MILLISECONDS_24_HOURS = 86400000;   
    public static final long MILLISECONDS_1_WEEK = 604800000;
    private TaskerNudgeTooltip taskerTooltip;


    public static TaskerLauncher getTaskerLauncherInstance(
            final PlayerBase player,
            Settings settings) {

        return new TaskerLauncher(player, settings);
    }

    private TaskerLauncher(PlayerBase player, Settings settings) {
        super(settings, "tasker");
        this.player = player;
        String segmentName = (settings.segment_name() != null)
                ? settings.segment_name()
                : settings.label();
        info = AnalyticsInfo.create(TASKER_SRC, settings.segment_id(),
                segmentName);
        storageHandler = new StorageHandler();
        addShortcut();
        if (Customizer.hasCompletedTasks()) {
            tracker = new ExternalTracker();
        } else {
            tracker = GWT.create(WfTracker.class);
        }
        widgetPopup.addStyleName(Overlay.CSS.taskerShadow());
        Common.handleZoom(widgetPopup);
        CrossMessager.addListener(this, prefix("_destroy"), prefix("_close"),
                prefix("_run"), prefix("_frame_data"), prefix("_video"),
                prefix("_content"), prefix("_desktop_content"));
    }

    private void addShortcut() {
        Shortcut shortCut = Themer.taskListShortcut();
        if (shortCut != null) {
            register(shortCut, this);
        }
    }

    @Override
    public void onShortcut(Shortcut shortcut) {
        onClick(null);
    }

    @Override
    public void onMessage(String type, String content) {
        if ((prefix("_close")).equals(type)) {
            setFocusOnLauncherFlag(content);
            closeWidget();
            GaUtil.trackClose(EmbedEvent.TASK_LIST_CLOSE.builder(), content,
                    TASKER_SRC);
        } else if ((prefix("_run")).equals(type)) {
            showDeck(content);
        } else if ((prefix("_destroy")).equals(type)) {
            destroy();
        } else if (prefix("_frame_data").equals(type)) {
            EnterpriseSettings enterpriseSettings = EnterpriseSettings
                    .getEnterpriseSettings(settings);
            GaUtil.setNewInteraction(enterpriseSettings);
            enterpriseSettings.isMobile(MOBILE.equals(mode()));
            enterpriseSettings.setPreviewMode(ExtensionHelper.isPreviewMode());
            CrossMessager.sendMessageToFrame(frame.getElement(), "frame_data",
                    StringUtils.stringifyObject(enterpriseSettings));
        } else if ((prefix("_video")).equals(type)) {
            showVideoPopup(type, content);
        } else if ((prefix("_content").equals(type))
                || prefix("_desktop_content").equals(type)) {
            markContentAsCompleted(content);
        }
    }

    private void setFocusOnLauncherFlag(String content) {
        Where where = DataUtil.create(content);
        String closeBy = where.getString("closeBy");
        if (CROSS.equals(closeBy) || ESCAPE.equals(closeBy)
                || SHORTCUT.equals(closeBy)) {
            shouldFocusOnLauncher = true;
        }
    }

    @Override
    public void initialize(AsyncCallback<Void> cb) {
		// not initializing in case of desktop popups
		if (isTaskerVisible()) {
			super.initialize(cb);
			storageHandler.initialize();
		}
    }

    @Override
    public void show() {
        super.show();
        PlayerBase.scheduler.scheduleFixedDelay(new RepeatingCommand() {
            @Override
            public boolean execute() {
                refreshLauncherPosition();
                return false;
            }
        }, 50);
    }

    @Override
    protected void showDeck(String content) {
        player.showDeck(content, "tasker", Callbacks.<String> emptyCb());
    }
    
    private void markContentAsCompleted(String msg) {
        Content content = DataUtil.create(msg);
        player.markContentCompleted(content.flow_id());
    }
    
    private void showVideoPopup(String type, final String content) {
        final Video video = DataUtil.create(content);
        new VideoContentPopup(Common.getLaunchVideoInfo(video));
        markContentAsCompleted(content);
    }

    @Override
    protected void initializeListeners() {
        super.initializeListeners();

        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                update(content);
            }
        }, "tasker_update");

        CrossMessager.addListener(new CrossListener() {

            @Override
            public void onMessage(String type, String content) {
                
                if(AppFactory.isMobileApp()) {
                    onClick(null);
                    return;
                }
                
                if (taskerInfo.flows().length() > 0
                        || settings.isGroupPresent()) {
                    onClick(null);
                }
            }
        }, "tasker_open");
        
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
            	if (isTaskerOpen) {
            		closeWidget();
            	}
            }
        }, "close_widgets");
        
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                userActionUpdate(content);
            }
        }, UserActionsPlayer.MESSAGE_UA_COMPLETED);
    }
    
    @Override
    protected void showFirstTime() {
        super.showFirstTime();
        if (canShow()) {
            Common.tracker().onEvent(EmbedEvent.TASK_LIST_LOADED.builder()
                    .userDetails(((TaskerSettings) settings).role_tags())
                    .analyticsInfo(info).widget(TASKER_SRC).build());
        }
    }
    
    protected void beforeTaskListLoad() {
        Customizer.onBeforeTaskListShow(taskerInfo.total(),
                taskerInfo.completed());
    }

    public void trackCompleted(String userId) {
        int percent = computePercent(taskerInfo.total(),
                taskerInfo.completed());
        Common.tracker().trackTaskCompleted(info.segment_name(), userId,
                percent, info.segment_name(), info.segment_id());
        if (null != tasklistNudgeCondition
                && tasklistNudgeCondition == TaskListNudgeCondition.TASKS_PER_WEEK) {
            updateNudgeCounters(
                    Where.create(SHOULD_INCREMENT_TASK_COUNT, true));
        }
    }

    private static int computePercent(double totalTasks, int done) {
        double percentPerTask = (100 / totalTasks);
        double offset = (percentPerTask - (int) percentPerTask);

        double offsetTillNow = ((done - 1) * offset);
        double missedOffset = (offsetTillNow - (int) offsetTillNow);
        double offSetNow = missedOffset + offset;

        int percent = (int) percentPerTask;
        if (offSetNow >= 1) {
            percent += 1;
        }
        return percent;
    }

    private void refresh() {
        tracker.refresh(taskerInfo,
                invalidatableCb(new AsyncCallback<TaskerInfo>() {
                    @Override
                    public void onSuccess(TaskerInfo result) {
                        taskerInfo = result;
                        updateRemaining();
                    }

                    @Override
                    public void onFailure(Throwable caught) {

                    }
                }));
    }

    protected void update(String flow_id) {
        tracker.update(taskerInfo, flow_id,
                invalidatableCb(new AsyncCallback<TaskerInfo>() {
                    @Override
                    public void onSuccess(TaskerInfo result) {
                        updateRemaining();
                        storageHandler.notifyStorage();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                    }
                }));
    }
    
    protected void userActionUpdate(String useractionIds) {
        String[] ids = useractionIds.split(",");
        for (String goalId : ids) {
            update(taskerInfo.getAttachedFlow(goalId));
        }
    }


    @Override
    protected boolean canShow() {
        /*
         * While fetching tasks (i.e contents) for TaskList, tasks which are part of
         * the Groups are not fetched. In case of no individual tasks (i.e all tasks
         * are part of some groups) flows in the TaskerInfo will be 0. Group
         * presence need to be check in case when flows in TaskerInfo are 0 to show
         * tasklist
         * 
         */
        return super.canShow() && (taskerInfo.flows().length() > 0
                || settings.isGroupPresent());
    }

    @Override
    protected void removeListeners() {
        super.removeListeners();
        CrossMessager.removeListener(this, "tasker_update", "tasker_open", "close_widgets");
        CrossMessager.removeListener(this, prefix("_destroy"), prefix("_close"),
                prefix("_run"), prefix("_frame_data"), prefix("_video"),
                prefix("_content"));
    }
    
    @Override
    public void onClose(CloseEvent<PopupPanel> event) {
        super.onClose(event);
        if (event.isAutoClosed()) {
            disposeTaskerTip();
            // TODO: Add segment details for TaskList 
            Common.tracker().onEvent(EmbedEvent.TASK_LIST_CLOSE.builder()
                            .srcId(TrackerEventOrigin.TASK_LIST.getSrcName())
                            .replaceInPage("closedBy", "click")
                            .widget(TrackerEventOrigin.TASK_LIST)
                            .build());
        }
    }

    private class StorageHandler implements RepeatingCommand {
        private Storage localStore = null;
        private String lastRefresh;
        private static final String key = "_wfx_tasker";

        public void initialize() {
            localStore = Storage.getLocalStorageIfSupported();
            if (localStore != null) {
                String val = localStore.getItem(key);
                if (val == null) {
                    notifyStorage();
                } else {
                    lastRefresh = val;
                }
                Scheduler.get().scheduleFixedDelay(this, 4000);
            }
        }

        @Override
        public boolean execute() {
            String val = localStore.getItem(key);
            if (!lastRefresh.equals(val)) {
                lastRefresh = val;
                refresh();
            }
            return active;
        }

        protected void notifyStorage() {
            if (localStore != null) {
                lastRefresh = String.valueOf(System.currentTimeMillis());
                localStore.setItem(key, lastRefresh);
            }
        }
    }

    public static class ExternalTracker implements Tracker {
        private static Settings settings;
        private ContentManager manager;
        private LaunchTasker launcher;
        private CompletedTasksTracker completedTracker;

        @Override
        public void initialize(Settings settings, ContentManager manager,
                LaunchTasker launcher) {
            ExternalTracker.settings = settings;
            this.manager = manager;
            this.launcher = launcher;
            if (Customizer.hasCompletedTasks()) {
                completedTracker = new CustomTaskListTracker();
            } else {
                completedTracker = new LocalStorageTaskListTracker();
            }
        }

        @Override
        public void fetchFlows(final AsyncCallback<TaskerInfo> callback) {
        	UserInfo user= Security.unq_id_with_enc_status(AppUser.getUserId());
        	
            String[][] mixSettings = settings.mixedTypeValue();
            manager.tasks(mixSettings[0], mixSettings[1], Security.user_id(),
                    user, settings.order(),
                    null, new AsyncCallback<TaskerData>() {

                @Override
                public void onSuccess(TaskerData tasks) {
                    TaskerInfo info = new TaskerInfo(tasks, (TaskerSettings)settings);
                    refresh(info, callback);
                }

                @Override
                public void onFailure(Throwable caught) {
                    callback.onFailure(null);
                }
            });

        };

        @Override
        public void refresh(TaskerInfo taskerInfo,
                AsyncCallback<TaskerInfo> callback) {
            JsArrayString completedFlowIds = completedTracker
                    .completedTasks(taskerInfo.all());
            JsArrayString filteredFlowIds = (JsArrayString) JsArrayString
                    .createArray();
            if (null != completedFlowIds) {
                for (int index = 0; index < completedFlowIds
                        .length(); index++) {
                    String flow_id = completedFlowIds.get(index);
                    if (taskerInfo.allContains(flow_id)) {
                        filteredFlowIds.push(flow_id);
                    }
                }
            }
            taskerInfo.setCompleted(filteredFlowIds);
            callback.onSuccess(taskerInfo);
        }

        @Override
        public void update(TaskerInfo taskerInfo, String flow_id,
                AsyncCallback<TaskerInfo> callback) {
            int lastCompleted = taskerInfo.completed();
            completedTracker.onFlowCompleted(flow_id);
            refresh(taskerInfo, callback);
            int nowCompleted = taskerInfo.completed();
            if (nowCompleted == lastCompleted + 1) {
                launcher.trackCompleted(Security.unq_id(AppUser.getUser()));
            }
        }

        @Override
        public void onTasksCompleted() {
            completedTracker.onTasksCompleted();
        }
        
        @Override
        public void getNudgeCounters(Map<String, String> taskerNudgeCounterData,
                AsyncCallback<String> callback) {
            completedTracker.getNudgeCounters(taskerNudgeCounterData, callback);
        }

        @Override
        public void updateNudgeCounters(Where counters) {
            completedTracker.updateNudgeCounters(counters);
        } 
        
        /**
         * This method adds counter data to the map
         * @param taskerNudgeCounterData
         * @param callback
         * @param data
         */
        public static void addCounterData(
                Map<String, String> taskerNudgeCounterData,
                AsyncCallback<String> callback,
                CustomTaskerNudgeCountersData data) {
            taskerNudgeCounterData.put(NUDGE_COUNT,
                    String.valueOf(data != null ? data.nudge_count() : 0));
            taskerNudgeCounterData.put(NUDGE_EVAL_TIMESTAMP, String.valueOf(
                    data != null ? data.nudge_eval_timestamp() : 0));
            taskerNudgeCounterData.put(TASK_COMPLETE_COUNT, String.valueOf(
                    data != null ? data.task_complete_count() : 0));
            taskerNudgeCounterData.put(LAST_RESET_TIMESTAMP, String.valueOf(
                    data != null ? data.last_reset_timestamp() : 0));
            callback.onSuccess(null);
        }
        
        /**
         * Updates and increments the counters if required 
         * @param shouldIncrementNudgeCount
         * @param nudgeEvalTs
         * @param shouldIncrementTaskCount
         * @param oldCounters
         */
        private static void updateAndIncrement(boolean shouldIncrementNudgeCount,
                long nudgeEvalTs, boolean shouldIncrementTaskCount,
                Map<String, String> oldCounters) {
            if (nudgeEvalTs != 0L) {
                oldCounters.put(NUDGE_EVAL_TIMESTAMP,
                        String.valueOf(nudgeEvalTs));
            }
            if (shouldIncrementNudgeCount) {
                oldCounters.put(NUDGE_COUNT, String.valueOf(
                        Long.parseLong(oldCounters.get(NUDGE_COUNT)) + 1));
            }
            if (shouldIncrementTaskCount) {
                oldCounters.put(TASK_COMPLETE_COUNT, String.valueOf(
                        Long.parseLong(oldCounters.get(TASK_COMPLETE_COUNT))
                                + 1));
            }
        }
        
        /**
         * Reset the counters and update last_reset_ts
         * @param lastResetTs
         * @param oldCounters
         */
        private static void resetCounters(long lastResetTs,
                Map<String, String> oldCounters) {
            for (Entry<String, String> entry : oldCounters.entrySet()) {
                entry.setValue(String.valueOf(0));
            }
            oldCounters.put(LAST_RESET_TIMESTAMP,
                    String.valueOf(lastResetTs));
        }

        /**
         * Updates the nudge counters and calls "updateCb". "updateCb" updates
         * the counter based on either local storage or custom implementation
         * 
         * @param counters
         * @param oldCounters
         * @param updateCb
         */
        private static void doUpdateNudgeCounters(Where counters,
                Map<String, String> oldCounters,
                AsyncCallback<CustomTaskerNudgeCountersData> updateCb) {
            boolean shouldIncrementNudgeCount = counters
                    .getBoolean(SHOULD_INCREMENT_NUDGE_COUNT);
            long nudgeEvalTs = (long) counters
                    .getDouble(NUDGE_EVAL_TIMESTAMP);
            boolean shouldIncrementTaskCount = counters
                    .getBoolean(SHOULD_INCREMENT_TASK_COUNT);
            boolean shouldResetCounter = counters
                    .getBoolean(SHOULD_RESET_COUNTERS);
            long lastResetTs = (long) counters
                    .getDouble(LAST_RESET_TIMESTAMP);
            if (shouldResetCounter) {
                if (lastResetTs >= Long.parseLong(
                        oldCounters.get(LAST_RESET_TIMESTAMP))) {
                    resetCounters(lastResetTs, oldCounters);
                } else {
                    Console.debug(
                            "last_reset_timestamp is either missing or having expired value:"
                                    + lastResetTs);
                    return;
                }
            }
            updateAndIncrement(shouldIncrementNudgeCount,
                    nudgeEvalTs, shouldIncrementTaskCount,
                    oldCounters);
            CustomTaskerNudgeCountersData counter = (CustomTaskerNudgeCountersData) JavaScriptObject
                    .createObject();
            counter.nudge_count(
                    Long.parseLong(oldCounters.get(NUDGE_COUNT)));
            counter.nudge_eval_timestamp(Long.parseLong(
                    oldCounters.get(NUDGE_EVAL_TIMESTAMP)));
            counter.task_complete_count(Long.parseLong(
                    oldCounters.get(TASK_COMPLETE_COUNT)));
            counter.last_reset_timestamp(Long.parseLong(
                    oldCounters.get(LAST_RESET_TIMESTAMP)));
            updateCb.onSuccess(counter);
        }

        public interface CompletedTasksTracker {

            public JsArrayString completedTasks(JsArrayString flowIds);

            public void onTasksCompleted();

            public void onFlowCompleted(String flowId);
            
            public void getNudgeCounters(Map<String, String> taskerNudgeCounterData,
                    AsyncCallback<String> callback);
            
            public void updateNudgeCounters(Where counters);
            

        }

        public static class LocalStorageTaskListTracker
                implements CompletedTasksTracker {
            private Storage localStorage = null;
            private static final String KEY = "wf_completedTasks";
            private static final String NUDGE_KEY = "wf_tl_nudge";

            public LocalStorageTaskListTracker() {
                localStorage = Storage.getLocalStorageIfSupported();
            }

            private boolean localStorageSupported() {
                if (localStorage != null) {
                    return true;
                } else {
                    return false;
                }
            }

            private JsArrayString getCompletedFlowIds(String key) {
                JsArrayString completedFlowIds = (JsArrayString) JsArrayString
                        .createArray();
                if (localStorageSupported()) {
                    String completed = localStorage.getItem(key);
                    if (completed != null) {
                        completedFlowIds = parseCompletedFlowIds(completed);
                    }
                }
                return completedFlowIds;
            }

            @Override
            public JsArrayString completedTasks(JsArrayString flowIds) {
                return getCompletedFlowIds(KEY);
            }

            @Override
            public void onFlowCompleted(String flow_id) {
                JsArrayString completedFlowIds = getCompletedFlowIds(KEY);
                completedFlowIds.push(flow_id);
                localStorage.setItem(KEY,
                        storeCompletedFlowIds(completedFlowIds));
            }

            @Override
            public void onTasksCompleted() {
                if (showProgress()
                        && ((TaskerSettings) settings).hide_on_complete()) {
                    CrossMessager.sendMessage("tasker_destroy", "");
                }
            }

            private native JsArrayString parseCompletedFlowIds(
                    String flowIds) /*-{
				return JSON.parse(flowIds);
            }-*/;

            private native String storeCompletedFlowIds(
                    JsArrayString flowIds) /*-{
				return JSON.stringify(flowIds);
            }-*/;
            
            private String getNudgeKey() {
                UserInfo user = Security
                        .unq_id_with_enc_status(AppUser.getUserId());
                return NUDGE_KEY + "_" + settings.segment_id() + "_"
                        + user.getUserId();
            }

            @Override
            public void getNudgeCounters(
                    Map<String, String> taskerNudgeCounterData,
                    AsyncCallback<String> callback) {
                if (localStorageSupported()) {
                    String counters = localStorage.getItem(getNudgeKey());
                    CustomTaskerNudgeCountersData data = null;
                    if (counters != null) {
                        data = parseNudgeCounters(counters);
                    }
                    addCounterData(taskerNudgeCounterData, callback, data);
                }
            }
            
            private native CustomTaskerNudgeCountersData parseNudgeCounters(
                    String flowIds) /*-{
                return JSON.parse(flowIds);
            }-*/;
            
            private native String convertToString(CustomTaskerNudgeCountersData data) /*-{
                return JSON.stringify(data);
            }-*/;

            @Override
            public void updateNudgeCounters(Where counters) {
                Map<String, String> oldCounters = new HashMap<>();
                // Callback to update in localstorage
                AsyncCallback<CustomTaskerNudgeCountersData> updateCb = new AsyncCallback<CustomTaskerNudgeCountersData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // Do nothing
                    }

                    @Override
                    public void onSuccess(
                            CustomTaskerNudgeCountersData counter) {
                        localStorage.setItem(getNudgeKey(),
                                convertToString(counter));
                    }
                };
                getNudgeCounters(oldCounters, new AsyncCallback<String>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // Do nothing
                    }

                    @Override
                    public void onSuccess(String result) {
                        doUpdateNudgeCounters(counters, oldCounters, updateCb);
                    }
                });
            }
        }

        public static class CustomTaskListTracker
                implements CompletedTasksTracker {

            @Override
            public JsArrayString completedTasks(JsArrayString flowIds) {
                // Completed flow ids have to be returned from custom implemented
                // code in AC
                JsArrayString completedFlowIds = Customizer
                        .completedTasks(flowIds,
                                Security.unq_id_with_enc_status(
                                        AppUser.getUserId()).getUserId(),
                                settings.segment_id());
                if (completedFlowIds == null) {
                    completedFlowIds = (JsArrayString) JsArrayString
                            .createArray();
                }
                return completedFlowIds;
            }

            @Override
            public void onFlowCompleted(String flowId) {
                // When each task is completed, passing the flow id to custom
                // implemented code in AC
                Customizer.onEachTaskCompleted(flowId,
                        Security.unq_id_with_enc_status(AppUser.getUserId())
                                .getUserId(),
                        settings.segment_id());
            }

            @Override
            public void onTasksCompleted() {
                // When all the tasks are complete
                Customizer.onTasksCompleted();
            }

            @Override
            public void getNudgeCounters(
                    Map<String, String> taskerNudgeCounterData,
                    AsyncCallback<String> callback) {
                CustomTaskerNudgeCountersData customData = Customizer
                        .getTasklistNudgeCounters(
                                Security.unq_id_with_enc_status(
                                        AppUser.getUserId()).getUserId(),
                                settings.segment_id());
                addCounterData(taskerNudgeCounterData, callback, customData);
            }

            @Override
            public void updateNudgeCounters(Where counters) {
                Map<String, String> oldCounters = new HashMap<>();
                // Pass counter data to custom implemented code in AC
                AsyncCallback<CustomTaskerNudgeCountersData> updateCb = new AsyncCallback<CustomTaskerNudgeCountersData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // Do nothing
                    }

                    @Override
                    public void onSuccess(
                            CustomTaskerNudgeCountersData counter) {
                        counter.user_id(Security
                                .unq_id_with_enc_status(AppUser.getUserId())
                                .getUserId());
                        counter.segment_id(settings.segment_id());
                        Customizer.updateTasklistNudgeCounters(counter);
                    }
                };
                getNudgeCounters(oldCounters, new AsyncCallback<String>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // Do nothing
                    }

                    @Override
                    public void onSuccess(String result) {
                        doUpdateNudgeCounters(counters, oldCounters, updateCb);
                    }
                });
            }
        }

    }

    public static class MobileTaskerLauncher extends TaskerLauncher {

        public static TaskerLauncher getTaskerLauncherInstance(
                final PlayerBase player, Settings settings) {

            return new MobileTaskerLauncher(player, settings);
        }

        private MobileTaskerLauncher(PlayerBase player, Settings settings) {
            super(player, settings);
        }

        @Override
        public void onClick(ClickEvent event) {
            openTasker();
        }

        @Override
        protected void openTasker() {
            hide();
            widgetPopup.setAutoHideEnabled(false);
            widgetPopup.setGlassEnabled(true);
            widgetPopup.setGlassStyleName(Common.CSS.glass());
            adjustTasker(true);
            Style popupStyle = widgetPopup.getElement().getStyle();
            popupStyle.clearBottom();
            popupStyle.clearRight();
            popupStyle.setPosition(Position.ABSOLUTE);
        }

        public void adjustTasker(boolean shouldCenter) {
            int width = setWidth(frame);
            int height = setHeight(frame);
            int left = (Window.getClientWidth() - width) >> 1;
            int top = (Window.getClientHeight() - height) >> 1;
            if (shouldCenter) {
                widgetPopup.center();
            }
            widgetPopup.setPopupPosition(
                    Math.max(Window.getScrollLeft() + left, 0),
                    Math.max(Window.getScrollTop() + top, 0));
            Style popupStyle = widgetPopup.getElement().getStyle();
            popupStyle.clearBottom();
            popupStyle.clearRight();
            popupStyle.setPosition(Position.ABSOLUTE);
        }

        private int setWidth(Frame frame) {
            double width = Window.getClientWidth();
            width = width * 0.8;
            if (width <= 270) {
                width = 270;
            }
            frame.getElement().getStyle().setWidth(width, Unit.PX);
            return (int) width;
        }

        private int setHeight(Frame frame) {
            double height = Window.getClientHeight();
            height = height * 0.7;
            if (height <= 260) {
                height = 260;
            }
            frame.getElement().getStyle().setHeight(height, Unit.PX);
            return (int) height;
        }

        @Override
        protected String mode() {
            return MOBILE;
        }

    }

    /**
     * This method is called to evaluate and handle nudge if required
     */
    public void handleTaskerNudge() {
        if (settings == null) {
            return;
        }
        tracker.getNudgeCounters(taskerNudgeCounterData, new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                // Do nothing
            }

            @Override
            public void onSuccess(String result) {
                if (!taskerNudgeCounterData.isEmpty()) {
                    validateAndNudge();
                }                
            }
        });
    }

    private void validateAndNudge() {
        SegmentNudgeParameters parameters = ((TaskerSettings) settings)
                .nudge_parameters();
        if (parameters == null) {
            return;
        }
        tasklistNudgeCondition = TaskListNudgeCondition
                .getNudgeConditionFromString(parameters.condition());
        if (null == tasklistNudgeCondition) {
            return;
        }
        switch (tasklistNudgeCondition) {
            case TASKS_PER_WEEK:
                handleTasksPerWeekNudge();
                break;
            case INITIAL_FREQUENCY:
                handleFrequencyNudge();
                break;
            case TASK_COMPLETION_PERCENTAGE:
                handleCompletionPercentageNudge();
                break;
            default:
                return;
        }
    }

    /**
     * This is used to nudge the user appropriately
     */
    public void nudgeUser() {
        sendTaskerNudgeGaEvent();
        if (Customizer.onBeforeTasklistNudge()) {
            return;
        }
        TaskListNudgeTrigger trigger = TaskListNudgeTrigger
                .getNudgeTriggerFromString(((TaskerSettings) settings)
                        .nudge_parameters().trigger());
        // Show tooltip only in FULL mode and not in mobile
        shouldShowTaskerTooltip = FULL.equalsIgnoreCase(mode())
                && trigger == TaskListNudgeTrigger.OPEN_WITH_MESSAGE
                && StringUtils.isNotBlank(
                        ((TaskerSettings) settings).nudge_message());
        openTasker();
    }

    /**
     * This method is called to evaluate nudge when "task completetion rate < n
     * per weeks" is selected as condition
     */
    public void handleTasksPerWeekNudge() {
        Where counters = Where.create();
        long nudgeEvalTs = Long.parseLong(taskerNudgeCounterData.get(NUDGE_EVAL_TIMESTAMP));
        long taskCompleteCount = Long.parseLong(taskerNudgeCounterData.get(TASK_COMPLETE_COUNT));
        
        boolean isCounterReset = shouldResetNudgeCounters();
        // Reset counters
        if (isCounterReset) {
            updateMapForReset(counters);
            nudgeEvalTs = 0;
            taskCompleteCount = 0;
        }
        long currTs = System.currentTimeMillis();
        // Only on first load of tasklist
        boolean isLoadedForFirstTime = nudgeEvalTs == 0L;
        if (isLoadedForFirstTime) {
            // Storing first load timestamp
            nudgeEvalTs = currTs;
            taskCompleteCount = 0L;
        }

        // To identify if 1 weeek is complete
        if (currTs > nudgeEvalTs + MILLISECONDS_1_WEEK) {
            String segmentTaskCompleteCount = ((TaskerSettings) settings)
                    .nudge_parameters().value();
            // If criteria satisfies
            if (StringUtils.isNotBlank(segmentTaskCompleteCount)
                    && taskCompleteCount < Long
                            .valueOf(segmentTaskCompleteCount)) {
                nudgeUser();
            }
            updateMapForReset(counters);
            Where.setAll(counters, NUDGE_EVAL_TIMESTAMP,
                    getNearestWeekTs(currTs, nudgeEvalTs));
            updateNudgeCounters(counters);
        } else if (isLoadedForFirstTime) {
            counters.set(NUDGE_EVAL_TIMESTAMP, nudgeEvalTs);
            updateNudgeCounters(counters);
        } else {
            // Else do nothing
        }
    }

    /**
     * Returns the ts which is a nearest week from nudgeEvalTs and also less
     * than currTs
     * 
     * @param currTs
     * @param nudgeEvalTs
     * @return
     */
    private long getNearestWeekTs(long currTs, long nudgeEvalTs) {
        if (nudgeEvalTs == 0L) {
            return currTs;
        }
        long weeksToIncrement = (currTs - nudgeEvalTs) / MILLISECONDS_1_WEEK;
        return nudgeEvalTs + (weeksToIncrement * MILLISECONDS_1_WEEK);
    }

    /**
     * This method is called to evaluate nudge if "For first n times" is
     * selected as condition
     */
    public void handleFrequencyNudge() {
        Where counters = Where.create(SHOULD_INCREMENT_NUDGE_COUNT, true);
        long nudgeCount = Long.parseLong(taskerNudgeCounterData.get(NUDGE_COUNT));
        // Reset counters
        if (shouldResetNudgeCounters()) {
            updateMapForReset(counters);
            nudgeCount = 0L;
        }
        String value = ((TaskerSettings) settings).nudge_parameters().value();
        if (StringUtils.isNotBlank(value) && nudgeCount < Long.valueOf(value)) {
            nudgeUser();
            updateNudgeCounters(counters);
        }
    }

    /**
     * This method is called to evaluate nudge if "Tasklist completion is less
     * than n%" is selected as condition
     */
    public void handleCompletionPercentageNudge() {
        Where counters = Where.create();
        long nudgeEvalTs = Long.parseLong(taskerNudgeCounterData.get(NUDGE_EVAL_TIMESTAMP));
        boolean isCounterReset = shouldResetNudgeCounters();
        // Reset counters
        if (isCounterReset) {
            updateMapForReset(counters);
            nudgeEvalTs = 0L;
        }
        String value = ((TaskerSettings) settings).nudge_parameters().value();
        int completionPercentage = (int) Math.ceil(
                (double) taskerInfo.completed() / taskerInfo.total() * 100);
        long currTS = System.currentTimeMillis();
        // If completion % is less than desired or user is not already nudged in
        // less than 24hrs
        if (StringUtils.isNotBlank(value)
                && completionPercentage < Integer.valueOf(value)
                && (currTS - nudgeEvalTs) > MILLISECONDS_24_HOURS) {
            nudgeUser();
            counters.set(NUDGE_EVAL_TIMESTAMP, currTS);
            updateNudgeCounters(counters);
        }
        else if(isCounterReset) {
            updateNudgeCounters(counters);
        } else {
            // Do nothing
        }
    }

    /**
     * This method evaluates based on user and segment timestamp is the counters
     * have to be reset
     * 
     * @return
     */
    private boolean shouldResetNudgeCounters() {
        long segmentNudgeTimestamp = (long) ((TaskerSettings) settings)
                .nudge_update_timestamp();
        Long lastResetTs = Long.parseLong(taskerNudgeCounterData.get(LAST_RESET_TIMESTAMP));
        long userResetTimestamp = null != lastResetTs
                ? Long.valueOf(String.valueOf(lastResetTs))
                : 0L;
        return userResetTimestamp < segmentNudgeTimestamp;
    }

    private void updateNudgeCounters(Where counters) {
        tracker.updateNudgeCounters(counters);
    }

    /**
     * Adding reset fields
     * @param counters
     */
    private void updateMapForReset(Where counters) {
        Where.setAll(counters, SHOULD_RESET_COUNTERS, true,
                LAST_RESET_TIMESTAMP,
               ((TaskerSettings) settings).nudge_update_timestamp());
    }

    @Override
    protected void processBeforeClose() {
        disposeTaskerTip();
    }
    
    /**
     * Creates a tooltip and places on tasklist when this method is called
     */
    @Override
    protected void setToolTip() {
        taskerTooltip = new TaskerNudgeTooltip(getTipPlacement(), widgetPopup,
                ((TaskerSettings) settings).nudge_message(),
                new AsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        disposeTaskerTip();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        // Do nothing
                    }
                });
    }

    private String getTipPlacement() {
        return TASKER_POSITION_BOTTOM_LEFT.equalsIgnoreCase(getPosition())
                ? TaskerNudgeTooltip.TOOLTIP_RIGHT_TOP
                : TaskerNudgeTooltip.TOOLTIP_LEFT_TOP;
    }

    private void disposeTaskerTip() {
        if (taskerTooltip != null) {
            shouldShowTaskerTooltip = false;
            taskerTooltip.dispose();
            taskerTooltip = null;
        }
    }
    
    /**
     * Used to refresh the tooltip position if present
     */
    public void refreshTooltipPositionIfPresent() {
        if (taskerTooltip != null) {
            taskerTooltip.refreshTooltipPosition();
        }
    }

    private void sendTaskerNudgeGaEvent() {
        Common.tracker()
                .onEvent(EmbedEvent.TASK_LIST_NUDGE.builder()
                        .segment(settings.segment_id(), settings.segment_name())
                        .srcId(TrackerEventOrigin.TASK_LIST.getSrcName())
                        .widget(TrackerEventOrigin.TASK_LIST).build());
    }
    

    
}