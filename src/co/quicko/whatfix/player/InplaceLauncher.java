package co.quicko.whatfix.player;

import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.event.EventPayload.Builder;

public class InplaceLauncher extends WidgetLauncher {

    public InplaceLauncher(PlayerBase player, Settings settings) {
        super(player, settings);
    }

    @Override
    protected String getWidgetPosition() {
        return Themer.value(Themer.INPLACE.ICON_POSITION, settings.position());
    }
    
    @Override
    protected String getUserSpecificPosition() {
        return null;
    }
    
    protected boolean isWidgetMobileInstance() {
        return false;
    }
    
    protected TrackerEventOrigin getWidgetTracker() {
        return TrackerEventOrigin.INPLACE_WIDGET;
    }
    
    protected Builder getWidgetLoadedBuilder() {
        return EmbedEvent.INPLACE_WIDGET_LOADED.builder();
    }
    
    @Override
    protected void applyTheme(Widget widget) {
        Themer.applyTheme(widget, Themer.STYLE.BACKGROUD_COLOR,
                Themer.INPLACE.ICON_BG_COLOR, Themer.STYLE.FONT,
                Themer.INPLACE.FONT);
    }
    
    @Override
    protected BaseWidget initWidget(RePositionCB rePositionCB) {
        return new IPWidget(player, settings, rePositionCB);
    }
}
