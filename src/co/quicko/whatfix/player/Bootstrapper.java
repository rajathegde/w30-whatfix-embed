package co.quicko.whatfix.player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.logger.LOGConstants;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft.Condition;
import co.quicko.whatfix.data.Segment;
import co.quicko.whatfix.data.Tag;
import co.quicko.whatfix.data.TagCache;
import co.quicko.whatfix.overlay.data.EndUser;
import co.quicko.whatfix.overlay.data.WfxData;
import co.quicko.whatfix.player.launchers.BeaconBootstrapper;
import co.quicko.whatfix.player.launchers.BootstrapperWrapper;
import co.quicko.whatfix.player.launchers.InplaceBootstrapper;
import co.quicko.whatfix.player.launchers.PopupBootstrapper;
import co.quicko.whatfix.player.launchers.SmartTipBootstrapper;
import co.quicko.whatfix.player.launchers.TaskerBootstrapper;
import co.quicko.whatfix.player.launchers.WidgetBootstrapper;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.workflowengine.app.AppFactory;
import co.quicko.whatfix.workflowengine.data.strategy.Strategies;
import co.quicko.whatfix.workflowengine.data.strategy.TagStrategy;
import co.quicko.whatfix.workflowengine.data.strategy.UserActionStrategy;
import co.quicko.whatfix.workflowengine.data.strategy.VariableStrategy;

public class Bootstrapper {
    
    private Bootstrapper() {
    }
    
    private static final String END_USER_VARIABLE = "_wfx_data.end_user";
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();
    private static final String END_USER_FIELD_CUSTOM = "custom";
    private static final String END_USER_FIELD_USERACTION = "useraction";
    private static final int END_USER_TOTAL_FIELD_COUNT = 2;

    public static void tryEndUserData(Set<String> endUserFields,
            AsyncCallback<EndUser> cb) {
        EndUser.get(endUserFields, new AsyncCallback<EndUser>() {
            @Override
            public void onSuccess(EndUser endUserData) {
                WfxData.updateEndUserData(endUserData);
                cb.onSuccess(endUserData);
            }

            @Override
            public void onFailure(Throwable caught) {
                cb.onFailure(caught);
            }
        });
    }

    public static Set<String> getEndUserFieldsForComponent(
            BootstrapperWrapper component) {
        return getEndUserFieldsFromSegments(component.segments);
    }
    
    private static Set<String> getEndUserFieldsFromSegments(
            JsArray<? extends Segment> segments) {
        Set<String> endUserFields = new HashSet<>();
        if (segments != null && segments.length() != 0) {
            for (int i = 0; i < segments.length(); i++) {
                Segment segment = segments.get(i);
                if (segment.enabled()) {
                    endUserFields
                            .addAll(getEndUserFields(segment.conditions()));
                    if (hasAllEndUserFields(endUserFields)) {
                        return endUserFields;
                    }
                }
            }
        }
        return endUserFields;
    }

    private static Set<String> getEndUserFieldsFromRoles() {
        Set<String> endUserFields = new HashSet<>();
        Set<Tag> tags = TagCache.getRoleTags();
        if (tags != null && !tags.isEmpty()) {
            for (Tag tag : tags) {
                endUserFields.addAll(
                            getEndUserFields(tag.conditions()));
                if (hasAllEndUserFields(endUserFields)) {
                    return endUserFields;
                }
            }
        }
        return endUserFields;
    }

    public static JsArray<JavaScriptObject> getCombinedSegmentsForOnboarding() {
        // Create combined segments from sp_segments() and gp_segments()
        JsArray<JavaScriptObject> combinedSegments = DataUtil.createArray()
                .cast();
        if (Enterpriser.hasFeature(AdditionalFeatures.ot_newpopup)) {
            JsArray popup = Enterpriser.enterprise().popup_segments();
            JavaScriptObject segment = null;
            if (popup != null && popup.length() != 0) {
                for (int i = 0; i < popup.length(); i++) {
                    segment = popup.get(i);
                    combinedSegments.push(segment);
                }
            }
            return combinedSegments;
        }
        
        JsArray sp = Enterpriser.enterprise().sp_segments();
        JavaScriptObject segment = null;
        if (sp != null && sp.length() != 0) {
            for (int i = 0; i < sp.length(); i++) {
                segment = sp.get(i);
                combinedSegments.push(segment);
            }
        }
        
        JsArray gp = Enterpriser.enterprise().gp_segments();
        if (gp != null && gp.length() != 0) {
            for (int i = 0; i < gp.length(); i++) {
                segment = gp.get(i);
                combinedSegments.push(segment);
            }
        }
        
        return combinedSegments;
    }
    
    private static Set<String> getEndUserFields(JsArray<Condition> conditions) {
        Set<String> endUserFields = new HashSet<>();
        if (conditions == null || conditions.length() == 0) {
            return endUserFields;
        }
        for (int index = 0; index < conditions.length(); index++) {
            Condition condition = conditions.get(index);
            endUserFields.addAll(getEndUserFieldsForCondition(condition));
            if (hasAllEndUserFields(endUserFields)) {
                return endUserFields;
            }
        }
        return endUserFields;
    }

    private static Set<String> getEndUserFieldsForCondition(
            Condition condition) {
        Set<String> endUserFields = new HashSet<>();
        if (condition.type() != null) {
            switch (condition.type().toLowerCase()) {
                case VariableStrategy.TYPE:
                    String o1 = condition.operand(1);
                    if (o1 != null && o1.startsWith(END_USER_VARIABLE)) {
                        endUserFields.add(END_USER_FIELD_CUSTOM);
                    }
                    break;
                case Strategies.USER_ATTR_STRATEGY:
                    endUserFields.add(END_USER_FIELD_CUSTOM);
                    break;
                case TagStrategy.ROLE_TAG:
                    String tagId = condition.operand(1);
                    Tag tag = TagCache.tagByIdDirect(tagId);
                    if (tag != null) {
                        JsArray<Condition> tagconditions = tag.conditions();
                        endUserFields.addAll(getEndUserFields(tagconditions));
                    }
                    break;
                case UserActionStrategy.USER_ACTION:
                    endUserFields.add(END_USER_FIELD_USERACTION);
                    break;
                default:
                    break;
            }
        }
        return endUserFields;
    }

    private static boolean hasAllEndUserFields(Set<String> endUserFields) {
        return endUserFields.size() >= END_USER_TOTAL_FIELD_COUNT;
    }

    private static void bootstrapAllComponents(
            List<BootstrapperWrapper> components, PlayerBase pb) {
        LOGGER.createGroupLogsforKey(LOGConstants.BOOT_STRAP_GROUP_MESSAGE);
        for (BootstrapperWrapper component : components) {
            long sTime1 = System.currentTimeMillis();
            component.componentBootstrapper.bootstrapComponent(pb);
            LOGGER.putGroupLogsforKey(LOGConstants.BOOT_STRAP_GROUP_MESSAGE,
                    LOGGER.getLogMessage(WfxLogLevel.PERFORMANCE,
                            Bootstrapper.class,
                            LOGConstants.BOOT_STRAP_ALL_COMPS, WfxModules.JIT,
                            LOGConstants.BOOT_STRAP_MESSAGE,
                            component.componentBootstrapper.getClass()
                                    .getSimpleName(),
                            (System.currentTimeMillis() - sTime1)));
        }
        LOGGER.flushGroupLogsforKey(LOGConstants.BOOT_STRAP_GROUP_MESSAGE);
    }

    private static List<BootstrapperWrapper> getBootstrapperWrapperList() {
        if (AppFactory.isMobileApp()) {
            return getWidgetWrappers();
        }
        BootstrapperWrapper[] list = {
                new BootstrapperWrapper(new WidgetBootstrapper(),
                        Enterpriser.enterprise().sh_segments()),
                new BootstrapperWrapper(new TaskerBootstrapper(),
                        Enterpriser.enterprise().tl_segments()),
                new BootstrapperWrapper(new PopupBootstrapper(),
                        Bootstrapper.getCombinedSegmentsForOnboarding()),
                new BootstrapperWrapper(new SmartTipBootstrapper(),
                        Enterpriser.enterprise().st_segments()),
                new BootstrapperWrapper(new BeaconBootstrapper(),
                        Enterpriser.enterprise().be_segments()),
                new BootstrapperWrapper(new InplaceBootstrapper(),
                        Enterpriser.enterprise().ip_segments()) };

        return Arrays.asList(list);
    }

    public static List<BootstrapperWrapper> getWidgetWrappers() {
        List<BootstrapperWrapper> listArr = new ArrayList<BootstrapperWrapper>();

        if (MobileUtil.pageIsForTaskList()) {
            listArr.add(new BootstrapperWrapper(new TaskerBootstrapper(),
                    Enterpriser.enterprise().sh_segments()));
        }

        if (MobileUtil.pageIsForSelfHelp()) {
            listArr.add(new BootstrapperWrapper(new WidgetBootstrapper(),
                    Enterpriser.enterprise().sh_segments()));
        }

        if (MobileUtil.pageIsForPopup()) {
            listArr.add(new BootstrapperWrapper(new PopupBootstrapper(),
                    Enterpriser.enterprise().sh_segments()));
        }

        return listArr;
    }

    private static void verifyRoleCheckRequired(BootstrapperWrapper component,
            ArrayList<BootstrapperWrapper> allConditionAvailableList,
            ArrayList<BootstrapperWrapper> roleTagWaitingList) {
        // Check if widget is SelfHelp or TaskList
        if (component.componentBootstrapper instanceof WidgetBootstrapper
                || component.componentBootstrapper instanceof TaskerBootstrapper) {
            // If TL or SH does not use user attributes in segments then add to
            // roleTagWaitingList.
            roleTagWaitingList.add(component);
        } else {
            // If widget other than TL or SH does not use user attributes in
            // segments then add to allConditionAvailableList.
            allConditionAvailableList.add(component);
        }
    }

    private static String getWidgetString(ArrayList<BootstrapperWrapper> list) {
        StringBuilder widgetType = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            BootstrapperWrapper obj = list.get(i);
            widgetType.append(",");
            widgetType.append(obj.componentBootstrapper.getClass().getName());
        }
        return widgetType.toString();
    }

    private static Set<String> checkRoleTagsForEndUserFields(
            ArrayList<BootstrapperWrapper> allConditionAvailableList,
            ArrayList<BootstrapperWrapper> waitList,
            ArrayList<BootstrapperWrapper> roleTagWaitingList) {

        Set<String> endUserFieldsForRoleTags = new HashSet<>();
        if (!roleTagWaitingList.isEmpty()) {
            long roleLookupStartTime = System.currentTimeMillis();
            endUserFieldsForRoleTags.addAll(getEndUserFieldsFromRoles());
            if (!endUserFieldsForRoleTags.isEmpty()) {
                waitList.addAll(roleTagWaitingList);
            } else {
                allConditionAvailableList.addAll(roleTagWaitingList);
            }
            LOGGER.log(WfxLogLevel.PERFORMANCE, Bootstrapper.class,
                    LOGConstants.INITLAUNCHERS_METHOD, WfxModules.INTEGRATIONS,
                    "Time took for roleTag lookup for endUserFields {0} ms",
                    (System.currentTimeMillis() - roleLookupStartTime));
        }
        return endUserFieldsForRoleTags;
    }

    public static void initLaunchers(PlayerBase pb) {
        List<BootstrapperWrapper> list = getBootstrapperWrapperList();

        // List of all components waiting for end_user to be resolved
        final ArrayList<BootstrapperWrapper> waitList = new ArrayList<>();
        // Set of end user fields to retrieve
        final Set<String> endUserFields = new HashSet<>();

        // List of components ready to be loaded no condition pending
        final ArrayList<BootstrapperWrapper> allConditionAvailableList = new ArrayList<>();
        
        // List of all components need roleTags to be checked for evalution
        final ArrayList<BootstrapperWrapper> roleTagWaitingList = new ArrayList<>();

        long segLookupStartTime = System.currentTimeMillis();
        for (BootstrapperWrapper component : list) {
            Set<String> endUserFieldsForComponent = Bootstrapper
                    .getEndUserFieldsForComponent(component);
            if (!endUserFieldsForComponent.isEmpty()) {
                endUserFields.addAll(endUserFieldsForComponent);
                waitList.add(component);
            } else {
                verifyRoleCheckRequired(component, allConditionAvailableList,
                        roleTagWaitingList);
            }
        }

        // If SelfHelp or TaskList is requires to check for userAttribtes in
        // roleTags
        endUserFields.addAll(checkRoleTagsForEndUserFields(
                allConditionAvailableList, waitList, roleTagWaitingList));

        LOGGER.log(WfxLogLevel.PERFORMANCE, Bootstrapper.class,
                LOGConstants.INITLAUNCHERS_METHOD, WfxModules.INTEGRATIONS,
                "Time took for segment lookup for endUserFields {0} ms",
                (System.currentTimeMillis() - segLookupStartTime));

        LOGGER.log(WfxLogLevel.DEBUG, Bootstrapper.class,
                LOGConstants.INITLAUNCHERS_METHOD, WfxModules.INTEGRATIONS,
                "Widgets waiting for endUser call: [" + getWidgetString(waitList)
                        + "],Widgets were waiting for roleTagLookup: ["
                        + getWidgetString(roleTagWaitingList) + "]"
                        + "],Widgets available: ["
                        + getWidgetString(allConditionAvailableList) + "]");

        // try-catch to catch certain exceptions while API calls
        try {
            if (!waitList.isEmpty()) {
                LOGGER.log(WfxLogLevel.PERFORMANCE, Bootstrapper.class,
                        LOGConstants.INITLAUNCHERS_METHOD, WfxModules.JIT,
                        "Waiting for Bootstrapper tryEndUserData callback at {0} ms ",
                        System.currentTimeMillis());
                long sTime = System.currentTimeMillis();
                Bootstrapper.tryEndUserData(endUserFields,
                        new AsyncCallback<EndUser>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        LOGGER.log(WfxLogLevel.PERFORMANCE, Bootstrapper.class,
                                LOGConstants.INITLAUNCHERS_METHOD,
                                WfxModules.JIT,
                                "tryEndUserData, onFailure callback received after {0} ms ",
                                System.currentTimeMillis() - sTime);
                        bootstrapAllComponents(waitList, pb);
                    }

                    @Override
                    public void onSuccess(EndUser endUserData) {
                        LOGGER.log(WfxLogLevel.PERFORMANCE, Bootstrapper.class,
                                LOGConstants.INITLAUNCHERS_METHOD,
                                WfxModules.JIT,
                                "tryEndUserData, onSuccess callback received after {0} ms ",
                                System.currentTimeMillis() - sTime);
                        bootstrapAllComponents(waitList, pb);
                    }
                });
            }
        } catch (Exception e) {
            Console.debug("Error in calling end user API: " + e.getMessage());
            bootstrapAllComponents(waitList, pb);
        }

        // Launch components not using end user data
        bootstrapAllComponents(allConditionAvailableList, pb);
    }
}