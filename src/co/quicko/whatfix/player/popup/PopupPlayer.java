package co.quicko.whatfix.player.popup;

/**
 * Popuplayer.java is the starting point class in embed.js that sets up popup listeners  
 * and creates an i-frame start.html, that invokes StartEntry::onModuleLoad() (start.js)
 * where the content inside popup is created.
 */
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.common.logger.NFRLogger;
import co.quicko.whatfix.common.logger.NFRProperty;
import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.ListenerRegister;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.PopupContent;
import co.quicko.whatfix.data.PopupSegment;
import co.quicko.whatfix.data.Segment;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer.PopupKeyResponse;
import co.quicko.whatfix.overlay.data.AppUser;
import co.quicko.whatfix.player.MobileUtil;
import co.quicko.whatfix.player.event.StartNewFlowEvent;
import co.quicko.whatfix.player.popup.event.PopupEvaluateEvent;
import co.quicko.whatfix.player.popup.event.PopupEvaluatedEvent;
import co.quicko.whatfix.player.popup.smart.event.NoSmartPopupSegmentEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.Service;
import co.quicko.whatfix.service.Service.DirectResponse;
import co.quicko.whatfix.service.Where;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.workflowengine.alg.DOMUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PopupPlayer {
    private static final String DONT_SHOW = "dont_show";
	public static final String CLOSE = "cross";
    public static final String START_URL = "url";
    public static final String START_FLOW = "flow";
    public static final String SKIP = "skip";

    public static final String POPUP_ID_SUFFIX = "popup";
    public static final String POPUP_START_PREFIX = "POPUP_START_";

    public PopupPlayer() {
        EventBus.addHandler(PopupEvaluateEvent.class,
                this::onPopupEvaluateEvent);
        EventBus.addHandler(PopupEvaluatedEvent.class,
                this::onPopupEvaluatedEvent);
    }

    /**
     * This is a handler invoked when PopupEvaluateEvent is fired from
     * PlayerBase. This is a wrapper method that invokes popup-evaluation, GA
     * and popup-creation.
     */
    public void onPopupEvaluateEvent(Event e) {
        long sTime = System.currentTimeMillis();
        Console.debugOn("Popup");
        JsArray<PopupSegment> segments = Enterpriser.enterprise()
                .popup_segments();

        ArrayList<Segment> segmentsArr = Enterpriser.evalAllSegments(segments);
        NFRLogger.addLog(NFRProperty.NEW_POPUPS,
                System.currentTimeMillis() - sTime);
        if (segmentsArr != null) {
            Console.debug(segmentsArr);
            // set of ids attached to all segments
            Set<String> allFlowIds = new HashSet<>();

            // list of set of flowIds attached at segment level so
            // that we dont have to parse the segment again
            List<Set<String>> segmentAttachedFlowIds = new ArrayList<>();
            
            if(!Customizer.isPopupActionCompleteDisabled()) {
                checkForAttachedFlowIds(segmentsArr, allFlowIds,
                        segmentAttachedFlowIds);
            } else {
                checkViewCountRecursively(segmentsArr, 0, segmentAttachedFlowIds,
                        Collections.emptySet(), Collections.emptySet());
            }
        } else {
            Console.debugOff();
            EventBus.fire(new NoSmartPopupSegmentEvent());
        }
    }

    protected void checkForAttachedFlowIds(ArrayList<Segment> segmentsArr,
            Set<String> allFlowIds, List<Set<String>> segmentAttachedFlowIds) {
        for (Segment segment : segmentsArr) {
            PopupSegment popupSegment = segment.cast();
            Set<String> flowIds = getFlowIdAttached(
                    popupSegment.getCompProps());
            segmentAttachedFlowIds.add(flowIds);
            allFlowIds.addAll(flowIds);
        }
        
        if(allFlowIds.isEmpty()) {
            // do not check for the completed content irrespective of flag
            checkViewCountRecursively(segmentsArr, 0, segmentAttachedFlowIds,
                    Collections.emptySet(), allFlowIds);
        } else {
            FlowService.IMPL.completedContents(allFlowIds.toArray(new String[0]),
                    Security.unq_id(),
                    Security.unq_id_with_enc_status(AppUser.getUserId()),
                    Where.create(),
                    new Service.ValidatingCb<DirectResponse<JsArrayString>>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            Console.debug(
                                    "Error on getting completed flowIds for popup action complete - "
                                            + caught.getMessage());
                            
                            // if the completed content call fails,
                            // still process the popup content
                            checkViewCountRecursively(segmentsArr, 0, segmentAttachedFlowIds,
                                    Collections.emptySet(), Collections.emptySet());
                        }

                        @Override
                        protected void onSureSuccess(
                                DirectResponse<JsArrayString> result) {
                            // get the result js array
                            JsArrayString array = result.data();

                            // convert to set to ignore any duplicate values
                            Set<String> completedFlowIds = new HashSet<>();
                            for (int i = 0; i < array.length(); i++) {
                                completedFlowIds.add(array.get(i));
                            }
                            
                            checkViewCountRecursively(segmentsArr, 0, segmentAttachedFlowIds,
                                    completedFlowIds, allFlowIds);
                        }
                    });
        }
    }

    public static void checkViewCountRecursively(List<Segment> segments,
            int index, List<Set<String>> segmentAttachedFlowIds,
            Set<String> allCompletedFlowIds, Set<String> allFlowIds) {
        if (index < segments.size() && null != segments.get(index)) {
            PopupSegment segment = (PopupSegment) segments.get(index);
            final TrackerEventOrigin widgetType = TrackerEventOrigin.POPUP;

            boolean hasCustomTime = Enterpriser.hasCustomTime(segment.conditions());
            String operation = Enterpriser.getOperator(segment.conditions());
            PopupKeyResponse popupKey = Customizer.getPopupKey(segment.name(), widgetType.getType());

            final AsyncCallback<Boolean> cb = PopupUtil.popupCb(new LogFailureCb<PopupSegment> () {
                @Override
                public void onSuccess(PopupSegment result) {
                    if (null == result) {
                        checkViewCountRecursively(segments, index + 1,
                                        segmentAttachedFlowIds, allCompletedFlowIds, allFlowIds);
                    }
                    if(allFlowIds.isEmpty() || Customizer.isPopupActionCompleteDisabled()) {
                        // if the flowIds attached are empty just fire the evaluated event 
                        // or the action is disabled from AC
                        EventBus.fire(new PopupEvaluatedEvent(result));
                    } else {                       
                        checkFlowCompletionForSegment(segments, index, segmentAttachedFlowIds,
                                allCompletedFlowIds, allFlowIds, result);
                    }
                }
                
            }, segment);

            Enterpriser.checkViewCount(segment.segment_id(), TrackerEventOrigin.POPUP, segment.times_to_show(),
              cb, hasCustomTime, null != popupKey ? popupKey.key() : null, operation);
        }
    }
    
    /**
     * Check the given segment at an index has the flow attached completed or not
     * if not completed it will fire a new popup event
     * @param segments
     * @param index
     * @param segmentAttachedFlowIds
     * @param allCompletedFlowIds
     * @param isPopupActionCompleteDisabled
     * @param result
     */
    protected static void checkFlowCompletionForSegment(List<Segment> segments,
            int index, List<Set<String>> segmentAttachedFlowIds,
            Set<String> allCompletedFlowIds, Set<String> allFlowIds,
            PopupSegment result) {
        if (segmentAttachedFlowIds.get(index).isEmpty()) {
            EventBus.fire(new PopupEvaluatedEvent(result));
        } else {
            boolean bShowPopup = false;

            for (String flowId : segmentAttachedFlowIds.get(index)) {
                if (!allCompletedFlowIds.contains(flowId)) {
                    bShowPopup = true;
                    break;
                }
            }

            if (bShowPopup) {
                EventBus.fire(new PopupEvaluatedEvent(result));
            } else {
                checkViewCountRecursively(segments, index + 1,
                        segmentAttachedFlowIds, allCompletedFlowIds, allFlowIds);
            }
        }
    }
    
    public void onPopupEvaluatedEvent(Event e) {
        PopupEvaluatedEvent event = (PopupEvaluatedEvent) e;
        PopupSegment segment = event.getSegment();
        if (segment != null) {
            final TrackerEventOrigin widgetType = TrackerEventOrigin.POPUP;
            final AnalyticsInfo info = AnalyticsInfo.create(segment,
                    widgetType);
            // Invoke AC handler before showing a Popup 
            Customizer.onBeforePopUpShow(segment.segment_id(),
                    widgetType.getType());
            final PopupContent content = PopupContent.create(segment,
                    widgetType);
            showPopup(content, info, popupSuccessCb(widgetType,
                            segment.segment_id(), info));
            MobileUtil.createSmartPopup();
        }
    }

    @SuppressWarnings("unchecked")
    private Set<String> getFlowIdAttached(String componentProps) {
        Set<String> flowIds = new HashSet<>();
        if (componentProps != null) {
            Map<String, Object> propsMap = Common
                    .getComponentPropertiesById(componentProps);
            propsMap.keySet().forEach(id -> {
                Map<String, Object> innerPropsMap = (Map<String, Object>) propsMap
                        .get(id);
                if (null != innerPropsMap && innerPropsMap.containsKey("type")
                        && "WfxButton".equalsIgnoreCase(
                                (String) innerPropsMap.get("type"))
                        && "flow".equalsIgnoreCase(
                                (String) innerPropsMap.get("btn_action"))) {
                    flowIds.add((String) innerPropsMap.get("btn_target"));
                }
            });
        }
        return flowIds;
    }

    /**
     * This method sends all the popup events to GA, invokes dont-show api if
     * dont-show is checked, and invokes AC methods if present.
     */
    public static AsyncCallback<String> popupSuccessCb(
            TrackerEventOrigin widgetType, String segmentId,
            AnalyticsInfo info) {

        return new LogFailureCb<String>() {
            @Override
            public void onSuccess(final String result) {

                // Sending close, start, skip events to GA
                EmbedEvent event = null;
                ButtonEvent buttonEvent = new ButtonEvent(result);
                boolean isButtonTextRequired = false;
                boolean incrementPopupViewedCount = false;
                switch (buttonEvent.eventType) {
                    case CLOSE:
                        event = EmbedEvent.NEW_POPUP_CLOSE;
                        incrementPopupViewedCount = true;
                        break;
                    case START_URL:
                        event = EmbedEvent.NEW_POPUP_START_URL;
                        isButtonTextRequired = true;
                        incrementPopupViewedCount = true;
                        break;
                    case START_FLOW:
                        event = EmbedEvent.NEW_POPUP_START_FLOW;
                        isButtonTextRequired = true;
                        incrementPopupViewedCount = true;
                        break;
                    case SKIP:
                        event = EmbedEvent.NEW_POPUP_SKIP;
                        break;
                    default:
                }

                // in case of CLOSE, START_URL or START_FLOW increase the count
                if (incrementPopupViewedCount) {
                    // Increase the count on starting a flow
                    PopupUtil.popupCloseIncrementer(info.segment_name(), info.segment_id(),
                            TrackerEventOrigin.POPUP);
                }

                if (event != null) {
                    if (event.name().contains(POPUP_START_PREFIX)) {
                    	JSONObject jsonObject = (JSONObject) JSONParser.parseStrict(buttonEvent.additionalInfo);
                		String btnText = ((JSONString) jsonObject.get("buttonText")).stringValue();
                    	if(buttonEvent.eventType.equals(START_URL)) {
                    		String url = ((JSONString) jsonObject.get("url")).stringValue();
                    		GaUtil.trackPopUpURLStartEvent(event, btnText,info,url);//to send url click event
                    	}
                    	else {
                        	GaUtil.trackPopUpFlowStartEvent(event, btnText,info, isButtonTextRequired);//to send flow click event
                    	}
                    }
                    else {
                        GaUtil.trackPopUpEvent(event, widgetType, info);//to send close,skip event
                    }
                }

                sendDontShowStatus(result, segmentId, widgetType, info, event);
            }
        };
    }
    
    private static void sendDontShowStatus(String result, String segmentId,
            TrackerEventOrigin widgetType, AnalyticsInfo info,
            EmbedEvent event) {
        // Invoke dontShowPopup API and send dont-show status to GA
        // if dontShow check-box is checked
        if (result != null && result.contains(DONT_SHOW)) {
            PopupUtil.updateDontShowStatus(segmentId, widgetType);
            GaUtil.trackPopUpEvent(EmbedEvent.NEW_POPUP_DO_NOT_SHOW, widgetType,
                    info);// to send dont_show event
        }
        handlersForAC(event, segmentId);

    }

    /**
     * Invoke AC methods if present on popup success, close and skip actions
     * 
     * @param event
     */
    private static void handlersForAC(EmbedEvent event, String segmentId) {
        if (event != null) {
            switch (event) {
                case NEW_POPUP_CLOSE:
                    Customizer.onPopupClose(segmentId);
                    break;
                case NEW_POPUP_SKIP:
                    Customizer.onPopupSkip(segmentId);
                    break;
                default:
                    Customizer.onPopupSuccess(segmentId);
            }
        }
    }

    /**
     * Creates an i-frame for popup, that invokes StartEntry::onModuleLoad().
     * Invokes common popup listeners.
     */
    private FixedPopup showPopup(PopupContent content, AnalyticsInfo info,
            AsyncCallback<String> onClose) {
        int width = getPopupWidth(content);
        final FixedPopup popup = Common.popup(Framers.start(), width,
                PopupUtil.initialPopupHeight(), POPUP_ID_SUFFIX, "popup");
        newPopupListeners(popup, content, onClose, info);

        PopupUtil.popupSeenTracker(TrackerEventOrigin.POPUP, info);

        popup.center();
        return popup;
    }

    /**
     * <p>
     * <ul>
     * Cross-listeners for the new popup
     * <li>flowHandler : receives the flow-info from popup iframe and sets the
     * flow+segment info in the analytics cache that's used by all the flow play
     * ga events fired when the flow is live.
     * 
     * @param popup
     * @param popupContent
     * @param onClose
     * @param info
     */
    public static void newPopupListeners(final FixedPopup popup,
            final PopupContent popupContent,
            final AsyncCallback<String> onClose, final AnalyticsInfo info) {
        ListenerRegister popupListeners = new ListenerRegister();
        PopupUtil.popupListeners(popup, popupContent, onClose, popupListeners,
                info);

        final CrossListener flowHandler = new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                GaUtil.startInteraction();
                PlayState state = DataUtil.create(content);
                AnalyticsInfo information = AnalyticsInfo.create(
                        info.segment_id(), info.segment_name(),
                        (Flow) state.flow(),
                        TrackerEventOrigin.POPUP.getSrcName());
                GaUtil.setAnalytics(information);

                EventBus.fire(new StartNewFlowEvent(state.flow(),
                        TrackerEventOrigin.POPUP.getSrcName(),
                        TrackerEventOrigin.POPUP.getSrcName()));

            }
        };

        popupListeners.add(flowHandler, "run_flow_from_new_popup");
    }
    

    private int getPopupWidth(PopupContent content) {
        int width = 0;
        if (content.component_properties() != null) {
            JSONObject jsonObj = JSONParser.parseStrict(content.component_properties())
                    .isObject();
            if (jsonObj != null && jsonObj.get("template") != null) {
                JSONObject templateSize = jsonObj.get("template").isObject();
                width = DOMUtil.isMobile()
                        ? Integer.parseInt(templateSize.get("mobile_width")
                                .isString().stringValue())
                        : Integer.parseInt(templateSize.get("desktop_width")
                                .isString().stringValue());
            }
        }
        return width;
    }

    /**
     * Extracts the event type (flow,url,skip,close) and the target in case of
     * flow/url(flow-id,url) from the result sent on button-click from
     * PopupButton.java (start.js).
     */
    private static class ButtonEvent {
        private String eventType;
        private String additionalInfo;// this can have buttonText, URL

        public ButtonEvent(String result) {
            this.eventType = result.substring(0, result.indexOf('_'));

            int targetBegIndex = this.eventType.length() + 1;
            this.additionalInfo = validate(result.substring(targetBegIndex));
        }
    }
    /**
     * This will remove "_" or "_dont_show" from 
     * additional info json string, it will return valid json string 
     * @param val
     * @return
     */
	public static String validate(String val) {
		// additional info can be example- {"buttonText":"Okay, Got_it"}_
		if (val.endsWith("_")) {
			val = val.substring(0, val.length() - 1);
		}
		// additional info can be example- {"buttonText":"Okay, Got it"}_dont_show
		else if (val.endsWith("_" + DONT_SHOW)) {
			val = val.replace("_" + DONT_SHOW, "");
		} else {
		    // do nothing
		}
		return val;
	}  
}