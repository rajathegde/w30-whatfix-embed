package co.quicko.whatfix.player.popup.smart.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.SmartPopupSegment;

public class SmartPopupEvaluatedEvent implements Event {
    private final SmartPopupSegment segment;

    public SmartPopupEvaluatedEvent(SmartPopupSegment segment) {
        this.segment = segment;
    }

    public SmartPopupSegment getSegment() {
        return segment;
    }

}
