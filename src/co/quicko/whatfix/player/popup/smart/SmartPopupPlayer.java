package co.quicko.whatfix.player.popup.smart;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.logger.NFRLogger;
import co.quicko.whatfix.common.logger.NFRProperty;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.ListenerRegister;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.data.GuidedPopupSegment;
import co.quicko.whatfix.data.PopupContent;
import co.quicko.whatfix.data.SmartPopupSegment;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.Action;
import co.quicko.whatfix.data.WidgetType;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.Customizer.PopupKeyResponse;
import co.quicko.whatfix.player.MobileEvents;
import co.quicko.whatfix.player.MobileUtil;
import co.quicko.whatfix.player.PlayerBase;
import co.quicko.whatfix.player.popup.PopupUtil;
import co.quicko.whatfix.player.popup.smart.event.EvaluateSmartPopupEvent;
import co.quicko.whatfix.player.popup.smart.event.NoSmartPopupSegmentEvent;
import co.quicko.whatfix.player.popup.smart.event.SmartPopupEvaluatedEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class SmartPopupPlayer {

    public static final String SMART_POPUP_ID_SUFFIX = "smartPopup";

    public SmartPopupPlayer() {
        EventBus.addHandler(SmartPopupEvaluatedEvent.class,
                this::onSmartPopupEvaluatedEvent);

        EventBus.addHandler(EvaluateSmartPopupEvent.class,
                this::onEvaluateSmartPopupEvent);
    }

    public void onEvaluateSmartPopupEvent(Event e) {
        long sTime = System.currentTimeMillis();
        Console.debugOn("Smart Popup");
        final SmartPopupSegment segment = (GuidedPopupSegment) Enterpriser
                .evalSegments(Enterpriser.enterprise().sp_segments());
        NFRLogger.addLog(NFRProperty.SMART_POPUPS,
                System.currentTimeMillis() - sTime);
        if (segment != null) {
            Console.debug(segment);
            boolean showOnceDaily = Enterpriser
                    .hasCustomTime(segment.conditions());
            PopupKeyResponse popupKey = Customizer.getPopupKey(segment.name(),
                    TrackerEventOrigin.SMART_POPUP.getType());
            Enterpriser.checkViewCount(segment.segment_id(),
                    TrackerEventOrigin.SMART_POPUP, segment.times_to_show(),
                    PopupUtil.popupCb(new LogFailureCb<SmartPopupSegment>() {

                        @Override
                        public void onSuccess(SmartPopupSegment result) {
                            if (result != null) {
                                EventBus.fire(new SmartPopupEvaluatedEvent(result));
                                MobileUtil.createSmartPopup();
                            } else {
                                EventBus.fire(new NoSmartPopupSegmentEvent());
                            }
                        }
                    }, segment), showOnceDaily,
                    null != popupKey ? popupKey.key() : null, null);
        } else {
            Console.debugOff();
            EventBus.fire(new NoSmartPopupSegmentEvent());
        }
    }

    public void onSmartPopupEvaluatedEvent(Event e) {
        SmartPopupEvaluatedEvent event = (SmartPopupEvaluatedEvent) e;
        SmartPopupSegment smartSegment = event.getSegment();
        // in case of desktop, 2 instance of smart popup are
        // getting created on minimizing the application.
        // keeping the latest popup instance only
        if (AppFactory.isDesktopApp()) {
            String[] destroyActions = WidgetType.POP_UP.getDestroyAction();
            String crossMsgContent = GaUtil.PopupCloseOption.cross.toString();
            PlayerBase.destroyLaunchers(destroyActions, crossMsgContent);
            Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
                @Override
                public boolean execute() {
                    createSmartPopup(smartSegment);
                    return false;
                }
            }, 1000);
        } else {
            createSmartPopup(smartSegment);
        }

    }
    
    private void createSmartPopup(final SmartPopupSegment smartSegment) {
        final TrackerEventOrigin widgetType = TrackerEventOrigin.SMART_POPUP;
        final String segmentId = smartSegment.segment_id();
        final AnalyticsInfo info = AnalyticsInfo.create(smartSegment,
                widgetType);
        JavaScriptObject result = Customizer.onBeforePopUpShow(segmentId,
                widgetType.getType());
        showSmartPopup(smartSegment.name(),
                PopupContent.create(smartSegment, widgetType, result), info,
                new LogFailureCb<String>() {

                    @Override
                    public void onSuccess(final String result) {
                        EmbedEvent event;
                        if (result != null && result.contains(
                                GaUtil.PopupCloseOption.cross.toString())) {
                            event = EmbedEvent.POPUP_CLOSE;

                        } else {
                            event = EmbedEvent.POPUP_START;
                        }
                        GaUtil.trackPopUpEvent(event, widgetType, info);
                        if (result != null && result.contains("dont_show")) {
                            PopupUtil.updateDontShowStatus(segmentId,
                                    widgetType);
                            GaUtil.trackPopUpEvent(EmbedEvent.POPUP_DO_NOT_SHOW,
                                    widgetType, info);
                        }
                        if (EmbedEvent.POPUP_CLOSE.equals(event)) {
                            Customizer.onPopupClose(segmentId);
                        } else {
                            Customizer.onPopupSuccess(segmentId);
                        }

                    }
                });
    }

    public FixedPopup showSmartPopup(String segmentName,
            PopupContent popupContent, AnalyticsInfo info,
            AsyncCallback<String> onClose) {
        // this was done because of the issue of lower width set to popups in
        // desktop on minimizing the app randomly
        int popupWidth = AppFactory.isDesktopApp()
                ? Framers.popupWidthForDesktop()
                : Framers.popupWidth();
        final FixedPopup popup = Common.popup(Framers.start(),
                popupWidth, PopupUtil.initialPopupHeight(),
                SMART_POPUP_ID_SUFFIX, "smart_popup");
        PopupUtil.popupListeners(popup, popupContent, onClose,
                new ListenerRegister(), info);
        PopupUtil.popupSeenIncrementer(segmentName, info.segment_id(),
                TrackerEventOrigin.SMART_POPUP);
        PopupUtil.popupSeenTracker(TrackerEventOrigin.SMART_POPUP, info);
        popup.center();
        return popup;
    }

}
