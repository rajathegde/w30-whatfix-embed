package co.quicko.whatfix.player.popup.end;

import static co.quicko.whatfix.ga.GaUtil.analyticsCache;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.Framers.Framer;
import co.quicko.whatfix.common.ListenerRegister;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.FlowEndResponse;
import co.quicko.whatfix.data.FlowEndResponse.FeedbackOptions;
import co.quicko.whatfix.data.PopupData;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.player.event.StartNewFlowEvent;
import co.quicko.whatfix.player.popup.PopupUtil;
import co.quicko.whatfix.tracker.ExitPoint;
import co.quicko.whatfix.tracker.event.EmbedEvent;

public class EndPopupPlayer {

    private static final String END_POPUP = "endPopup";

    // this is the current state
    private PopupPanel endPop;

    public EndPopupPlayer() {
        EventBus.addHandler(ShowEndPopupEvent.class, this::onFlowCompleted);
        EventBus.addHandler(HideEndPopupEvent.class, this::onHideEndPopup);
    }

    private void onFlowCompleted(Event e) {
        ShowEndPopupEvent event = (ShowEndPopupEvent) e;
        showEnd(event.didComplete(), event.getFlow(), event.draft());
    }

    private void onHideEndPopup(Event event) {
        if (endPop != null && endPop.isShowing()) {
            CrossMessager.sendMessage("popup_close", "");

        } else {
            endPop = null;
        }
    }

    private void showEnd(final boolean completed, Draft flow, boolean draft) {

        endPop = showEndPopup(Framers.end(flow, draft), flow, completed,
                new AsyncCallback<String>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(final String result) {
                        EventBus.fire(new HideEndPopupEvent());
                        if (result == null || result.length() == 0) {
                            return;
                        }
                        if (GaUtil.EndPopupCloseOptions.button.toString()
                                .equals(result)) {
                            GaUtil.trackEndPopUpCloseEvent(
                                    EmbedEvent.END_POPUP_CLOSE_BUTTON);
                            return;
                        } else if (GaUtil.EndPopupCloseOptions.cross.toString()
                                .equals(result)) {
                            GaUtil.trackEndPopUpCloseEvent(
                                    EmbedEvent.END_POPUP_CLOSE_CROSS);
                            return;
                        }
                        FlowEndResponse response = DataUtil.create(result);
                        if (response.next_flow() != null && StringUtils
                                .stringifyObject(response.next_flow())
                                .length() != 0) {
                            GaUtil.trackEndPopUpCloseEvent(
                                    EmbedEvent.END_POPUP_CLOSE);
                            GaUtil.resetFlowInCache(analyticsCache.segment_id(),
                                    analyticsCache.segment_name(),
                                    response.next_flow(), "end_popup");
                            EventBus.fire(new StartNewFlowEvent(
                                    response.next_flow(), "end_popup",
                                    TrackerEventOrigin.END_POPUP.getSrcName()));
                        }
                        if (response.feedback() != null) {
                            ContextualInfo contextualOptionsInfo = DataUtil
                                    .create();
                            FlowEndResponse.FeedbackOptions options = extractOptions(
                                    response);
                            if (options != null) {
                                contextualOptionsInfo = FlowEndResponse
                                        .getFeedbackOptions(
                                                ContextualInfo.OuterKey.FEEDBACK_OPTIONS,
                                                options);
                            }
                            Customizer.onFlowFeedback(response.feedback());
                            Common.tracker()
                                    .onEvent(EmbedEvent.FLOW_FEEDBACK.builder()
                                            .replaceInPage("feedback",
                                                    response.feedback())
                                            .analyticsInfo(analyticsCache)
                                            .playId(analyticsCache.play_id())
                                            .srcId(analyticsCache.src_id())
                                            .contextualInfo(contextualOptionsInfo)
                                            .build());
                            // Sending source_flow_id here as in case of
                            // branching we consider
                            // source_flow_id as the completed flow

                            analyticsCache.addToFlowPaths(
                                    analyticsCache.source_flow_id()
                                            + GaUtil.COLON_SEPARATOR
                                            + ExitPoint.FEEDBACK,
                                    analyticsCache.source_flow_title()
                                            + GaUtil.COLON_SEPARATOR
                                            + ExitPoint.FEEDBACK);
                        }
                    }

                    private native FeedbackOptions extractOptions(
                            FlowEndResponse response) /*-{
                        return response.feedback_options;
                    }-*/;

                }, analyticsCache);
    }

    private PopupPanel showEndPopup(Framer framer, final Draft flow,
            final boolean completed, final AsyncCallback<String> onClose,
            final AnalyticsInfo info) {
        int popupWidth = Framers.popupWidth();
        if (popupWidth > Framers.MAX_END_WIDTH) {
            popupWidth = Framers.MAX_END_WIDTH;
        }
        final FixedPopup popup = Common.popup(framer, popupWidth,
                PopupUtil.initialPopupHeight(), END_POPUP, PopupUtil.POPUP_FRAME_TITLE);
        PopupUtil.commonPopupListeners(popup,
                PopupData.getPopupData(flow, popupWidth), onClose,
                new ListenerRegister(), info, END_POPUP);
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                CrossMessager.removeListener(this, "end_popup_seen");
                EventBus.fire(new EndPopupSeenEvent());
            }
        }, "end_popup_seen");
        popup.center();
        return popup;
    }
}
