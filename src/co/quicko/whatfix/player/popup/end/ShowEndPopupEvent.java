package co.quicko.whatfix.player.popup.end;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

public class ShowEndPopupEvent implements Event {

    private final boolean wasComplete;

    private final Draft flow;

    private final boolean draft;

    public ShowEndPopupEvent(boolean wasComplete, Draft flow, boolean draft) {
        this.wasComplete = wasComplete;
        this.flow = flow;
        this.draft = draft;
    }

    public boolean didComplete() {
        return wasComplete;
    }

    public Draft getFlow() {
        return this.flow;
    }

    public boolean draft() {
        return this.draft;
    }
}
