package co.quicko.whatfix.player.popup;

import java.sql.Timestamp;
import java.util.Date;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.ConfirmationPopUp;
import co.quicko.whatfix.common.EntDataVersion;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.ListenerRegister;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.PopUpCallback;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.PopUpConstants.ConfirmationPopUpImageEnum;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.contextualinfo.LinkInfo;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.PopupContent;
import co.quicko.whatfix.data.PopupData;
import co.quicko.whatfix.data.Segment;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.data.UserPreferences;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.Customizer.PopupKeyResponse;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.Callbacks;
import co.quicko.whatfix.service.EnterpriseService;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.service.WfxInfo;
import co.quicko.whatfix.service.online.AdminServiceOnline;
import co.quicko.whatfix.service.online.FlowServiceOnline;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class PopupUtil {
    
    public static final String WIDGET_POPUP = "widgetPopup";
    
    public static final String POPUP_FRAME_TITLE = "popup";
    
    public static ConfirmationPopUp dataVersionPopup;
    
    public static final Long MILLISECONDS_IN_DAY = 1000l * 60l * 60l * 24l;
    
    public static void popupSeenTracker(final TrackerEventOrigin widgetType,
            final AnalyticsInfo info) {
        popupSeenTracker(widgetType, widgetType, info);
    }

    public static void popupSeenTracker(final TrackerEventOrigin widgetType,
            final TrackerEventOrigin srcWidget, final AnalyticsInfo info) {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                CrossMessager.removeListener(this, "popup_seen");
                if (widgetType.equals(TrackerEventOrigin.POPUP)) {
                    Common.tracker()
                    .onEvent(EmbedEvent.NEW_POPUP_LOADED.builder()
                            .widget(widgetType).analyticsInfo(info)
                            .srcId(srcWidget).build());
                } else {
                    Common.tracker()
                    .onEvent(EmbedEvent.POPUP_LOADED.builder()
                            .widget(widgetType).analyticsInfo(info)
                            .srcId(srcWidget).build());
                }
                

                // To be removed once the analytics events are finalized
                if (!widgetType.equals(TrackerEventOrigin.POPUP)) {
                    Common.tracker()
                            .onEvent(EmbedEvent.POPUP_LOADED.builder()
                                    .widget(widgetType).analyticsInfo(info)
                                    .srcId(srcWidget).build());
                }
            }
        }, "popup_seen");
    }
    
    public static void popupSeenIncrementer(final String segmentName,
            final String segment_id,
            final TrackerEventOrigin widgetType) {
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                CrossMessager.removeListener(this, "popup_seen");
                PopupKeyResponse popupKey = Customizer
                        .getPopupKey(segmentName, widgetType.getType());
                FlowService.IMPL.incrementPopupViewCount(segment_id, widgetType,
                        null != popupKey ? popupKey.key() : null,
                        Callbacks.emptyVoidCb());
            }
        }, "popup_seen");
    }

    // On clicking the close button increment the popupViewCount
    public static void popupCloseIncrementer(final String segmentName,
            final String segment_id,
            final TrackerEventOrigin widgetType) {

        PopupKeyResponse popupKey = Customizer
                .getPopupKey(segmentName, widgetType.getType());
        FlowService.IMPL.incrementPopupViewCount(segment_id, widgetType,
                null != popupKey ? popupKey.key() : null,
                Callbacks.emptyVoidCb());
    }

    public static void updateDontShowStatus(String segment_id,
            TrackerEventOrigin widgetType) {
        FlowService.IMPL.dontShowPopup(segment_id, widgetType, Callbacks.emptyVoidCb());
    }
    
    /*
     * Listeners for Smart, Guided and End Popups
     */
    public static void commonPopupListeners(final FixedPopup popup,
            final PopupData popupData, final AsyncCallback<String> onClose,
            final ListenerRegister popupListeners, final AnalyticsInfo info,
            final String popupType) {
        final CrossListener openListener = new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                if (WIDGET_POPUP.equals(popupType)) {
                    popup.setModal(true);
                    popup.getGlassElement()
                            .removeClassName(Common.CSS.hideGlass());
                }
                CrossMessager.sendMessageToFrame(popup.getWidget().getElement(),
                        "frame_data", StringUtils.stringifyObject(popupData));
            }
        };
        final CrossListener closeListener = new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                popup.hide();
                popupListeners.clear();
                onClose.onSuccess(content);
            }
        };
        final CrossListener resizeListener = new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                popup.removeStyleName(Common.CSS.hideForAnimation());
                popup.setHeight(content + "px");

                // Make center alignment for all pop-up apart form new pop-up
                if (info.src_id() != POPUP_FRAME_TITLE) {
                  popup.center();
                }
            }
        };
        
        
        final CrossListener linkClickListener = new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                LinkInfo link = DataUtil.create(content);
                ContextualInfo contextualLinkInfo = LinkInfo
                        .getLinkInfo(ContextualInfo.OuterKey.LINK_CLICK, link);
                Common.tracker().onEvent(EmbedEvent.LINK_CLICK.builder()
                        .contextualInfo(contextualLinkInfo)
                        .analyticsInfo(info).build());
            }
        };
        
        final CrossListener zoomHandler = new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                CrossMessager.sendMessageToFrame(popup.getWidget().getElement(),
                        "resize_on_zoom",
                        String.valueOf(Window.getClientHeight()));

                if (info.src_id() == POPUP_FRAME_TITLE) {
                    positionPopupAtCenter(popup);
                }
            }
        };

		final CrossListener overFlowListener = (String type, String content) -> {
			popup.getWidget().getElement().setAttribute("Scrolling", "auto");

			// Place the popup at center of the screen by screen
			positionPopupAtCenter(popup);
			// Remove the class pop-up and add the new class newPopupCenter
			popup.removeStyleName(Common.CSS.popup());
			popup.addStyleName(Common.CSS.newPopupCenter());
		};

        
        popupListeners.add(openListener, "popup_frame_data");
        popupListeners.add(closeListener, "popup_close");
        popupListeners.add(resizeListener, "resize_popup");
        popupListeners.add(linkClickListener, "link_click");
        popupListeners.add(zoomHandler, "zoom_handle");
        popupListeners.add(overFlowListener, "enable_auto_scroll");
    }
    
    // Initial height of Iframe set to 1
    // Specifically to handle export version inside Chrome Extension
    // Chrome Engine's rendering for Elements inside Popup was inconsistent
    // for iframes with height 0px
    public static int initialPopupHeight() {
        return 1;
    }

    
    public static <T extends Segment> AsyncCallback<Boolean> popupCb(
            final AsyncCallback<T> segmentCb, final T segment) {
        return new LogFailureCb<Boolean>() {

            @Override
            public void onSuccess(Boolean result) {
                segmentCb.onSuccess((result == true) ? segment : null);
            }
        };
    }
    
    
    /*
     * Smart/Native/Guided Pop-ups
     */
    public static void popupListeners(final FixedPopup popup,
            final PopupContent popupContent,
            final AsyncCallback<String> onClose,
            final ListenerRegister popupListeners, final AnalyticsInfo info) {
        int popupWidth = AppFactory.isDesktopApp()
                ? Framers.popupWidthForDesktop()
                : Framers.popupWidth();
        PopupData popupData = PopupData.getPopupData(popupContent, popupWidth);
        PopupUtil.commonPopupListeners(popup, popupData, onClose, popupListeners, info,
                PopupUtil.WIDGET_POPUP);
    }
    
    public static void showDataVersionPopup(String title, String text) {
        if(dataVersionPopup != null) {
            if(!dataVersionPopup.isShowing()) {
                showDataVersionPopup(title, text, true, false, null); 
            }
        } else {
            showDataVersionPopup(title, text, true, false, null);
        }
    }
    
    public static void showDataVersionPopup(String title, String text,
            boolean isAutoUpGrade) {
        showDataVersionPopup(title, text, false, isAutoUpGrade, null);
    }

    public static void showDataVersionPopup(String title, String text,
            boolean isAutoUpGrade, String formatedDate) {
        showDataVersionPopup(title, text, false, isAutoUpGrade, formatedDate);
    }

    public static void showDataVersionPopup(String title, String text,
            boolean isRefreshPopup, boolean isAutoUpGrade,
            String formatedDate) {
        int zIndex = 2147483647;
        String primaryButtonName;
        PopUpCallback callback = getUpdateUSerPrefrence(isAutoUpGrade);
        ConfirmationPopUpImageEnum popupSvg = ConfirmationPopUpImageEnum.TICK_SVG;
        String releaseNoteLink = WfxInfo.Links.RELEASE_NOTE.toString();
        String nameForReleaseNoteLink = Common.i18nConstants.readReleaseNotes();
        
        if(isAutoUpGrade) {
            primaryButtonName = Common.i18nConstants.upgradeNow();
            popupSvg = ConfirmationPopUpImageEnum.WARNING_TRIANGLE;
            nameForReleaseNoteLink = Common.i18nConstants.readReleaseNotes();
        } else if(isRefreshPopup) {
            primaryButtonName = Common.i18nConstants.refresh();
            callback = getPopupCallBackWithRefresh();  
        } else {
            primaryButtonName = Common.i18nConstants.okay();            
        }
        if (isAutoUpGrade) {
            dataVersionPopup = new ConfirmationPopUp(popupSvg, title, text,
                    primaryButtonName, zIndex, callback, releaseNoteLink,
                    nameForReleaseNoteLink, formatedDate,
                    "Okay");
        } else {
            dataVersionPopup = new ConfirmationPopUp(popupSvg, title, text,
                    primaryButtonName, zIndex, callback, releaseNoteLink,
                    nameForReleaseNoteLink, "");
        }
        dataVersionPopup.showPopup();
        dataVersionPopup.addStyleName(Common.CSS.dvRefreshPopup());
    }
    
    public static void showLastDataVersionTimer(Date releaseDate, String link) {
        PopUpCallback callback = getUpdateUSerPrefrence(false, true);
        dataVersionPopup = new ConfirmationPopUp(
                Common.i18nConstants.whatFixAutoUpgrade(),
                Common.i18nConstants.startingIn(), Common.i18nConstants.gotIt(),
                callback, link, Common.i18nConstants.readReleaseNotes(), releaseDate,
                Common.i18nConstants.lastNotificationDescription());
        dataVersionPopup.showPopup();
        dataVersionPopup.addStyleName(Common.CSS.dvRefreshPopup());
    }
    
    private static PopUpCallback getPopupCallBackWithRefresh() {
        return new PopUpCallback() {

            @Override
            public void onPositiveAction() {
                Window.Location.reload();
            }

            @Override
            public void onNegativeAction() {
                dataVersionPopup.close();
            }
        };
    }
    
    private static void updateUserPreference(boolean isAutoUpgrade,
            UserPreferences userPref) {
        AdminServiceOnline.IMPL.updateUserPreferences(Security.user_id(),
                userPref, new AsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        if (isAutoUpgrade) {
                            Security.preference(
                                    Common.i18nConstants
                                            .firstDataVersionNotification(),
                                    Boolean.toString(Boolean.FALSE));
                        } else {
                            Security.preference(
                                    Common.i18nConstants.dataVersionUpdated(),
                                    Boolean.toString(Boolean.FALSE));
                        }
                        dataVersionPopup.close();
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        dataVersionPopup.close();
                    }
                });
    }
    
    private static PopUpCallback getUpdateUSerPrefrence(boolean isAutoUpgrade) {
        return getUpdateUSerPrefrence(isAutoUpgrade, false);
    }

    private static PopUpCallback getUpdateUSerPrefrence(boolean isAutoUpgrade,
            boolean isLastNotification) {
        return new PopUpCallback() {

            @Override
            public void onPositiveAction() {
                UserPreferences userPref = DataUtil.create();
                if (isAutoUpgrade) {
                    FlowServiceOnline.IMPL
                            .updateDataVersion(new AsyncCallback<Void>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    // Nothing to be done if fails
                                }

                                @Override
                                public void onSuccess(Void result) {
                                    userPref.data_version_updated(false);
                                    PopupUtil.showDataVersionPopup(
                                            Common.i18nConstants.whatfixSucessfullyUpgraded(),
                                            Common.i18nConstants.dataVersionMessage());

                                }
                            });
                } else if (isLastNotification) {
                    userPref.last_dv_notification(false);
                } else {
                    userPref.data_version_updated(false);
                }
                updateUserPreference(isAutoUpgrade, userPref);
            }

            @Override
            public void onNegativeAction() {
                if (isAutoUpgrade) {
                    UserPreferences userPref = DataUtil.create();
                    userPref.first_dv_notification(0);
                    updateUserPreference(isAutoUpgrade, userPref);
                }
                dataVersionPopup.close();
            }
        };
    }
    
    public static void editorLastDVNotification() {
        EnterpriseService.IMPL
                .getVersionDetails(new AsyncCallback<EntDataVersion>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // Nothing to be done
                    }

                    @Override
                    public void onSuccess(EntDataVersion result) {
                        checkForLastDVNotification(result);
                    }
                });
    }

    public static Timer getShedulerForNotification(Date releaseDate) {
        return new Timer() {
            public void run() {
                EnterpriseService.IMPL
                        .getVersionDetails(new AsyncCallback<EntDataVersion>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // Nothing can be done
                            }

                            @Override
                            public void onSuccess(EntDataVersion results) {
                                if (Integer.parseInt(
                                        results.currentDataVersion()) != Integer
                                                .parseInt(results
                                                        .latestDataVersion())) {
                                    showLastDataVersionTimer(releaseDate,
                                            results.latestDataVersionLink());
                                }
                            }
                        });
            }
        };
    }

    public static void shdulerNotificationForDV(Timer timer, Date releaseDate,
            Long time2, EntDataVersion result) {
        long diffInTime = (releaseDate.getTime() - time2);
        int timeInSec = (int) (diffInTime / 1000);
        int diffInHr = (int) (diffInTime / (60 * 60 * 1000));
        if (timeInSec > 0 && diffInHr < 24) {

            int diffInMin = (int) (diffInTime / (60 * 1000));
            int notifyBeforeMin = result.notifyBeforeMinutes();

            // If the number of minutes before which the pop-up
            // needs to be show is already over then show the
            // pop-up immediately
            if (diffInHr == 0 && diffInMin < notifyBeforeMin) {
                timer.schedule(1000);
            } else {
                // Else reduce the diff in time by the given
                // minute so that that pop-up can be shown before
                int newTime = (int) (diffInTime)
                        - (result.notifyBeforeMinutes() * 60 * 1000);
                timer.schedule(newTime);
            }
        }
    }

    public static void checkForLastDVNotification(EntDataVersion result) {
        if (Boolean.FALSE.toString().equalsIgnoreCase(Security
                .preference(Common.i18nConstants.lastDVNotification()))) {
            return;
        }
        String currentDVVersion = result.currentDataVersionReleaseDate();
        if (StringUtils.isBlank(currentDVVersion)) {
            return;
        }
        Long time2 = System.currentTimeMillis();
        Long time = Long.parseLong(currentDVVersion)
                + (Integer.parseInt(result.versionValidTillDays())
                        * PopupUtil.MILLISECONDS_IN_DAY);
        Timestamp stamp = new Timestamp(time);
        Date releaseDate = new Date(stamp.getTime());
        Timer timer = getShedulerForNotification(releaseDate);
        shdulerNotificationForDV(timer, releaseDate, time2, result);
    }

    public static void positionPopupAtCenter (FixedPopup popup) {
		Number width = popup.getElement().getClientWidth() * 0.5;
		Number height = popup.getElement().getClientHeight() * 0.5;

        String styleString = ""
            + "top: calc(50% - " + height + "px) !important;"
            + "left: calc(50% - " + width + "px) !important;";

        popup.getElement().setAttribute("style", styleString);
    }

}
