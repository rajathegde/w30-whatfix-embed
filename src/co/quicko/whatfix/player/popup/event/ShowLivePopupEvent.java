package co.quicko.whatfix.player.popup.event;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.TrackerEventOrigin;

public class ShowLivePopupEvent implements Event {

    private PlayState state;
    private AnalyticsInfo info;
    private TrackerEventOrigin srcWidget;

    public ShowLivePopupEvent(PlayState state, AnalyticsInfo info,
            TrackerEventOrigin srcWidget) {
        this.state = state;
        this.info = info;
        this.srcWidget = srcWidget;
    }

    public PlayState getState() {
        return state;
    }

    public AnalyticsInfo getAnalyticsInfo() {
        return info;
    }

    public TrackerEventOrigin getSrcWidget() {
        return srcWidget;
    }

}
