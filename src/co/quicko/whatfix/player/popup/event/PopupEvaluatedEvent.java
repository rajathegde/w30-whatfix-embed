package co.quicko.whatfix.player.popup.event;

/**
 * This event is fired from PopupPlayer::onPopupEvaluate()
 * when the popup evaluation is successful.
 * Handler : PopupPlayer :: onPopupEvaluatedEvent()
 */
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.PopupSegment;

public class PopupEvaluatedEvent implements Event {
    private final PopupSegment segment;

    public PopupEvaluatedEvent(PopupSegment segment) {
        this.segment = segment;
    }

    public PopupSegment getSegment() {
        return segment;
    }
}
