package co.quicko.whatfix.player.popup.event;

/**
 * This event is fired from PlayerBase::tryOnboarding() 
 * when new-popup feature is enabled.
 * Handler : PopupPlayer :: onPopupEvaluateEvent()
 */
import co.quicko.whatfix.common.EventBus.Event;

public class PopupEvaluateEvent implements Event {

}