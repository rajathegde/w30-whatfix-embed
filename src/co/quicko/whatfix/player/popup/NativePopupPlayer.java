package co.quicko.whatfix.player.popup;

import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.ListenerRegister;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.PopupContent;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.player.event.StartNewFlowEvent;
import co.quicko.whatfix.player.popup.event.ShowLivePopupEvent;
import co.quicko.whatfix.tracker.event.EmbedEvent;

/**
 * This class handles launching flow as a popup from TL/SH in the end-users
 * page.
 * <p>
 * Following actions are performed
 * <ul>
 * <li>launching flow as a popup
 * <li>GA events
 * <li>AC events
 * </ul>
 */
public class NativePopupPlayer {

    public static final String CLOSE = "cross";
    public static final String SKIP = "skip";
    public static final String NATIVE_POPUP_ID_SUFFIX = "nativePopup";

    public NativePopupPlayer() {
        EventBus.addHandler(ShowLivePopupEvent.class, this::onShowLivePopup);
    }

    public void onShowLivePopup(Event e) {
        Console.debugOn("Native Popup");
        ShowLivePopupEvent event = (ShowLivePopupEvent) e;
        PlayState state = event.getState();
        AnalyticsInfo info = event.getAnalyticsInfo();
        TrackerEventOrigin srcWidget = event.getSrcWidget();
        Draft flow = state.flow();
        PopupContent popupContent = PopupContent.create(flow);

        final FixedPopup popup = Common.popup(Framers.start(),
                Framers.popupWidth(), PopupUtil.initialPopupHeight(),
                NATIVE_POPUP_ID_SUFFIX, PopupUtil.POPUP_FRAME_TITLE);

        AsyncCallback<String> onClose = nativePopupSuccessCb(srcWidget, state,
                info);
        PopupUtil.popupListeners(popup, popupContent, onClose,
                new ListenerRegister(), info);
        PopupUtil.popupSeenTracker(TrackerEventOrigin.GUIDED_POPUP, srcWidget,
                info);
        popup.center();
    }

    private AsyncCallback<String> nativePopupSuccessCb(
            TrackerEventOrigin srcWidget, PlayState state, AnalyticsInfo info) {

        return new LogFailureCb<String>() {
            @Override
            public void onSuccess(String result) {
                // Sending start, close, skip events to GA
                GaUtil.setAnalytics(info);
                String GaEvent = result.substring(0, result.indexOf("_"));
                EmbedEvent event = null;
                switch (GaEvent) {
                    case CLOSE:
                        event = EmbedEvent.POPUP_CLOSE;
                        break;
                    case SKIP:
                        event = EmbedEvent.POPUP_SKIP;
                        break;
                    default:
                        event = EmbedEvent.POPUP_START;
                }
                GaUtil.trackPopUpEvent(event, TrackerEventOrigin.GUIDED_POPUP,
                        info);

                // Invoke AC methods if present
                // on popup success, close and skip actions.
                // Start a flow on clicking GUIDE-ME.
                String flowId = state.flow().flow_id();
                switch (event) {
                    case POPUP_CLOSE:
                        Customizer.onPopupClose(flowId);
                        break;
                    case POPUP_SKIP:
                        Customizer.onPopupSkip(flowId);
                        break;
                    default:
                    	Customizer.onPopupSuccess(flowId);
                        if (state.flow().isPlayAsPopup()) {
                            state.flow().is_flow_playing(true);
                        }
                        EventBus.fire(new StartNewFlowEvent(state.flow(),
                                srcWidget.getSrcName(),
                                TrackerEventOrigin.GUIDED_POPUP.getSrcName()));
                }
                Console.debugOff();
            }
        };
    }
}
