package co.quicko.whatfix.player.popup.guided.event;

import co.quicko.whatfix.common.EventBus.Event;

/**
 * This event is called at the start of the application when one needs to
 * evaluate the guided popup segments for the enterprise.
 * 
 * @author sureshsarda
 *
 */
public class EvaluateGuidedPopupEvent implements Event {

}
