package co.quicko.whatfix.player.popup.guided.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.GuidedPopupSegment;

public class GuidedPopupEvaluatedEvent implements Event {

    private final GuidedPopupSegment segment;

    public GuidedPopupEvaluatedEvent(GuidedPopupSegment segment) {
        this.segment = segment;
    }

    public GuidedPopupSegment getSegment() {
        return segment;
    }

}
