package co.quicko.whatfix.player.popup.guided;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.logger.NFRLogger;
import co.quicko.whatfix.common.logger.NFRProperty;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.ListenerRegister;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.PlayState;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.GuidedPopupSegment;
import co.quicko.whatfix.data.PopupContent;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.Customizer.PopupKeyResponse;
import co.quicko.whatfix.player.MobileUtil;
import co.quicko.whatfix.player.event.StartNewFlowEvent;
import co.quicko.whatfix.player.popup.PopupUtil;
import co.quicko.whatfix.player.popup.guided.event.EvaluateGuidedPopupEvent;
import co.quicko.whatfix.player.popup.guided.event.GuidedPopupEvaluatedEvent;
import co.quicko.whatfix.player.popup.guided.event.NoGuidedPopupSegmentEvent;
import co.quicko.whatfix.player.popup.smart.event.EvaluateSmartPopupEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.tracker.event.EmbedEvent;

public class GuidedPopupPlayer {

    public static final String CLOSE = "cross";
    public static final String SKIP = "skip";
    public static final String GUIDED_POPUP_ID_SUFFIX = "guidedPopup";

    public GuidedPopupPlayer() {
        EventBus.addHandler(GuidedPopupEvaluatedEvent.class,
                this::onGuidedPopupEvaluatedEvent);

        EventBus.addHandler(EvaluateGuidedPopupEvent.class,
                this::onEvalueateGuidedPopupEvent);
    }

    public void onEvalueateGuidedPopupEvent(Event e) {
        long sTime = System.currentTimeMillis();
        Console.debugOn("Guided Popup");
        final GuidedPopupSegment segment = (GuidedPopupSegment) Enterpriser
                .evalSegments(Enterpriser.enterprise().gp_segments());
        NFRLogger.addLog(NFRProperty.GUIDED_POPUPS,
                System.currentTimeMillis() - sTime);
        if (segment != null) {
            Console.debug(segment);
            boolean showOnceDaily = Enterpriser
                    .hasCustomTime(segment.conditions());
            PopupKeyResponse popupKey = Customizer.getPopupKey(segment.name(),
                    TrackerEventOrigin.GUIDED_POPUP.getType());
            Enterpriser.checkViewCount(segment.segment_id(),
                    TrackerEventOrigin.GUIDED_POPUP, segment.times_to_show(),
                    PopupUtil.popupCb(new LogFailureCb<GuidedPopupSegment>() {

                        @Override
                        public void onSuccess(GuidedPopupSegment result) {
                            EventBus.fire(
                                    new GuidedPopupEvaluatedEvent(result));
                        }
                    }, segment), showOnceDaily,
                    null != popupKey ? popupKey.key() : null, null);
        } else {
            Console.debugOff();
            EventBus.fire(new EvaluateSmartPopupEvent());
        }
    }

    public void onGuidedPopupEvaluatedEvent(Event e) {
        GuidedPopupEvaluatedEvent event = (GuidedPopupEvaluatedEvent) e;
        GuidedPopupSegment segment = event.getSegment();
        if (segment != null) {
            final TrackerEventOrigin widgetType = TrackerEventOrigin.GUIDED_POPUP;
            final JavaScriptObject result = Customizer.onBeforePopUpShow(
                    segment.segment_id(), widgetType.getType());
            String flowId = segment.flow_id();
            FlowService.IMPL.flow(flowId, new AsyncCallback<Flow>() {
                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(final Flow flow) {
                    PlayState state = DataUtil.create();
                    state.flow(flow);
                    state.src_id(widgetType.getSrcName());
                    AnalyticsInfo info = AnalyticsInfo.create(segment, flow,
                            widgetType);
                    showGuidedPopup(state.flow(),
                            PopupContent.create(segment, widgetType, result),
                            segment.segment_id(), guidedPopupSuccessCb(state,
                                    info, widgetType, segment.segment_id()),
                            info);
                    MobileUtil.createGuidedPopup();
                }
            });
        } else { // Go for Static
            EventBus.fire(new NoGuidedPopupSegmentEvent());
        }
    }

    /**
     * For Guided Popups - Success Callback
     */
    public static AsyncCallback<String> guidedPopupSuccessCb(
            final PlayState state, final AnalyticsInfo info,
            final TrackerEventOrigin widgetType, final String popupId) {
        return new LogFailureCb<String>() {

            @Override
            public void onSuccess(String result) {
                // Sending start, close, skip and dont-show events to GA
                GaUtil.startInteraction();
                String GaEvent = result.substring(0, result.indexOf("_"));
                GaUtil.setAnalytics(info);
                EmbedEvent event = null;
                switch (GaEvent) {
                    case CLOSE:
                        event = EmbedEvent.POPUP_CLOSE;
                        break;
                    case SKIP:
                        event = EmbedEvent.POPUP_SKIP;
                        break;
                    default:
                        event = EmbedEvent.POPUP_START;
                }
                GaUtil.trackPopUpEvent(event, widgetType, info);

                // Invoke dont-show api if it's checked
                String popUpId = info.segment_id();
                if (result != null && result.contains("dont_show")) {
                    PopupUtil.updateDontShowStatus(popUpId, widgetType);
                    GaUtil.trackPopUpEvent(EmbedEvent.POPUP_DO_NOT_SHOW,
                            widgetType, info);
                }

                // Start a flow on click GUIDE-ME.
                // Invoke AC methods if present
                // on popup success, close and skip actions
                switch (event) {
                    case POPUP_CLOSE:
                        Customizer.onPopupClose(popUpId);
                        break;
                    case POPUP_SKIP:
                        // Customizer.onSkip(createPlayState(flow, ""));
                        Customizer.onPopupSkip(popUpId);
                        break;
                    default:
                    	Customizer.onPopupSuccess(popupId);
                        EventBus.fire(new StartNewFlowEvent(state.flow(),
                                widgetType.getSrcName(),
                                TrackerEventOrigin.GUIDED_POPUP.getSrcName()));
                }
            }
        };
    }
    
    private FixedPopup showGuidedPopup(final Draft flow,
            PopupContent popupContent, String segmentId,
            AsyncCallback<String> onClose, AnalyticsInfo info) {
        final FixedPopup popup = Common.popup(Framers.start(),
                Framers.popupWidth(), PopupUtil.initialPopupHeight(),
                GUIDED_POPUP_ID_SUFFIX, PopupUtil.POPUP_FRAME_TITLE);
        PopupUtil.popupListeners(popup, popupContent, onClose, new ListenerRegister(), info);        
        PopupUtil.popupSeenIncrementer(flow.name(), segmentId,
                TrackerEventOrigin.GUIDED_POPUP);
        PopupUtil.popupSeenTracker(TrackerEventOrigin.GUIDED_POPUP, info);
        popup.center();
        return popup;
    }
}
