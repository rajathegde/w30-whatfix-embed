package co.quicko.whatfix.player;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.DataTransfer;
import com.google.gwt.event.dom.client.DragEndEvent;
import com.google.gwt.event.dom.client.DragStartEvent;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.AnalyticsInfo;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.JsUtils;
import co.quicko.whatfix.common.LogFailureCb;
import co.quicko.whatfix.common.TimeStampTracker;
import co.quicko.whatfix.common.contextualinfo.AttentionInfo;
import co.quicko.whatfix.common.contextualinfo.ContextualInfo;
import co.quicko.whatfix.common.contextualinfo.LoadTimeInfo;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.EmbedData.SelfHelpWidgetSettings;
import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.data.EmbedData.TagEnabledWidgetSettings;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.ga.GaUtil;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.UserPreferences;
import co.quicko.whatfix.player.DragDropCallbacks.DropEventCB;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.player.notification.NotificationsTopic;
import co.quicko.whatfix.player.notification.SelfHelpNotification;
import co.quicko.whatfix.player.notification.event.HideNotificationEvent;
import co.quicko.whatfix.player.notification.event.ShowNotificationEvent;
import co.quicko.whatfix.player.notification.newcontent.NewContentNotification;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.security.Security;
import co.quicko.whatfix.service.LocaleUtil;
import co.quicko.whatfix.tracker.event.EmbedEvent;
import co.quicko.whatfix.tracker.event.EventPayload.Builder;
import co.quicko.whatfix.workflowengine.app.AppFactory;

public class WidgetLauncher {
    public static final String FULL_MODE = "full";
    public static final String MOBILE_MODE = "mobile";
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();

    protected BaseWidget widget;

    private static Set<String> fixedPositions;
    private static Set<String> relativePositions;

    private HandlerRegistration resizeRegn;
    private static SelfHelpDropPanel[] dropZones;
    private PopupPanel dragImagePanel;
    private Label dragImageLabel;

    protected PlayerBase player;
    protected Settings settings;

    protected Set<String> unseenContent;

    static {
        fixedPositions = new HashSet<>();
        Collections.addAll(fixedPositions, "tlm", "t", "trm", "rtm", "r", "rbm",
                "brm", "b", "blm", "lbm", "l", "ltm");

        relativePositions = new HashSet<>();
        Collections.addAll(relativePositions, "tl-bl", "tr-br", "bl-tl",
                "br-tr");
    }

    public WidgetLauncher(final PlayerBase player, final Settings settings) {
        this.player = player;
        this.settings = settings;
        String position = getWidgetPosition();
        settings.position(position);

        String userSpecificPosition = getUserSpecificPosition();
        if (userSpecificPosition != null) {
            settings.position(userSpecificPosition);
        }

        if (settings.target() == null) {
            if (position == null || !fixedPositions.contains(position)) {
                position = "rtm";
                settings.position(position);
            }
        } else {
            if (position == null || !relativePositions.contains(position)) {
                position = "tr-br";
                settings.position(position);
            }
        }

        co.quicko.whatfix.overlay.Overlay.CONSTANTS
                .use(Enterpriser.properties(LocaleUtil.locale(true)));

        createWidget(player, settings);

        resizeRegn = Window.addResizeHandler(new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent event) {
                int currentWidth = Window.getClientWidth();
                int currentHeight = Window.getClientHeight();
                if (currentWidth > Framers.MIN_WIDGET_WIDTH
                        && currentHeight > Framers.MIN_WIDGET_HEIGHT) {
                    // Normal scenario where we show desktop version of
                    // Launcher
                    if (isWidgetMobileInstance()) {
                        recreateWidget(player, settings);
                        return;
                    }
                } else if (currentWidth <= Framers.MIN_WIDGET_WIDTH
                        || currentHeight <= Framers.MIN_WIDGET_HEIGHT) {
                    if (widget instanceof HelpWidget
                            && !isWidgetMobileInstance()) {
                        // This is when width is reduced and earlier
                        // user was shown desktop version and now
                        // switching to mobile
                        recreateWidget(player, settings);
                        return;
                    } else {
                        // User was viewing mobile version but handling
                        // further zoom
                        ((MobileHelpWidget) widget).adjustWidget();
                    }
                }
                widget.handleResize(event);
            }
        });
        
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
                destroy();
                resizeRegn.removeHandler();
                CrossMessager.removeListener(this, getDestroyAction());
            }
        }, getDestroyAction());
        
        CrossMessager.addListener(new CrossListener() {
            @Override
            public void onMessage(String type, String content) {
            	if(widget.isShowingFull()) {
            		widget.closeFull();
            	}
            }
        }, "close_widgets");

        Common.tracker().initialize(Security.unq_id());
    }
    
    protected String getDestroyAction() {
        return widget.prefix("_destroy");
    }
    
    protected boolean isWidgetMobileInstance() {
        return widget instanceof MobileHelpWidget;
    }
    
    protected String getUserSpecificPosition() {
        return UserPreferences.getSelfHelpPosition();
    }
    
    protected void setUserSpecificPosition(String position) {
        UserPreferences.saveSelfHelpPosition(position);
    }
    
    protected String getWidgetPosition() {
        return Themer.value(Themer.SELFHELP.ICON_POSITION, settings.position());
    }

    private void destroy() {
        widget.destroy();
        disposeExternalComponents();
    }

	public void showWidget(Settings settings) {
        if (Enterpriser.hasFeature(AdditionalFeatures.notifications)) {
            SelfHelpNotification.enableNotifications();
        }

        if (!Enterpriser.hasFeature(AdditionalFeatures.NEW_NOTIFICATION)) {
            actualShowWidget();
        } else {
            NewNotification.hasUnseenContent((SelfHelpWidgetSettings) settings,
                    new LogFailureCb<Set<String>>() {
                @Override
                public void onSuccess(Set<String> result) {
                    Console.debug("Unseen content is present. Count: " + result.size());
                    unseenContent = result;
                    settings.unseenContent(JsUtils.toJsArray(result.toArray(new String[0])));

                    widget.setUnseenContent(result);
                            if (!result.isEmpty()) {
                                LOGGER.log(WfxLogLevel.DEBUG,
                                        WidgetLauncher.class, "showWidget",
                                        WfxModules.NEW_NOTIFICATION,
                                        "Unseen content is present. Count: {0}. ShowNotificationEvent",
                                        result.size());
                                EventBus.fire(new ShowNotificationEvent(
                                        new NewContentNotification()));
                            } else {
                                LOGGER.log(WfxLogLevel.DEBUG,
                                        WidgetLauncher.class, "showWidget",
                                        WfxModules.NEW_NOTIFICATION,
                                        "Unseen content is empty. HideNotificationEvent",
                                        result.size());
                                EventBus.fire(new HideNotificationEvent(
                                        NotificationsTopic.NEW_CONTENT));
                                EventBus.fire(new HideNotificationEvent(
                                        NotificationsTopic.NEW_CONTENT_BADGE));
                            }
                    actualShowWidget();
                }
            });
        }
	}

	private void actualShowWidget() {
        widget.showFirstTime(false);

        String segmentName = (settings.segment_name() != null) ? settings.segment_name() : settings.title();
        AnalyticsInfo info = AnalyticsInfo.create(TrackerEventOrigin.SELF_HELP, settings.segment_id(),
                segmentName);
        AttentionInfo attentionInfo = DataUtil.create();
        String attentionAnimation = AppFactory.isMobileApp()? "none" : ("show".equalsIgnoreCase(Themer.value(Themer.SELFHELP.ANIMATE))
                ? Themer.value(Themer.SELFHELP.ANIMATION)
                : "none");
        attentionInfo.attention_animation(attentionAnimation);
        AnalyticsInfo.addContextualInfo(ContextualInfo.OuterKey.ATTENTION, attentionInfo);
        LoadTimeInfo loadTimeInfo = DataUtil.create();
        loadTimeInfo.load_time((int) (System.currentTimeMillis() - TimeStampTracker.
                getTime(TimeStampTracker.Event.EMBED_START_TIME)));
        AnalyticsInfo.addContextualInfo(ContextualInfo.OuterKey.LOAD_TIME, loadTimeInfo);
        long sTime2 = System.currentTimeMillis();
        Common.tracker().onEvent(getWidgetLoadedBuilder().analyticsInfo(info)
                .widget(TrackerEventOrigin.SELF_HELP).currentPageTags(PlayerBase.getCurrentPageTags())
                .userDetails(((TagEnabledWidgetSettings) settings).role_tags())
                .contextualInfo(GaUtil.analyticsCache.contextual_info()).metrics(settings.metrics()).build());
        LOGGER.log(WfxLogLevel.PERFORMANCE, WidgetLauncher.class,
                "actualShowWidget", WfxModules.JIT,
                "Time taken for Show Widget Analytics Event : {0} ms",
                (System.currentTimeMillis() - sTime2));
    }

    protected TrackerEventOrigin getWidgetTracker() {
        return TrackerEventOrigin.SELF_HELP;
    }

    protected Builder getWidgetLoadedBuilder() {
        return EmbedEvent.SELF_HELP_LOADED.builder();
    }

    private void createWidget(PlayerBase player, Settings settings) {
        RePositionCB rePositionCB = getCallBack();
        widget = initWidget(rePositionCB);
        widget.createStructure();
    }

    protected BaseWidget initWidget(RePositionCB rePositionCB) {
        int windowWidth = Window.getClientWidth();
        int windowHeight = Window.getClientHeight();
        if (windowWidth < Framers.MIN_WIDGET_WIDTH
                || windowHeight < Framers.MIN_WIDGET_HEIGHT 
                || AppFactory.isMobileApp()) {
            return new MobileHelpWidget(player, settings, getMobileCallback());
        } else {
            if (settings.position().length() == 1) {
                return new CentreHelpWidget(player, settings, rePositionCB);
            } else {
                return new HelpWidget(player, settings, rePositionCB);
            }
        }
    }

    private void recreateWidget(PlayerBase player, Settings settings) {
        if (widget != null) {
            destroy();
        }
        createWidget(player, settings);
        widget.showFirstTime(true);
    }

    private RePositionCB getCallBack() {
        return new RePositionCB() {

            @Override
            public void setUpDragImagePanel(String widgetLabel) {
                setupDragImagePanel(widgetLabel);
            }

            @Override
            public void onDragStart(DragStartEvent event) {
                dragStartHandler(event);
            }

            @Override
            public void onDragEnd(DragEndEvent event) {
                dragEndHandler(event);
            }
        };
    }
    
    private RePositionCB getMobileCallback() {
        return new RePositionCB() {

            @Override
            public void setUpDragImagePanel(String widgetLabel) {
            }

            @Override
            public void onDragStart(DragStartEvent event) {
            }

            @Override
            public void onDragEnd(DragEndEvent event) {
            }
        };
    }

    private void dragStartHandler(DragStartEvent event) {
        event.stopPropagation();
        dragImagePanel.show();
        event.setData("text", widget.getWidgetLabel(settings));
        event.getDataTransfer().setDragImage(dragImagePanel.getElement(), 10,
                10);
        showDropZones();
        setEffectAllowed(event.getDataTransfer());
    }

    private void dragEndHandler(DragEndEvent event) {
        event.stopPropagation();
        dragImagePanel.hide();
        removeDropZones();
    }

    private void setupDragImagePanel(String widgetLabel) {
        dragImagePanel = new PopupPanel();
        dragImagePanel.setStyleName(Overlay.CSS.dragImagePanel());

        dragImageLabel = new Label();
        dragImageLabel.setText(widgetLabel);
        Themer.applyTheme(dragImagePanel, Themer.STYLE.BACKGROUD_COLOR,
                Themer.SELFHELP.ICON_BG_COLOR,Themer.STYLE.FONT,
                Themer.SELFHELP.FONT);

        dragImagePanel.add(dragImageLabel);
    }
    
    protected void applyTheme(Widget widget) {
        Themer.applyTheme(widget, Themer.STYLE.BACKGROUD_COLOR,
                Themer.SELFHELP.ICON_BG_COLOR, Themer.STYLE.FONT,
                Themer.SELFHELP.FONT);
    }

    // Needed for IE and Firefox
    private final native void setEffectAllowed(DataTransfer dt) /*-{
		dt.effectAllowed = "move";
    }-*/;

    private void disposeExternalComponents() {
        if (null != dragImageLabel) {
            dragImagePanel.hide();
        }
        removeDropZones();
        dropZones = null;
    }

    private void removeDropZones() {
        if (null != dropZones) {
            for (SelfHelpDropPanel dropZone : dropZones) {
                dropZone.hide();
            }
        }
    }

    private void showDropZones() {
        dropZones = new SelfHelpDropPanel[fixedPositions.size() - 1];
        int count = 0;
        // clear previous drop zones
        for (final String widgetPosition : fixedPositions) {
            if (!settings.position().equalsIgnoreCase(widgetPosition)) {
                final SelfHelpDropPanel dropZone = new SelfHelpDropPanel(widgetPosition,
                        new DropEventCB() {

                            @Override
                            public void onDrop(DropEvent event,
                                    String position) {
                                event.stopPropagation();
                                event.preventDefault();
                                dragImagePanel.hide();
                                settings.position(position);

                                // TODO: API call to save user preferred
                                // Position
                                UserPreferences
                                        .saveSelfHelpPosition(position);
                                recreateWidget(player, settings);
                                Customizer.onLauncherDrag();
                                removeDropZones();
                            }
                        });
                dropZones[count++] = dropZone;
                Scheduler.get().scheduleDeferred(new ScheduledCommand() {
                    @Override
                    public void execute() {
                        widget.positionElement(dropZone.getElement(),
                                dropZone.getPosition(),
                                dropZone.getOffsetHeight(),
                                dropZone.getOffsetWidth(), true);
                    }
                });
                dropZone.show();
            }
        }
    }

    public BaseWidget getWidget() {
        return widget;
    }
}
