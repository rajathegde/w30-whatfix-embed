package co.quicko.whatfix.player;

public enum GracefulExitEnum {
    DEFAULT("default"), MAX("max"), MIN("min");

    private String modalType;

    private GracefulExitEnum(String modalType) {
        this.modalType = modalType;
    }

    public String getString() {
        return modalType;
    }
}
