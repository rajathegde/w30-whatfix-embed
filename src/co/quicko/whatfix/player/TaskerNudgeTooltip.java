package co.quicko.whatfix.player;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FocusPanel;

import co.quicko.whatfix.common.AriaProperty;
import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.NativeKeyDownHandler;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.Popover;
import co.quicko.whatfix.overlay.Popover.ClickRegister;
import co.quicko.whatfix.overlay.Popover.PopListener;
import co.quicko.whatfix.overlay.StepPop;
import co.quicko.whatfix.overlay.StepStaticPop;
import co.quicko.whatfix.overlay.actioner.ActionerPopover;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;

/**
 * This class is used for showing tooltip on tasklist when user is nudged
 * 
 * @author gautam
 *
 */
public class TaskerNudgeTooltip {
    private ActionerPopover popover;
    private String position;
    private FixedPopup widgetPopup;
    private String toolTipMessage = "";
    private AsyncCallback<Void> closeCallback;
    public static final String TOOLTIP_RIGHT_TOP = "rt";
    public static final String TOOLTIP_LEFT_TOP = "lt";
    
    public FocusPanel tipStart = new FocusPanel();
    public FocusPanel tipEnd = new FocusPanel();   
    
    public static final String TOOLTIP_START_LABEL = "start of tooltip";
    public static final String TOOLTIP_END_LABEL = "end of tooltip";
    
    public static final String TOOLTIP_START_ID = "wfx-tasklist-tooltip-start";
    public static final String TOOLTIP_END_ID = "wfx-tasklist-tooltip-end";
    
    public static final String TOOLTIP_TITLE_ID = "wfx-tooltip-title";
    
    public static NativeEvent nativeEvent;

    public TaskerNudgeTooltip(String position, FixedPopup widgetPopup,
            String message, AsyncCallback<Void> closeCallback) {
        this.position = position;
        this.widgetPopup = widgetPopup;
        if (StringUtils.isNotBlank(message)) {
            this.toolTipMessage = message;
        }
        this.closeCallback = closeCallback;
        showTooltip();
    }

    private void showTooltip() {

        Step step = DataUtil.create();
        step.placement(position);
        int width = widgetPopup.getElement().getOffsetWidth();
        int height = widgetPopup.getElement().getOffsetHeight();
        int top = widgetPopup.getElement().getOffsetTop();
        int bottom = top + height;
        int left = widgetPopup.getElement().getOffsetLeft();
        int right = left + width;
        popover = new ActionerPopover(new TaskerTipExtender(), null, null, step,
                top, right, bottom, left, width, height);
        popover.popover().addStyleName(Overlay.CSS.fixedTip());
        Common.handleZoom(popover.popover());
        popover.show(top, right, bottom, left, width, height, position);
        
        // Setting the tab-index of the tool-tip text to 0, so that it is
        // focused on tab stroke as per the wcag requirement
        Element tooltipTitle = DOM.getElementById(TOOLTIP_TITLE_ID);
        tooltipTitle.setTabIndex(0);
        
        setBoundary(popover);
    }

    /**
     * <ul>
     * <li>Add an empty <div> at the start and end of the tooltip
     * <li>Set intial focus on the tooltip
     * </ul>
     * 
     * @param popover
     */
    private void setBoundary(ActionerPopover popover) {
        
        tipStart.setStyleName(Overlay.CSS.hideBoundryElement());
        tipEnd.setStyleName(Overlay.CSS.hideBoundryElement());
        
        tipStart.getElement().setId(TOOLTIP_START_ID);
        tipEnd.getElement().setId(TOOLTIP_END_ID);

        Common.setAttributes(tipStart.getElement(), AriaProperty.LABEL,
                TOOLTIP_START_LABEL);
        Common.setAttributes(tipEnd.getElement(), AriaProperty.LABEL,
                TOOLTIP_END_LABEL);

        popover.popover().getElement().insertFirst(tipStart.getElement());
        popover.popover().getElement().insertAfter(tipEnd.getElement(),
                popover.popover().getElement().getLastChild());

        // Set focus on tooltip when the tasklist is open with a tooltip
        Scheduler.get().scheduleDeferred(() -> tipStart.setFocus(true));
    }
    
    public void dispose() {
        popover.dispose();
    }

    /**
     * Sets the tooltip to latest positions and dimensions
     */
    public void refreshTooltipPosition() {
        int width = widgetPopup.getElement().getOffsetWidth();
        int height = widgetPopup.getElement().getOffsetHeight();
        int top = widgetPopup.getElement().getOffsetTop();
        int bottom = top + height;
        int left = widgetPopup.getElement().getOffsetLeft();
        int right = left + width;
        popover.show(top, right, bottom, left, width, height, position);
    }

    private class TaskerTipExtender extends PopoverExtender {
        @Override
        protected Popover makePopover(Step action, PopListener listener,
                ClickRegister register, boolean alignment) {
            return new TaskerTipPopover(listener, register, alignment);
        }

        @Override
        protected StepPop stepPop(Step step, String footnote, String placement,
                String stepnote) {
            return new TaskerTipPop(step, footnote, placement, false);
        }

        @Override
        protected String footnote(Draft draft, Step action) {
            return "";
        }
    }

    public class TaskerTipPopover extends Popover {
        
        public TaskerTipPopover(PopListener listener, ClickRegister register,
                boolean isLTR) {
            super(listener, register, true);
            setTabHandler(register);
            // Setting aria-hidden of close icon to false here
            ((FlexTable) head).getCellFormatter().getElement(0, 1)
                    .setAttribute(AriaProperty.HIDDEN, String.valueOf(false));
            close.getElement().setTabIndex(0);
        }

        /**
         * Enable tab-cycling within the tooltip, when the tooltip is open
         * @param register
         */
        private void setTabHandler(ClickRegister register) {
            register.registerNativeKeyDown(tipStart,
                    new NativeKeyDownHandler() {
                        @Override
                        public void onKeyDown(KeyDownEvent event) {
                            // do nothing
                        }

                        @Override
                        public void onNativeKeyDown(NativeEvent nativeEvent) {
                            if (nativeEvent.getKeyCode() == KeyCodes.KEY_TAB
                                    && nativeEvent.getShiftKey()) {
                                tipEnd.setFocus(true);
                                nativeEvent.preventDefault();
                            }
                        }
                    });

            register.registerNativeKeyDown(tipEnd, new NativeKeyDownHandler() {
                @Override
                public void onKeyDown(KeyDownEvent event) {
                    // do nothing
                }

                @Override
                public void onNativeKeyDown(NativeEvent nativeEvent) {
                    if (nativeEvent.getKeyCode() == KeyCodes.KEY_TAB
                            && !nativeEvent.getShiftKey()) {
                        tipStart.setFocus(true);
                        nativeEvent.preventDefault();
                    }
                }
            });
        }

        @Override
        protected String getThemeType() {
            return Themer.TIP.TipThemeType.CLASSIC.name;
        }

        @Override
        protected boolean isBackNeeded() {
            return false;
        }

        @Override
        protected void onClose() {
            // Send a cross-message to set focus on tasklist on smartip close
            CrossMessager.sendMessageToFrame(
                    widgetPopup.getWidget().getElement(), "tasker_tooltip_close", "");
            closeCallback.onSuccess(null);
        }

        @Override
        protected PopTheme fetchTheme() {
            return new ActionerPopTheme() {
                @Override
                public String bodybgcolor() {
                    return Themer.TASK_LIST.TIP_COLOR;
                }

                @Override
                public String closeColor() {
                    return Themer.TASK_LIST.CLOSE_COLOR;
                }
                
                @Override
                public String titleSize() {
                    return Themer.TASK_LIST.TIP_FONT_SIZE;
                }

                @Override
                public String titleWeight() {
                    return Themer.TASK_LIST.TIP_FONT_WEIGHT;
                }
            };
        }

        @Override
        protected int getWidth() {
            return 360;
        }
        
        @Override
        public void refresh(Pop pop){
            super.refresh(pop);
            addCustomStyles();
        }

        private void addCustomStyles() {
            title.addStyleName(Overlay.CSS.taskerTipTitle());
            Map<String, String> borderStyle = new HashMap<>();
            borderStyle.put(Themer.STYLE.BORDER_RADIUS,
                    Themer.value(Themer.TASK_LIST.TIP_CORNER_RADIUS) + "px");
            Common.appendToStyle(content.getElement(), borderStyle);
            Map<String, String> titleStyle = new HashMap<>();
            titleStyle.put(Themer.STYLE.OVERFLOW, "auto");
            Common.appendToStyle(title.getElement(), titleStyle);
        }
    }

    private class TaskerTipPop extends StepStaticPop {

        public TaskerTipPop(Step step, String footnote, String placement,
                boolean isNextRequired) {
            super(step, footnote, placement, isNextRequired);
        }

        @Override
        public boolean closeable() {
            return true;
        }

        @Override
        public String description_md() {
            return toolTipMessage;
        }

        @Override
        public boolean addFootnote() {
            return false;
        }

        @Override
        public String footnote() {
            return "";
        }

        @Override
        public String note_md() {
            return "";
        }

        @Override
        public String note() {
            return "";
        }

    }
}
