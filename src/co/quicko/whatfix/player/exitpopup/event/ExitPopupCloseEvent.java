package co.quicko.whatfix.player.exitpopup.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.tracker.event.EmbedEvent;

public class ExitPopupCloseEvent implements Event {
    private EmbedEvent embedEvent;
    private String content;

    public ExitPopupCloseEvent(String content, EmbedEvent embedEvent) {
        this.embedEvent = embedEvent;
        this.content = content;
    }

    public EmbedEvent getEmbedEvent() {
        return embedEvent;
    }
    
    public String getContent() {
        return content;
    }
}
