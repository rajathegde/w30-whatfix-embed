package co.quicko.whatfix.player.exitpopup;

import com.google.gwt.user.client.ui.Frame;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Console;
import co.quicko.whatfix.common.EndUserWidgetIds;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.FixedPopup;
import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.WcagUtils;
import co.quicko.whatfix.data.PopupData;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.exitpopup.event.ExitPopupCloseEvent;
import co.quicko.whatfix.player.exitpopup.event.ShowExitPopupEvent;
import co.quicko.whatfix.player.gracefulexit.event.GracefulExitCloseEvent;
import co.quicko.whatfix.tracker.event.EmbedEvent;

/**
 * Exit Pop Player will play the Exit Popup prompted when user clicks on cross
 * icon of GracefulExit Modal
 * 
 * @author arpitamishra
 *
 */
public class ExitPopPlayer extends FixedPopup implements CrossListener {
    private FixedPopup exitPop;
    private Frame frame;
    private String locale;

    private enum HtmlConstants {
        FRAME_TITLE("Exit Popup"), MODAL_ID("_exitpop_wfx_");

        private String id;

        private HtmlConstants(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    private enum Events {
        POPUP_CLOSE("exit_popup_close"),
        FRAME_DATA("frame_data"),
        EXIT_POPUP_FRAME_DATA("popup_frame_data"),
        CLOSE("close"),
        YES("yes"),
        NO("maximize"),
        CROSS("cross"),
        BLANK(""),
        EXIT_POP("exitPop");

        private String event;

        private Events(String event) {
            this.event = event;
        }

        private String getEvent() {
            return event;
        }
    }

    public ExitPopPlayer(final String locale) {
        this.locale = locale;
        EventBus.addHandler(ShowExitPopupEvent.class,
                this::onShowExitPopupEvent);
        EventBus.addHandler(ExitPopupCloseEvent.class,
                this::onExitPopupCloseEvent);
    }

    private void makeExitPopup() {
        exitPop = new FixedPopup();
        exitPop.addStyleName(Common.CSS.popup());
        exitPop.setGlassEnabled(true);
        exitPop.setGlassStyleName(Common.CSS.popupGlass());
        Common.setZoomOnGlass(exitPop.getGlassElement());
        exitPop.setWidget(getFrame());
        exitPop.center();
        Common.setId(exitPop, HtmlConstants.MODAL_ID.getId());
        WcagUtils.setWCAGPopup(exitPop);
    }

    private Frame getFrame() {
        frame = Framers.end().buildFrame();
        Common.setId(frame, EndUserWidgetIds.WFX_EXIT_POPUP);
        frame.setTitle(HtmlConstants.FRAME_TITLE.getId());
        frame.addStyleName(Overlay.CSS.exitPopFrame());
        return frame;
    }

    private void onShowExitPopupEvent(Event e) {
        makeExitPopup();
        CrossMessager.addListener(this, Events.POPUP_CLOSE.getEvent(),
                Events.EXIT_POPUP_FRAME_DATA.getEvent());
    }

    private void onExitPopupCloseEvent(Event e) {
        exitPop.hide();
    }

    @Override
    public void onMessage(String type, String content) {
        if ((Events.EXIT_POPUP_FRAME_DATA.getEvent()).equals(type)) {
            CrossMessager.sendMessageToFrame(frame.getElement(),
                    Events.FRAME_DATA.getEvent(),
                    StringUtils.stringifyObject(getData()));
        } else if ((Events.POPUP_CLOSE.getEvent()).equals(type)) {
            exitPop.hide();
            removeListeners();
            trackExitPop(content);
        }
    }

    private PopupData getData() {
        int popupWidth = Framers.popupWidth();
        if (popupWidth > Framers.MAX_END_WIDTH) {
            popupWidth = Framers.MAX_END_WIDTH;
        }
        return PopupData.getPopupData(popupWidth, Events.EXIT_POP.getEvent(),
                locale);
    }

    private void trackExitPop(String content) {
        try {
            Events event = Events.valueOf(content.toUpperCase());
            switch (event) {
                case YES:
                    EventBus.fire(new ExitPopupCloseEvent(content,
                            EmbedEvent.EXIT_POPUP_CLOSE_YES));
                    EventBus.fire(new GracefulExitCloseEvent(
                            EmbedEvent.GRACEFUL_EXIT_CLOSE, true));
                    break;
                case NO:
                    EventBus.fire(new ExitPopupCloseEvent(content,
                            EmbedEvent.EXIT_POPUP_CLOSE_NO));
                    break;
                case CROSS:
                    EventBus.fire(new ExitPopupCloseEvent(content,
                            EmbedEvent.EXIT_POPUP_CLOSE_CROSS));
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            Console.debug(
                    "Tracking Exit Confirmation Popup Failed: " + e.getCause());
        }
    }

    private void removeListeners() {
        CrossMessager.removeListener(this, Events.POPUP_CLOSE.getEvent());
        CrossMessager.removeListener(this,
                Events.EXIT_POPUP_FRAME_DATA.getEvent());
    }
}