package co.quicko.whatfix.player;

public enum StepFailureReason {
    DIFFERENT_URL("This page is different from the page used to create the flow."), 
    FINDER_NOT_CONFIGURED("Contact support to configure finder rules."), 
    ELEMENT_NOT_IDENTIFIED("Element not identified.");

    private String reason;

    private StepFailureReason(String reason) {
        this.reason = reason;
    }

    public String getString() {
        return reason;
    }
}
