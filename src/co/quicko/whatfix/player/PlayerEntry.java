package co.quicko.whatfix.player;

import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Framers;
import co.quicko.whatfix.common.ResizeHandler;
import co.quicko.whatfix.common.SortedEntryPoint;
import co.quicko.whatfix.extension.util.ExtensionUtil;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.CrossMessager.CrossListener;

public class PlayerEntry extends SortedEntryPoint {

    @Override
    protected void onTopLoad() {
        Framers.FRAME_URL.initBaseUrl(new AsyncCallback<Void>() {
            @Override
            public void onSuccess(Void result) {
                ResizeHandler.initResizer();
                final PlayerBase player = new PlayerBase();
                player.initializeActioner();
                player.initLaunchers();
                player.play();
                CrossMessager.addListener(new CrossListener() {
                    @Override
                    public void onMessage(String type, String content) {
                        player.refreshPlay();
                    }
                }, "play_refresh");
            }

            @Override
            public void onFailure(Throwable caught) {
            }
        }, ExtensionUtil.WHATFIX_DOMAIN + "/");
    }

    @Override
    protected void onFrameLoad() {
        Actioner.initialize();
    }
}
