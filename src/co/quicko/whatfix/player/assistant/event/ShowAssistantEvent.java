package co.quicko.whatfix.player.assistant.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.AssistantConfig;

/**
 * @author akash
 *
 */
public class ShowAssistantEvent implements Event {

    private AssistantConfig assistantConfig;

    public ShowAssistantEvent(AssistantConfig config) {
        this.assistantConfig = config;
    }

    /**
     * Creates a show event for Assistant tip triggered from script at runtime.
     * 
     * @return event with meta {@link AssistantConfig}
     */
    public static ShowAssistantEvent fromJsApi(AssistantConfig config) {
        return new ShowAssistantEvent(config);
    }

    public AssistantConfig getAssistantConfig() {
        return assistantConfig;
    }

}
