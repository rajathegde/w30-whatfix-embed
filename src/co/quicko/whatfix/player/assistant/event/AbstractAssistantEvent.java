package co.quicko.whatfix.player.assistant.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;

/**
 * @author akash
 *
 */
public abstract class AbstractAssistantEvent implements Event {

    private Draft draft;

    /**
     * @param draft
     */
    public AbstractAssistantEvent(Draft draft) {
        this.draft = draft;
    }

    public Draft getDraft() {
        return this.draft;
    }

}
