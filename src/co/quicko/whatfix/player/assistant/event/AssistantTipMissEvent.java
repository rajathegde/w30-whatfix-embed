package co.quicko.whatfix.player.assistant.event;

import com.google.gwt.core.client.JsArray;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.JavaScriptObjectExt;

/**
 * @author akash
 *
 */
public class AssistantTipMissEvent implements Event {

    private JsArray<JavaScriptObjectExt> reason;
    private Draft draft;
    
    /**
     * @param draft
     */
    public AssistantTipMissEvent(Draft draft,
            JsArray<JavaScriptObjectExt> reason) {
        this.draft = draft;
        this.reason = reason;
    }

    public JsArray<JavaScriptObjectExt> getReason() {
        return reason;
    }

    public Draft getDraft() {
        return draft;
    }

}
