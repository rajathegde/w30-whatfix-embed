package co.quicko.whatfix.player.assistant.event;

import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.data.AssistantConfig;

/**
 * @author akash
 *
 */
public class AssistantTipCTAClickedEvent implements Event {

    private AssistantConfig config;

    /**
     * @param draft
     */
    public AssistantTipCTAClickedEvent(AssistantConfig config) {
        this.config = config;
    }

    public AssistantConfig getConfig() {
        return config;
    }

}
