package co.quicko.whatfix.player.assistant.event;

import co.quicko.whatfix.data.Draft;

/**
 * Assistant tip closed event, possible actions to close are cross button, flow
 * launched in the same page from the CTA.
 * 
 * @author akash
 *
 */
public class AssistantTipClosedEvent extends AbstractAssistantEvent {

    /**
     * @param draft
     */
    public AssistantTipClosedEvent(Draft draft) {
        super(draft);
    }

}
