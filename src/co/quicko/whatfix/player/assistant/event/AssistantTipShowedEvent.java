package co.quicko.whatfix.player.assistant.event;

import co.quicko.whatfix.data.Draft;

/**
 * The Assistant is now shown and the current trigger is from the JavaScript
 * i.e., from the Advance Customization script.
 * 
 * @author akash
 *
 */
public class AssistantTipShowedEvent extends AbstractAssistantEvent {

    /**
     * @param draft
     */
    public AssistantTipShowedEvent(Draft draft) {
        super(draft);
    }

}
