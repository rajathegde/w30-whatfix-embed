package co.quicko.whatfix.player.assistant;

import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.data.AssistantConfig;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.*;
import co.quicko.whatfix.overlay.actioner.FullActionerPopover.PopoverExtender;

/**
 * @author akash
 *
 */
public class AssistantTip extends PopoverExtender {

    private AssistantConfig config;

    public AssistantTip(AssistantConfig config) {
        this.config = config;
    }

    @Override
    protected Popover makePopover(Step action, Popover.PopListener listener,
            Popover.ClickRegister register, boolean alignment) {
        return new AssistantTipPopover(listener, register, alignment, config);
    }

    @Override
    protected StepPop stepPop(Step step, String footnote, String placement,
            String stepnote) {
        return new AssistantTipPop(step, footnote, placement, true);
    }

    @Override
    protected String footnote(Draft draft, Step action) {
        return StringUtils.BLANK_STRING;
    }

}
