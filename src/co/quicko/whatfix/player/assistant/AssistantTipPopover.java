package co.quicko.whatfix.player.assistant;

import com.google.gwt.aria.client.Roles;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Widget;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.AssistantConfig;
import co.quicko.whatfix.data.DataUtil;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.JavaScriptObjectExt;
import co.quicko.whatfix.data.Themer;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.extension.util.Runner;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.overlay.CrossMessager;
import co.quicko.whatfix.overlay.Customizer;
import co.quicko.whatfix.overlay.FullPopover;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.overlay.OverlayUtil;
import co.quicko.whatfix.player.assistant.event.AssistantTipCTAClickedEvent;
import co.quicko.whatfix.service.FlowService;

/**
 * @author akash
 */
public class AssistantTipPopover extends FullPopover {

    private static final int HORIZONTAL_TIP_WIDTH = 490;
    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();
    private AssistantConfig assistantTipConfig;

    public AssistantTipPopover(PopListener listener, ClickRegister register,
            boolean alignment, AssistantConfig config) {
        super(listener, register, true, config);
    }

    @Override
    protected void createAdditionalConfig(JavaScriptObject config) {
        this.assistantTipConfig = (AssistantConfig) config;
    }

    @Override
    public int[] placeSpl(int relTop, int relRight, int relBottom, int relLeft,
            String placement, int engage) {
        int[] pos = super.placeSpl(relTop, relRight, relBottom, relLeft,
                placement, engage);
        if (pos == null) {
            /**
             * New placements with "i", meaning inwards. The tip will show on
             * the inside of the element.
             */
            int popHeight = getElement().getAbsoluteBottom()
                    - getElement().getAbsoluteTop();
            popHeight = Math.max(popHeight, minHeight());
            int eleHeight = relBottom - relTop;
            int eleWidth = relRight - relLeft;
            int popupLeft;
            int popupTop;
            if (placement.equals("irb")) {
                popupLeft = relRight - popoverWidth - Overlay.CSS.edge_gap();
                popupTop = relBottom - popHeight - Overlay.CSS.edge_gap();
            } else if (placement.equals("ir")) {
                popupLeft = relRight - popoverWidth - Overlay.CSS.edge_gap();
                popupTop = relTop + (eleHeight / 2) - (popHeight / 2);
            } else if (placement.equals("ilb")) {
                popupLeft = relLeft + 2 * engage;
                popupTop = relBottom - popHeight - Overlay.CSS.edge_gap();
            } else if (placement.equals("il")) {
                popupLeft = relLeft + 2 * engage;
                popupTop = relTop + (eleHeight / 2) - (popHeight / 2);
            } else if (placement.equals("itl")) {
                popupLeft = relLeft + Overlay.CSS.edge_gap();
                popupTop = relTop;
            } else if (placement.equals("itr")) {
                popupLeft = relRight - popoverWidth - Overlay.CSS.edge_gap();
                popupTop = relTop;
            } else if (placement.equals("it")) {
                popupLeft = relLeft + eleWidth / 2 - popoverWidth / 2;
                popupTop = relTop - 2 * engage;
            } else if (placement.equals("ib")) {
                popupLeft = relLeft + eleWidth / 2 - popoverWidth / 2;
                popupTop = relBottom - popHeight - Overlay.CSS.edge_gap();
            } else {
                return null;
            }
            return new int[] { popupLeft, popupTop };
        }
        return pos;
    }

    @Override
    protected String getThemeType() {
        return Themer.TIP.TipThemeType.MODERN.name;
    }

    @Override
    protected boolean isBackNeeded() {
        return false;
    }

    @Override
    protected boolean isEditOrNextNeeded() {
        return !(StringUtils.isBlank(assistantTipConfig.btn_text())
                || StringUtils.isBlank(assistantTipConfig.flow_to_play()));
    }

    @Override
    protected void refreshExtra(Pop pop) {
        super.refreshExtra(pop);
        if (null != next) {
            next.setText(assistantTipConfig.btn_text());
            next.addStyleName(Overlay.CSS.nowrap());
            Themer.applyTheme(next, Themer.STYLE.BACKGROUD_COLOR,
                    Themer.SELFHELP.TIP_BUTTON_BG_COLOR, Themer.STYLE.COLOR,
                    Themer.SELFHELP.TIP_TEXT_COLOR);
        }
    }

    @Override
    protected void onNext() {
        EventBus.fire(new AssistantTipCTAClickedEvent(assistantTipConfig));
        FlowService.IMPL.flow(assistantTipConfig.flow_to_play(),
                new AsyncCallback<Flow>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        LOGGER.log(WfxLogLevel.DEBUG, AssistantTipPopover.class,
                                "onNext", WfxModules.JIT,
                                "Flow fetch failed, make sure the flow with id "
                                        + assistantTipConfig.flow_to_play()
                                        + " is valid.",
                                caught.toString());
                    }

                    @Override
                    public void onSuccess(Flow flow) {
                        final HasHTML run = (HasHTML) next;
                        String srcId = TrackerEventOrigin.ASSISTANT
                                .getSrcName();
                        Runner.shouldRun(run, "run", flow,
                                new AsyncCallback<Void>() {
                                    @Override
                                    public void onSuccess(Void result) {
                                        LOGGER.log(WfxLogLevel.DEBUG,
                                                AssistantTipPopover.class,
                                                "onNext.Runner.shouldRun.onSuccess",
                                                WfxModules.JIT,
                                                "Flow launched for id "
                                                        + assistantTipConfig
                                                                .flow_to_play());
                                        if (assistantTipConfig.new_tab()) {
                                            if (OverlayUtil.isTopWindow()) {
                                                Common.replaceCustomDataForStepInputs(
                                                        flow, Customizer
                                                                .getCustomData());
                                            }
                                            Runner.justRun("run", flow, srcId);
                                        } else {
                                            disposeAssistantTip();
                                            JavaScriptObjectExt message = DataUtil
                                                    .create();
                                            message.put("flowId",
                                                    flow.flow_id());
                                            message.put("srcId", srcId);
                                            CrossMessager.sendMessageToTop(
                                                    "embed_run_from_id",
                                                    StringUtils.stringifyObject(
                                                            message));
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        LOGGER.log(WfxLogLevel.DEBUG,
                                                AssistantTipPopover.class,
                                                "onNext.Runner.shouldRun.onFailure",
                                                WfxModules.JIT,
                                                "Flow launch failed, for id "
                                                        + assistantTipConfig
                                                                .flow_to_play(),
                                                caught.toString());
                                    }
                                });
                    }
                });
    }

    /**
     * To dispose the tip on flow launch in same tab.
     */
    private void disposeAssistantTip() {
        Actioner.hide(ContentType.assistant);
    }

    @Override
    protected PopTheme fetchTheme() {
        return new ActionerPopTheme() {
            @Override
            public String bodybgcolor() {
                return Themer.SMART_TIP.BODY_BG_COLOR;
            }

            @Override
            public String titleColor() {
                return Themer.SMART_TIP.TITLE_COLOR;
            }

            @Override
            public String titleLineHeight() {
                return Themer.ASSISTANT_TIP.TITLE_LINE_HEIGHT;
            }
        };
    }

    /**
     * For horizontally stacked content tip, currently hard value for the width.
     */
    @Override
    protected int getWidth() {
        return assistantTipConfig.is_vertical()
                ? Overlay.CSS.popover_largeWidth()
                : HORIZONTAL_TIP_WIDTH;
    }

    @Override
    protected String getProperty() {
        return assistantTipConfig.is_vertical() ? "max-width" : "width";
    }

    @Override
    protected void setTitleStyle() {
        super.setTitleStyle();
        if (!assistantTipConfig.is_vertical()) {
            title.addStyleName(Overlay.CSS.popoverTitleHorizontal());
        } else {
            title.addStyleName(Overlay.CSS.popoverTitleVertical());
        }
    }

    private boolean hasVerticalStackedContent() {
        return assistantTipConfig.is_vertical();
    }

    @Override
    protected FullModernPopoverRenderer getFullModernPopoverRenderer() {
        return new AssistantFullPopoverRenderer();
    }

    private class AssistantFullPopoverRenderer
            extends FullModernPopoverRenderer {

        @Override
        public void addHoverController(Element el) {
            // Do nothing
        }

        @Override
        public void addLeaveController(Element el) {
            // Do nothing
        }

        @Override
        public void addExtraComponents() {
            FlexTable tempHead = nextAndBackBtnContainer;
            tempHead.getCellFormatter().addStyleName(tempHead.getRowCount(), 0,
                    Overlay.CSS.popoverClassicButtonPadding());
        }

        @Override
        public void refreshExtra(Pop pop) {
            if (!pop.addFootnote()) {
                Themer.applyTheme(content, Themer.STYLE.BACKGROUD_COLOR,
                        popTheme.bodybgcolor());
            }
        }

    }

    @Override
    protected void getToolTip(ClickRegister register) {
        contentInner = new FlowPanel();
        contentInner.setStyleName(Overlay.CSS.popoverContentInner());
        head = new FlowPanel();
        FlowPanel tempHead = (FlowPanel) head;
        tempHead.addStyleName(styleHead());
        tempHead.add(close);
        nextAndBackBtnContainer = new FlexTable();
        Roles.getPresentationRole().set(nextAndBackBtnContainer.getElement());
        content = new FlowPanel();
        FlowPanel tempContent = (FlowPanel) content;
        tempContent.addStyleName(Overlay.CSS.popoverContent());
        if (hasVerticalStackedContent()) {
            // Vertically stacked content tip.
            nextAndBackBtnContainer.addStyleName(Overlay.CSS.fullWidth());
        } else {
            // Horizontally stacked content tip.
            tempContent.addStyleName(Overlay.CSS.horizontalPopoverContent());
        }
        tempContent.add(title);
        if (isEditOrNextNeeded()) {
            Widget nextAnchor = makeEditOrNextAnchor(register);
            nextAndBackBtnContainer.setWidget(0, 1, nextAnchor);
            if (!hasVerticalStackedContent()) {
                nextAndBackBtnContainer.getCellFormatter().setStyleName(0, 1,
                        Overlay.CSS.popoverAnchorTdCenterHorizontal());
            } else {
                nextAndBackBtnContainer.getCellFormatter().setStyleName(0, 1,
                        Overlay.CSS.popoverAnchorTdCenter());
            }
            nextAndBackBtnContainer.getCellFormatter().setHorizontalAlignment(0,
                    1, HasHorizontalAlignment.ALIGN_CENTER);
        }
        tempContent.add(nextAndBackBtnContainer);
        contentInner.add(head);
        contentInner.add(content);
        contentWrapper.add(contentInner);
    }

    /**
     * FIX-14002 Re-evaluation on window resize event for Assistant horizontally
     * stacked content tip type is not needed for now.
     */
    @Override
    public void handleResize() {
        if (!assistantTipConfig.is_vertical()) {
            super.handleResize();
        }
    }

}
