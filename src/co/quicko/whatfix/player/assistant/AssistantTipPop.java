package co.quicko.whatfix.player.assistant;

import co.quicko.whatfix.data.Step;
import co.quicko.whatfix.overlay.StepStaticPop;

class AssistantTipPop extends StepStaticPop {

    private String placement;

    public AssistantTipPop(Step step, String footnote, String placement,
            boolean isNextRequired) {
        super(step, footnote, placement, isNextRequired);
        this.placement = placement;
    }

    @Override
    public boolean arrow() {
        return !placement.contains("i");
    }

    @Override
    public boolean closeable() {
        return true;
    }

}