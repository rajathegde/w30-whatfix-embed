package co.quicko.whatfix.player.assistant;

import com.google.gwt.user.client.rpc.AsyncCallback;

import co.quicko.whatfix.common.Common;
import co.quicko.whatfix.common.Common.ContentType;
import co.quicko.whatfix.common.EventBus;
import co.quicko.whatfix.common.EventBus.Event;
import co.quicko.whatfix.common.StringUtils;
import co.quicko.whatfix.common.logger.WfxLogFactory;
import co.quicko.whatfix.common.logger.WfxLogLevel;
import co.quicko.whatfix.common.logger.WfxLogger;
import co.quicko.whatfix.common.logger.WfxModules;
import co.quicko.whatfix.data.AssistantConfig;
import co.quicko.whatfix.data.Draft;
import co.quicko.whatfix.data.Flow;
import co.quicko.whatfix.data.TrackerEventOrigin;
import co.quicko.whatfix.overlay.Actioner;
import co.quicko.whatfix.player.assistant.event.AbstractAssistantEvent;
import co.quicko.whatfix.player.assistant.event.AssistantTipCTAClickedEvent;
import co.quicko.whatfix.player.assistant.event.AssistantTipClosedEvent;
import co.quicko.whatfix.player.assistant.event.AssistantTipMissEvent;
import co.quicko.whatfix.player.assistant.event.AssistantTipShowedEvent;
import co.quicko.whatfix.player.assistant.event.AssistantTipStopEvent;
import co.quicko.whatfix.player.assistant.event.ShowAssistantEvent;
import co.quicko.whatfix.security.Enterpriser;
import co.quicko.whatfix.service.FlowService;
import co.quicko.whatfix.tracker.event.EmbedEvent;

/**
 * @author akash
 *
 */
public class AssistantTipPlayer {

    private static final WfxLogger LOGGER = WfxLogFactory.getLogger();
    private static final TrackerEventOrigin ASSISTANT_SRC = TrackerEventOrigin.ASSISTANT;
    private Draft draft;

    public AssistantTipPlayer() {
        onInit();
    }

    private void onInit() {
        EventBus.addHandler(ShowAssistantEvent.class,
                this::onShowAssistantEvent);
        EventBus.addHandler(AssistantTipShowedEvent.class, this::onShow);
        EventBus.addHandler(AssistantTipClosedEvent.class, this::onClose);
        EventBus.addHandler(AssistantTipMissEvent.class, this::onMiss);
        EventBus.addHandler(AssistantTipCTAClickedEvent.class, this::onCTAClick);
        EventBus.addHandler(AssistantTipStopEvent.class, this::onStop);
    }

    /**
     * handle ShowAssistantEvent
     *
     * @param event
     */
    private void onShowAssistantEvent(Event event) {
        ShowAssistantEvent e = (ShowAssistantEvent) event;
        AssistantConfig config = e.getAssistantConfig();
        if (StringUtils.isNotBlank(config.mock_flow_id())) {
            fetchDraft(config);
        }
    }

    /**
     * handle AssistantTipShowedEvent
     *
     * @param event
     */
    private void onShow(Event event) {
        track(EmbedEvent.ASSISTANT_SHOWN, (AbstractAssistantEvent) event);
    }

    /**
     * handle AssistantTipClosedEvent
     *
     * @param event
     */
    private void onClose(Event event) {
        disposeAssistantTip();
        track(EmbedEvent.ASSISTANT_CLOSE, (AbstractAssistantEvent) event);
    }

    /**
     * handle AssistantTipMissEvent
     *
     * @param event
     */
    private void onMiss(Event event) {
        trackMiss(EmbedEvent.ASSISTANT_MISS, (AssistantTipMissEvent) event);
    }

    /**
     * handle AssistantTipCTAClickedEvent
     */
    private void onCTAClick(Event event) {
        AssistantTipCTAClickedEvent e = (AssistantTipCTAClickedEvent) event;
        co.quicko.whatfix.tracker.event.Event assitantEvent = EmbedEvent.ASSISTANT_CTA_CLICKED
                .builder()
                .flow(e.getConfig().mock_flow_id(), StringUtils.BLANK_STRING)
                .srcId(ASSISTANT_SRC).build();
        Common.tracker().onEvent(assitantEvent);
    }

    /**
     * handle AssistantTipStopEvent
     */
    private void onStop(Event e) {
        LOGGER.log(WfxLogLevel.DEBUG, AssistantTipPlayer.class, "onStop",
                WfxModules.JIT,
                "Assistant tip is stoped from script for enterprise "
                        + Enterpriser.ent_id());
        disposeAssistantTip();
    }

    /**
     * Track Assistant analytics events
     *
     * @param eventType
     * @param event
     */
    private void track(EmbedEvent eventType,
            AbstractAssistantEvent event) {
        co.quicko.whatfix.tracker.event.Event assitantEvent = eventType.builder()
                .flow(event.getDraft().flow_id(), event.getDraft().title())
                .srcId(ASSISTANT_SRC).build();
        Common.tracker().onEvent(assitantEvent);
    }

    private void trackMiss(EmbedEvent type,
            AssistantTipMissEvent event) {
        co.quicko.whatfix.tracker.event.Event assistantMissEvent = type
                .builder()
                .flow(event.getDraft().flow_id(), event.getDraft().title())
                .srcId(ASSISTANT_SRC).fillElementData(null, event.getReason())
                .build();

        Common.tracker().onEvent(assistantMissEvent);
    }

    private void fetchDraft(AssistantConfig config) {
        FlowService.IMPL.flow(config.mock_flow_id(), new AsyncCallback<Flow>() {

            @Override
            public void onFailure(Throwable caught) {
                LOGGER.log(WfxLogLevel.DEBUG, AssistantTipPlayer.class,
                        "fetchDraft", WfxModules.JIT,
                        "Failed to fetch the flow for flow id "
                                + config.mock_flow_id()
                                + ". Please make sure that the flow id is valid for ent "
                                + Enterpriser.ent_id());
            }

            @Override
            public void onSuccess(Flow flow) {
                draft = flow;
                draft.type(ContentType.assistant.name());
                draft.assistant_config(config);
                Actioner.showAssistantTip(draft);
            }
        });
    }

    private void disposeAssistantTip() {
        Actioner.hide(ContentType.assistant);
    }

}
