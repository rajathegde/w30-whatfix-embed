package co.quicko.whatfix.player;

import com.google.gwt.core.client.GWT;

public class Player {
    public static final PlayerBundle BUNDLE = GWT.create(PlayerBundle.class);
    public static final PlayerCss CSS = BUNDLE.css();

    static {
        CSS.ensureInjected();
    }
}
