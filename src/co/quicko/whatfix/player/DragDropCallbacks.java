package co.quicko.whatfix.player;

import com.google.gwt.event.dom.client.DragEndEvent;
import com.google.gwt.event.dom.client.DragStartEvent;
import com.google.gwt.event.dom.client.DropEvent;

/**
 * @author akash
 *
 */
public class DragDropCallbacks {

    public interface DropEventCB {
        public void onDrop(DropEvent event, String position);
    }

    public interface RePositionCB {

        public void setUpDragImagePanel(String widgetLabel);

        public void onDragStart(DragStartEvent event);

        public void onDragEnd(DragEndEvent event);

    }
    
}
