package co.quicko.whatfix.player;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;

import co.quicko.whatfix.data.EmbedData.Settings;
import co.quicko.whatfix.overlay.Overlay;
import co.quicko.whatfix.player.DragDropCallbacks.RePositionCB;
import co.quicko.whatfix.security.AdditionalFeatures;
import co.quicko.whatfix.security.Enterpriser;

public class CentreHelpWidget extends HelpWidget {

    public CentreHelpWidget(PlayerBase player, Settings settings,
            RePositionCB rePositionCB) {
        super(player, settings, rePositionCB);
    }

    @Override
    protected void setFixedPosition() {
        setMargins();
        super.setFixedPosition();
    }

    protected void setMargins() {
        Element elem = getElement();
        Style style = elem.getStyle();
        String position = settings.position();

        if (position.startsWith("b") || position.startsWith("t")) {
            int margin = ((WIDTH - width) / 2);
            style.setMarginLeft(margin, Unit.PX);
            style.setMarginRight(margin, Unit.PX);
            if (position.startsWith("b")) {
                style.setMarginTop(100, Unit.PX);
            } else if (position.startsWith("t")) {
                style.setMarginBottom(100, Unit.PX);
            }
        } else if (position.startsWith("l") || position.startsWith("r")) {
            int margin = ((HEIGHT - height) / 2);
            style.setMarginTop(margin, Unit.PX);
            style.setMarginBottom(margin, Unit.PX);
            if (position.startsWith("l")) {
                style.setMarginRight(100, Unit.PX);
            } else if (position.startsWith("r")) {
                style.setMarginLeft(100, Unit.PX);
            }
        }
    }
    
    @Override
    protected void showFull() {
        super.showFull();
        removeStyleName(Overlay.CSS.centerWidgetLauncherDrag());
    }

    @Override
    protected void closeFull() {
        super.closeFull();
        addStyleName(Overlay.CSS.centerWidgetLauncherDrag());
    }

    @Override
    protected String widgetAnimation() {
        return Overlay.CSS.centreWidgetAnimation();
    }

    @Override
    protected String widgetExpanded() {
        if (Enterpriser.hasFeature(AdditionalFeatures.chat_bot_tab)) {
            return Overlay.CSS.centreWidgetExpandedX();
        } else {
            return Overlay.CSS.centreWidgetExpanded();
        }
    }

    @Override
    protected int start(String position, int total, int pixels, int adjust,
            String threeForth, Boolean isDropZone) {
        if (position.length() == 1) {
            if (position.startsWith("b") || position.startsWith("t")) {
                int margin = ((WIDTH - pixels) / 2);
                return super.start(position, total, pixels, margin, threeForth,
                        isDropZone);
            } else if (position.startsWith("l") || position.startsWith("r")) {
                int margin = ((HEIGHT - pixels) / 2);
                return super.start(position, total, pixels, margin, threeForth,
                        isDropZone);
            }
        }
        return super.start(position, total, pixels, adjust, threeForth,
                isDropZone);
    }
}
